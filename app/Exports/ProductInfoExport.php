<?php

namespace App\Exports;

use App\Models\ProductInfo;
use App\Models\ProductColor;
use App\Models\ProductSize;
use Maatwebsite\Excel\Concerns\FromCollection;

class ProductInfoExport implements FromCollection
{
    protected $search;
    protected $group;
    protected $color;
    protected $sex;
    protected $size;
    protected $roductInfoModel;
    protected $productColorModel;
    protected $productSizeModel;

    function __construct($search, $group, $color, $sex, $size) {
        $this->search = $search;
        $this->group = $group;
        $this->color = $color;
        $this->sex = $sex;
        $this->size = $size;
        $this->productInfoModel = new ProductInfo();
        $this->productColorModel = new ProductColor();
        $this->productSizeModel = new ProductSize();
    }

    public function collection()
    {
        $search = $this->search;
        $group = $this->group;
        $color = $this->color;
        $sex = $this->sex;
        $size = $this->size;

        $sql = $this->productInfoModel->query();
        if($search != null){
            $sql->where('email', 'LIKE', "%{$search}%")
                ->orWhere('product_general_code', 'LIKE', "%{$search}%")
                ->orWhere('product_general_name', 'LIKE', "%{$search}%")
                ->orWhere('product_variant_code ', 'LIKE', "%{$search}%")
                ->orWhere('product_variant_name ', 'LIKE', "%{$search}%");
        }
        if($group != null){
            $sql->where('product_group' , $group);
        }
        if($color != null){
            $sql->where('color_id' , $color);
        }
        if($sex != null){
            $sql->where('sex' , $sex);
        }
        if($size != null){
            $sql->where('size_id' , $size);
        }

        $data = $sql->orderBy('id', 'DESC')->get();
        if(!empty($data) && $data->count() > 0) {
            foreach ($data as $key => $item) {

                $color = $this->productColorModel->query()->where('pcolor_id', $item->color_id)->first();
                $size = $this->productSizeModel->query()->where('psize_id', $item->size_id)->first();

                $item->color_name = !empty($color->pcolor_code) ? $color->pcolor_code : '';
                $item->size_name = !empty($size->psize_value) ? $size->psize_value : '';
            }
        }

        return $data;
    }
}
