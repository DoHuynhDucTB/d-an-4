<?php

namespace App\Exports;

use App\Models\Subscribe;
use Maatwebsite\Excel\Concerns\FromCollection;

class SubscribeExport implements FromCollection
{
    protected $status;
    protected $time;

    function __construct($status, $time) {
        $this->status = $status;
        $this->time = $time;
    }

    public function collection()
    {
        $status = $this->status;
        $time = $this->time;

        if($status == ''){
            $status = null;
        }

        if($status != null && $time != null){

            $time = date("Y-m-d", strtotime($time));

            return Subscribe::query()->orderBy('subscribe_id', 'DESC')
                ->where('subscribe_status', $status)
                ->whereDate('subscribe_created_at', '=', $time)
                ->get();
        }elseif ($status != null && $time == null){
            return Subscribe::query()->orderBy('subscribe_id', 'DESC')
                ->where('subscribe_status', $status)
                ->get();

        }elseif ($status == null && $time != null){
            $time = date("Y-m-d", strtotime($time));

            return Subscribe::query()->orderBy('subscribe_id', 'DESC')
                ->whereDate('subscribe_created_at', '=', $time)
                ->get();
        }elseif ($status == null && $time == null){
            return Subscribe::query()->orderBy('subscribe_id', 'DESC')->get();
        }

    }
}
