<?php
namespace App\Helpers;

class ArrayHelper
{
    /**
     * @param array $array
     * @param string $keyOfArray
     * @return array
     */
    static function valueAsKey(array $array, string $keyOfArray): array
    {
        return array_combine(array_column($array, $keyOfArray), $array);
    }

    /**
     * @param $arrayValue
     * @param $arrayKey
     * @return array
     */
    static function arrayCombine($arrayValue, $arrayKey){
        $array = [];

        if(count($arrayValue) == count($arrayKey)){
            $array = array_combine($arrayKey, $arrayValue);
        }

        if(count($arrayValue) > count($arrayKey)){
            $number = count($arrayKey) - 1;
            if($number == 0){
                $arrayValueNew = array($arrayValue[0]);
            }else{
                $arrayValueNew = array_slice($arrayValue, $number);
            }

            $array = array_combine($arrayKey, $arrayValueNew);
        }

        if(count($arrayValue) < count($arrayKey)){
            $number = count($arrayValue) - 1;
            if($number == 0){
                $arrayKeyNew = array($arrayKey[0]);
            }else{
                $arrayKeyNew = array_slice($arrayKey, $number);
            }
            $array = array_combine($arrayKeyNew, $arrayValue);
        }
        return $array;
    }

}
