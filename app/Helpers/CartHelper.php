<?php
namespace App\Helpers;


use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Str;

class CartHelper
{
    public static function htmlCart($cart, $isEnableQtyBtn = true)
    {
        $ascQtyBtn = $desQtyBtn = '';
        if ($isEnableQtyBtn == true) {
           // $desQtyBtn = '<span class="dec qtybtn">-</span>';
            //$ascQtyBtn = '<span class="asc qtybtn">+</span>';
        }

        $html = '';
        if (!empty($cart) && count($cart) > 0) {
            foreach ($cart as $key => $item) {
                $html .= '<tr >
                            <td class="shoping__cart__item" >
                                <img src = "' . $item['image'] . '" alt = "" >
                            </td >
                            <td class="shoping__cart__item" >
                                <h5 >' . $item['name'] . '</h5 >
                            </td >
                            <td class="shoping__cart__quantity" >
                                <div class="quantity" >
                                    <div class="pro-qty1" >
                                        '.$desQtyBtn.'
                                        <input style="border-radius: 4px;border: 1px solid #b7b2b2;width: 60px;text-align: center;" type = "number" data-id="'. $item['id'] .'" class="quantity" min="1" value = "' . $item['quantity'] . '" >
                                        '.$ascQtyBtn.'
                                    </div >
                                </div >
                            </td >
                            <td class="shoping__cart__total" >' . ProductHelper::formatMoney($item['sub_total_price']) . '
                            </td >
                                <td class="shoping__cart__item__close" >
                                    <a data-id="' . $item['id'] . '" href = "#" class="" ><span class="icon_close" ></span ></a >
                                </td >
                            </tr >';
            }
        }

        return $html;
    }

    public static function htmlTotal($totalPriceCart)
    {
        $html = '
                        <ul>
                            <li>'.__('Tổng tiền sản phẩm').' <span>'. ProductHelper::formatMoney($totalPriceCart).'</li>
                        </ul>
                        <div class="checkout__order__subtotal">'.__('TỔNG TIỀN THANH TOÁN').' <span>'. ProductHelper::formatMoney($totalPriceCart) . '</span></div>
                    ';

        return $html;
    }
}
