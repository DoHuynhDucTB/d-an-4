<?php
namespace App\Helpers;


class StringHelper
{
    /**
     * @param $character
     * @return string
     */
    public static function createCodeVocher($character): string
    {
        return strtoupper($character).rand(1000,9999).self::generate_string(5);
    }

    /**
     * @param $strength
     * @return string
     */
    public static function generate_string($strength){
        $chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $input_length = strlen($chars);
        $random_string = '';
        for($i = 0; $i < $strength; $i++) {
            $random_character = $chars[mt_rand(0, $input_length - 1)];
            $random_string .= $random_character;
        }

        return $random_string;
    }
}
