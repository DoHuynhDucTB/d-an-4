<?php
namespace App\Helpers;


class LanguageHelper
{
    /**
     * @param string $language
     * @return string
     */
    public static function getAllowedLanguage(string $language): string
    {
        if (preg_match('/^(' . config('app.locales') . ')$/', $language)) {
            return $language;
        } else {
            return config('app.locale');
        }
    }

}
