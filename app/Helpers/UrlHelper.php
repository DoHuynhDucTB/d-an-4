<?php
namespace App\Helpers;


class UrlHelper
{

    /**
     * @param string $controllerName
     * @param string $actionName
     * @param array $queries
     * @return string
     */
    public static function admin(string $controllerName = 'index', string $actionName = 'index', array $queries = []): string
    {
        $query = http_build_query($queries);
        $route = route(ADMIN_ROUTE, ['controller' => $controllerName, 'action' => $actionName]);
        if ($query) {
            $route = $route .'?'. $query;
        }
        return $route;
    }


    /**
     * @param string $icon
     * @return string
     */
    public static function adminIcon(string $icon): string
    {
        return asset('/public/admin/dist/icons/' . $icon);
    }
}
