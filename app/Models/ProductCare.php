<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
class ProductCare extends BaseModel
{
    use HasFactory;
    protected $table    = PRODUCT_CARE_TBL;
    protected $primaryKey = 'pcare_id';
    const CREATED_AT = 'pcare_created_at';
    const UPDATED_AT = 'pcare_updated_at';

    protected $fillable = [
        'pcare_name',
        'pcare_name_en',
        'pcare_status',
        'pcare_is_delete',
        'pcare_created_at',
        'pcare_updated_at'
    ];

    const ALIAS = [
        'pcare_id'             => 'id',
        'pcare_name'           => 'name',
        'pcare_name_en'        => 'nameEn',
        'pcare_status'         => 'status',
        'pcare_is_delete'      => 'isDelete',
        'pcare_created_at'     => 'createdAt',
        'pcare_updated_at'     => 'updatedAt'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Builder
     */
    static function parentQuery(){
        return parent::query();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Builder
     */
    static function query()
    {
        return parent::query()->notDeleted();
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeNotDeleted($query)
    {
        return $query->where('pcare_is_delete', 'no');
    }
}
