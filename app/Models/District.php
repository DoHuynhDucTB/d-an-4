<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class District extends BaseModel
{
    use HasFactory;

    protected $table = DISTRICT_TBL;
    protected $primaryKey = 'id';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'name',
        'gso_id',
        'province_id',
        'created_at',
        'updated_at',
    ];

    /**
     * @return Builder
     */
    static function parentQuery(): Builder
    {
        return parent::query();
    }

    /**
     * @return Builder
     */
    static function query(): Builder
    {
        return parent::query();
    }

}
