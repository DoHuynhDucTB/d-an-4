<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Block extends BaseModel
{
    use HasFactory;

    protected $table = BLOCK_TBL;
    protected $primaryKey = 'block_id';
    const CREATED_AT = 'block_created_at';
    const UPDATED_AT = 'block_updated_at';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'block_id',
        'block_code',
        'block_name',
        'block_description',
        'block_sorted',
        'block_status',
        'block_group_id',
        'banner_group_id',
        'block_created_at',
        'block_updated_at',
    ];

    const ALIAS = [
        'block_id'           => 'id',
        'block_code'         => 'code',
        'block_name'         => 'name',
        'block_description'  => 'description',
        'block_sorted'       => 'sorted',
        'block_status'       => 'status',
        'block_group_id'     => 'blockGroupId',
        'banner_group_id'    => 'bannerGroupId',
        'block_created_at'   => 'createdAt',
        'block_updated_at'   => 'updatedAt',
    ];

    /**
     * @return Builder
     */
    static function parentQuery(): Builder
    {
        return parent::query();
    }

    /**
     * @return Builder
     */
    static function query(): Builder
    {
        return parent::query();
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeIsActivated($query)
    {
        return $query->where('block_status', 'activated');
    }

    /**
     * @param string $status
     * @return Builder[]|Collection
     */
    public function findByStatus($status = 'activated')
    {
        return self::parentQuery()->where('block_status', $status)->orderBy('block_sorted', 'asc')->get();
    }
}
