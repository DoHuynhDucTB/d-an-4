<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\HasMany;

class ProductVariant extends BaseModel
{
    use HasFactory;
    protected $table    = PRODUCT_VARIANT_TBL;
    protected $primaryKey = 'pvariant_id';
    const CREATED_AT = 'pvariant_created_at';
    const UPDATED_AT = 'pvariant_updated_at';

    const STATUS_NAME = [
        'stocking' => 'còn hàng',
        'out_of_stock' => 'hết hàng',
    ];

    const STATUS_SHOW_NAME = [
        'yes' => 'Bật',
        'no' => 'Tắt',
    ];

    const SEX_NAME = [
        'male' => 'Nam',
        'female' => 'Nữ',
        'unisex' => 'Unisex',
    ];

    protected $fillable = [
        'pvariant_id',
        'product_id',
        'pvariant_code',
        'pvariant_status',
        'pvariant_price',
        'pvariant_new_price',
        'pvariant_discount',
        'pvariant_length',
        'pvariant_width',
        'pvariant_height',
        'pvariant_weight',
        'pvariant_view',

        'pvariant_status_show',
        'pvariant_is_hot',
        'pvariant_is_gift',
        'pvariant_is_new',
        'pvariant_is_free_ship',
        'pvariant_is_sale',
        'pvariant_is_selling',
        'pvariant_sex',
        'pvariant_size',
        'pvariant_color',
        'pvariant_is_delete',
        'pvariant_related',
        'pvariant_created_at',
        'pvariant_updated_at',

        //vi
        'pvariant_name',
        'pvariant_short_description',
        'pvariant_description',
        'pvariant_meta_title',
        'pvariant_meta_keywords',
        'pvariant_meta_description',
        'pvariant_note',

        //en
        'pvariant_name_en',
        'pvariant_short_description_en',
        'pvariant_description_en',
        'pvariant_meta_title_en',
        'pvariant_meta_keywords_en',
        'pvariant_meta_description_en',
        'pvariant_note_en',
    ];

    const ALIAS = [
        'pvariant_id'               => 'id',
        'product_id'                => 'product_id',
        'pvariant_code'             => 'code',
        'pvariant_status'           => 'status',
        'pvariant_price'            => 'price',
        'pvariant_new_price'        => 'newPrice',
        'pvariant_discount'         => 'discount',
        'pvariant_length'           => 'length',
        'pvariant_width'            => 'width',
        'pvariant_height'           => 'height',
        'pvariant_weight'           => 'weight',
        'pvariant_view'             => 'view',

        'pvariant_status_show'      => 'statusShow',
        'pvariant_is_hot'           => 'isHot',
        'pvariant_is_gift'          => 'isGift',
        'pvariant_is_new'           => 'isNew',
        'pvariant_is_free_ship'     => 'isFreeShip',
        'pvariant_is_sale'          => 'isSale',
        'pvariant_is_selling'       => 'isSelling',
        'pvariant_sex'              => 'sex',
        'pvariant_size'             => 'size',
        'pvariant_color'            => 'color',
        'pvariant_is_delete'        => 'isDelete',
        'pvariant_related'          => 'related',
        'pvariant_created_at'       => 'createdAt',
        'pvariant_updated_at'       => 'updateAt',

        //vi
        'pvariant_name'                 => 'name',
        'pvariant_short_description'    => 'shortDescription',
        'pvariant_description'          => 'description',
        'pvariant_meta_title'           => 'metaTitle',
        'pvariant_meta_keywords'        => 'metaKeywords',
        'pvariant_meta_description'     => 'metaDescription',
        'pvariant_note'                 => 'note',

        //en
        'pvariant_name_en'              => 'nameEn',
        'pvariant_short_description_en' => 'shortDescriptionEn',
        'pvariant_description_en'       => 'descriptionEn',
        'pvariant_meta_title_en'        => 'metaTitle',
        'pvariant_meta_keywords_en'     => 'metaKeywords',
        'pvariant_meta_description_en'  => 'metaDescription',
        'pvariant_note_en'              => 'noteEn',
    ];

    /**
     * @return Builder
     */
    static function parentQuery(): Builder
    {
        return parent::query();
    }

    /**
     * @return Builder
     */
    static function query()
    {
        return parent::query()->notDeleted();
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeNotDeleted($query)
    {
        return $query->where('pvariant_is_delete', 'no');
    }

    /**
     * @param int $limit
     * @return Builder[]|Collection
     */
    public function findVariantIsHot(int $limit = 10)
    {
        return self::parentQuery()->where(['pvariant_is_hot' => 'yes', 'pvariant_is_delete' => 'no', 'pvariant_status' => 'stocking'])->limit($limit)->get();
    }

    /**
     * @return HasOne
     */
    public function avatar(): HasOne
    {
        return parent::hasOne('App\Models\Image', '3rd_key', 'pvariant_id')
            ->where(['3rd_type' => 'productvariant', 'image_value' => config('my.image.value.product_variant.avatar'), 'image_status' => 'activated']);
    }

    /**
     * @return HasMany
     */
    public function banner(): HasMany
    {
        return parent::hasMany('App\Models\Image', '3rd_key', 'pvariant_id')
            ->where(['3rd_type' => 'productvariant', 'image_value' => config('my.image.value.product_variant.banner'), 'image_status' => 'activated']);
    }

    /**
     * @return HasMany
     */
    public function thumbnail(): HasMany
    {
        return parent::hasMany('App\Models\Image', '3rd_key', 'pvariant_id')
            ->where(['3rd_type' => 'productvariant', 'image_value' => config('my.image.value.product_variant.thumbnail'), 'image_status' => 'activated']);
    }

    /**
     * @param array $productIds
     * @return Builder[]|Collection
     */
    public function findVariantAreStocking(array $productIds)
    {
        return self::query()
            ->select('*')
            ->where(['pvariant_status' => 'stocking'])
            ->whereIn('pvariant_id', $productIds)
            ->selectRaw('IF(pvariant_new_price > 0, pvariant_new_price, pvariant_price - pvariant_discount) AS price')
            ->get();
    }


    /**
     * @return HasOne
     */
    public function color(): HasOne
    {
        return parent::hasOne('App\Models\ProductColor', 'pcolor_id', 'pvariant_color');
    }

    public function size(): HasOne
    {
        return parent::hasOne('App\Models\ProductSize', 'psize_id', 'pvariant_size');
    }

}
