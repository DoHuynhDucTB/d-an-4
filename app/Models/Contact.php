<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Contact extends BaseModel
{
    use HasFactory;
    protected $table    = CONTACT_TBL;
    protected $primaryKey = 'contact_id';
    const CREATED_AT = 'contact_created_at';
    const UPDATED_AT = 'contact_updated_at';

    protected $fillable = [
        'contact_id',
        'contact_created_at',
        'contact_updated_at',
        'contact_name',
        'contact_email',
        'contact_status',
        'contact_subject',
        'contact_content',
        'contact_is_delete',
        'contact_phone_number',
    ];

    const ALIAS = [
        'contact_id'                => 'id',
        'contact_created_at'        => 'createdAt',
        'contact_updated_at'        => 'updatedAt',
        'contact_name'              => 'name',
        'contact_email'             => 'email',
        'contact_status'            => 'status',
        'contact_subject'           => 'subject',
        'contact_content'           => 'content',
        'contact_is_delete'         => 'isDelete',
        'contact_phone_number'      => 'phoneNumber'
    ];

    /**
     * @return Builder
     */
    static function parentQuery(): Builder
    {
        return parent::query();
    }

    /**
     * @return Builder
     */
    static function query(): Builder
    {
        return parent::query()->notDeleted();
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeNotDeleted($query)
    {
        return $query->where('contact_is_delete', 'no');
    }
}
