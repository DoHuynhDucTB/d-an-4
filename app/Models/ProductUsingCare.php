<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
class ProductUsingCare extends BaseModel
{
    use HasFactory;
    protected $table    = PRODUCT_USING_CARES_TBL;
    protected $primaryKey = 'id';

    protected $fillable = [
        'id',
        'product_id',
        'pcare_id',
        'is_delete'
    ];
}
