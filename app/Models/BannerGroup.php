<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class BannerGroup extends BaseModel
{
    use HasFactory;

    protected $table = BANNER_GROUP_TBL;
    protected $primaryKey = 'bangroup_id';
    const CREATED_AT = null;
    const UPDATED_AT = null;

    protected $fillable = [
        'bangroup_id',
        'bangroup_code',
        'bangroup_name',
        'bangroup_description',
    ];

    const ALIAS = [
        'bangroup_id'           => 'id',
        'bangroup_code'         => 'code',
        'bangroup_name'         => 'name',
        'bangroup_description'  => 'description',
    ];

    /**
     * @return Builder
     */
    static function parentQuery(): Builder
    {
        return parent::query();
    }
}
