<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

/**
 * Class AdminGroup
 * @property int adgroup_id
 * @property string name
 * @property string description
 * @property string status
 * @property string admenu_ids
 * @package App\Models
 */
class Menu extends BaseModel
{
    use HasFactory;

    protected $table = MENU_TBL;
    protected $primaryKey = 'menu_id';

    const CREATED_AT = 'menu_created_at';
    const UPDATED_AT = 'menu_created_at';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'menu_name',
        'menugroup_description',
        'menu_url',
        'group_id',
        'parent_id',
        'menu_number',
        'menu_status',
        'menu_is_delete',
        'menu_created_at',
        'menu_updated_at',

        'menu_name_en',
        'menu_url_en',
    ];

    const ALIAS = [
        'menu_id'                   => 'id',
        'menu_name'                 => 'name',
        'menugroup_description'     => 'description',
        'menu_url'                  => 'url',
        'group_id'                  => 'groupId',
        'parent_id'                 => 'parentId',
        'menu_number'               => 'number',
        'menu_status'               => 'status',
        'menu_is_delete'            => 'isDelete',
        'menu_created_at'           => 'createdAt',
        'menu_updated_at'           => 'updatedAt',

        'menu_name_en'              => 'nameEn',
        'menu_url_en'               => 'urlEn',
    ];

    /**
     * @return Builder
     */
    public static function parentQuery(): Builder
    {
        return parent::query(); // TODO: Change the autogenerated stub
    }

    /**
     * @return Builder
     */
    static function query(): Builder
    {
        return parent::query()->notDeleted();
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeNotDeleted($query)
    {
        return $query->where('menu_is_delete', 'no');
    }
}
