<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends BaseModel
{
    use HasFactory;

    protected $table = 'categories';

    /**
     * @return Builder
     */
    static function parentQuery(): Builder
    {
        return parent::query();
    }
}
