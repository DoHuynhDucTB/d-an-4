<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Comment extends BaseModel
{
    use HasFactory;
    protected $table    = COMMENT_TBL;
    protected $primaryKey = 'comment_id';
    const CREATED_AT = 'comment_created_at';
    const UPDATED_AT = 'comment_updated_at';

    protected $fillable = [
        'comment_content',
        'comment_status',
        'comment_created_at',
        'comment_updated_at',
        'comment_parent_id',
        'comment_level',
        'comment_rating',
        'comment_3rd_id',
        'comment_type',
        'user_id',
    ];

    const ALIAS = [
        'comment_content'           => 'content',
        'comment_status'            => 'status',
        'comment_created_at'        => 'createdAt',
        'comment_updated_at'        => 'updatedAt',
        'comment_parent_id'         => 'parentId',
        'comment_level'             => 'level',
        'comment_rating'            => 'rating',
        'comment_3rd_id'            => '3rdId',
        'comment_type'              => 'type',
        'user_id'                   => 'userId',
    ];

    /**
     * @return Builder
     */
    static function parentQuery(): Builder
    {
        return parent::query();
    }

    /**
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo('App\Models\User', 'id', 'user_id');
    }
}
