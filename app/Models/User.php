<?php

namespace App\Models;

use App\Notifications\ResetPassword;
use App\Notifications\VerifyEmail;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

/**
 * @property string email
 * Class User
 * @package App\Models
 */
class User extends Authenticatable implements MustVerifyEmail
{
    use HasFactory, Notifiable;

    protected $table = USER_TBL;

    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'phone',
        'name',
        'code',
        'phone',
        'email',
        'password',
        'oauth_id',
        'type',
        'status',
        'avatar',
        'birthday',
        'sex',
        'facebook',
        'google',
        'zalo',
        'product_wishlists',
        'address',
        'ward_id',
        'province_id',
        'district_id',
        'created_at',
        'updated_at',
        'pcare_ids',
        'pskin_ids',
        'pmakeup_ids'
    ];

    const ALIAS = [
        'name'               => 'name',
        'code'               => 'code',
        'phone'              => 'phone',
        'email'              => 'email',
        'password'           => 'password',
        'oauth_id'           => 'oauth_id',
        'type'               => 'type',
        'status'             => 'status',
        'avatar'             => 'avatar',
        'birthday'           => 'birthday',
        'sex'                => 'sex',
        'facebook'           => 'facebook',
        'google'             => 'google',
        'zalo'               => 'zalo',
        'product_wishlists'  => 'product_wishlists',
        'address'            => 'address',
        'ward_id'            => 'wardId',
        'province_id'        => 'provinceId',
        'district_id'        => 'districtId',
        'created_at'         => 'created_at',
        'updated_at'         => 'updated_at',
        'date_of_birth'      => 'date_of_birth',
    ];

    const VALUE_DEFAULT_OF_FIELDS = [
        'sex' => ['male', 'female']
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    /**
     * Send the email verification notification.
     *
     * @return void
     */
    public function sendEmailVerificationNotification()
    {
        $this->notify(new VerifyEmail());
    }

    /**
     * Mark the given user's email as verified.
     *
     * @return bool
     */
    public function markEmailAsVerified()
    {
        $this->forceFill([
            'email_verified_at' => $this->freshTimestamp(),
        ])->save();
    }

    /**
     * @param string $token
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPassword($token));
    }


    /**
     * Lấy trường mật khẩu cuả user, ví dụ có thể lấy trường email ... làm mật khẩu - mặc định là $this->password
     * Lưu ý khi thay đổi trường này, hàng loạt các vấn đề liên quan sẽ phải thay đổi như: login, verify email ...
     * KHÔNG nên thay đổi.
     * Get the password for the user.
     *
     * @return string
     */
    public function getAuthPassword()
    {
        return $this->password;
    }


    /**
     * @return Builder
     */
    public static function parentQuery(): Builder
    {
        return parent::query();
    }

    /**
     * @return HasMany
     */
    public function orders(): HasMany
    {
        return parent::hasMany('App\Models\Order', 'user_id', 'id');
    }
}
