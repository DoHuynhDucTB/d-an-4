<?php


namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;

class BaseModel extends Model
{
    const ALIAS = []; //To convert data key and format

    /**
     * Nếu data theo ngôn ngữ hiện tại null thì lấy data của ngôn ngũ mặc định.
     * Ví dụ: title_en là null thì lấy title.
     * @var bool
     */
    private $getDataByLanguageDefaultIfCurrentDataIsNull = true;


    /**
     * @param string $fieldName
     * @param string $valueDefault
     * @return mixed|string
     */
    public function getDataByLanguage(string $fieldName, $valueDefault = ''): string
    {
        $language = app()->getLocale();
        if ($language == config('app.default_locale')) {
            return $this->{$fieldName} ?? $valueDefault;
        }elseif($this->getDataByLanguageDefaultIfCurrentDataIsNull === true){
            $value = $this->{$fieldName . "_$language"};
            return $value ? $value : ($this->{$fieldName} ?? $valueDefault);
        } else {
            return $this->{$fieldName . "_$language"};
        }
    }

    /**
     * @param $data
     * @return array
     */
    public function revertAlias($data): array
    {
        $return = [];
        foreach (static::ALIAS as $field => $alias) {
            if (($value = Arr::get($data, $alias)) !== null) {
                Arr::set($return, $field, $value);
            }
        }

        return $return;
    }


    /**
     * @param array $ids
     * @return string
     */
    public function getIds(array $ids): string
    {
        return ',' . implode(',', $ids) . ',';
    }
}
