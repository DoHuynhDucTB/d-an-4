<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
class ProductUsingMakeUp extends BaseModel
{
    use HasFactory;
    protected $table    = PRODUCT_USING_MAKEUP_TBL;
    protected $primaryKey = 'id';

    protected $fillable = [
        'id',
        'product_id',
        'pmakeup_id',
        'is_delete'
    ];

    /**
     * @return Builder
     */
    static function parentQuery(): Builder
    {
        return parent::query();
    }
}
