<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
class AdvertGroup extends BaseModel
{
    use HasFactory;
    protected $table    = ADVERT_GROUP_TBL;
    protected $primaryKey = 'adgroup_id';
    const CREATED_AT = 'adgroup_created_at';
    const UPDATED_AT = 'adgroup_updated_at';

    protected $fillable = [
        'adgroup_id',
        'adgroup_created_at',
        'adgroup_update_at',
        'adgroup_created_by',
        'adgroup_name',
        'adgroup_status',
        'adgroup_description',
        'adgroup_code',
        'adgroup_is_delete',

        'adgroup_name_en',
        'adgroup_description_en',
    ];

    const ALIAS = [
        'adgroup_id'               => 'id',
        'adgroup_created_at'       => 'createdAt',
        'adgroup_update_at'        => 'updatedAt',
        'adgroup_created_by'       => 'createdBy',
        'adgroup_name'             => 'name',
        'adgroup_status'           => 'status',
        'adgroup_description'      => 'description',
        'adgroup_code'             => 'code',
        'adgroup_is_delete'        => 'isDelete',


        'adgroup_name_en'        => 'nameEn',
        'adgroup_description_en' => 'descriptionEn',
    ];

    /**
     * @return Builder
     */
    static function parentQuery(): Builder
    {
        return parent::query();
    }

    /**
     * @return Builder
     */
    static function query(): Builder
    {
        return parent::query()->notDeleted();
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeNotDeleted($query)
    {
        return $query->where('adgroup_is_delete', 'no');
    }
}
