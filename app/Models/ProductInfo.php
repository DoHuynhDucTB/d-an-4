<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\HasMany;

class ProductInfo extends BaseModel
{
    use HasFactory;
    protected $table    = PRODUCT_INFO_TBL;
    protected $primaryKey = 'id';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'update_at';

    protected $fillable = [
        'id',
        'email',
        'product_group',
        'product_general_code',
        'product_general_name',
        'product_variant_code',
        'product_variant_name',
        'color_id',
        'sex',
        'size_id',
        'created_at',
        'update_at',
        'is_delete',
    ];

    const ALIAS = [
        'id'                        => 'id',
        'email'                     => 'email',
        'product_group'             => 'group',
        'product_general_code'      => 'generalCode',
        'product_general_name'      => 'generalName',
        'product_variant_code'      => 'variantCode',
        'product_variant_name'      => 'variantName',
        'color_id'                  => 'colorId',
        'sex'                       => 'sex',
        'size_id'                   => 'sizeId',
        'created_at'                => 'createdAt',
        'update_at'                 => 'updateAt',
        'is_delete'                 => 'isDelete',
    ];

    /**
     * @return Builder
     */
    static function parentQuery(): Builder
    {
        return parent::query();
    }

    /**
     * @return Builder
     */
    static function query()
    {
        return parent::query()->notDeleted();
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeNotDeleted($query)
    {
        return $query->where('is_delete', 'no');
    }
}
