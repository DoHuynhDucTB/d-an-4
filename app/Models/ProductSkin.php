<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
class ProductSkin extends BaseModel
{
    use HasFactory;
    protected $table    = PRODUCT_SKIN_TBL;
    protected $primaryKey = 'pskin_id';
    const CREATED_AT = 'pskin_created_at';
    const UPDATED_AT = 'pskin_updated_at';

    protected $fillable = [
        'pskin_name',
        'pskin_name_en',
        'pskin_status',
        'pskin_is_delete',
        'pskin_created_at',
        'pskin_updated_at'
    ];

    const ALIAS = [
        'pskin_id'             => 'id',
        'pskin_name'           => 'name',
        'pskin_name_en'        => 'nameEn',
        'pskin_status'         => 'status',
        'pskin_is_delete'      => 'isDelete',
        'pskin_created_at'     => 'createdAt',
        'pskin_updated_at'     => 'updatedAt'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Builder
     */
    static function parentQuery(){
        return parent::query();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Builder
     */
    static function query()
    {
        return parent::query()->notDeleted();
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeNotDeleted($query)
    {
        return $query->where('pskin_is_delete', 'no');
    }
}
