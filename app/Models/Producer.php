<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Producer extends BaseModel
{
    use HasFactory;
    protected $table    = PRODUCER_TBL;
    protected $primaryKey = 'producer_id';
    const CREATED_AT = 'producer_created_at';
    const UPDATED_AT = 'producer_updated_at';

    protected $fillable = [
        'producer_id',
        'producer_created_at',
        'producer_updated_at',

        //vi
        'producer_name',
        'producer_code',
        'producer_description',
        'producer_content',
        'producer_status',
        'producer_meta_title',
        'producer_meta_keywords',
        'producer_meta_description',
        'producer_is_delete',

        //en
        'producer_name_en',
        'producer_code_en',
        'producer_description_en',
        'producer_content_en',
        'producer_meta_title_en',
        'producer_meta_keywords_en',
        'producer_meta_description_en',
    ];

    const ALIAS = [
        'producer_id'               => 'id',
        'producer_created_at'       => 'createdAt',
        'producer_updated_at'       => 'updatedAt',

        //vi
        'producer_name'             => 'name',
        'producer_code'             => 'code',
        'producer_description'      => 'description',
        'producer_content'          => 'content',
        'producer_status'           => 'status',
        'producer_meta_title'       => 'metaTitle',
        'producer_meta_keywords'    => 'metaKeywords',
        'producer_meta_description' => 'metaDescription',
        'producer_is_delete'        => 'isDelete',


        'producer_name_en'          => 'nameEn',
        'producer_code_en'          => 'codeEn',
        'producer_description_en'   => 'descriptionEn',
        'producer_content_en'       => 'contentEn',
        'producer_meta_title_en'    => 'metaTitleEn',
        'producer_meta_keywords_en' => 'metaKeywordsEn',
        'producer_meta_description_en' => 'metaDescriptionEn',
    ];

    /**
     * @return Builder
     */
    static function parentQuery(): Builder
    {
        return parent::query();
    }

    /**
     * @return Builder
     */
    static function query(): Builder
    {
        return parent::query()->notDeleted();
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeNotDeleted($query)
    {
        return $query->where('producer_is_delete', 'no');
    }

    /**
     * @return HasOne
     */
    public function avatar(): HasOne
    {
        return parent::hasOne('App\Models\Image', '3rd_key', 'producer_id')
            ->where(['3rd_type' => 'producer', 'image_value' => config('my.image.value.producer.avatar'), 'image_status' => 'activated']);
    }
}
