<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\HasOne;

class PerceivedValue extends BaseModel
{
    use HasFactory;

    protected $table = PERCEIVED_VALUE_TBL;
    protected $primaryKey = 'pervalue_id';
    const CREATED_AT = null;
    const UPDATED_AT = null;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'pervalue_description',
        'pervalue_description_en',
        'pervalue_fullname',
        'pervalue_status',
    ];

    const ALIAS = [
        'pervalue_id' => 'id',
        'pervalue_description' => 'description',
        'pervalue_description_en' => 'descriptionEn',
        'pervalue_fullname' => 'fullname',
        'pervalue_status' => 'status',
    ];

    /**
     * @return Builder
     */
    static function parentQuery(): Builder
    {
        return parent::query();
    }

    /**
     * @return Builder
     */
    static function query(): Builder
    {
        return parent::query()->isActivated();
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeIsActivated($query)
    {
        return $query->where('pervalue_status', 'activated');
    }


    /**
     * @return HasOne
     */
    public function avatar(): HasOne
    {
        return parent::hasOne('App\Models\Image', '3rd_key', 'pervalue_id')
            ->where(['3rd_type' => 'perceivedvalue', 'image_value' => config('my.image.value.perceivedvalue.avatar'), 'image_status' => 'activated']);
    }
}
