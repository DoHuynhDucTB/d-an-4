<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;

class VoucherSerie extends BaseModel
{
    use HasFactory;
    protected $table    = VOUCHER_SERIE_TBL;
    protected $primaryKey = 'voucher_serie_id';
    const CREATED_AT = 'voucher_serie_created_at';
    const UPDATED_AT = 'voucher_serie_updated_at';

    protected $fillable = [
        'voucher_serie_id',
        'voucher_serie_character',
        'voucher_serie_number',
        'voucher_serie_created_at',
        'voucher_serie_updated_at',
        'voucher_serie_code',
        'voucher_serie_name',
        'voucher_serie_description',
        'voucher_serie_status',
        'voucher_serie_percent',
        'voucher_serie_money',
        'voucher_serie_from',
        'voucher_serie_to',
        'min_value_order',
        'product_type',
        'product_category',
        'product_id',
        'order_number_application',
        'order_number_used',
        'max_product',
        'voucher_serie_is_delete',
        'user_ids',
    ];

    const ALIAS = [
        'voucher_serie_id'                => 'id',

        'voucher_serie_character'           => 'character',
        'voucher_serie_number'              => 'number',
        'voucher_serie_created_at'        => 'createdAt',
        'voucher_serie_updated_at'        => 'updatedAt',
        'voucher_serie_code'              => 'code',
        'voucher_serie_name'              => 'name',
        'voucher_serie_description'       => 'description',
        'voucher_serie_status'            => 'status',
        'voucher_serie_percent'           => 'percent',
        'voucher_serie_money'             => 'money',
        'voucher_serie_from'              => 'fromTime',
        'voucher_serie_to'                => 'toTime',
        'min_value_order'           => 'minValueOrder',
        'product_type'              => 'type',
        'product_category'          => 'pcatId',
        'product_id'                => 'productId',
        'order_number_application'  => 'orderNumberApplication',
        'order_number_used'         => 'orderNumberUsed',
        'max_product'               => 'maxProduct',
        'voucher_serie_is_delete'         => 'isDelete',
        'user_ids'                  => 'userIds',
    ];

    /**
     * @return Builder
     */
    static function parentQuery(): Builder
    {
        return parent::query();
    }

    /**
     * @return Builder
     */
    static function query()
    {
        return parent::query()->notDeleted();
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeNotDeleted($query)
    {
        return $query->where('voucher_serie_is_delete', 'no');
    }

    /**
     * @return HasOne
     */
    public function avatar(): HasOne
    {
        return parent::hasOne('App\Models\Image', '3rd_key', 'voucher_serie_id')
            ->where(['3rd_type' => 'voucherserie', 'image_value' => config('my.image.value.voucher_serie.avatar'), 'image_status' => 'activated']);
    }

    /**
     * @param $data
     * @param $id
     * @return Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public static function getUserVoucher($data, $id){
        $userIds = array_column($data, 'id');
        $times = array_column($data, 'time');

        $users = User::query()->whereIn('id', $userIds)->get();

        if(!empty($users) && count($users) > 0){
            foreach ($users as $key => $itemUser){
                $orders = Order::getOrderByUserAndVoucher($itemUser['id'], $id);
                $itemUser->orderNumber = $orders->count();
                $itemUser->timeVoucher = $times[$key];
            }
        }
        return $users;
    }
}
