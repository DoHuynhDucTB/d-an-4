<?php

namespace App\Models;

use App\Notifications\ResetPassword;
use App\Notifications\VerifyEmail;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

/**
 * Class AdminUser
 * @property int aduser_id
 * @property string name
 * @property string username
 * @property string email
 * @property string avatar
 * @property string email_verified_at
 * @property string password
 * @property string status
 * @property string is_deleted
 * @property string remember_token
 * @property string created_at
 * @property string updated_at
 * @property int adgroup_id
 * @package App\Models
 */
class AdminUser extends Authenticatable implements MustVerifyEmail
{
    use HasFactory, Notifiable;

    protected $table = ADMIN_USER_TBL;

    protected $primaryKey = 'aduser_id';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'username',
        'email',
        'avatar',
        'password',
        'status',
        'adgroup_id',
        'is_deleted',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    /**
     * Send the email verification notification.
     *
     * @return void
     */
    public function sendEmailVerificationNotification()
    {
        $this->notify(new VerifyEmail());
    }

    /**
     * Mark the given user's email as verified.
     *
     * @return bool
     */
    public function markEmailAsVerified()
    {
        $this->forceFill([
            'email_verified_at' => $this->freshTimestamp(),
        ])->save();
    }

    /**
     * @param string $token
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPassword($token));
    }


    /**
     * Lấy trường mật khẩu cuả user, ví dụ có thể lấy trường email ... làm mật khẩu - mặc định là $this->password
     * Lưu ý khi thay đổi trường này, hàng loạt các vấn đề liên quan sẽ phải thay đổi như: login, verify email ...
     * KHÔNG nên thay đổi.
     * Get the password for the user.
     *
     * @return string
     */
    public function getAuthPassword()
    {
        return $this->password;
    }


    /**
     * @return HasOne
     */
    public function adminGroup(): HasOne
    {
        return $this->hasOne(AdminGroup::class, 'adgroup_id', 'adgroup_id');
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeNotRootUser($query)
    {
        return $query->whereNotIn('aduser_id', ROOT_USER_IDS);
    }

    /**
     * @return Builder
     */
    public static function query()
    {
        return parent::query()->notRootUser(); // TODO: Change the autogenerated stub
    }

    /**
     * @return Builder
     */
    public static function parentQuery()
    {
        return parent::query();
    }
}
