<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Agency extends BaseModel
{
    use HasFactory;
    protected $table    = AGENCY_TBL;
    protected $primaryKey = 'agency_id';
    const CREATED_AT = 'agency_created_at';
    const UPDATED_AT = 'agency_updated_at';

    protected $fillable = [
        'agency_id',
        'agency_created_at',
        'agency_updated_at',
        'agency_name',
        'agency_status',
        'agency_description',
        'agency_status',
        'agency_is_delete',
        'agency_name_en',
        'agency_description_en',
        'agency_country',
        'agency_province',
        'agency_district',
        'agency_ward',
        'agency_address',
    ];

    const ALIAS = [
        'agency_id'                 => 'id',
        'agency_created_at'         => 'createdAt',
        'agency_updated_at'         => 'updatedAt',
        'agency_name'               => 'name',
        'agency_status'             => 'status',
        'agency_description'        => 'description',
        'agency_is_delete'          => 'isDelete',
        'agency_name_en'            => 'nameEn',
        'agency_description_en'     => 'descriptionEn',
        'agency_country'            => 'country',
        'agency_province'           => 'province',
        'agency_district'           => 'district',
        'agency_ward'               => 'ward',
        'agency_address'            => 'address',
    ];

    const COUNTRY = [
        1 => 'Việt Nam',
        2 => 'Nhật Bản',
        3 => 'Thái Lan'
    ];

    /**
     * @return Builder
     */
    static function parentQuery(): Builder {
        return parent::query();
    }

    /**
     * @return Builder
     */
    static function query()
    {
        return parent::query()->notDeleted();
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeNotDeleted($query)
    {
        return $query->where('agency_is_delete', 'no');
    }

    /**
     * @return HasOne
     */
    public function avatar(): HasOne
    {
        return parent::hasOne('App\Models\Image', '3rd_key', 'agency_id')
            ->where(['3rd_type' => 'agency', 'image_value' => config('my.image.value.agency.avatar'), 'image_status' => 'activated']);
    }


    /**
     * @return HasOne
     */
    public function province(): HasOne
    {
        return parent::hasOne('App\Models\Province', 'id', 'agency_province');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function shops()
    {
        return parent::hasMany('App\Models\Shop', 'agency_id', 'agency_id')->where([
            'shop_status' => 'activated',
            'shop_is_delete' => 'no',
        ]);
    }

}
