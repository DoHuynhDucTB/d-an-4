<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Voucher extends BaseModel
{
    use HasFactory;
    protected $table    = VOUCHER_TBL;
    protected $primaryKey = 'voucher_id';
    const CREATED_AT = 'voucher_created_at';
    const UPDATED_AT = 'voucher_updated_at';

    protected $fillable = [
        'voucher_id',
        'voucher_created_at',
        'voucher_updated_at',
        'voucher_code',
        'voucher_name',
        'voucher_description',
        'voucher_status',
        'voucher_percent',
        'voucher_money',
        'voucher_from',
        'voucher_to',
        'min_value_order',
        'product_type',
        'product_category',
        'product_id',
        'order_number_application',
        'order_number_used',
        'max_product',
        'voucher_is_delete',
        'user_ids',
    ];

    const ALIAS = [
        'voucher_id'                => 'id',
        'voucher_created_at'        => 'createdAt',
        'voucher_updated_at'        => 'updatedAt',
        'voucher_code'              => 'code',
        'voucher_name'              => 'name',
        'voucher_description'       => 'description',
        'voucher_status'            => 'status',
        'voucher_percent'           => 'percent',
        'voucher_money'             => 'money',
        'voucher_from'              => 'fromTime',
        'voucher_to'                => 'toTime',
        'min_value_order'           => 'minValueOrder',
        'product_type'              => 'type',
        'product_category'          => 'pcatId',
        'product_id'                => 'productId',
        'order_number_application'  => 'orderNumberApplication',
        'order_number_used'         => 'orderNumberUsed',
        'max_product'               => 'maxProduct',
        'voucher_is_delete'         => 'isDelete',
        'user_ids'                  => 'userIds',
    ];

    /**
     * @return Builder
     */
    static function parentQuery(): Builder
    {
        return parent::query();
    }

    /**
     * @return Builder
     */
    static function query()
    {
        return parent::query()->notDeleted();
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeNotDeleted($query)
    {
        return $query->where('voucher_is_delete', 'no');
    }

    /**
     * @return HasOne
     */
    public function avatar(): HasOne
    {
        return parent::hasOne('App\Models\Image', '3rd_key', 'voucher_id')
            ->where(['3rd_type' => 'voucher', 'image_value' => config('my.image.value.voucher.avatar'), 'image_status' => 'activated']);
    }

    /**
     * @param $data
     * @param $id
     * @return Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public static function getUserVoucher($data, $id){
        $users = [];
        if($data){
            $userIds = array_column($data, 'id');
            $times = array_column($data, 'time');

            $users = User::query()->whereIn('id', $userIds)->get()->toArray();

            if(!empty($users) && count($users) > 0){
                foreach ($users as $key => $itemUser){
                    $orders = Order::getOrderByUserAndVoucher($itemUser['id'], $id);
                    $users[$key]['orderNumber'] = $orders->count() > 0 ? 'Đã dùng' : 'Chưa dùng';
                    $users[$key]['timeVoucher'] = $times[$key];
                }
            }
        }
        return $users;
    }

    /**
     * @param $vouchers
     */
    public static function getUserVoucherArray($vouchers){
        $data = [];
        $userTotalIds = [];

        if(!empty($vouchers) && $vouchers->count() > 0){
            foreach ($vouchers as $key => $item){
                $userIds = json_decode($item->user_ids, 1);

                if(!empty($userIds) && count($userIds) > 0){
                    foreach ($userIds as $keyU => $itemU){
                        $userTotalIds[$itemU['id']][] = $item->voucher_id;
                    }
                }
            }
        }

        if(!empty($userTotalIds) && count($userTotalIds) > 0){
            foreach ($userTotalIds as $userId => $arrayVoucherId){
                $user = User::query()->where('id', $userId)->first();
                if($user){
                    $user = $user->toArray();
                    $array = [];
                    if(!empty($arrayVoucherId) && count($arrayVoucherId) > 0){
                        foreach ($arrayVoucherId as $key => $itemVoucherId){

                            $voucher = Voucher::query()->where('voucher_id', $itemVoucherId)->first();
                            $orders = Order::getOrderByUserAndVoucher($userId, $itemVoucherId);
                            $array[$key]['status'] = $orders->count() > 0 ? 'Đã dùng' : 'Chưa dùng';
                            $array[$key]['code'] = $voucher->voucher_code;
                        }
                    }
                    $user['listVoucher'] = $array;
                    $data[] = $user;
                }
            }
        }
        return $data;
    }
}
