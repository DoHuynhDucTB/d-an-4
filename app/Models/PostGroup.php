<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\HasOne;

class PostGroup extends BaseModel
{
    use HasFactory;
    protected $table    = POST_GROUP_TBL;
    protected $primaryKey = 'postgroup_id';
    const CREATED_AT = 'postgroup_created_at';
    const UPDATED_AT = 'postgroup_updated_at';

    protected $fillable = [
        'postgroup_id',
        'postgroup_created_at',
        'postgroup_updated_at',

        //vi
        'postgroup_name',
        'postgroup_description',
        'postgroup_status',
        'postgroup_meta_title',
        'postgroup_meta_keywords',
        'postgroup_meta_description',
        'postgroup_is_delete',
        'postgroup_parent',


        //en
        'postgroup_name_en',
        'postgroup_description_en',
        'postgroup_meta_title_en',
        'postgroup_meta_keywords_en',
        'postgroup_meta_description_en',
    ];

    const ALIAS = [
        'postgroup_id'                  => 'id',
        'postgroup_created_at'          => 'createdAt',
        'postgroup_updated_at'          => 'updatedAt',

        //vi
        'postgroup_name'                => 'name',
        'postgroup_description'         => 'description',
        'postgroup_status'              => 'status',
        'postgroup_meta_title'          => 'metaTitle',
        'postgroup_meta_keywords'       => 'metaKeywords',
        'postgroup_meta_description'    => 'metaDescription',
        'postgroup_is_delete'           => 'isDelete',
        'postgroup_parent'              => 'parent',


        //en
        'postgroup_name_en'             => 'nameEn',
        'postgroup_description_en'      => 'descriptionEn',
        'postgroup_meta_title_en'       => 'metaTitleEn',
        'postgroup_meta_keywords_en'    => 'metalKeywordsEn',
        'postgroup_meta_description_en' => 'metaDescriptionEn',
    ];

    /**
     * @return Builder
     */
    static function parentQuery(): Builder
    {
        return parent::query();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Builder
     */
    static function query()
    {
        return parent::query()->notDeleted();
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeNotDeleted($query)
    {
        return $query->where('postgroup_is_delete', 'no');
    }

    /**
     * @return HasOne
     */
    public function avatar(): HasOne
    {
        return parent::hasOne('App\Models\Image', '3rd_key', 'postgroup_id')
            ->where(['3rd_type' => 'postgroup', 'image_value' => config('my.image.value.postgroup.avatar'), 'image_status' => 'activated']);
    }
}
