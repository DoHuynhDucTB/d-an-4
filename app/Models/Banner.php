<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Banner extends BaseModel
{
    use HasFactory;

    protected $table = BANNER_TBL;
    protected $primaryKey = 'banner_id';
    const CREATED_AT = 'banner_created_at';
    const UPDATED_AT = 'banner_updated_at';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'banner_name',
        'banner_name_en',
        'banner_url',
        'banner_description',
        'banner_description_en',
        'banner_status',
        'banner_is_delete',
        'bangroup_id',
        'banner_sorted',
        'banner_created_at',
        'banner_updated_at',
    ];

    const ALIAS = [
        'banner_id'               => 'id',
        'banner_created_at'       => 'createdAt',
        'banner_updated_at'       => 'updateAt',
        'banner_name'             => 'name',
        'banner_name_en'          => 'nameEn',
        'banner_url'              => 'url',
        'banner_description'      => 'description',
        'banner_description_en'   => 'descriptionEn',
        'banner_status'           => 'status',
        'banner_is_delete'        => 'delete',
        'bangroup_id'             => 'groupId',
        'banner_sorted'           => 'sorted',
        'banner_color_hex'        => 'colorHex',
    ];

    /**
     * @return Builder
     */
    static function parentQuery(): Builder
    {
        return parent::query();
    }

    /**
     * @return Builder
     */
    static function query(): Builder
    {
        return parent::query()->notDeleted();
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeNotDeleted($query)
    {
        return $query->where('banner_is_delete', 'no');
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeIsActivated($query)
    {
        return $query->where('banner_status', 'activated');
    }

    /**
     * @param int $bannerGroupId
     * @return mixed
     */
    public function findByBannerGroup(int $bannerGroupId)
    {
        return parent::query()->where(['bangroup_id' => $bannerGroupId])->isActivated()->get();
    }

    /**
     * @param string $bannerCode
     * @param int $limit
     * @return mixed
     */
    public function findByBannerCode(string $bannerCode, int $limit = 10)
    {
        return parent::query()->join(BANNER_GROUP_TBL, BANNER_GROUP_TBL . '.bangroup_id', BANNER_TBL . '.bangroup_id')
            ->where(['bangroup_code' => $bannerCode])->isActivated()->limit($limit)->orderBy('banner_sorted', 'asc')->get();
    }

    /**
     * @return BelongsTo
     */
    public function groups(): BelongsTo
    {
        return $this->belongsTo('App\Models\BannerGroup', 'bangroup_id', 'bangroup_id');
    }

    /**
     * @return HasOne
     */
    public function avatar(): HasOne
    {
        return parent::hasOne('App\Models\Image', '3rd_key', 'banner_id')
            ->where(['3rd_type' => 'banner', 'image_value' => config('my.image.value.banner.avatar'), 'image_status' => 'activated']);
    }


    /**
     * Banner dùng chung cho các trang ngoại trừ homepage (frontend)
     * @return Builder|Model|object|null
     */
    public function getBannerAllPage()
    {
        return self::query()->where('bangroup_id', 5)->orderBy('banner_id', 'desc')->first();
    }
}
