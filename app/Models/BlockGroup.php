<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class BlockGroup extends BaseModel
{
    use HasFactory;

    protected $table = BLOCK_GROUP_TBL;
    protected $primaryKey = 'blockgroup_id';
    const CREATED_AT = null;
    const UPDATED_AT = null;

    protected $fillable = [
        'blockgroup_id',
        'blockgroup_code',
        'blockgroup_name',
        'blockgroup_description',
    ];

    const ALIAS = [
        'blockgroup_id'           => 'id',
        'blockgroup_code'         => 'code',
        'blockgroup_name'         => 'name',
        'blockgroup_description'  => 'description',
    ];

    /**
     * @return Builder
     */
    static function parentQuery(): Builder
    {
        return parent::query();
    }
}
