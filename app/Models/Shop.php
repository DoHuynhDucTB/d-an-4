<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Shop extends BaseModel
{
    use HasFactory;
    protected $table    = SHOP_TBL;
    protected $primaryKey = 'shop_id';
    const CREATED_AT = 'shop_created_at';
    const UPDATED_AT = 'shop_updated_at';

    protected $fillable = [
        'shop_id',
        'shop_created_at',
        'shop_updated_at',
        'shop_status',
        'shop_name',
        'shop_description',
        'shop_time',
        'shop_hotline',
        'shop_address',
        'shop_status',
        'shop_is_delete',
        'shop_name_en',
        'shop_description_en',
        'shop_time_en',
        'shop_hotline_en',
        'shop_address_en',
        'agency_id',
    ];

    const ALIAS = [
        'shop_id'                 => 'id',
        'shop_created_at'         => 'createdAt',
        'shop_updated_at'         => 'updatedAt',
        'shop_status'             => 'status',
        'shop_name'               => 'name',
        'shop_description'        => 'description',
        'shop_time'               => 'time',
        'shop_hotline'            => 'hotline',
        'shop_address'            => 'address',
        'shop_is_delete'          => 'isDelete',
        'shop_name_en'            => 'nameEn',
        'shop_description_en'     => 'descriptionEn',
        'shop_time_en'            => 'timeEn',
        'shop_hotline_en'         => 'hotlineEn',
        'shop_address_en'         => 'addressEn',
        'agency_id'               => 'agencyId',
    ];

    /**
     * @return Builder
     */
    static function parentQuery(): Builder
    {
        return parent::query();
    }

    /**
     * @return Builder
     */
    static function query()
    {
        return parent::query()->notDeleted();
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeNotDeleted($query)
    {
        return $query->where('shop_is_delete', 'no');
    }

    /**
     * @return HasOne
     */
    public function avatar(): HasOne
    {
        return parent::hasOne('App\Models\Image', '3rd_key', 'shop_id')
            ->where(['3rd_type' => 'shop', 'image_value' => config('my.image.value.shop.avatar'), 'image_status' => 'activated']);
    }

}
