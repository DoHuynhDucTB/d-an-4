<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
class ProductMakeUp extends BaseModel
{
    use HasFactory;
    protected $table    = PRODUCT_MAKE_UP_TBL;
    protected $primaryKey = 'pmakeup_id';
    const CREATED_AT = 'pmakeup_created_at';
    const UPDATED_AT = 'pmakeup_updated_at';

    protected $fillable = [
        'pmakeup_name',
        'pmakeup_name_en',
        'pmakeup_status',
        'pmakeup_is_delete',
        'pmakeup_created_at',
        'pmakeup_updated_at'
    ];

    const ALIAS = [
        'pmakeup_id'             => 'id',
        'pmakeup_name'           => 'name',
        'pmakeup_name_en'        => 'nameEn',
        'pmakeup_status'         => 'status',
        'pmakeup_is_delete'      => 'isDelete',
        'pmakeup_created_at'     => 'createdAt',
        'pmakeup_updated_at'     => 'updatedAt'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Builder
     */
    static function parentQuery(): \Illuminate\Database\Eloquent\Builder
    {
        return parent::query();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Builder
     */
    static function query()
    {
        return parent::query()->notDeleted();
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeNotDeleted($query)
    {
        return $query->where('pmakeup_is_delete', 'no');
    }
}
