<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Advert extends BaseModel
{
    use HasFactory;
    protected $table    = ADVERT_TBL;
    protected $primaryKey = 'advert_id';
    const CREATED_AT = 'advert_created_at';
    const UPDATED_AT = 'advert_updated_at';

    protected $fillable = [
        'advert_id',
        'advert_created_at',
        'advert_updated_at',
        'advert_created_by',
        'advert_name',
        'advert_url',
        'group_id',
        'advert_status',
        'advert_description',
        'advert_number',
        'advert_view',
        'advert_delete',

        //en
        'advert_name_en',
        'advert_description_en',
    ];

    const ALIAS = [
        'advert_id'               => 'id',
        'advert_created_at'       => 'createdAt',
        'advert_updated_at'       => 'updateAt',
        'advert_created_by'       => 'createdBy',
        'advert_name'             => 'name',
        'advert_url'              => 'url',
        'group_id'                => 'groupId',
        'advert_status'           => 'status',
        'advert_description'      => 'description',
        'advert_number'           => 'number',
        'advert_view'             => 'view',
        'advert_is_delete'        => 'delete',

        'advert_name_en'          => 'nameEn',
        'advert_description_en'   => 'descriptionEn',
    ];

    /**
     * @return Builder
     */
    static function parentQuery(): Builder
    {
        return parent::query();
    }

    /**
     * @return Builder
     */
    static function query()
    {
        return parent::query()->notDeleted();
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeNotDeleted($query)
    {
        return $query->where('advert_is_delete', 'no');
    }

    /**
     * @return HasOne
     */
    public function groups(): BelongsTo
    {
        return $this->belongsTo('App\Models\AdvertGroup', 'adgroup_id', 'group_id');
    }

    /**
     * @return HasOne
     */
    public function avatar(): HasOne
    {
        return parent::hasOne('App\Models\Image', '3rd_key', 'advert_id')
            ->where(['3rd_type' => 'advert', 'image_value' => config('my.image.value.advert.avatar'), 'image_status' => 'activated']);
    }
}
