<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Subscribe extends BaseModel
{
    use HasFactory;

    protected $table = SUBSCRIBE_TBL;
    protected $primaryKey = 'subscribe_id';
    const CREATED_AT = 'subscribe_created_at';
    const UPDATED_AT = 'subscribe_updated_at';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'subscribe_id',
        'subscribe_name',
        'subscribe_email',
        'subscribe_status',
        'subscribe_created_at',
        'subscribe_updated_at',
        'subscribe_is_delete',
    ];

    const ALIAS = [
        'subscribe_id'           => 'id',
        'subscribe_name'         => 'name',
        'subscribe_email'        => 'email',
        'subscribe_status'       => 'status',
        'subscribe_created_at'   => 'createdAt',
        'subscribe_updated_at'   => 'updatedAt',
        'subscribe_is_delete'    => 'isDelete',
    ];

    /**
     * @return Builder
     */
    static function parentQuery(): Builder
    {
        return parent::query();
    }

    /**
     * @return Builder
     */
    static function query(): Builder
    {
        return parent::query()->notDeleted();
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeNotDeleted($query)
    {
        return $query->where('subscribe_is_delete', 'no');
    }
}
