<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class VoucherSerieRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $task = $this->task;

        if($task && $task="update"){
            return [
                'name' => 'required',
                'character' => 'required',
            ];
        }else{
            return [
                'name' => 'required',
                'number' => 'required',
                'character' => 'required',
            ];
        }
    }

    public function messages()
    {
        return [
            'name.required' => 'Tên không được để trống',
            'number.required' => 'Số Lượng mã không được để trống',
            'character.required' => 'Kí Tự Đầu Tiên không được để trống',
        ];
    }
}
