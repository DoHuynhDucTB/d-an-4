<?php

namespace App\Http\Controllers\Backend;

use App\Helpers\ImageHelper;
use App\Helpers\UrlHelper;
use App\Http\Requests\ProductCategoryRequest;
use App\Models\Banner;
use App\Models\ProductCategory;
use App\Models\Image;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;

class ProductcategoryController extends BaseController
{
    private $view = 'productcategory';
    private $model = 'productcategory';
    private $imageModel;

    /**
     * @var Banner
     */
    private Banner $bannerModel;

    /**
     * @var ProductCategory
     */
    private ProductCategory $productCategoryModel;

    public function __construct(
        ProductCategory $productCategoryModel,
        Banner $bannerModel,
        Image $imageModel)
    {
        $this->productCategoryModel = $productCategoryModel;
        $this->imageModel = $imageModel;
        $this->bannerModel = $bannerModel;
    }

    /**
     * @return Application|Factory|View
     */
    public function index()
    {
        return \view($this->viewPath . $this->view . '.index');
    }

    /**
     * Lấy danh sach theo $type
     * @return Application|Factory|View
     */
    public function type()
    {
        $type = \request()->get('type', 'accessories');
        $type = array_key_exists($type, $this->productCategoryModel::TYPE) ? $type : 'accessories';

        $data['type'] = $type;
        $data['productCategories'] = $this->productCategoryModel->findByType($type);
        $data['title'] = $this->productCategoryModel::TYPE_NAME[$type];

        return \view($this->viewPath . $this->view . '.type', compact('data'));
    }


    /**
     * @return Application|Factory|View|RedirectResponse
     */
    public function detail()
    {
        $pathAvatar = config('my.path.image_product_cate_avatar_of_module');
        $valueAvatar = config('my.image.value.product_cate.avatar');
        $pathSave = $this->model.'_m/avatar';

        $id     = \request()->get('id', null);
        $type   = request()->get('type');
        $type   = array_key_exists($type, $this->productCategoryModel::TYPE) ? $type : 'accessories';
        $title  = $this->productCategoryModel::TYPE_NAME[$type];

        $data['title'] = "$title: [Thêm]";
        if ($id != null) {
            $data['title'] = "$title: [Sửa]";
            $productCategory = $this->productCategoryModel::parentQuery()->whereNotIn('pcat_id', $this->productCategoryModel::NODE_ROOT)->find($id);
            $data['productCategory'] = $productCategory;
            if (!$productCategory){
                return redirect()
                    ->to(UrlHelper::admin('productcategory', 'index'))
                    ->with('error', "$title không tìm thấy");
            }
        }

        $imageAvatar  = $this->imageModel->query()->where(['3rd_key' => $id, '3rd_type' => $this->model, 'image_value' => $valueAvatar])->first();
        if($imageAvatar) {
            $data['urlAvatar'] = $pathAvatar . $imageAvatar->image_name;
        }

        $data['banners'] = $this->bannerModel::query()->select(['banner_id', 'banner_name'])->get();

        $data['productCategories'] = $this->productCategoryModel->findByType($type);
        $data['type'] = $type;
        return view($this->viewPath . $this->view . '.detail' , compact('data'));
    }


    /**
     * @param ProductCategoryRequest $request
     * @return RedirectResponse
     * @throws \Exception
     */
    public function save(ProductCategoryRequest $request)
    {
        $id     = $request->post('id', null);
        $status = $request->post('pcat_status');
        $name   = $request->post('pcat_name', '');
        $nameEn = $request->post('pcat_name_en', '');
        $colorHex = $request->post('pcat_color_hex', '');
        $bannerId = $request->post('banner_id', '');
        $showShopPage = $request->post('is_show_shoppage', '');
        $parent = $request->post('parent', 1);
        $description   = $request->post('pcat_description', '');
        $descriptionEn = $request->post('pcat_description_en', '');

        $pathAvatar = config('my.path.image_product_cate_avatar_of_module');
        $valueAvatar = config('my.image.value.product_cate.avatar');
        $pathSaveAvatar = $this->model.'_m/avatar';

        $type       = $request->post('type', 'accessories');
        //$groupValue = ($type== 'shoes') ? 'Giày' : 'Mỹ phẩm';

        $data = [
            'parent'       => $parent,
            'pcat_name'    => strip_tags($name),
            'pcat_name_en' => strip_tags($nameEn),
            'pcat_status'  => ($status == 'activated') ? 'activated' : 'inactive',
            'pcat_color_hex'=> $colorHex,
            'banner_id'=> intval($bannerId),
            'is_show_shoppage'=> $showShopPage,
            'pcat_description'=> $description,
            'pcat_description_en'=> $descriptionEn,
        ];

        $message = "Tạo mới nhóm thành công.";
        if ($id > 0){ // Update
            $message = "Cập nhật nhóm thành công.";
            $productCategory = $this->productCategoryModel::parentQuery()->whereNotIn('pcat_id', $this->productCategoryModel::NODE_ROOT)->find($id);

            if (!$productCategory) {
                return redirect()
                    ->to(UrlHelper::admin('productcategory', 'index'))
                    ->with('error', "Nhóm không tìm thấy") ;
            } else {
                if($request->imageAvatar != null) {
                    $imageAvatar = $request->imageAvatar;
                    /**check image exist*/
                    $image = Image::query()->where(['3rd_key' => $id, '3rd_type' => $this->model, 'image_value' => $valueAvatar])->first();
                    if ($image) {
                        ImageHelper::uploadUpdateImage($imageAvatar, $valueAvatar, $image->image_id, $pathSaveAvatar);
                    } else {
                        ImageHelper::uploadImage($imageAvatar, $this->model, $id, $valueAvatar, $pathSaveAvatar);
                    }
                }
                $this->productCategoryModel->updateNode($data, $id);
            }
        }else{ // Create new
            if ($this->productCategoryModel->createNode($data)) {
                $lastProductCategory = $this->productCategoryModel::parentQuery()->orderBy('pcat_id', 'desc')->first();
                $id = $lastProductCategory->pcat_id;

                if($request->imageAvatar != null){
                    $imageAvatar = $request->imageAvatar;
                    ImageHelper::uploadImage($imageAvatar, $this->model, $id, $valueAvatar, $pathSaveAvatar);
                }
            }else{
                return redirect()
                    ->to(UrlHelper::admin('productcategory', 'index'))
                    ->with('error', "Nhóm không tìm thấy") ;
            }
        }

        if ($request->post('action_type') == 'save') {
            return redirect()->to(UrlHelper::admin('productcategory', 'type', ['type' => $type]))->with('success', $message);
        } else {
            return redirect()->to(UrlHelper::admin('productcategory', 'detail', ['id' => $id, 'type' => $type]))->with('success', $message) ;
        }
    }


    /**
     * @return RedirectResponse
     * @throws \Exception
     */
    public function delete(): RedirectResponse
    {
        $type            = request()->get('type', 'cosmetic');
        $id              = request()->get('id');
        $productCategory = $this->productCategoryModel::parentQuery()
            ->whereNotIn('pcat_id', $this->productCategoryModel::NODE_ROOT)
            ->find($id);
        //$groupValue      = ($type == 'shoes') ? 'Giày' : 'Mỹ phẩm';
        if (!$productCategory) {
            return redirect()
                ->to(UrlHelper::admin('productcategory', 'type', ['type' => $type]))
                ->with('error', "Nhóm không tìm thấy") ;
        }

        $partnerId = $this->productCategoryModel::TYPE[$type];
        $this->productCategoryModel->deleteNode(['pcat_id' => $id], $partnerId);
        return redirect()
            ->to(UrlHelper::admin('productcategory', 'type', ['type' => $type]))
            ->with('success', "Xóa nhóm thành công") ;
    }

}
