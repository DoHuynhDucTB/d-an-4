<?php

namespace App\Http\Controllers\Backend;

use App\Exports\ProductInfoExport;
use App\Models\Product;
use App\Models\ProductInfo;
use App\Models\ProductColor;
use App\Models\ProductSize;;
use App\Helpers\UrlHelper;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class ProductInfoController extends BaseController
{
    private $view = '.productinfo';
    private $model = 'productinfo';
    private $productModel;
    private $productInfoModel;
    private $productSizeModel;
    private $productColorModel;
    public function __construct()
    {
        $this->productModel = new Product();
        $this->productInfoModel = new ProductInfo();
        $this->productSizeModel = new ProductSize();
        $this->productColorModel = new ProductColor();
    }

    /**
     * @return Application|Factory|View
     */
    public function index()
    {
        $data['title'] = 'Quản Lý Danh Sách Email Khi Có Hàng';
        $data['view']  = $this->viewPath . $this->view . '.list';

        $infos = $this->productInfoModel::query()->orderBy('id', 'DESC')->paginate(50);

        if($infos->count() > 0){
            foreach ($infos as $key => $item){

                $color = $this->productColorModel->query()->where('pcolor_id', $item->color_id)->first();
                $size = $this->productSizeModel->query()->where('psize_id', $item->size_id)->first();

                $item->color_name = !empty($color->pcolor_code) ? $color->pcolor_code : '';
                $item->size_name = !empty($size->psize_value) ? $size->psize_value : '';
            }
        }

        $colors = $this->productColorModel->query()->get();
        $sizes = $this->productSizeModel->query()->get();


        $data['infos'] = $infos;
        $data['colors'] = $colors;
        $data['sizes'] = $sizes;
        return view($data['view'] , compact('data'));
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete(Request $request)
    {
        $ids = request()->post('cid', []);
        $success = 'Xóa email nhận hàng thành công.';
        $error   = 'Xóa email nhận hàng thất bại.';

        $redirect = UrlHelper::admin($this->model, 'index');
        $number = $this->productInfoModel->query()->whereIn('id', $ids)->update(['is_delete' => 'yes']);
        if($number > 0) {
            return redirect()->to($redirect)->with('success', $success);
        }else{
            return redirect()->to($redirect)->with('error', $error);
        }
    }

    public function sort(Request $request){
        $search = $request->search;
        $group = $request->group;
        $color = $request->color;
        $sex = $request->sex;
        $size = $request->size;

        $html = '';
        $sql = $this->productInfoModel->query();
        if($search != null){
            $sql->where('email', 'LIKE', "%{$search}%")
                ->orWhere('product_general_code', 'LIKE', "%{$search}%")
                ->orWhere('product_general_name', 'LIKE', "%{$search}%")
                ->orWhere('product_variant_code ', 'LIKE', "%{$search}%")
                ->orWhere('product_variant_name ', 'LIKE', "%{$search}%");
        }
        if($group != null){
            $sql->where('product_group' , $group);
        }
        if($color != null){
            $sql->where('color_id' , $color);
        }
        if($sex != null){
            $sql->where('sex' , $sex);
        }
        if($size != null){
            $sql->where('size_id' , $size);
        }

        $data = $sql->orderBy('id', 'DESC')->paginate(50);

        if(!empty($data) && $data->count() > 0){
            foreach ($data as $key => $item){

                $color = $this->productColorModel->query()->where('pcolor_id', $item->color_id)->first();
                $size = $this->productSizeModel->query()->where('psize_id', $item->size_id)->first();

                $item->color_name = !empty($color->pcolor_code) ? $color->pcolor_code : '';
                $item->size_name = !empty($size->psize_value) ? $size->psize_value : '';

                $key = $key + 1;
                $html .= '
                    <tr>
                        <th><input type="checkbox" class="checkItem" name="cid[]" value="' .$item->id. '"></th>
                        <td>' .$key. '</td>
                        <td>' .$item->email. '</td>
                        <td>' .$item->product_general_code. '</td>
                        <td>' .$item->product_general_name. '</td>
                        <td>' .$item->product_variant_code. '</td>
                        <td>' .$item->product_variant_name. '</td>
                        <td>' .$item->color_name. '</td>
                        <td>' .$item->sex. '</td>
                        <td>' .$item->size_name. '</td>
                        <td>' .$item->created_at. '</td>
                        <td>' .$item->id. '</td>
                    </tr>
                ';
            }
        }

        $response = [
            'html' => $html,
        ];
        return json_encode($response);
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function exportExcel (Request $request)
    {
        $search = $request->search;
        $group = $request->group;
        $color = $request->color;
        $sex = $request->sex;
        $size = $request->size;

        return Excel::download(new ProductInfoExport($search, $group, $color, $sex, $size), 'emails.xlsx');
    }
}
