<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\PostGroup;
use App\Models\Image;
use App\Models\AdminUser;
use App\Helpers\ImageHelper;
use App\Helpers\UrlHelper;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use App\Http\Requests\PostRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PostgroupController extends BaseController
{
    private $view = '.postgroup';
    private $model = 'postgroup';
    private $postGroupModel;
    private $imageModel;
    private $adminUserModel;
    public function __construct()
    {
        $this->postGroupModel = new PostGroup();
        $this->imageModel = new Image();
        $this->adminUserModel = new AdminUser();
    }

    /**
     * @return Application|Factory|View
     */
    public function index()
    {
        $data['title'] = 'Quản lý nhóm bài viết ';
        $data['view']  = $this->viewPath . $this->view . '.list';

        $pathAvatar = config('my.path.image_postgroup_of_module');
        $valueAvatar = config('my.image.value.postgroup.avatar');

        $groups = $this->postGroupModel::query()->get();
        if($groups->count() > 0){
            foreach ($groups as $key => $item){
                $group  = $this->postGroupModel->query()->where('postgroup_id', $item->postgroup_parent)->first();
                $item->urlAvatar = !empty($item->avatar) ? $pathAvatar . $item->avatar->image_name : '';
                $item->parent = !empty($group) ? $group->postgroup_name : '';
            }
        }
        $data['groups'] = $groups;
        return view($data['view'] , compact('data'));
    }

    /**
     * @return Application|Factory|View
     */
    public function create()
    {
        $data['title'] = 'Quản lý nhóm bài viết: [Thêm]';
        $data['view']  = $this->viewPath . $this->view . '.add';
        $data['groups'] = $this->postGroupModel::query()->get();
        return view($data['view'] , compact('data'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(PostRequest $request)
    {
        $actionType = request()->post('action_type', 'save');
        if($actionType == 'save'){
            $redirect = UrlHelper::admin($this->model);
        }else{
            $redirect = UrlHelper::admin($this->model,'create');
        }

        $success = 'Đã thêm mới nhóm bài viết thành công.';
        $error   = 'Thêm mới nhóm bài viết thất bại.';
        $pathAvatar = config('my.path.image_postgroup_of_module');
        $valueAvatar = config('my.image.value.postgroup.avatar');;
        $pathSave = $this->model.'_m';

        $params = $this->postGroupModel->revertAlias($request->all());

        try {
            $groupId = 0;
            $group = $this->postGroupModel::query()->create($params);
            if($group){
                $groupId = $group->postgroup_id;
            }

            if($request->imageAvatar != null){
                $imageAvatar = $request->imageAvatar;
                ImageHelper::uploadImage($imageAvatar, $this->model, $groupId, $valueAvatar, $pathSave);
            }

            return redirect()->to($redirect)->with('success', $success);
        } catch ( \Exception $e ) {
            $redirect = UrlHelper::admin($this->model, 'create');
            return redirect()->to($redirect)->with('error', $error);
        }
    }

    /**
     * @return Application|Factory|View|\Illuminate\Http\RedirectResponse
     */
    public function copy()
    {
        $ids = request()->post('cid', []);
        $id = isset($ids[0]) ? $ids[0] : 0;
        $pathAvatar = config('my.path.image_postgroup_of_module');
        $valueAvatar = config('my.image.value.postgroup.avatar');;

        $group = $this->postGroupModel::query()->where('postgroup_id', $id)->first();
        if($group){
            $data['title'] = 'Quản lý bài viết: [Sao Chép]';
            $data['view']  = $this->viewPath . $this->view . '.copy';
            $data['group'] = $group;
            $data['urlAvatar'] = '';
            $data['groups'] = $this->postGroupModel::query()->where('postgroup_id','!=', $id)->get();

            $imageAvatar  = $this->imageModel->query()->where(['3rd_key' => $id,'3rd_type' => $this->model, 'image_value' => $valueAvatar])->first();
            if($imageAvatar) {
                $data['urlAvatar'] = $pathAvatar.$imageAvatar->image_name;
                $data['avatarId'] = $imageAvatar->image_id;
            }

            return view($data['view'] , compact('data'));
        }else{
            $error   = 'Không tìm nhóm thấy bài viết';
            $redirect = UrlHelper::admin($this->model);
            return redirect()->to($redirect)->with('error', $error);
        }
    }


    /**
     * @param $id
     * @return Application|Factory|View|\Illuminate\Http\RedirectResponse
     */
    public function edit()
    {
        $pathAvatar = config('my.path.image_postgroup_of_module');
        $valueAvatar = config('my.image.value.postgroup.avatar');;

        $id = (int) request()->get('id', 0);
        $group = $this->postGroupModel::query()->where('postgroup_id', $id)->first();
        if($group){
            $data['title'] = 'Quản lý nhóm bài viết: [Sửa]';
            $data['view']  = $this->viewPath . $this->view . '.edit';
            $data['group'] = $group;
            $data['urlAvatar'] = '';
            $data['groups'] = $this->postGroupModel::query()->where('postgroup_id','!=', $id)->get();

            $imageAvatar  = $this->imageModel->query()->where(['3rd_key' => $id,'3rd_type' => $this->model, 'image_value' => $valueAvatar])->first();
            if($imageAvatar) {
                $data['urlAvatar'] = $pathAvatar . $imageAvatar->image_name;
            }

            return view($data['view'] , compact('data'));
        }else{
            $error   = 'Không tìm thấy nhóm bài viết';
            $redirect = UrlHelper::admin($this->model);
            return redirect()->to($redirect)->with('error', $error);
        }
    }

    /**
     * @param PostRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(PostRequest $request)
    {
        $actionType = request()->post('action_type', 'save');
        $id = request()->post('id', 0);

        if($actionType == 'save'){
            $redirect = UrlHelper::admin($this->model);
        }else{
            $redirect = UrlHelper::admin($this->model,'edit', ['id' => $id]);
        }

        $success = 'Cập nhật nhóm bài viết thành công.';
        $error   = 'Cập nhật nhóm bài viết thất bại.';
        $pathAvatar = config('my.path.image_postgroup_of_module');
        $valueAvatar = config('my.image.value.postgroup.avatar');;
        $pathSave = $this->model.'_m';

        $params = $this->postGroupModel->revertAlias(request()->post());

        try {
            $this->postGroupModel::query()->where('postgroup_id', $id)->update($params);

            if($request->imageAvatar != null){
                $imageAvatar = $request->imageAvatar;

                /**check image exist*/
                $image = Image::query()->where(['3rd_key' => $id, '3rd_type' => $this->model,  'image_value' => $valueAvatar])->first();
                if($image){
                    ImageHelper::uploadUpdateImage($imageAvatar, $valueAvatar, $image->image_id, $pathSave);
                }else{
                    ImageHelper::uploadImage($imageAvatar, $this->model, $id, $valueAvatar, $pathSave);
                }
            }

            return redirect()->to($redirect)->with('success', $success);
        } catch ( \Exception $e ) {
            $redirect = UrlHelper::admin($this->model);
            return redirect()->to($redirect)->with('error', $error);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function duplicate(PostRequest $request)
    {
        $user = Auth::guard('admin')->user();
        $actionType = request()->post('action_type', 'save');
        $id = request()->post('id', 0);

        if($actionType == 'save'){
            $redirect = UrlHelper::admin($this->model);
        }else{
            $redirect = UrlHelper::admin($this->model,'edit', ['id' => $id]);
        }

        $success = 'Sao chép nhóm bài viết thành công.';
        $error   = 'Sao chép nhóm bài viết thất bại.';
        $pathAvatar = config('my.path.image_postgroup_of_module');
        $valueAvatar = config('my.image.value.postgroup.avatar');;
        $pathSave = $this->model.'_m';

        $params = $this->postGroupModel->revertAlias($request->all());
        unset($params['postgroup_id']);

        try {
            $groupId = 0;
            $group = $this->postGroupModel::query()->create($params);
            if($group){
                $groupId = $group->postgroup_id;
            }

            if($request->imageAvatar != null){
                $imageAvatar = $request->imageAvatar;
                ImageHelper::uploadImage($imageAvatar, $this->model, $groupId, $valueAvatar, $pathSave);
            }else{
                /**duplicate image avatar*/
                $avatarId = (int)$request->avatarId;
                $imageAvatar = $this->imageModel->getDataDuplicate($avatarId);
                if($imageAvatar){
                    $imageAvatar = $imageAvatar->toArray();
                    $imageAvatar['3rd_key'] = $groupId;
                    $imageAvatar['3rd_type'] = $this->model;
                    $this->imageModel->query()->create($imageAvatar);
                }
            }
            return redirect()->to($redirect)->with('success', $success);
        } catch ( \Exception $e ) {
            $redirect = UrlHelper::admin($this->model);
            return redirect()->to($redirect)->with('error', $error);
        }
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete(Request $request)
    {
        $ids = request()->post('cid', []);
        $success = 'Xóa nhóm bài viết thành công.';
        $error   = 'Xóa nhóm bài viết thất bại.';

        $redirect = UrlHelper::admin($this->model);
        $number = $this->postGroupModel->query()->whereIn('postgroup_id', $ids)->update(['postgroup_is_delete' => 'yes']);
        if($number > 0) {
            return redirect()->to($redirect)->with('success', $success);
        }else{
            return redirect()->to($redirect)->with('error', $error);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function active(Request $request)
    {
        $ids = request()->post('cid', []);
        $success = 'Bật nhóm bài viết thành công.';
        $error   = 'Bật nhóm bài viết thất bại.';

        $redirect = UrlHelper::admin($this->model);
        $number = $this->postGroupModel->query()->whereIn('postgroup_id', $ids)->update(['postgroup_status' => 'activated']);
        if($number > 0) {
            return redirect()->to($redirect)->with('success', $success);
        }else{
            return redirect()->to($redirect)->with('error', $error);
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function inactive(Request $request)
    {
        $ids = request()->post('cid', []);
        $success = 'Tắt nhóm bài viết thành công.';
        $error   = 'Tắt nhóm bài viết thất bại.';

        $redirect = UrlHelper::admin($this->model);
        $number = $this->postGroupModel->query()->whereIn('postgroup_id', $ids)->update(['postgroup_status' => 'inactive']);
        if($number > 0) {
            return redirect()->to($redirect)->with('success', $success);
        }else{
            return redirect()->to($redirect)->with('error', $error);
        }
    }
}
