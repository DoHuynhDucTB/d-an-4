<?php

namespace App\Http\Controllers\Backend;

use App\Helpers\ArrayHelper;
use App\Models\Product;
use App\Models\Image;
use App\Models\AdminUser;
use App\Models\Producer;
use App\Models\ProductCategory;
use App\Models\ProductColor;
use App\Models\ProductSize;
use App\Helpers\ImageHelper;
use App\Helpers\UrlHelper;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use App\Http\Requests\ProductRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProductController extends BaseController
{
    private $view = '.product';
    private $model = 'product';
    private $array_type = [
        'shoes' => 'Sản phẩm Giày',
        'sandals' => 'Sản phẩm Dép',
        'raincoat' => 'Sản phẩm Áo Mưa',
        'accessories' => 'Sản phẩm Phụ Kiện',
    ];
    private $productModel;
    private $imageModel;
    private $adminUserModel;
    private $producerModel;
    private $productSizeModel;
    private $productColorModel;
    private $productCategoryModel;
    public function __construct()
    {
        $this->productModel = new Product();
        $this->producerModel = new Producer();
        $this->imageModel = new Image();
        $this->productSizeModel = new ProductSize();
        $this->productColorModel = new ProductColor();
        $this->adminUserModel = new AdminUser();
        $this->productCategoryModel = new ProductCategory();
    }

    /**
     * Lấy danh sach theo $type
     * @return Application|Factory|View
     */
    public function type()
    {
        return \view($this->viewPath . $this->view . '.type');
    }

    /**
     * @return Application|Factory|View
     */
    public function index()
    {
        $type = \request()->get('type', 'shoes');
        $type = in_array($type, ['shoes', 'sandals', 'raincoat', 'accessories']) ? $type : 'shoes';
        $array_type = $this->array_type;
        $data['title'] = 'Quản Lý ' . $array_type[$type];
        $data['view']  = $this->viewPath . $this->view . '.list';
        $data['type']  = $type;

        $pathAvatar = config('my.path.image_product_avatar_of_module');
        $valueAvatar = config('my.image.value.product.avatar');

        $products = $this->productModel::query()->where('product_type', $type)->get();
        $producers = $this->producerModel::query()->get();
        $data['producers'] = array_combine(array_column($producers->toArray(), 'producer_id'), array_column($producers->toArray(), 'producer_name'));

        if($products->count() > 0){
            foreach ($products as $key => $item){
                $producer = $this->producerModel->query()->where('producer_id', $item->producer)->first();
                $item->producerName = !empty($producer->producer_name) ? $producer->producer_name : '';
                $item->urlAvatar = !empty($item->avatar) ? $pathAvatar . $item->avatar->image_name : '';
                $item->groupName = !empty($item->pcat) ? $item->pcat->pcat_name : '';
            }
        }
        $data['products'] = $products;
        return view($data['view'] , compact('data'));
    }

    /**
     * @return Application|Factory|View
     */
    public function create()
    {
        $type = \request()->get('type');
        $type = in_array($type, ['shoes', 'sandals', 'raincoat', 'accessories']) ? $type : 'shoes';
        $array_type = $this->array_type;
        $data['type'] = $type;
        $data['title'] = 'Quản Lý ' . $array_type[$type] . ' [Thêm]';
        $user = Auth::guard('admin')->user();
        $data['view']  = $this->viewPath . $this->view . '.add';
        $data['adminName']  = $user->username;
        $data['producers'] = $this->producerModel::query()
                                ->select('producer_id', 'producer_name')
                                ->get()->toArray();
        $data['products'] = $this->productModel::query()->where('product_type', $type)->get();
        $data['colors'] = $this->productColorModel::query()->get();
        $data['sizes'] = $this->productSizeModel::query()->get();
        $data['productCategories'] = $this->productCategoryModel->findByType(\request()->get('type', 'shoes'));
        return view($data['view'] , compact('data'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(ProductRequest $request)
    {
        $type = !empty($request->type) ? $request->type : 'shoes';
        $actionType = request()->post('action_type', 'save');
        if($actionType == 'save'){
            $redirect = UrlHelper::admin($this->model, 'index', ['type' => $type]);
        }else{
            $redirect = UrlHelper::admin($this->model,'create', ['type' => $type]);
        }

        $success = 'Đã thêm mới sản phẩm thành công.';
        $error   = 'Thêm mới sản phẩm thất bại.';

        $pathAvatar = config('my.path.image_product_avatar_of_module');
        $valueAvatar = config('my.image.value.product.avatar');
        $pathSaveAvatar = $this->model.'_m/avatar';

        $pathThumbnail = config('my.path.image_product_thumbnail_of_module');
        $valueThumbnail = config('my.image.value.product.thumbnail');
        $pathSaveThumbnail = $this->model.'_m/thumbnail';

        $pathBanner = config('my.path.image_product_banner_of_module');
        $valueBanner = config('my.image.value.product.banner');
        $pathSaveBanner = $this->model.'_m/banner';

        $user = Auth::guard('admin')->user();
        $params = $this->productModel->revertAlias($request->all());
        $params['product_created_by'] = $user->aduser_id;

        if($request->related){
            $params['product_related'] = array_diff(array_map('intval', $params['product_related']), [0]);
            $params['product_related'] = $this->producerModel->getIds($params['product_related']);
        }

        try {
            $productId = 0;
            $product = $this->productModel::query()->create($params);
            if($product){
                $productId = $product->product_id;
            }

            if($request->imageAvatar != null){
                $imageAvatar = $request->imageAvatar;
                ImageHelper::uploadImage($imageAvatar, $this->model, $productId, $valueAvatar, $pathSaveAvatar);
            }
            if($request->imageThumbnail != null && count($request->imageThumbnail) > 0){
                $imageThumbnail = $request->imageThumbnail;
                ImageHelper::uploadMultipleImage($imageThumbnail, $this->model, $productId, $valueThumbnail, $pathSaveThumbnail);
            }
            if($request->imageBanner != null && count($request->imageBanner) > 0){
                $imageBanner = $request->imageBanner;
                ImageHelper::uploadMultipleImage($imageBanner, $this->model, $productId, $valueBanner, $pathSaveBanner);
            }

            return redirect()->to($redirect)->with('success', $success);
        } catch ( \Exception $e ) {
            $redirect = UrlHelper::admin($this->model, 'create', ['type' => $type]);
            return redirect()->to($redirect)->with('error', $error);
        }
    }


    /**
     * @param $id
     * @return Application|Factory|View|\Illuminate\Http\RedirectResponse
     */
    public function edit()
    {
        $id = (int) request()->get('id', 0);

        $pathAvatar = config('my.path.image_product_avatar_of_module');
        $valueAvatar = config('my.image.value.product.avatar');

        $pathThumbnail = config('my.path.image_product_thumbnail_of_module');
        $valueThumbnail = config('my.image.value.product.thumbnail');

        $pathBanner = config('my.path.image_product_banner_of_module');
        $valueBanner = config('my.image.value.product.banner');

        $product = $this->productModel::query()->where('product_id', $id)->first();


        $type = \request()->get('type', 'shoes');
        $type = in_array($type, ['shoes', 'sandals', 'raincoat', 'accessories']) ? $type : 'shoes';
        if($product){
            $creater  = $this->adminUserModel::query()->where('aduser_id', $product->product_created_by)->first();

            $imageAvatar  = $this->imageModel->query()->where(['3rd_key' => $id, '3rd_type' => $this->model, 'image_value' => $valueAvatar])->first();
            $imageThumbnail = $this->imageModel->query()->where(['3rd_key' => $id, '3rd_type' => $this->model, 'image_value' => $valueThumbnail])->orderBy('image_id', 'ASC')->get();
            $imageBanner = $this->imageModel->query()->where(['3rd_key' => $id, '3rd_type' => $this->model, 'image_value' => $valueBanner])->orderBy('image_id', 'ASC')->get();


            $array_type = $this->array_type;
            $data['type'] = $type;
            $data['title'] = 'Quản Lý ' . $array_type[$type] . ' [Sửa]';
            $data['view']  = $this->viewPath . $this->view . '.edit';
            $data['product'] = $product;
            $data['urlAvatar'] = '';

            $data['products'] = $this->productModel::query()->where([['product_id', '!=', $id]])->get();
            $data['colors'] = $this->productColorModel::query()->get();

            $data['arrayRelated'] = !empty($product->product_related) ? array_diff(array_map('intval', explode(',', $product->product_related)), [0]) : [];
            $data['adminName'] = !empty($creater) ? $creater->username : '';
            $data['producers'] = $this->producerModel::query()
                                    ->select('producer_id', 'producer_name')
                                    ->get()->toArray();
            $data['thumbnailIds'] = array_column($imageThumbnail->toArray(), 'image_id');
            $data['bannerIds'] = array_column($imageBanner->toArray(), 'image_id');
            $data['colorImageIds'] = count(array_column($imageThumbnail->toArray(), 'rd_type_2')) >= count(array_column($imageBanner->toArray(), 'rd_type_2')) ? array_column($imageThumbnail->toArray(), 'rd_type_2') : array_column($imageBanner->toArray(), 'rd_type_2');
            $data['productCategories'] = $this->productCategoryModel->findByType(\request()->get('type', 'shoes'));

            if($imageAvatar) {
                $data['urlAvatar'] = $pathAvatar . $imageAvatar->image_name;
            }
            if($imageThumbnail->count() > 0) {
                foreach ($imageThumbnail as $key => $itemT){
                    $data['thumbnail'][] = [
                        'link'  => $pathThumbnail . $itemT->image_name,
                        'id'    => $itemT->image_id,
                        'color' => $itemT->rd_type_2,
                    ];
                }
            }

            if($imageBanner->count() > 0) {
                foreach ($imageBanner as $key => $itemB){
                    $data['banner'][] = [
                        'link'  => $pathBanner . $itemB->image_name,
                        'id'    => $itemB->image_id,
                        'color' => $itemB->rd_type_2,
                    ];
                }
            }

            return view($data['view'] , compact('data'));
        }else{
            $error   = 'Không tìm thấy sản phẩm';
            $redirect = UrlHelper::admin($this->model, 'index',  ['type' => $type]);
            return redirect()->to($redirect)->with('error', $error);
        }
    }

    /**
     * @param ProductRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(ProductRequest $request)
    {
        $actionType = request()->post('action_type', 'save');
        $id = request()->post('id', 0);
        $type = !empty($request->type) ? $request->type : 'shoes';

        if($actionType == 'save'){
            $redirect = UrlHelper::admin($this->model, 'index', ['type' => $type]);
        }else{
            $redirect = UrlHelper::admin($this->model,'edit', ['id' => $id, 'type' => $type]);
        }

        $success = 'Cập nhật sản phẩm thành công.';
        $error   = 'Cập nhật sản phẩm thất bại.';

        $pathAvatar = config('my.path.image_product_avatar_of_module');
        $valueAvatar = config('my.image.value.product.avatar');
        $pathSaveAvatar = $this->model.'_m/avatar';

        $pathThumbnail = config('my.path.image_product_thumbnail_of_module');
        $valueThumbnail = config('my.image.value.product.thumbnail');
        $pathSaveThumbnail = $this->model.'_m/thumbnail';

        $pathBanner = config('my.path.image_product_banner_of_module');
        $valueBanner = config('my.image.value.product.banner');
        $pathSaveBanner = $this->model.'_m/banner';


        $params = $this->productModel->revertAlias(request()->all());
        if($request->related){
            $params['product_related'] = array_diff(array_map('intval', $params['product_related']), [0]);
            $params['product_related'] = $this->producerModel->getIds($params['product_related']);
        }


        try {
            $product = $this->productModel::query()->where('product_id', $id)->first();
            $product->update($params);

            if($request->color != null){
                $color = $request->color;
                $product->colors()->sync($color);
            }
            if($request->size != null){
                $size = $request->size;
                $product->sizes()->sync($size);
            }

            if($request->imageAvatar != null){
                $imageAvatar = $request->imageAvatar;

                /**check image exist*/
                $image = Image::query()->where(['3rd_key' => $id, '3rd_type' => $this->model,  'image_value' => $valueAvatar])->first();
                if($image){
                    ImageHelper::uploadUpdateImage($imageAvatar, $valueAvatar, $image->image_id, $pathSaveAvatar);
                }else{
                    ImageHelper::uploadImage($imageAvatar, $this->model, $id, $valueAvatar, $pathSaveAvatar);
                }
            }

            /** cập nhật delete*/
            $thumbnailIds = json_decode($request->thumbnailIds);
            $bannerIds = json_decode($request->bannerIds);

            if($request->tIds != null && count($request->tIds) > 0){
                $tIds = $request->tIds;
                $arrayDelete = array_diff($thumbnailIds, $tIds);
                if(count($arrayDelete) > 0){
                    $this->imageModel->query()->whereIn('image_id', $arrayDelete)->update(['image_is_deleted' => 'yes']);
                }
            }

            if($request->bIds != null && count($request->bIds) > 0){
                $bIds = $request->bIds;
                $arrayDelete = array_diff($bannerIds, $bIds);
                if(count($arrayDelete) > 0){
                    $this->imageModel->query()->whereIn('image_id', $arrayDelete)->update(['image_is_deleted' => 'yes']);
                }
            }

            /**upload update*/
            if($request->dataThumbnailUpdate != null && count($request->dataThumbnailUpdate) > 0){
                $dataThumbnailUpdate = $request->dataThumbnailUpdate;
                foreach ($dataThumbnailUpdate as $key => $itemThumbnail){
                    ImageHelper::uploadUpdateImage($itemThumbnail, $valueThumbnail, $key, $pathSaveThumbnail);
                }
            }
            if($request->dataBannerUpdate != null && count($request->dataBannerUpdate) > 0){
                $dataBannerUpdate = $request->dataBannerUpdate;
                foreach ($dataBannerUpdate as $key => $itemBanner){
                    ImageHelper::uploadUpdateImage($itemBanner, $valueBanner, $key, $pathSaveBanner);
                }
            }

            /** upload create*/
            if($request->dataThumbnailCreate != null && count($request->dataThumbnailCreate) > 0){
                $dataThumbnailCreate = $request->dataThumbnailCreate;
                foreach ($dataThumbnailCreate as $key => $itemThumbnail){
                    ImageHelper::uploadImage($itemThumbnail, $this->model, $id, $valueThumbnail, $pathSaveThumbnail);

                }
            }
            if($request->dataBannerCreate != null && count($request->dataBannerCreate) > 0){
                $dataBannerCreate = $request->dataBannerCreate;
                foreach ($dataBannerCreate as $key => $itemBanner){
                    ImageHelper::uploadImage($itemBanner, $this->model, $id, $valueBanner, $pathSaveBanner);

                }
            }

            /** update color cho thumnail vả banner*/
            $dataColorUpdate = $request->dataColorUpdate ? $request->dataColorUpdate : [];
            if(count($dataColorUpdate) > 0){
                $this->imageModel->updateColor($dataColorUpdate, $this->model, $id);
            }

            if($request->imageThumbnail != null && count($request->imageThumbnail) > 0){
                $imageThumbnail = $request->imageThumbnail;
                ImageHelper::uploadMultipleImage($imageThumbnail, $this->model, $id, $valueThumbnail, $pathSaveThumbnail);
            }
            if($request->imageBanner != null && count($request->imageBanner) > 0) {
                $imageBanner = $request->imageBanner;
                ImageHelper::uploadMultipleImage($imageBanner, $this->model, $id, $valueBanner, $pathSaveBanner);
            }

            return redirect()->to($redirect)->with('success', $success);
        } catch ( \Exception $e ) {
            $redirect = UrlHelper::admin($this->model, 'index', ['type' => $type]);
            return redirect()->to($redirect)->with('error', $error);
        }
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete(Request $request)
    {
        $type = !empty($request->type) ? $request->type : 'shoes';
        $type = in_array($type, ['shoes', 'sandals', 'raincoat', 'accessories']) ? $type : 'shoes';
        $ids = request()->post('cid', []);
        $success = 'Xóa sản phẩm thành công.';
        $error   = 'Xóa sản phẩm thất bại.';

        $redirect = UrlHelper::admin($this->model, 'index', ['type' => $type]);
        $number = $this->productModel->query()->whereIn('product_id', $ids)->update(['product_is_delete' => 'yes']);
        if($number > 0) {
            return redirect()->to($redirect)->with('success', $success);
        }else{
            return redirect()->to($redirect)->with('error', $error);
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function active(Request $request)
    {
        $type = !empty($request->type) ? $request->type : 'shoes';
        $type = in_array($type, ['shoes', 'sandals', 'raincoat', 'accessories']) ? $type : 'shoes';

        $ids = request()->post('cid', []);
        $success = 'Bật sản phẩm thành công.';
        $error   = 'Bật sản phẩm thất bại.';

        $redirect = UrlHelper::admin($this->model, 'index', ['type' => $type]);
        $number = $this->productModel->query()->whereIn('product_id', $ids)->update(['product_status_show' => 'yes']);
        if($number > 0) {
            return redirect()->to($redirect)->with('success', $success);
        }else{
            return redirect()->to($redirect)->with('error', $error);
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function inactive(Request $request)
    {
        $type = !empty($request->type) ? $request->type : 'shoes';
        $type = in_array($type, ['shoes', 'sandals', 'raincoat', 'accessories']) ? $type : 'shoes';

        $ids = request()->post('cid', []);
        $success = 'Tắt sản phẩm thành công.';
        $error   = 'Tắt sản phẩm thất bại.';

        $redirect = UrlHelper::admin($this->model, 'index', ['type' => $type]);
        $number = $this->productModel->query()->whereIn('product_id', $ids)->update(['product_status_show' => 'no']);
        if($number > 0) {
            return redirect()->to($redirect)->with('success', $success);
        }else{
            return redirect()->to($redirect)->with('error', $error);
        }
    }
}
