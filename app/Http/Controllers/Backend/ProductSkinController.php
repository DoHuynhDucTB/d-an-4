<?php

namespace App\Http\Controllers\Backend;

use App\Http\Requests\ProductSkinRequest;
use App\Models\ProductSkin;
use App\Models\AdminUser;
use App\Helpers\UrlHelper;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;

class ProductSkinController extends BaseController
{
    private $view = '.productskin';
    private $model = 'productskin';
    private $productSkinModel;
    private $adminUserModel;
    public function __construct()
    {
        $this->productSkinModel = new ProductSkin();
        $this->adminUserModel = new AdminUser();
    }

    /**
     * @return Application|Factory|View
     */
    public function index()
    {
        $data['title'] = 'Quản lý loại da';
        $data['view']  = $this->viewPath . $this->view . '.list';

        $skins = $this->productSkinModel::query()->get();
        $data['skins'] = $skins;
        return view($data['view'] , compact('data'));
    }

    /**
     * @return Application|Factory|View
     */
    public function create()
    {
        $data['title'] = 'Quản lý loại da: [Thêm]';
        $data['view']  = $this->viewPath . $this->view . '.add';
        return view($data['view'] , compact('data'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(ProductSkinRequest $request)
    {
        $actionType = request()->post('action_type', 'save');
        if($actionType == 'save'){
            $redirect = UrlHelper::admin($this->model);
        }else{
            $redirect = UrlHelper::admin($this->model,'create');
        }

        $success = 'Đã thêm mới loại da thành công.';
        $error   = 'Thêm mới loại da thất bại.';
        $params = $this->productSkinModel->revertAlias($request->all());

        try {
            $this->productSkinModel::query()->create($params);
            return redirect()->to($redirect)->with('success', $success);
        } catch ( \Exception $e ) {
            $redirect = UrlHelper::admin($this->model, 'create');
            return redirect()->to($redirect)->with('error', $error);
        }
    }


    /**
     * @param $id
     * @return Application|Factory|View|\Illuminate\Http\RedirectResponse
     */
    public function edit()
    {
        $id = (int) request()->get('id', 0);
        $skin = $this->productSkinModel::query()->where('pskin_id', $id)->first();
        if($skin){
            $data['title'] = 'Quản lý bộ sưu tập: [Sửa]';
            $data['view']  = $this->viewPath . $this->view . '.edit';
            $data['skin'] = $skin;

            return view($data['view'] , compact('data'));
        }else{
            $error   = 'Không tìm thấy bộ sưu tập';
            $redirect = UrlHelper::admin($this->model);
            return redirect()->to($redirect)->with('error', $error);
        }
    }

    /**
     * @param ProductSkinRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(ProductSkinRequest $request)
    {
        $actionType = request()->post('action_type', 'save');
        $id = request()->post('id', 0);

        if($actionType == 'save'){
            $redirect = UrlHelper::admin($this->model);
        }else{
            $redirect = UrlHelper::admin($this->model,'edit', ['id' => $id]);
        }

        $success = 'Cập nhật loại da thành công.';
        $error   = 'Cập nhật loại da thất bại.';
        $params = $this->productSkinModel->revertAlias($request->all());

        try {
            $this->productSkinModel::query()->where('pskin_id', $id)->update($params);

            return redirect()->to($redirect)->with('success', $success);
        } catch ( \Exception $e ) {
            $redirect = UrlHelper::admin($this->model);
            return redirect()->to($redirect)->with('error', $error);
        }
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete(Request $request)
    {
        $ids = request()->post('cid', []);
        $success = 'Xóa bộ loại da thành công.';
        $error   = 'Xóa bộ loại da thất bại.';

        $redirect = UrlHelper::admin($this->model);
        $number = $this->productSkinModel->query()->whereIn('pskin_id', $ids)->update(['pskin_is_delete' => 'yes']);
        if($number > 0) {
            return redirect()->to($redirect)->with('success', $success);
        }else{
            return redirect()->to($redirect)->with('error', $error);
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function active(Request $request)
    {
        $ids = request()->post('cid', []);
        $success = 'Bật loại da thành công.';
        $error   = 'Bật loại da hất bại.';

        $redirect = UrlHelper::admin($this->model);
        $number = $this->productSkinModel->query()->whereIn('pskin_id', $ids)->update(['pskin_status' => 'activated']);
        if($number > 0) {
            return redirect()->to($redirect)->with('success', $success);
        }else{
            return redirect()->to($redirect)->with('error', $error);
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function inactive(Request $request)
    {
        $ids = request()->post('cid', []);
        $success = 'Tắt loại da thành công.';
        $error   = 'Tắt loại da thất bại.';

        $redirect = UrlHelper::admin($this->model);
        $number = $this->productSkinModel->query()->whereIn('pskin_id', $ids)->update(['pskin_status' => 'inactive']);
        if($number > 0) {
            return redirect()->to($redirect)->with('success', $success);
        }else{
            return redirect()->to($redirect)->with('error', $error);
        }
    }
}
