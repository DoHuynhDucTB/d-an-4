<?php
namespace App\Http\Controllers\Backend;

use App\Helpers\FileHelper;
use App\Helpers\UrlHelper;
use App\Http\Requests\BlockRequest;
use App\Models\Block;
use App\Models\BannerGroup;
use App\Models\BlockGroup;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class BlockController extends BaseController
{
    private $model = 'block';

    /**
     * @var Block
     */
    private Block $blockModel;
    private BannerGroup $bannergroupModel;
    private BlockGroup $blockgroupModel;

    /**
     * @var string
     */
    private $view = 'block';

    public function __construct(Block $blockModel, BannerGroup $bannergroupModel, BlockGroup $blockgroupModel)
    {
        $this->blockModel = $blockModel;
        $this->bannergroupModel = $bannergroupModel;
        $this->blockgroupModel = $blockgroupModel;
    }

    /**
     * @return Application|Factory|View
     */
    public function index()
    {
        $data['blocks'] = $this->blockModel::parentQuery()
            ->orderBy('block_sorted', 'asc')
            ->leftJoin(BLOCK_GROUP_TBL, 'blockgroup_id', 'block_group_id')
            ->get();
        $data['title'] = 'Quản lý Khối ở trang chủ';
        $data['view']  = $this->viewPath . $this->view . '.list';
        return view($data['view'] , compact('data'));
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function active(Request $request): RedirectResponse
    {
        $ids = $request->post('cid', []);
        $this->blockModel::parentQuery()->whereIn('block_id', $ids)->update(['block_status' => 'activated']);
        return back()->with('success', 'Bật khối thành công');
    }


    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function inactive(Request $request): RedirectResponse
    {
        $ids = $request->post('cid', []);
        $this->blockModel::parentQuery()->whereIn('block_id', $ids)->update(['block_status' => 'inactive']);
        return back()->with('success', 'Tắt khối thành công');
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function sort(Request $request): RedirectResponse
    {
        $ids     = request()->post('cid', []);
        $sorts   = request()->post('sort', []);
        $redirect = UrlHelper::admin('block', 'index');

        if (!is_array($ids) || count($ids) == 0) {
            return redirect()->to($redirect)->with('error', 'Vui lòng chọn giá trị để sắp xếp');
        }

        foreach ($ids as $key => $id) {
            $this->blockModel::parentQuery()->where('block_id', $id)->update(['block_sorted' => intval($sorts[$key])]);
        }
        return redirect()->to($redirect)->with('success', 'Sắp xếp thành công');
    }

    /**
     * @return Application|Factory|View
     */
    public function create()
    {
        $data['title'] = 'Quản lý khối[Thêm]';
        $data['view']  = $this->viewPath . $this->view . '.add';

        $bannerGroups = $this->bannergroupModel::query()->orderBy('bangroup_id', 'desc')->get();
        $blockGroups = $this->blockgroupModel::query()->orderBy('blockgroup_id', 'desc')->get();
        $data['bannerGroups'] = $bannerGroups;
        $data['blockGroups'] = $blockGroups;

        return view($data['view'] , compact('data'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(BlockRequest $request)
    {
        $actionType = request()->post('action_type', 'save');
        if($actionType == 'save'){
            $redirect = UrlHelper::admin($this->model);
        }else{
            $redirect = UrlHelper::admin($this->model,'create');
        }

        $success = 'Đã thêm mới khối thành công.';
        $error   = 'Thêm mới khối thất bại.';

        $params = $this->blockModel->revertAlias($request->all());

        try {
            $block = $this->blockModel::query()->create($params);

            return redirect()->to($redirect)->with('success', $success);
        } catch ( \Exception $e ) {
            $redirect = UrlHelper::admin($this->model, 'create');
            return redirect()->to($redirect)->with('error', $error);
        }
    }

    /**
     * @param $id
     * @return Application|Factory|View|\Illuminate\Http\RedirectResponse
     */
    public function edit()
    {
        $id = (int) request()->get('id', 0);
        $block = $this->blockModel::query()->where('block_id', $id)->first();
        if($block){
            $data['title'] = 'Quản lý khối[Sửa]';
            $data['view']  = $this->viewPath . $this->view . '.edit';
            $data['block'] = $block;

            $bannerGroups = $this->bannergroupModel::query()->orderBy('bangroup_id', 'desc')->get();
            $blockGroups = $this->blockgroupModel::query()->orderBy('blockgroup_id', 'desc')->get();
            $data['bannerGroups'] = $bannerGroups;
            $data['blockGroups'] = $blockGroups;

            return view($data['view'] , compact('data'));
        }else{
            $error   = 'Không tìm thấy khối';
            $redirect = UrlHelper::admin($this->model);
            return redirect()->to($redirect)->with('error', $error);
        }
    }

    /**
     * @param BlockRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(BlockRequest $request)
    {
        $actionType = request()->post('action_type', 'save');
        $id = request()->post('id', 0);

        if($actionType == 'save'){
            $redirect = UrlHelper::admin($this->model);
        }else{
            $redirect = UrlHelper::admin($this->model,'edit', ['id' => $id]);
        }

        $success = 'Cập nhật khối thành công.';
        $error   = 'Cập nhật khối thất bại.';
        $params = $this->blockModel->revertAlias(request()->post());

        try {
            $this->blockModel::query()->where('block_id', $id)->update($params);

            return redirect()->to($redirect)->with('success', $success);
        } catch ( \Exception $e ) {
            $redirect = UrlHelper::admin($this->model);
            return redirect()->to($redirect)->with('error', $error);
        }
    }

}
