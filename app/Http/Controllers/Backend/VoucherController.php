<?php

namespace App\Http\Controllers\Backend;

use App\Http\Requests\VoucherRequest;
use App\Models\Image;
use App\Models\AdminUser;
use App\Models\Voucher;
use App\Models\ProductCategory;
use App\Models\Product;
use App\Models\User;
use App\Models\Order;
use App\Helpers\ImageHelper;
use App\Helpers\UrlHelper;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class VoucherController extends BaseController
{
    private $view = '.voucher';
    private $model = 'voucher';
    private $voucherModel;
    private $productCategoryModel;
    private $productModel;
    private $imageModel;
    private $orderModel;
    private $adminUserModel;
    private $userModel;
    public function __construct()
    {
        $this->voucherModel = new Voucher();
        $this->productCategoryModel = new ProductCategory();
        $this->productModel = new Product();
        $this->imageModel = new Image();
        $this->adminUserModel = new AdminUser();
        $this->userModel = new User();
        $this->orderModel = new Order();
    }

    /**
     * @return Application|Factory|View
     */
    public function index()
    {
        $serie = \request()->get('serie');
        $data['title'] = 'Quản lý phiếu giảm giá';
        $data['view']  = $this->viewPath . $this->view . '.list';
        $pathAvatar = config('my.path.image_voucher_of_module');
        $valueAvatar = config('my.image.value.voucher.avatar');

        if($serie){
            $vouchers = $this->voucherModel::query()->where('serie_id', $serie)->get();
        }else{
            $vouchers = $this->voucherModel::query()->get();
        }

        if($vouchers->count() > 0){
            foreach ($vouchers as $key => $item){
                $item->urlAvatar = !empty($item->avatar) ? $pathAvatar . $item->avatar->image_name : '';
            }
        }
        $data['vouchers'] = $vouchers;
        return view($data['view'] , compact('data'));
    }

    /**
     * @return Application|Factory|View
     */
    public function create()
    {
        $user = Auth::guard('admin')->user();
        $data['title'] = 'Quản lý phiếu giảm giá[Thêm]';

        $data['view']  = $this->viewPath . $this->view . '.add';
        $data['users'] = $this->userModel::query()->orderBy('id', 'DESC')->get();
        return view($data['view'] , compact('data'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(VoucherRequest $request)
    {
        $actionType = request()->post('action_type', 'save');
        if($actionType == 'save'){
            $redirect = UrlHelper::admin($this->model);
        }else{
            $redirect = UrlHelper::admin($this->model,'create');
        }

        $success = 'Đã thêm mới phiếu giảm giá thành công.';
        $error   = 'Thêm mới phiếu giảm giá thất bại.';
        $pathAvatar = config('my.path.image_voucher_of_module');
        $valueAvatar = config('my.image.value.voucher.avatar');
        $pathSave = $this->model.'_m';

        $params = $this->voucherModel->revertAlias($request->all());
        $params['product_type'] = !empty($params['product_type']) ? json_encode($params['product_type']) : null;
        $params['product_category'] = !empty($params['product_category']) ? json_encode($params['product_category']) : null;
        $params['product_id'] = !empty($params['product_id']) ? json_encode($params['product_id']) : null;
        /**
         * lưu user được tặng voucher
         */
        $userIds = !empty($request->userIds) ? $request->userIds : [];
        $arrayUserIds = array();
        if(!empty($userIds) && count($userIds) > 0){
            foreach ($userIds as $key => $itemUser){
                $arrayUserIds[$itemUser] = [
                    'id' => $itemUser,
                    'time' => time(),
                ];
            }
            $params['user_ids'] = json_encode($arrayUserIds);
        }

        try {
            $voucherId = 0;
            $voucher = $this->voucherModel::query()->create($params);
            if($voucher){
                $voucherId = $voucher->voucher_id;
            }

            if($request->imageAvatar != null){
                $imageAvatar = $request->imageAvatar;
                ImageHelper::uploadImage($imageAvatar, $this->model, $voucherId, $valueAvatar, $pathSave);
            }

            return redirect()->to($redirect)->with('success', $success);
        } catch ( \Exception $e ) {
            $redirect = UrlHelper::admin($this->model, 'create');
            return redirect()->to($redirect)->with('error', $error);
        }
    }

    /**
     * @param $id
     * @return Application|Factory|View|\Illuminate\Http\RedirectResponse
     */
    public function edit()
    {
        $id = (int) request()->get('id', 0);
        $voucher = $this->voucherModel::query()->where('voucher_id', $id)->first();
        $pathAvatar = config('my.path.image_voucher_of_module');
        $valueAvatar = config('my.image.value.voucher.avatar');
        $pathSave = $this->model.'_m';

        if($voucher){
            $data['title'] = 'Quản lý phiếu giảm giá[Sửa]';
            $data['view']  = $this->viewPath . $this->view . '.edit';
            $data['voucher'] = $voucher;
            $data['urlAvatar'] = '';

            $productCategories = [];
            $typeArray = json_decode($voucher->product_type, 1);
            if(!empty($typeArray) && count($typeArray) > 0){
                foreach ($typeArray as $key => $item){
                    $arrayItem = $this->productCategoryModel->findByType($item)->toArray();
                    $productCategories = array_merge($productCategories, $arrayItem);
                }
            }
            $data['productCategories'] = $productCategories;
            $data['products'] = $this->productModel::query()->whereIn('product_group', array_column($productCategories, 'pcat_id'))->get();

            $data['userVouchers'] = $this->voucherModel::getUserVoucher(json_decode($voucher->user_ids, 1), $id);
            $data['orders'] = $this->orderModel::getOrderByVoucher($id);
            $data['users'] = $this->userModel::query()->orderBy('id', 'DESC')->get();
            $data['linkSelectUser'] = UrlHelper::admin($this->model, 'select-user', ['id' => $id]);

            $imageAvatar  = $this->imageModel->query()->where(['3rd_key' => $id, '3rd_type' => $this->model, 'image_value' => $valueAvatar])->first();
            if($imageAvatar) {
                $data['urlAvatar'] = $pathAvatar . $imageAvatar->image_name;
            }

            return view($data['view'] , compact('data'));
        }else{
            $error   = 'Không tìm thấy phiếu giảm giá';
            $redirect = UrlHelper::admin($this->model);
            return redirect()->to($redirect)->with('error', $error);
        }
    }

    /**
     * @param voucherRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(VoucherRequest $request)
    {
        $actionType = request()->post('action_type', 'save');
        $id = request()->post('id', 0);

        if($actionType == 'save'){
            $redirect = UrlHelper::admin($this->model);
        }else{
            $redirect = UrlHelper::admin($this->model,'edit', ['id' => $id]);
        }

        $success = 'Cập nhật phiếu giảm giá thành công.';
        $error   = 'Cập nhật phiếu giảm giá thất bại.';
        $pathAvatar = config('my.path.image_voucher_of_module');
        $valueAvatar = config('my.image.value.voucher.avatar');
        $pathSave = $this->model.'_m';

        $params = $this->voucherModel->revertAlias(request()->post());
        $params['product_type'] = !empty($params['product_type']) ? json_encode($params['product_type']) : null;
        $params['product_category'] = !empty($params['product_category']) ? json_encode($params['product_category']) : null;
        $params['product_id'] = !empty($params['product_id']) ? json_encode($params['product_id']) : null;

        $voucher = $this->voucherModel::query()->where('voucher_id', $id)->first();


        /**
         * lưu user được tặng voucher
         */
        $dataUserVoucher = json_decode($voucher->user_ids, 1);
        $UserIdsOld = !empty($dataUserVoucher && count($dataUserVoucher) > 0) ? array_column($dataUserVoucher, 'id') : [];
        $userIds = !empty($request->userIds) ? $request->userIds  : [];

        $removeIds = array_diff($UserIdsOld, $userIds);

        if(!empty($removeIds) && count($removeIds) > 0){
            foreach ($dataUserVoucher as $key => $itemVoucher){
                if(in_array($itemVoucher['id'], $removeIds)){
                    unset($dataUserVoucher[$key]);
                }
            }
        }

        $addIds = array_diff($userIds, $UserIdsOld);
        if(!empty($addIds) && count($addIds) > 0){
            foreach ($addIds as $key => $itemAddId){
                $dataUserVoucher[] = [
                    'id' => $itemAddId,
                    'time' => time(),
                ];
            }
        }
        if(!empty($dataUserVoucher) && count($dataUserVoucher) > 0){
            $params['user_ids'] = json_encode($dataUserVoucher);
        }


        try {
            $orders = $this->orderModel::getOrderByVoucher($id);
            if($orders->count() <= 0){
                $voucher->update($params);

                if($request->imageAvatar != null){
                    $imageAvatar = $request->imageAvatar;

                    /**check image exist*/
                    $image = Image::query()->where(['3rd_key' => $id, '3rd_type' => $this->model,  'image_value' => $valueAvatar])->first();
                    if($image){
                        ImageHelper::uploadUpdateImage($imageAvatar, $valueAvatar, $image->image_id, $pathSave);
                    }else{
                        ImageHelper::uploadImage($imageAvatar, $this->model, $id, $valueAvatar, $pathSave);
                    }
                }

                return redirect()->to($redirect)->with('success', $success);
            }else{
                $error = 'Phiếu giảm giá đã được sử dụng, không cho phép sử dụng';
                $redirect = UrlHelper::admin($this->model);
                return redirect()->to($redirect)->with('error', $error);
            }

        } catch ( \Exception $e ) {
            $redirect = UrlHelper::admin($this->model);
            return redirect()->to($redirect)->with('error', $error);
        }
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete(Request $request)
    {
        $ids = request()->post('cid', []);
        $success = 'Xóa phiếu giảm giá thành công.';
        $error   = 'Xóa phiếu giảm giá thất bại.';

        $redirect = UrlHelper::admin($this->model);
        $number = $this->voucherModel->query()->whereIn('voucher_id', $ids)->update(['voucher_is_delete' => 'yes']);
        if($number > 0) {
            return redirect()->to($redirect)->with('success', $success);
        }else{
            return redirect()->to($redirect)->with('error', $error);
        }
    }

    /**
     * @param Request $request
     * @return Application|Factory|View
     */
    public function selectUser(Request $request){
        $id = (int) $_GET['id'];
        $data['title'] = 'Quản lý phiếu giảm giá [Tặng voucher]';
        $data['view']  = $this->viewPath . $this->view . '.list_user';
        $redirect = UrlHelper::admin($this->model,'edit', ['id' => $id]);

        $voucher = $this->voucherModel::query()->where('voucher_id', $id)->first();
        if($voucher){
            if(isset($_GET['value'])){
                $value = $_GET['value'];
                $data['users'] = $this->userModel::query()->orderBy('id', 'DESC')
                    ->where('name', 'like' ,'%'.$value.'%')
                    ->orWhere('email', 'like','%'.$value.'%')
                    ->orWhere('phone', 'like','%'.$value.'%')
                    ->paginate(50);
            }else{
                $data['users'] = $this->userModel::query()->orderBy('id', 'DESC')->paginate(500);
            }

            $userIds  = !empty(!empty($voucher) && $voucher->user_ids) ? json_decode($voucher->user_ids, 1) : [];
            $data['userIds'] =  !empty($userIds && count($userIds) > 0) ? array_column($userIds, 'id') : [];
            $data['voucherId'] = $id;
            return view($data['view'] , compact('data'));
        }else{
            $error   = 'không tìm thấy phiếu giảm giá';
            return redirect()->to($redirect)->with('error', $error);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postSelectUser(Request $request){
        $userIds = request()->post('cid', []);
        $voucherId = (int) request()->post('voucherId', 0);

        $redirect = UrlHelper::admin($this->model, 'select-user', ['id' => $voucherId]);
        $success = 'Cập nhật thông tin tặng phiếu giảm giá thành công.';
        $error   = 'Cập nhật thông tin tặng phiếu giảm giá thất bại.';

        $voucher = $this->voucherModel::query()->where('voucher_id', $voucherId)->first();
        if($voucher){
            $dataUserVoucher = json_decode($voucher->user_ids, 1);
            $UserIdsOld = !empty($dataUserVoucher && count($dataUserVoucher) > 0) ? array_column($dataUserVoucher, 'id') : [];

            $removeIds = array_diff($UserIdsOld, $userIds);

            if(!empty($removeIds) && count($removeIds) > 0){
                foreach ($dataUserVoucher as $key => $itemVoucher){
                    if(in_array($itemVoucher['id'], $removeIds)){
                        unset($dataUserVoucher[$key]);
                    }
                }
            }

            $addIds = array_diff($userIds, $UserIdsOld);
            if(!empty($addIds) && count($addIds) > 0){
                foreach ($addIds as $key => $itemAddId){
                    $dataUserVoucher[] = [
                        'id' => $itemAddId,
                        'time' => time(),
                    ];
                }
            }
            if(!empty($dataUserVoucher) && count($dataUserVoucher) > 0){
                $userIds = json_encode($dataUserVoucher);
            }

            $result = $voucher->update(['user_ids' => $userIds]);
            if($result){
                return redirect()->to($redirect)->with('success', $success);
            }else{
                return redirect()->to($redirect)->with('error', $error);
            }
        }else{
            $error   = 'không tìm thấy phiếu giảm giá';
            return redirect()->to($redirect)->with('error', $error);
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function active(Request $request)
    {
        $ids = request()->post('cid', []);
        $success = 'Bật phiếu giảm giá thành công.';
        $error   = 'Bật phiếu giảm giá thất bại.';

        $redirect = UrlHelper::admin($this->model);
        $number = $this->voucherModel->query()->whereIn('voucher_id', $ids)->update(['voucher_status' => 'activated']);
        if($number > 0) {
            return redirect()->to($redirect)->with('success', $success);
        }else{
            return redirect()->to($redirect)->with('error', $error);
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function inactive(Request $request)
    {
        $ids = request()->post('cid', []);
        $success = 'Tắt phiếu giảm giá thành công.';
        $error   = 'Tắt phiếu giảm giá thất bại.';

        $redirect = UrlHelper::admin($this->model);
        $number = $this->voucherModel->query()->whereIn('voucher_id', $ids)->update(['voucher_status' => 'inactive']);
        if($number > 0) {
            return redirect()->to($redirect)->with('success', $success);
        }else{
            return redirect()->to($redirect)->with('error', $error);
        }
    }

    /**
     * @param Request $request
     * @return false|string
     */
    public function getProductCategory(Request $request)
    {
        $typeArray = $request->type;
        $data = [];
        if(!empty($typeArray) && count($typeArray) > 0){
            foreach ($typeArray as $key => $item){
                $arrayItem = $this->productCategoryModel->findByType($item)->toArray();
                $data = array_merge($data, $arrayItem);
            }
        }
        $html = '<option value="">'.__('Chọn danh mục sản phẩm').'</option>';
        if(!empty($data)){
            foreach ($data as $key => $item){
                $html .= '<option value="' . $item['pcat_id'] . '">'. $item['pcat_name'] . '</option>';
            }
        }

        $response = [
            'html' => $html,
        ];
        return json_encode($response);
    }

    /**
     * @param Request $request
     * @return false|string
     */
    public function getProduct(Request $request)
    {
        $arrayPcatId = $request->pcatId;
        $data =  $this->productModel::query()->whereIn('product_group', $arrayPcatId)->get();

        $html = '<option value="">'.__('Chọn sản phẩm').'</option>';
        if(!empty($data)){
            foreach ($data as $key => $item){
                $html .= '<option value="' . $item->product_id . '">'. $item->product_name . '</option>';
            }
        }

        $response = [
            'html' => $html,
        ];
        return json_encode($response);
    }
}
