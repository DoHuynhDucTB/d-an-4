<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Comment;
use App\Models\AdminUser;
use App\Models\User;
use App\Models\Product;
use App\Models\Post;
use App\Helpers\UrlHelper;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use App\Http\Requests\PostRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CommentController extends BaseController
{
    private $view = '.comment';
    private $model = 'comment';
    private $adminUserModel;
    private $userModel;
    private $commentModel;
    private $productModel;
    private $postModel;

    public function __construct()
    {
        $this->commentModel = new Comment();
        $this->adminUserModel = new AdminUser();
        $this->userModel = new User();
        $this->productModel = new Product();
        $this->postModel = new Post();
    }

    /**
     * @return Application|Factory|View
     */
    public function index()
    {
        $data['title'] = 'Quản lý comment';
        $data['view']  = $this->viewPath . $this->view . '.list';

        $comments = $this->commentModel::query()->orderBy('comment_id', 'DESC')->paginate(50);

        if($comments->count() > 0) {
            foreach ($comments as $key => $item) {
                $user = $this->userModel->query()->where('id', $item->user_id)->first();
                $item->userName = !empty($user->name) ? $user->name : '';

                $type = $item->comment_type;

                $name_3rd = '';
                if($type == 'product'){
                    $product = $this->productModel->query()->where('product_id', $item->comment_3rd_id)->first();
                    $name_3rd = !empty($product->product_name) ? $product->product_name : '';
                }

                if($type == 'post'){
                    $post = $this->postModel->query()->where('post_id', $item->comment_3rd_id)->first();
                    $name_3rd = !empty($post->post_name) ? $post->post_name : '';
                }

                $item->name_3rd = $name_3rd;
            }
        }
        $data['comments'] = $comments;
        return view($data['view'] , compact('data'));
    }

    /**
     * @param $id
     * @return Application|Factory|View|\Illuminate\Http\RedirectResponse
     */
    public function edit()
    {
        $id = (int) request()->get('id', 0);
        $comment = $this->commentModel::query()->where('comment_id', $id)->first();
        if($comment){
            $data['title'] = 'Quản lý comment[Sửa]';
            $data['view']  = $this->viewPath . $this->view . '.edit';
            $data['comment'] = $comment;

            return view($data['view'] , compact('data'));
        }else{
            $error   = 'Không tìm thấy comment';
            $redirect = UrlHelper::admin($this->model);
            return redirect()->to($redirect)->with('error', $error);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request)
    {
        $actionType = request()->post('action_type', 'save');
        $id = request()->post('id', 0);

        if($actionType == 'save'){
            $redirect = UrlHelper::admin($this->model);
        }else{
            $redirect = UrlHelper::admin($this->model,'edit', ['id' => $id]);
        }

        $success = 'Cập nhật commnent thành công.';
        $error   = 'Cập nhật comment thất bại.';
        $params = $this->commentModel->revertAlias(request()->post());

        try {
            $this->commentModel::query()->where('comment_id', $id)->update($params);
            return redirect()->to($redirect)->with('success', $success);
        } catch ( \Exception $e ) {
            $redirect = UrlHelper::admin($this->model);
            return redirect()->to($redirect)->with('error', $error);
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function active(Request $request)
    {
        $ids = request()->post('cid', []);
        $success = 'Bật comment thành công.';
        $error   = 'Bật comment thất bại.';

        $redirect = UrlHelper::admin($this->model);
        $number = $this->commentModel->query()->whereIn('comment_id', $ids)->update(['comment_status' => 'approved']);
        if($number > 0) {
            return redirect()->to($redirect)->with('success', $success);
        }else{
            return redirect()->to($redirect)->with('error', $error);
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function inactive(Request $request)
    {
        $ids = request()->post('cid', []);
        $success = 'Tắt comment thành công.';
        $error   = 'Tắt comment thất bại.';

        $redirect = UrlHelper::admin($this->model);
        $number = $this->commentModel->query()->whereIn('comment_id', $ids)->update(['comment_status' => 'unapproved']);
        if($number > 0) {
            return redirect()->to($redirect)->with('success', $success);
        }else{
            return redirect()->to($redirect)->with('error', $error);
        }
    }
}
