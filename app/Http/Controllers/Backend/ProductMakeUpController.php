<?php

namespace App\Http\Controllers\Backend;

use App\Http\Requests\ProductMakeUpRequest;
use App\Models\ProductMakeUp;
use App\Models\AdminUser;
use App\Helpers\UrlHelper;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;

class ProductMakeUpController extends BaseController
{
    private $view = '.productmakeup';
    private $model = 'productmakeup';
    private $productmakeupModel;
    private $adminUserModel;
    public function __construct()
    {
        $this->productmakeupModel = new ProductMakeUp();
        $this->adminUserModel = new AdminUser();
    }

    /**
     * @return Application|Factory|View
     */
    public function index()
    {
        $data['title'] = 'Quản lý phong cách trang điểm';
        $data['view']  = $this->viewPath . $this->view . '.list';

        $makeups = $this->productmakeupModel::query()->get();
        $data['makeups'] = $makeups;
        return view($data['view'] , compact('data'));
    }

    /**
     * @return Application|Factory|View
     */
    public function create()
    {
        $data['title'] = 'Quản lý phong cách trang điểm: [Thêm]';
        $data['view']  = $this->viewPath . $this->view . '.add';
        return view($data['view'] , compact('data'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(ProductMakeUpRequest $request)
    {
        $actionType = request()->post('action_type', 'save');
        if($actionType == 'save'){
            $redirect = UrlHelper::admin($this->model);
        }else{
            $redirect = UrlHelper::admin($this->model,'create');
        }

        $success = 'Đã thêm mới phong cách trang điểm thành công.';
        $error   = 'Thêm mới phong cách trang điểm thất bại.';
        $params = $this->productmakeupModel->revertAlias($request->all());

        try {
            $this->productmakeupModel::query()->create($params);
            return redirect()->to($redirect)->with('success', $success);
        } catch ( \Exception $e ) {
            $redirect = UrlHelper::admin($this->model, 'create');
            return redirect()->to($redirect)->with('error', $error);
        }
    }


    /**
     * @param $id
     * @return Application|Factory|View|\Illuminate\Http\RedirectResponse
     */
    public function edit()
    {
        $id = (int) request()->get('id', 0);
        $makeup = $this->productmakeupModel::query()->where('pmakeup_id', $id)->first();
        if($makeup){
            $data['title'] = 'Quản lý phong cách trang điểm: [Sửa]';
            $data['view']  = $this->viewPath . $this->view . '.edit';
            $data['makeup'] = $makeup;

            return view($data['view'] , compact('data'));
        }else{
            $error   = 'Không tìm thấy phong cách trang điểm';
            $redirect = UrlHelper::admin($this->model);
            return redirect()->to($redirect)->with('error', $error);
        }
    }

    /**
     * @param ProductMakeUpRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(ProductMakeUpRequest $request)
    {
        $actionType = request()->post('action_type', 'save');
        $id = request()->post('id', 0);

        if($actionType == 'save'){
            $redirect = UrlHelper::admin($this->model);
        }else{
            $redirect = UrlHelper::admin($this->model,'edit', ['id' => $id]);
        }

        $success = 'Cập nhật phong cách trang điểm thành công.';
        $error   = 'Cập nhật phong cách trang điểm thất bại.';
        $params = $this->productmakeupModel->revertAlias($request->all());

        try {
            $this->productmakeupModel::query()->where('pmakeup_id', $id)->update($params);

            return redirect()->to($redirect)->with('success', $success);
        } catch ( \Exception $e ) {
            $redirect = UrlHelper::admin($this->model);
            return redirect()->to($redirect)->with('error', $error);
        }
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete(Request $request)
    {
        $ids = request()->post('cid', []);
        $success = 'Xóa bộ phong cách trang điểm thành công.';
        $error   = 'Xóa bộ phong cách trang điểm thất bại.';

        $redirect = UrlHelper::admin($this->model);
        $number = $this->productmakeupModel->query()->whereIn('pmakeup_id', $ids)->update(['pmakeup_is_delete' => 'yes']);
        if($number > 0) {
            return redirect()->to($redirect)->with('success', $success);
        }else{
            return redirect()->to($redirect)->with('error', $error);
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function active(Request $request)
    {
        $ids = request()->post('cid', []);
        $success = 'Bật phong cách trang điểm thành công.';
        $error   = 'Bật phong cách trang điểm hất bại.';

        $redirect = UrlHelper::admin($this->model);
        $number = $this->productmakeupModel->query()->whereIn('pmakeup_id', $ids)->update(['pmakeup_status' => 'activated']);
        if($number > 0) {
            return redirect()->to($redirect)->with('success', $success);
        }else{
            return redirect()->to($redirect)->with('error', $error);
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function inactive(Request $request)
    {
        $ids = request()->post('cid', []);
        $success = 'Tắt phong cách trang điểm thành công.';
        $error   = 'Tắt phong cách trang điểm thất bại.';

        $redirect = UrlHelper::admin($this->model);
        $number = $this->productmakeupModel->query()->whereIn('pmakeup_id', $ids)->update(['pmakeup_status' => 'inactive']);
        if($number > 0) {
            return redirect()->to($redirect)->with('success', $success);
        }else{
            return redirect()->to($redirect)->with('error', $error);
        }
    }
}
