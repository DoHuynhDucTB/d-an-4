<?php

namespace App\Http\Controllers\Backend;

use App\Exports\SubscribeExport;
use App\Http\Controllers;
use App\Http\Requests\SubscribeRequest;
use App\Models\Subscribe;
use App\Helpers\UrlHelper;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class SubscribeController extends BaseController
{
    private $view = '.subscribe';
    private $model = 'subscribe';
    private $subscribeModel;
    public function __construct()
    {
        $this->subscribeModel = new Subscribe();
    }

    /**
     * @return Application|Factory|View
     */
    public function index()
    {
        $data['title'] = 'Quản lý Newsletter';
        $data['view']  = $this->viewPath . $this->view . '.list';

        $subscribes = $this->subscribeModel::query()->orderBy('subscribe_id', 'desc')->get();
        $data['subscribes'] = $subscribes;
        return view($data['view'] , compact('data'));
    }


    /**
     * @param $id
     * @return Application|Factory|View|RedirectResponse
     */
    public function edit()
    {
        $id = (int) request()->get('id', 0);
        $subscribe = $this->subscribeModel::query()->where('subscribe_id', $id)->first();
        if($subscribe){
            $data['title'] = 'Quản lý subscribe: [Sửa]';
            $data['view']  = $this->viewPath . $this->view . '.edit';
            $data['subscribe'] = $subscribe;

            return view($data['view'] , compact('data'));
        }else{
            $error   = 'Không tìm thấy subscribe';
            $redirect = UrlHelper::admin($this->model);
            return redirect()->to($redirect)->with('error', $error);
        }
    }

    /**
     * @param subscribeRequest $request
     * @return RedirectResponse
     */
    public function update(subscribeRequest $request)
    {
        $actionType = request()->post('action_type', 'save');
        $id = request()->post('id', 0);

        if($actionType == 'save'){
            $redirect = UrlHelper::admin($this->model);
        }else{
            $redirect = UrlHelper::admin($this->model,'edit', ['id' => $id]);
        }

        $success = 'Cập nhật newsletter thành công.';
        $error   = 'Cập nhật newsletter thất bại.';
        $params = $this->subscribeModel->revertAlias(request()->post());

        try {
            $this->subscribeModel::query()->where('subscribe_id', $id)->update($params);

            return redirect()->to($redirect)->with('success', $success);
        } catch ( \Exception $e ) {
            $redirect = UrlHelper::admin($this->model);
            return redirect()->to($redirect)->with('error', $error);
        }
    }

    /**
     * @return RedirectResponse
     */
    public function delete(Request $request)
    {
        $ids = request()->post('cid', []);
        $success = 'Xóa newsletter thành công.';
        $error   = 'Xóa newsletter thất bại.';

        $redirect = UrlHelper::admin($this->model);
        $number = $this->subscribeModel->query()->whereIn('subscribe_id', $ids)->update(['subscribe_is_delete' => 'yes']);
        if($number > 0) {
            return redirect()->to($redirect)->with('success', $success);
        }else{
            return redirect()->to($redirect)->with('error', $error);
        }
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function active(Request $request): RedirectResponse
    {
        $ids = request()->post('cid', []);
        $success = 'Bật newsletter thành công.';
        $error   = 'Bật newsletter thất bại.';

        $redirect = UrlHelper::admin($this->model);
        $number = $this->subscribeModel->query()->whereIn('subscribe_id', $ids)->update(['subscribe_status' => 'activated']);
        if($number > 0) {
            return redirect()->to($redirect)->with('success', $success);
        }else{
            return redirect()->to($redirect)->with('error', $error);
        }
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function inactive(Request $request): RedirectResponse
    {
        $ids = request()->post('cid', []);
        $success = 'Tắt newsletter thành công.';
        $error   = 'Tắt newsletter thất bại.';

        $redirect = UrlHelper::admin($this->model);
        $number = $this->subscribeModel->query()->whereIn('subscribe_id', $ids)->update(['subscribe_status' => 'inactive']);
        if($number > 0) {
            return redirect()->to($redirect)->with('success', $success);
        }else{
            return redirect()->to($redirect)->with('error', $error);
        }
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function exportExcel (Request $request)
    {
        $status = $request->status;
        $time = $request->time;

        return Excel::download(new SubscribeExport($status, $time), 'newsletters.xlsx');
    }
}
