<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Post;
use App\Models\Image;
use App\Models\AdminUser;
use App\Models\PostGroup;
use App\Helpers\ImageHelper;
use App\Helpers\UrlHelper;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use App\Http\Requests\PostRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PostController extends BaseController
{
    private $view = '.post';
    private $model = 'post';
    private $postModel;
    private $postGroupModel;
    private $imageModel;
    private $adminUserModel;
    public function __construct()
    {
        $this->postModel = new Post();
        $this->imageModel = new Image();
        $this->postGroupModel = new PostGroup();
        $this->adminUserModel = new AdminUser();
    }

    /**
     * @return Application|Factory|View
     */
    public function index()
    {
        $data['title'] = 'Quản lý bài viết ';
        $data['view']  = $this->viewPath . $this->view . '.list';

        $pathAvatar = config('my.path.image_post_avatar_of_module');
        $valueAvatar = config('my.image.value.post.avatar');

        $posts = $this->postModel::query()->get();
        if($posts->count() > 0){
            foreach ($posts as $key => $item){
                $item->urlAvatar = !empty($item->avatar) ? $pathAvatar . $item->avatar->image_name : '';
                $group = $this->postGroupModel->query()->where('postgroup_id', $item->post_group)->first();
                $item->groupName = !empty($group->postgroup_name) ? $group->postgroup_name : '';
            }
        }
        $data['posts'] = $posts;
        return view($data['view'] , compact('data'));
    }

    /**
     * @return Application|Factory|View
     */
    public function create()
    {
        $user = Auth::guard('admin')->user();
        $data['title'] = 'Quản lý bài viết: [Thêm]';
        $data['view']  = $this->viewPath . $this->view . '.add';
        $data['adminName']  = $user->username;
        $data['posts'] = $this->postModel::query()->where('post_group', 70)->get();
        $data['groups'] = $this->postGroupModel::query()->get();
        return view($data['view'] , compact('data'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(PostRequest $request)
    {
        $actionType = request()->post('action_type', 'save');
        if($actionType == 'save'){
            $redirect = UrlHelper::admin($this->model);
        }else{
            $redirect = UrlHelper::admin($this->model,'create');
        }

        $success = 'Đã thêm mới bài viết thành công.';
        $error   = 'Thêm mới bài viết thất bại.';
        $pathAvatar = config('my.path.image_post_avatar_of_module');
        $valueAvatar = config('my.image.value.post.avatar');
        $pathSave = $this->model.'_m/avatar';

        $user = Auth::guard('admin')->user();
        $params = $this->postModel->revertAlias($request->all());
        $params['post_created_by'] = $user->aduser_id;

        if($request->related){
            $params['post_related'] = array_diff(array_map('intval', $params['post_related']), [0]);
            $params['post_related'] = implode(",", $params['post_related']);
        }

        try {
            $postId = 0;
            $post = $this->postModel::query()->create($params);
            if($post){
                $postId = $post->post_id;
            }

            if($request->imageAvatar != null){
                $imageAvatar = $request->imageAvatar;
                ImageHelper::uploadImage($imageAvatar, $this->model, $postId, $valueAvatar, $pathSave);
            }

            return redirect()->to($redirect)->with('success', $success);
        } catch ( \Exception $e ) {
            $redirect = UrlHelper::admin($this->model, 'create');
            return redirect()->to($redirect)->with('error', $error);
        }
    }

    /**
     * @return Application|Factory|View|\Illuminate\Http\RedirectResponse
     */
    public function copy()
    {
        $pathAvatar = config('my.path.image_post_avatar_of_module');
        $valueAvatar = config('my.image.value.post.avatar');

        $ids = request()->post('cid', []);
        $id = isset($ids[0]) ? $ids[0] : 0;
        $user = Auth::guard('admin')->user();

        $post = $this->postModel::query()->where('post_id', $id)->first();
        if($post){
            $data['title'] = 'Quản lý bài viết: [Sao Chép]';
            $data['view']  = $this->viewPath . $this->view . '.copy';
            $data['adminName'] = !empty($creater) ? $creater->username : '';
            $data['post'] = $post;
            $data['urlAvatar'] = '';
            $data['posts'] = $this->postModel::query()->where([['post_id','!=', $id]])->get();
            $data['groups'] = $this->postGroupModel::query()->get();
            $data['arrayRelated'] = !empty($post->post_related) ? explode(',', $post->post_related) : [];

            $imageAvatar  = $this->imageModel->query()->where(['3rd_key' => $id,'3rd_type' => $this->model, 'image_value' => $valueAvatar])->first();
            if($imageAvatar) {
                $data['urlAvatar'] = $pathAvatar.$imageAvatar->image_name;
                $data['avatarId'] = $imageAvatar->image_id;
            }

            return view($data['view'] , compact('data'));
        }else{
            $error   = 'Không tìm thấy bài viết';
            $redirect = UrlHelper::admin($this->model);
            return redirect()->to($redirect)->with('error', $error);
        }
    }


    /**
     * @param $id
     * @return Application|Factory|View|\Illuminate\Http\RedirectResponse
     */
    public function edit()
    {
        $pathAvatar = config('my.path.image_post_avatar_of_module');
        $valueAvatar = config('my.image.value.post.avatar');

        $id = (int) request()->get('id', 0);
        $post = $this->postModel::query()->where('post_id', $id)->first();
        if($post){
            $creater  = $this->adminUserModel::query()->where('aduser_id', $post->post_created_by)->first();
            $data['title'] = 'Quản lý bài viết: [Sửa]';
            $data['view']  = $this->viewPath . $this->view . '.edit';
            $data['post'] = $post;
            $data['urlAvatar'] = '';
            $data['posts'] = $this->postModel::query()->where([['post_id','!=', $id]])->where('post_group', 70)->get();
            $data['groups'] = $this->postGroupModel::query()->get();
            $data['arrayRelated'] = !empty($post->post_related) ? explode(',', $post->post_related) : [];
            $data['adminName'] = !empty($creater) ? $creater->username : '';

            $imageAvatar  = $this->imageModel->query()->where(['3rd_key' => $id,'3rd_type' => $this->model, 'image_value' => $valueAvatar])->first();
            if($imageAvatar) {
                $data['urlAvatar'] = $pathAvatar . $imageAvatar->image_name;
            }

            return view($data['view'] , compact('data'));
        }else{
            $error   = 'Không tìm thấy bài viết';
            $redirect = UrlHelper::admin($this->model);
            return redirect()->to($redirect)->with('error', $error);
        }
    }

    /**
     * @param PostRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(PostRequest $request)
    {
        $actionType = request()->post('action_type', 'save');
        $id = request()->post('id', 0);

        if($actionType == 'save'){
            $redirect = UrlHelper::admin($this->model);
        }else{
            $redirect = UrlHelper::admin($this->model,'edit', ['id' => $id]);
        }

        $success = 'Cập nhật bài viết thành công.';
        $error   = 'Cập nhật bài viết thất bại.';
        $pathAvatar = config('my.path.image_post_avatar_of_module');
        $valueAvatar = config('my.image.value.post.avatar');
        $pathSave = $this->model.'_m/avatar';

        $params = $this->postModel->revertAlias(request()->post());

        if($request->related){
            $params['post_related'] = array_diff(array_map('intval', $params['post_related']), [0]);
            $params['post_related'] = implode(",", $params['post_related']);
        }

        try {
            $this->postModel::query()->where('post_id', $id)->update($params);

            if($request->imageAvatar != null){
                $imageAvatar = $request->imageAvatar;

                /**check image exist*/
                $image = Image::query()->where(['3rd_key' => $id, '3rd_type' => $this->model,  'image_value' => $valueAvatar])->first();
                if($image){
                    ImageHelper::uploadUpdateImage($imageAvatar, $valueAvatar, $image->image_id, $pathSave);
                }else{
                    ImageHelper::uploadImage($imageAvatar, $this->model, $id, $valueAvatar, $pathSave);
                }
            }

            return redirect()->to($redirect)->with('success', $success);
        } catch ( \Exception $e ) {
            $redirect = UrlHelper::admin($this->model);
            return redirect()->to($redirect)->with('error', $error);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function duplicate(PostRequest $request)
    {
        $user = Auth::guard('admin')->user();
        $actionType = request()->post('action_type', 'save');
        $id = request()->post('id', 0);

        if($actionType == 'save'){
            $redirect = UrlHelper::admin($this->model);
        }else{
            $redirect = UrlHelper::admin($this->model,'edit', ['id' => $id]);
        }

        $success = 'Sao chép bài viết thành công.';
        $error   = 'Sao chép bài viết thất bại.';
        $pathAvatar = config('my.path.image_post_avatar_of_module');
        $valueAvatar = config('my.image.value.post.avatar');
        $pathSave = $this->model.'_m/avatar';

        $params = $this->postModel->revertAlias($request->all());
        $params['post_created_by'] = $user->aduser_id;
        unset($params['post_id']);

        if($request->related){
            $params['post_related'] = array_diff(array_map('intval', $params['post_related']), [0]);
            $params['post_related'] = implode(",", $params['post_related']);
        }

        try {
            $postId = 0;
            $post = $this->postModel::query()->create($params);
            if($post){
                $postId = $post->post_id;
            }

            if($request->imageAvatar != null){
                $imageAvatar = $request->imageAvatar;
                ImageHelper::uploadImage($imageAvatar, $this->model, $postId, $valueAvatar, $pathSave);
            }else{
                /**duplicate image avatar*/
                $avatarId = (int)$request->avatarId;
                $imageAvatar = $this->imageModel->getDataDuplicate($avatarId);
                if($imageAvatar){
                    $imageAvatar = $imageAvatar->toArray();
                    $imageAvatar['3rd_key'] = $postId;
                    $imageAvatar['3rd_type'] = $this->model;
                    $this->imageModel->query()->create($imageAvatar);
                }
            }
            return redirect()->to($redirect)->with('success', $success);
        } catch ( \Exception $e ) {
            $redirect = UrlHelper::admin($this->model);
            return redirect()->to($redirect)->with('error', $error);
        }
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete(Request $request)
    {
        $ids = request()->post('cid', []);
        $success = 'Xóa bài viết thành công.';
        $error   = 'Xóa bài viết thất bại.';

        $redirect = UrlHelper::admin($this->model);
        $number = $this->postModel->query()->whereIn('post_id', $ids)->update(['post_is_delete' => 'yes']);
        if($number > 0) {
            return redirect()->to($redirect)->with('success', $success);
        }else{
            return redirect()->to($redirect)->with('error', $error);
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function active(Request $request)
    {
        $ids = request()->post('cid', []);
        $success = 'Bật bài viết thành công.';
        $error   = 'Bật bài viết thất bại.';

        $redirect = UrlHelper::admin($this->model);
        $number = $this->postModel->query()->whereIn('post_id', $ids)->update(['post_status' => 'activated']);
        if($number > 0) {
            return redirect()->to($redirect)->with('success', $success);
        }else{
            return redirect()->to($redirect)->with('error', $error);
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function inactive(Request $request)
    {
        $ids = request()->post('cid', []);
        $success = 'Tắt bài viết thành công.';
        $error   = 'Tắt bài viết thất bại.';

        $redirect = UrlHelper::admin($this->model);
        $number = $this->postModel->query()->whereIn('post_id', $ids)->update(['post_status' => 'inactive']);
        if($number > 0) {
            return redirect()->to($redirect)->with('success', $success);
        }else{
            return redirect()->to($redirect)->with('error', $error);
        }
    }
}
