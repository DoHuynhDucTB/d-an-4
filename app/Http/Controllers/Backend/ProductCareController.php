<?php

namespace App\Http\Controllers\Backend;

use App\Http\Requests\ProductCareRequest;
use App\Models\ProductCare;
use App\Models\AdminUser;
use App\Helpers\UrlHelper;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;

class ProductCareController extends BaseController
{
    private $view = '.productcare';
    private $model = 'productcare';
    private $productcareModel;
    private $adminUserModel;
    public function __construct()
    {
        $this->productcareModel = new ProductCare();
        $this->adminUserModel = new AdminUser();
    }

    /**
     * @return Application|Factory|View
     */
    public function index()
    {
        $data['title'] = 'Quản lý nhu cầu chăm sóc da';
        $data['view']  = $this->viewPath . $this->view . '.list';

        $cares = $this->productcareModel::query()->get();
        $data['cares'] = $cares;
        return view($data['view'] , compact('data'));
    }

    /**
     * @return Application|Factory|View
     */
    public function create()
    {
        $data['title'] = 'Quản lý nhu cầu chăm sóc da: [Thêm]';
        $data['view']  = $this->viewPath . $this->view . '.add';
        return view($data['view'] , compact('data'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(ProductCareRequest $request)
    {
        $actionType = request()->post('action_type', 'save');
        if($actionType == 'save'){
            $redirect = UrlHelper::admin($this->model);
        }else{
            $redirect = UrlHelper::admin($this->model,'create');
        }

        $success = 'Đã thêm mới nhu cầu chăm sóc da thành công.';
        $error   = 'Thêm mới nhu cầu chăm sóc da thất bại.';
        $params = $this->productcareModel->revertAlias($request->all());

        try {
            $this->productcareModel::query()->create($params);
            return redirect()->to($redirect)->with('success', $success);
        } catch ( \Exception $e ) {
            $redirect = UrlHelper::admin($this->model, 'create');
            return redirect()->to($redirect)->with('error', $error);
        }
    }


    /**
     * @param $id
     * @return Application|Factory|View|\Illuminate\Http\RedirectResponse
     */
    public function edit()
    {
        $id = (int) request()->get('id', 0);
        $care = $this->productcareModel::query()->where('pcare_id', $id)->first();
        if($care){
            $data['title'] = 'Quản lý phong cách trang điểm: [Sửa]';
            $data['view']  = $this->viewPath . $this->view . '.edit';
            $data['care'] = $care;

            return view($data['view'] , compact('data'));
        }else{
            $error   = 'Không tìm thấy phong cách trang điểm';
            $redirect = UrlHelper::admin($this->model);
            return redirect()->to($redirect)->with('error', $error);
        }
    }

    /**
     * @param ProductCareRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(ProductCareRequest $request)
    {
        $actionType = request()->post('action_type', 'save');
        $id = request()->post('id', 0);

        if($actionType == 'save'){
            $redirect = UrlHelper::admin($this->model);
        }else{
            $redirect = UrlHelper::admin($this->model,'edit', ['id' => $id]);
        }

        $success = 'Cập nhật nhu cầu chăm sóc da thành công.';
        $error   = 'Cập nhật nhu cầu chăm sóc da thất bại.';
        $params = $this->productcareModel->revertAlias($request->all());

        try {
            $this->productcareModel::query()->where('pcare_id', $id)->update($params);

            return redirect()->to($redirect)->with('success', $success);
        } catch ( \Exception $e ) {
            $redirect = UrlHelper::admin($this->model);
            return redirect()->to($redirect)->with('error', $error);
        }
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete(Request $request)
    {
        $ids = request()->post('cid', []);
        $success = 'Xóa bộ nhu cầu chăm sóc da thành công.';
        $error   = 'Xóa bộ nhu cầu chăm sóc da thất bại.';

        $redirect = UrlHelper::admin($this->model);
        $number = $this->productcareModel->query()->whereIn('pcare_id', $ids)->update(['pcare_is_delete' => 'yes']);
        if($number > 0) {
            return redirect()->to($redirect)->with('success', $success);
        }else{
            return redirect()->to($redirect)->with('error', $error);
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function active(Request $request)
    {
        $ids = request()->post('cid', []);
        $success = 'Bật nhu cầu chăm sóc da thành công.';
        $error   = 'Bật nhu cầu chăm sóc da hất bại.';

        $redirect = UrlHelper::admin($this->model);
        $number = $this->productcareModel->query()->whereIn('pcare_id', $ids)->update(['pcare_status' => 'activated']);
        if($number > 0) {
            return redirect()->to($redirect)->with('success', $success);
        }else{
            return redirect()->to($redirect)->with('error', $error);
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function inactive(Request $request)
    {
        $ids = request()->post('cid', []);
        $success = 'Tắt nhu cầu chăm sóc da thành công.';
        $error   = 'Tắt nhu cầu chăm sóc da thất bại.';

        $redirect = UrlHelper::admin($this->model);
        $number = $this->productcareModel->query()->whereIn('pcare_id', $ids)->update(['pcare_status' => 'inactive']);
        if($number > 0) {
            return redirect()->to($redirect)->with('success', $success);
        }else{
            return redirect()->to($redirect)->with('error', $error);
        }
    }
}
