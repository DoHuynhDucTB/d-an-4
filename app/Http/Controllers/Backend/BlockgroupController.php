<?php

namespace App\Http\Controllers\Backend;

use App\Http\Requests\BlockGroupRequest;
use App\Models\BlockGroup;
use App\Helpers\UrlHelper;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;

class BlockgroupController extends BaseController
{
    private $view = '.blockgroup';
    private $model = 'blockgroup';
    private $blockGroupModel;
    public function __construct()
    {
        $this->blockGroupModel = new BlockGroup();
    }

    /**
     * @return Application|Factory|View
     */
    public function index()
    {
        $data['title'] = 'Quản lý nhóm khối';
        $data['view']  = $this->viewPath . $this->view . '.list';

        $groups = $this->blockGroupModel::query()->get();
        $data['groups'] = $groups;
        return view($data['view'] , compact('data'));
    }

    /**
     * @return Application|Factory|View
     */
    public function create()
    {
        $data['title'] = 'Quản lý nhóm khối[Thêm]';
        $data['view']  = $this->viewPath . $this->view . '.add';
        return view($data['view'] , compact('data'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(BlockGroupRequest $request)
    {
        $actionType = request()->post('action_type', 'save');
        if($actionType == 'save'){
            $redirect = UrlHelper::admin($this->model);
        }else{
            $redirect = UrlHelper::admin($this->model,'create');
        }

        $success = 'Đã thêm mới nhóm khối thành công.';
        $error   = 'Thêm mới nhóm khối thất bại.';

        $params = $this->blockGroupModel->revertAlias($request->all());


        try {
            $blockGroup = $this->blockGroupModel::query()->create($params);

            return redirect()->to($redirect)->with('success', $success);
        } catch ( \Exception $e ) {
            $redirect = UrlHelper::admin($this->model, 'create');
            return redirect()->to($redirect)->with('error', $error);
        }
    }

    /**
     * @param $id
     * @return Application|Factory|View|\Illuminate\Http\RedirectResponse
     */
    public function edit()
    {
        $id = (int) request()->get('id', 0);
        $blockGroup = $this->blockGroupModel::query()->where('blockgroup_id', $id)->first();
        if($blockGroup){
            $data['title'] = 'Quản lý nhóm khối[Sửa]';
            $data['view']  = $this->viewPath . $this->view . '.edit';
            $data['blockGroup'] = $blockGroup;

            return view($data['view'] , compact('data'));
        }else{
            $error   = 'Không tìm thấy nhóm khối';
            $redirect = UrlHelper::admin($this->model);
            return redirect()->to($redirect)->with('error', $error);
        }
    }

    /**
     * @param BlockGroupRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(BlockGroupRequest $request)
    {
        $actionType = request()->post('action_type', 'save');
        $id = request()->post('id', 0);

        if($actionType == 'save'){
            $redirect = UrlHelper::admin($this->model);
        }else{
            $redirect = UrlHelper::admin($this->model,'edit', ['id' => $id]);
        }

        $success = 'Cập nhật nhóm khối thành công.';
        $error   = 'Cập nhật nhóm khối thất bại.';
        $params = $this->blockGroupModel->revertAlias(request()->post());

        try {
            $this->blockGroupModel::query()->where('blockgroup_id', $id)->update($params);

            return redirect()->to($redirect)->with('success', $success);
        } catch ( \Exception $e ) {
            $redirect = UrlHelper::admin($this->model);
            return redirect()->to($redirect)->with('error', $error);
        }
    }
}
