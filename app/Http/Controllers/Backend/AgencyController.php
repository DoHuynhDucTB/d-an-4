<?php

namespace App\Http\Controllers\Backend;

use App\Http\Requests\AgencyRequest;
use App\Models\Image;
use App\Models\AdminUser;
use App\Models\Agency;
use App\Models\User;
use App\Models\Province;
use App\Models\District;
use App\Models\Ward;
use App\Helpers\ImageHelper;
use App\Helpers\UrlHelper;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AgencyController extends BaseController
{
    private $view = '.agency';
    private $model = 'agency';
    private $agencyModel;
    private $imageModel;
    private $provinceModel;
    private $districtModel;
    private $wardModel;
    private $adminUserModel;
    private $userModel;
    public function __construct()
    {
        $this->agencyModel = new Agency();
        $this->imageModel = new Image();
        $this->adminUserModel = new AdminUser();
        $this->userModel = new User();
        $this->provinceModel = new Province();
        $this->districtModel = new District();
        $this->wardModel = new Ward();
    }

    /**
     * @return Application|Factory|View
     */
    public function index()
    {
        $data['title'] = 'Quản lý đối tác';
        $data['view']  = $this->viewPath . $this->view . '.list';
        $pathAvatar = config('my.path.image_agency_of_module');
        $valueAvatar = config('my.image.value.agency.avatar');

        $agencies = $this->agencyModel::query()->get();

        if($agencies->count() > 0){
            foreach ($agencies as $key => $item){
                $item->urlAvatar = !empty($item->avatar) ? $pathAvatar . $item->avatar->image_name : '';
            }
        }
        $data['agencies'] = $agencies;
        return view($data['view'] , compact('data'));
    }

    /**
     * @return Application|Factory|View
     */
    public function create()
    {
        $data['title'] = 'Quản lý đối tác[Thêm]';

        $data['view']  = $this->viewPath . $this->view . '.add';
        $data['agencies'] = $this->agencyModel::query()->orderBy('agency_id', 'DESC')->get();
        return view($data['view'] , compact('data'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(AgencyRequest $request)
    {
        $actionType = request()->post('action_type', 'save');
        if($actionType == 'save'){
            $redirect = UrlHelper::admin($this->model);
        }else{
            $redirect = UrlHelper::admin($this->model,'create');
        }

        $success = 'Đã thêm mới đối tác thành công.';
        $error   = 'Thêm mới đối tác thất bại.';
        $pathAvatar = config('my.path.image_agency_of_module');
        $valueAvatar = config('my.image.value.agency.avatar');
        $pathSave = $this->model.'_m';

        $params = $this->agencyModel->revertAlias($request->all());

        try {
            $agencyId = 0;
            $agency = $this->agencyModel::query()->create($params);
            if($agency){
                $agencyId = $agency->agency_id;
            }

            if($request->imageAvatar != null){
                $imageAvatar = $request->imageAvatar;
                ImageHelper::uploadImage($imageAvatar, $this->model, $agencyId, $valueAvatar, $pathSave);
            }

            return redirect()->to($redirect)->with('success', $success);
        } catch ( \Exception $e ) {
            $redirect = UrlHelper::admin($this->model, 'create');
            return redirect()->to($redirect)->with('error', $error);
        }
    }

    /**
     * @param $id
     * @return Application|Factory|View|\Illuminate\Http\RedirectResponse
     */
    public function edit()
    {
        $id = (int) request()->get('id', 0);
        $agency = $this->agencyModel::query()->where('agency_id', $id)->first();
        $pathAvatar = config('my.path.image_agency_of_module');
        $valueAvatar = config('my.image.value.agency.avatar');
        $pathSave = $this->model.'_m';

        if($agency){
            $data['title'] = 'Quản lý đối tác[Sửa]';
            $data['view']  = $this->viewPath . $this->view . '.edit';
            $data['agency'] = $agency;
            $data['urlAvatar'] = '';

            $imageAvatar  = $this->imageModel->query()->where(['3rd_key' => $id, '3rd_type' => $this->model, 'image_value' => $valueAvatar])->first();
            if($imageAvatar) {
                $data['urlAvatar'] = $pathAvatar . $imageAvatar->image_name;
            }

            $data['provinces'] = $this->provinceModel::query()->where('country_id', $agency->agency_country)->get();
            $data['districts'] = $this->districtModel::query()->where('province_id', $agency->agency_province)->get();
            $data['wards'] = $this->wardModel::query()->where('district_id', $agency->agency_district)->get();

            return view($data['view'] , compact('data'));
        }else{
            $error   = 'Không tìm thấy đối tác';
            $redirect = UrlHelper::admin($this->model);
            return redirect()->to($redirect)->with('error', $error);
        }
    }


    /**
     * @param AgencyRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(AgencyRequest $request)
    {
        $actionType = request()->post('action_type', 'save');
        $id = request()->post('id', 0);

        if($actionType == 'save'){
            $redirect = UrlHelper::admin($this->model);
        }else{
            $redirect = UrlHelper::admin($this->model,'edit', ['id' => $id]);
        }

        $success = 'Cập nhật đối tác thành công.';
        $error   = 'Cập nhật đối tác thất bại.';
        $pathAvatar = config('my.path.image_agency_avatar_of_module');
        $valueAvatar = config('my.image.value.agency.avatar');
        $pathSave = $this->model.'_m';

        $params = $this->agencyModel->revertAlias(request()->post());

        try {
            $this->agencyModel::query()->where('agency_id', $id)->update($params);

            if($request->imageAvatar != null){
                $imageAvatar = $request->imageAvatar;

                /**check image exist*/
                $image = Image::query()->where(['3rd_key' => $id, '3rd_type' => $this->model,  'image_value' => $valueAvatar])->first();
                if($image){
                    ImageHelper::uploadUpdateImage($imageAvatar, $valueAvatar, $image->image_id, $pathSave);
                }else{
                    ImageHelper::uploadImage($imageAvatar, $this->model, $id, $valueAvatar, $pathSave);
                }
            }

            return redirect()->to($redirect)->with('success', $success);
        } catch ( \Exception $e ) {
            $redirect = UrlHelper::admin($this->model);
            return redirect()->to($redirect)->with('error', $error);
        }
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete(Request $request)
    {
        $ids = request()->post('cid', []);
        $success = 'Xóa đối tác thành công.';
        $error   = 'Xóa đối tác thất bại.';

        $redirect = UrlHelper::admin($this->model);
        $number = $this->agencyModel->query()->whereIn('agency_id', $ids)->update(['agency_is_delete' => 'yes']);
        if($number > 0) {
            return redirect()->to($redirect)->with('success', $success);
        }else{
            return redirect()->to($redirect)->with('error', $error);
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function active(Request $request)
    {
        $ids = request()->post('cid', []);
        $success = 'Bật đối tác thành công.';
        $error   = 'Bật đối tác thất bại.';

        $redirect = UrlHelper::admin($this->model);
        $number = $this->agencyModel->query()->whereIn('agency_id', $ids)->update(['agency_status' => 'activated']);
        if($number > 0) {
            return redirect()->to($redirect)->with('success', $success);
        }else{
            return redirect()->to($redirect)->with('error', $error);
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function inactive(Request $request)
    {
        $ids = request()->post('cid', []);
        $success = 'Tắt đối tác thành công.';
        $error   = 'Tắt đối tác thất bại.';

        $redirect = UrlHelper::admin($this->model);
        $number = $this->agencyModel->query()->whereIn('agency_id', $ids)->update(['agency_status' => 'inactive']);
        if($number > 0) {
            return redirect()->to($redirect)->with('success', $success);
        }else{
            return redirect()->to($redirect)->with('error', $error);
        }
    }

    public function getProvince(Request $request)
    {
        $id = $request->id;
        $data = $this->provinceModel::query()->where('country_id', $id)->get();

        $html = '<option value="">'.__('Tỉnh/Thành Phố').'</option>';
        if(!empty($data)){
            foreach ($data as $key => $item){
                $html .= '<option value="' . $item->id . '">'. $item->name . '</option>';
            }
        }

        $response = [
            'html' => $html,
        ];
        return json_encode($response);

    }

    public function getDistrict(Request $request)
    {
        $id = $request->id;
        $data = $this->districtModel::query()->where('province_id', $id)->get();

        $html = '<option value="">'.__('Quận/Huyện').'</option>';
        if(!empty($data)){
            foreach ($data as $key => $item){
                $html .= '<option value="' . $item->id . '">'. $item->name . '</option>';
            }
        }

        $response = [
            'html' => $html,
        ];
        return json_encode($response);
    }

    public function getWard(Request $request)
    {
        $id = $request->id;
        $data = $this->wardModel::query()->where('district_id', $id)->get();

        $html = '<option value="">'.__('Xã/Phường').'</option>';
        if(!empty($data)){
            foreach ($data as $key => $item){
                $html .= '<option value="' . $item->id . '">'. $item->name . '</option>';
            }
        }

        $response = [
            'html' => $html,
        ];
        return json_encode($response);
    }

}
