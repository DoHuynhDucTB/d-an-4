<?php
namespace App\Http\Controllers\Backend;

use App\Helpers\ImageHelper;
use App\Helpers\UrlHelper;
use App\Http\Requests\PerceivedValueRequest;
use App\Models\PerceivedValue;
use App\Models\Image;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class PerceivedvalueController extends BaseController
{
    /**
     * @var PerceivedValue
     */
    private PerceivedValue $perceivedValueModel;
    /**
     * @var Image
     */
    private Image $imageModel;

    private $view = '.perceivedvalue';
    private $model = 'perceivedvalue';

    /**
     * PerceivedvalueController constructor.
     * @param PerceivedValue $perceivedValueModel
     */
    public function __construct(PerceivedValue $perceivedValueModel,
                                Image $imageModel)
    {
        $this->perceivedValueModel = $perceivedValueModel;
        $this->imageModel = $imageModel;
    }

    /**
     * @return Application|Factory|View
     */
    public function index()
    {
        $data['perceivedValues'] = $this->perceivedValueModel::parentQuery()->orderBy('pervalue_sorted', 'desc')->get();
        $data['title'] = 'Quản lý Cảm nhận của Khách hàng';
        $data['view']  = $this->viewPath . $this->view . '.index';
        return view($data['view'] , compact('data'));
    }


    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function active(Request $request): RedirectResponse
    {
        $ids = $request->post('cid', []);
        $this->perceivedValueModel::parentQuery()->whereIn('pervalue_id', $ids)->update(['pervalue_status' => 'activated']);
        return back()->with('success', 'Bật cảm nhận Khách hàng thành công');
    }


    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function inactive(Request $request): RedirectResponse
    {
        $ids = $request->post('cid', []);
        $this->perceivedValueModel::parentQuery()->whereIn('pervalue_id', $ids)->update(['pervalue_status' => 'inactive']);
        return back()->with('success', 'Tắt cảm nhận Khách hàng thành công');
    }


    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function delete(Request $request): RedirectResponse
    {
        $ids = $request->post('cid', []);
        $this->perceivedValueModel::parentQuery()->whereIn('pervalue_id', $ids)->delete();
        return back()->with('success', 'Xóa thành công');
    }


    /**
     * @return Application|Factory|View
     */
    public function detail()
    {
        $id = \request()->get('id', null);
        $pathAvatar = config('my.path.image_perceivedvalue_of_module');
        $valueAvatar = config('my.image.value.perceivedvalue.avatar');

        $data['title'] = 'Cảm nhận khách hàng: [Thêm]';
        $data['perceivedValue'] = null;
        if ($id != null) {
            $data['title'] = 'Cảm nhận khách hàng: [Sửa]';
            $perceivedValue = $this->perceivedValueModel::parentQuery()->find($id);
            $data['perceivedValue'] = $perceivedValue;

            $imageAvatar  = $this->imageModel->query()->where(['3rd_key' => $id,'3rd_type' => $this->model, 'image_value' => $valueAvatar])->first();
            if($imageAvatar) {
                $data['urlAvatar'] = $pathAvatar . $imageAvatar->image_name;
            }

            if (!$perceivedValue){
                return redirect()
                    ->to(UrlHelper::admin('perceivedvalue', 'index'))
                    ->with('error', 'Cảm nhận khách hàng không tìm thấy');
            }
        }

        return view($this->viewPath . $this->view . '.detail' , compact('data'));
    }

    /**
     * @param PerceivedValueRequest $request
     * @return RedirectResponse
     */
    public function save(PerceivedValueRequest $request)
    {
        $id          = $request->post('id', null);
        $status      = $request->post('pervalue_status');
        $fullname    = $request->post('pervalue_fullname', '');
        $description = $request->post('pervalue_description', '');
        $descriptionEn = $request->post('pervalue_description_en', '');
        $data = [
            'pervalue_fullname'      => strip_tags($fullname),
            'pervalue_status'        => ($status == 'activated') ? 'activated' : 'inactive',
            'pervalue_description'   => strip_tags($description),
            'pervalue_description_en'   => strip_tags($descriptionEn),
        ];

        $pathAvatar = config('my.path.image_perceivedvalue_of_module');
        $valueAvatar = config('my.image.value.perceivedvalue.avatar');
        $pathSave = $this->model.'_m';

        $message = 'Tạo mới cảm nhận khách hàng thành công.';
        if ($id > 0){ // Update
            $message = 'Cập nhật cảm nhận khách hàng thành công.';
            $perceivedValue = $this->perceivedValueModel::parentQuery()->find($id);
            if (!$perceivedValue) {
                return redirect()
                    ->to(UrlHelper::admin('perceivedvalue', 'index'))
                    ->with('error', 'Cảm nhận khách hàng không tìm thấy') ;
            } else {
                $perceivedValue->update($data);

                if($request->imageAvatar != null){
                    $imageAvatar = $request->imageAvatar;
                    /**check image exist*/
                    $image = Image::query()->where(['3rd_key' => $id, '3rd_type' => $this->model,  'image_value' => $valueAvatar])->first();
                    if($image){
                        ImageHelper::uploadUpdateImage($imageAvatar, $valueAvatar, $image->image_id, $pathSave);
                    }else{
                        ImageHelper::uploadImage($imageAvatar, $this->model, $id, $valueAvatar, $pathSave);
                    }
                }
            }
        }else{ // Create new
            $newPerceivedValue = $this->perceivedValueModel::parentQuery()->create($data);
            $id = $newPerceivedValue->pervalue_id;

            if($request->imageAvatar != null){
                $imageAvatar = $request->imageAvatar;
                ImageHelper::uploadImage($imageAvatar, $this->model, $id, $valueAvatar, $pathSave);
            }
        }


        if ($request->post('action_type') == 'save') {
            return redirect()->to(UrlHelper::admin('perceivedvalue', 'index'))->with('success', $message);
        } else {
            return redirect()->to(UrlHelper::admin('perceivedvalue', 'detail', ['id' => $id]))->with('success', $message) ;
        }
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function sort(Request $request)
    {
        $ids     = request()->post('cid', []);
        $sorts   = request()->post('sort', []);
        $redirect = UrlHelper::admin('perceivedvalue', 'index');

        if (!is_array($ids) || count($ids) == 0) {
            return redirect()->to($redirect)->with('error', 'Vui lòng chọn giá trị để sắp xếp');
        }

        foreach ($ids as $key => $id) {
            $this->perceivedValueModel::parentQuery()->where('pervalue_id', $id)->update(['pervalue_sorted' => intval($sorts[$key])]);
        }
        return redirect()->to($redirect)->with('success', 'Sắp xếp thành công');
    }

}
