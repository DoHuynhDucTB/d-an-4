<?php

namespace App\Http\Controllers\Backend;

use App\Http\Requests\ShopRequest;
use App\Models\Image;
use App\Models\Shop;
use App\Models\Agency;
use App\Helpers\ImageHelper;
use App\Helpers\UrlHelper;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;

class ShopController extends BaseController
{
    private $view = '.shop';
    private $model = 'shop';
    private $shopModel;
    private $agencyModel;
    private $imageModel;
    public function __construct()
    {
        $this->shopModel = new Shop();
        $this->agencyModel = new Agency();
        $this->imageModel = new Image();
    }

    /**
     * @return Application|Factory|View
     */
    public function index()
    {
        $agency = \request()->get('agency');
        $data['title'] = 'Quản lý cửa hàng';
        $data['view']  = $this->viewPath . $this->view . '.list';
        $pathAvatar = config('my.path.image_shop_of_module');
        $valueAvatar = config('my.image.value.shop.avatar');

        if($agency){
            $shops = $this->shopModel::query()->where('agency_id', $agency)->get();
        }else{
            $shops = $this->shopModel::query()->get();
        }

        if($shops->count() > 0){
            foreach ($shops as $key => $item){
                $item->urlAvatar = !empty($item->avatar) ? $pathAvatar . $item->avatar->image_name : '';
            }
        }
        $data['shops'] = $shops;
        $data['agency'] = $agency;
        return view($data['view'] , compact('data'));
    }

    /**
     * @return Application|Factory|View
     */
    public function create()
    {
        $data['title'] = 'Quản lý cửa hàng[Thêm]';
        $agency = \request()->get('agency');

        $data['view']  = $this->viewPath . $this->view . '.add';
        $data['shops'] = $this->shopModel::query()->orderBy('shop_id', 'DESC')->get();
        $data['agency'] = $agency;
        return view($data['view'] , compact('data'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(ShopRequest $request)
    {
        $agency = !empty($request->agency) ? $request->agency : 0;
        $actionType = request()->post('action_type', 'save');
        if($actionType == 'save'){
            $redirect = UrlHelper::admin($this->model, 'index',['agency' => $agency]);
        }else{
            $redirect = UrlHelper::admin($this->model,'create', ['agency' => $agency]);
        }

        $success = 'Đã thêm mới cửa hàng thành công.';
        $error   = 'Thêm mới cửa hàng thất bại.';
        $pathAvatar = config('my.path.image_shop_of_module');
        $valueAvatar = config('my.image.value.shop.avatar');
        $pathSave = $this->model.'_m';

        $params = $this->shopModel->revertAlias($request->all());
        if($agency){
            $params['agency_id'] = $agency;
        }

        try {
            $shopId = 0;
            $shop = $this->shopModel::query()->create($params);
            if($shop){
                $shopId = $shop->shop_id;
            }

            if($request->imageAvatar != null){
                $imageAvatar = $request->imageAvatar;
                ImageHelper::uploadImage($imageAvatar, $this->model, $shopId, $valueAvatar, $pathSave);
            }

            return redirect()->to($redirect)->with('success', $success);
        } catch ( \Exception $e ) {
            $redirect = UrlHelper::admin($this->model, 'create',['agency' => $agency]);
            return redirect()->to($redirect)->with('error', $error);
        }
    }

    /**
     * @param $id
     * @return Application|Factory|View|\Illuminate\Http\RedirectResponse
     */
    public function edit()
    {
        $agency = \request()->get('agency');
        $id = (int) request()->get('id', 0);
        $shop = $this->shopModel::query()->where('shop_id', $id)->first();
        $pathAvatar = config('my.path.image_shop_of_module');
        $valueAvatar = config('my.image.value.shop.avatar');
        $pathSave = $this->model.'_m';

        if($shop){
            $data['title'] = 'Quản lý cửa hàng[Sửa]';
            $data['view']  = $this->viewPath . $this->view . '.edit';
            $data['shop'] = $shop;
            $data['urlAvatar'] = '';
            $data['agency'] = $agency;

            $imageAvatar  = $this->imageModel->query()->where(['3rd_key' => $id, '3rd_type' => $this->model, 'image_value' => $valueAvatar])->first();
            if($imageAvatar) {
                $data['urlAvatar'] = $pathAvatar . $imageAvatar->image_name;
            }

            return view($data['view'] , compact('data'));
        }else{
            $error   = 'Không tìm thấy cửa hàng';
            $redirect = UrlHelper::admin($this->model, 'index', ['agency' => $agency]);
            return redirect()->to($redirect)->with('error', $error);
        }
    }


    /**
     * @param shopRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(shopRequest $request)
    {
        $actionType = request()->post('action_type', 'save');
        $id = request()->post('id', 0);
        $agency = !empty($request->agency) ? $request->agency : 0;

        if($actionType == 'save'){
            $redirect = UrlHelper::admin($this->model, 'index', ['agency' => $agency]);
        }else{
            $redirect = UrlHelper::admin($this->model,'edit', ['id' => $id, 'agency' => $agency]);
        }

        $success = 'Cập nhật cửa hàng thành công.';
        $error   = 'Cập nhật cửa hàng thất bại.';
        $pathAvatar = config('my.path.image_shop_avatar_of_module');
        $valueAvatar = config('my.image.value.shop.avatar');
        $pathSave = $this->model.'_m';

        $params = $this->shopModel->revertAlias(request()->post());

        if($agency){
            $params['agency_id'] = $agency;
        }

        try {
            $this->shopModel::query()->where('shop_id', $id)->update($params);

            if($request->imageAvatar != null){
                $imageAvatar = $request->imageAvatar;

                /**check image exist*/
                $image = Image::query()->where(['3rd_key' => $id, '3rd_type' => $this->model,  'image_value' => $valueAvatar])->first();
                if($image){
                    ImageHelper::uploadUpdateImage($imageAvatar, $valueAvatar, $image->image_id, $pathSave);
                }else{
                    ImageHelper::uploadImage($imageAvatar, $this->model, $id, $valueAvatar, $pathSave);
                }
            }

            return redirect()->to($redirect)->with('success', $success);
        } catch ( \Exception $e ) {
            $redirect = UrlHelper::admin($this->model, 'index', ['agency' => $agency]);
            return redirect()->to($redirect)->with('error', $error);
        }
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete(Request $request)
    {
        $agency = !empty($request->agency) ? $request->agency : 0;
        $ids = request()->post('cid', []);
        $success = 'Xóa cửa hàng thành công.';
        $error   = 'Xóa cửa hàng thất bại.';

        $redirect = UrlHelper::admin($this->model, 'index',['agency' => $agency]);
        $number = $this->shopModel->query()->whereIn('shop_id', $ids)->update(['shop_is_delete' => 'yes']);
        if($number > 0) {
            return redirect()->to($redirect)->with('success', $success);
        }else{
            return redirect()->to($redirect)->with('error', $error);
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function active(Request $request)
    {
        $agency = !empty($request->agency) ? $request->agency : 0;
        $ids = request()->post('cid', []);
        $success = 'Bật cửa hàng thành công.';
        $error   = 'Bật cửa hàng thất bại.';

        $redirect = UrlHelper::admin($this->model,'index',['agency' => $agency]);
        $number = $this->shopModel->query()->whereIn('shop_id', $ids)->update(['shop_status' => 'activated']);
        if($number > 0) {
            return redirect()->to($redirect)->with('success', $success);
        }else{
            return redirect()->to($redirect)->with('error', $error);
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function inactive(Request $request)
    {
        $agency = !empty($request->agency) ? $request->agency : 0;
        $ids = request()->post('cid', []);
        $success = 'Tắt cửa hàng thành công.';
        $error   = 'Tắt cửa hàng thất bại.';

        $redirect = UrlHelper::admin($this->model,'index',['agency' => $agency]);
        $number = $this->shopModel->query()->whereIn('shop_id', $ids)->update(['shop_status' => 'inactive']);
        if($number > 0) {
            return redirect()->to($redirect)->with('success', $success);
        }else{
            return redirect()->to($redirect)->with('error', $error);
        }
    }
}
