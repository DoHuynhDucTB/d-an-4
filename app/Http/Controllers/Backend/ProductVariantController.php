<?php

namespace App\Http\Controllers\Backend;

use App\Models\ProductVariant;
use App\Models\Product;
use App\Models\Image;
use App\Models\AdminUser;
use App\Models\Producer;
use App\Models\ProductColor;
use App\Models\ProductSize;
use App\Helpers\ImageHelper;
use App\Helpers\UrlHelper;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use App\Http\Requests\ProductVariantRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class ProductVariantController extends BaseController
{
    private $view = '.productvariant';
    private $model = 'productvariant';
    private $array_type = [
        'shoes' => 'Sản phẩm Giày',
        'sandals' => 'Sản phẩm Dép',
        'raincoat' => 'Sản phẩm Áo Mưa',
        'accessories' => 'Sản phẩm Phụ Kiện',
    ];
    private $productModel;
    private $productVariantModel;
    private $imageModel;
    private $adminUserModel;
    private $producerModel;
    private $productSizeModel;
    private $productColorModel;
    public function __construct()
    {
        $this->productModel = new Product();
        $this->productVariantModel = new ProductVariant();
        $this->producerModel = new Producer();
        $this->imageModel = new Image();
        $this->productSizeModel = new ProductSize();
        $this->productColorModel = new ProductColor();
        $this->adminUserModel = new AdminUser();
    }

    /**
     * @return Application|Factory|View
     */
    public function index()
    {
        $type = \request()->get('type');
        $productId = \request()->get('product_id');
        $type = in_array($type, ['shoes', 'sandals', 'raincoat', 'accessories']) ? $type : 'shoes';
        $array_type = $this->array_type;
        $data['type'] = $type;
        $data['product_id'] = $productId;
        $data['view']  = $this->viewPath . $this->view . '.list';

        $product = $this->productModel::query()->where('product_id', $productId)->first();
        $data['title'] = $array_type[$type]. ' : ' . $product->product_name .' - Biến thể';

        $pathAvatar = config('my.path.image_product_variant_avatar_of_module');

        $variants = $this->productVariantModel::query()->where('product_id', $productId)->get();
        if($variants->count() > 0){
            foreach ($variants as $key => $item){
                $item->urlAvatar = !empty($item->avatar) ? $pathAvatar . $item->avatar->image_name : '';
            }
        }
        $data['variants'] = $variants;
        return view($data['view'] , compact('data'));
    }

    /**
     * @return Application|Factory|View
     */
    public function create()
    {
        $type = \request()->get('type');
        $productId = \request()->get('product_id');
        $type = in_array($type, ['shoes', 'sandals', 'raincoat', 'accessories']) ? $type : 'shoes';
        $array_type = $this->array_type;
        $data['type'] = $type;
        $data['product_id'] = $productId;

        $product = $this->productModel::query()->where('product_id', $productId)->first();
        $data['title'] = $array_type[$type]. ' : ' . $product->product_name .' - Biến thể [Thêm]';
        $user = Auth::guard('admin')->user();
        $data['view']  = $this->viewPath . $this->view . '.add';
        $data['adminName']  = $user->username;
        $data['product']  = $product;
        $data['producers'] = $this->producerModel::query()
                                ->select('producer_id', 'producer_name')
                                ->get()->toArray();
        $data['variants'] = $this->productVariantModel::query()
            ->where('product_id', $productId)
            ->get();
        $data['colors'] = $this->productColorModel::query()->get();
        $data['sizes'] = $this->productSizeModel::query()->get();
        return view($data['view'] , compact('data'));
    }

    /**
     * @param ProductVariantRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(ProductVariantRequest $request)
    {
        $type = !empty($request->type) ? $request->type : 'shoes';
        $code = !empty($request->product_code) ? $request->product_code : '';
        $productId = !empty($request->product_id) ? $request->product_id : 0;
        $actionType = request()->post('action_type', 'save');
        if($actionType == 'save'){
            $redirect = UrlHelper::admin('productvariant', 'index', ['type' => $type, 'product_id' => $productId]);
        }else{
            $redirect = UrlHelper::admin('productvariant','create', ['type' => $type, 'product_id' => $productId]);
        }

        $success = 'Đã thêm mới biến thể thành công.';
        $error   = 'Thêm mới biến thể thất bại.';

        $pathAvatar = config('my.path.image_product_variant_avatar_of_module');
        $valueAvatar = config('my.image.value.product_variant.avatar');
        $pathSaveAvatar = $this->model.'_m/avatar';

        $pathThumbnail = config('my.path.image_product_variant_thumbnail_of_module');
        $valueThumbnail = config('my.image.value.product_variant.thumbnail');
        $pathSaveThumbnail = $this->model.'_m/thumbnail';

        $pathBanner = config('my.path.image_product_variant_banner_of_module');
        $valueBanner = config('my.image.value.product_variant.banner');
        $pathSaveBanner = $this->model.'_m/banner';
        $params = $this->productVariantModel->revertAlias($request->all());

        if($request->related){
            $params['pvariant_related'] = array_diff(array_map('intval', $params['pvariant_related']), [0]);
            $params['pvariant_related'] = $this->productVariantModel->getIds($params['pvariant_related']);
        }

        $color = $this->productColorModel::query()->where('pcolor_id', $params['pvariant_color'])->first();
        $size = $this->productSizeModel::query()->where('psize_id', $params['pvariant_size'])->first();
        if($color && $size){
            $params['pvariant_code'] = $code .'-'. Str::slug($color->pcolor_code) .'-'. $params['pvariant_sex'] .'-'. Str::slug($size->psize_value);
        }

        try {
            $pvariantId = 0;
            $productVariant = $this->productVariantModel::query()->create($params);
            if($productVariant){
                $pvariantId = $productVariant->pvariant_id ;
            }

            if($request->imageAvatar != null){
                $imageAvatar = $request->imageAvatar;
                ImageHelper::uploadImage($imageAvatar, $this->model, $pvariantId, $valueAvatar, $pathSaveAvatar);
            }
            if($request->imageThumbnail != null && count($request->imageThumbnail) > 0){
                $imageThumbnail = $request->imageThumbnail;
                ImageHelper::uploadMultipleImage($imageThumbnail, $this->model, $pvariantId, $valueThumbnail, $pathSaveThumbnail);
            }
            if($request->imageBanner != null && count($request->imageBanner) > 0){
                $imageBanner = $request->imageBanner;
                ImageHelper::uploadMultipleImage($imageBanner, $this->model, $pvariantId, $valueBanner, $pathSaveBanner);
            }

            return redirect()->to($redirect)->with('success', $success);
        } catch ( \Exception $e ) {
            $redirect = UrlHelper::admin($this->model,'create', ['type' => $type, 'product_id' => $productId]);
            return redirect()->to($redirect)->with('error', $error);
        }
    }


    /**
     * @param $id
     * @return Application|Factory|View|\Illuminate\Http\RedirectResponse
     */
    public function edit()
    {
        $id = (int) request()->get('id', 0);

        $pathAvatar = config('my.path.image_product_variant_avatar_of_module');
        $valueAvatar = config('my.image.value.product_variant.avatar');

        $pathThumbnail = config('my.path.image_product_variant_thumbnail_of_module');
        $valueThumbnail = config('my.image.value.product_variant.thumbnail');

        $pathBanner = config('my.path.image_product_variant_banner_of_module');
        $valueBanner = config('my.image.value.product_variant.banner');

        $variant= $this->productVariantModel::query()->where('pvariant_id', $id)->first();


        $type = \request()->get('type');
        $productId = \request()->get('product_id');
        $type = in_array($type, ['shoes', 'sandals', 'raincoat', 'accessories']) ? $type : 'shoes';
        $array_type = $this->array_type;
        $data['type'] = $type;
        $data['product_id'] = $productId;
        $data['view']  = $this->viewPath . $this->view . '.edit';

        $product = $this->productModel::query()->where('product_id', $productId)->first();
        $data['title'] = $array_type[$type]. ' : ' . $product->product_name .' - Biến thể [Sửa]';

        if($variant){

            $imageAvatar  = $this->imageModel->query()->where(['3rd_key' => $id, '3rd_type' => $this->model, 'image_value' => $valueAvatar])->first();
            $imageThumbnail = $this->imageModel->query()->where(['3rd_key' => $id, '3rd_type' => $this->model, 'image_value' => $valueThumbnail])->orderBy('image_id', 'ASC')->get();
            $imageBanner = $this->imageModel->query()->where(['3rd_key' => $id, '3rd_type' => $this->model, 'image_value' => $valueBanner])->orderBy('image_id', 'ASC')->get();

            $data['variant'] = $variant;
            $data['product'] = $product;
            $data['urlAvatar'] = '';

            $data['variants'] = $this->productVariantModel::query()->where([['pvariant_id', '!=', $id]])->get();
            $data['colors'] = $this->productColorModel::query()->get();
            $data['sizes'] = $this->productSizeModel::query()->get();

            $data['arrayRelated'] = !empty($variant->pvariant_related) ? array_diff(array_map('intval', explode(',', $variant->pvariant_related)), [0]) : [];

            $data['producers'] = $this->producerModel::query()
                                    ->select('producer_id', 'producer_name')
                                    ->get()->toArray();
            $data['thumbnailIds'] = array_column($imageThumbnail->toArray(), 'image_id');
            $data['bannerIds'] = array_column($imageBanner->toArray(), 'image_id');
            $data['colorImageIds'] = count(array_column($imageThumbnail->toArray(), 'rd_type_2')) >= count(array_column($imageBanner->toArray(), 'rd_type_2')) ? array_column($imageThumbnail->toArray(), 'rd_type_2') : array_column($imageBanner->toArray(), 'rd_type_2');


            if($imageAvatar) {
                $data['urlAvatar'] = $pathAvatar . $imageAvatar->image_name;
            }
            if($imageThumbnail->count() > 0) {
                foreach ($imageThumbnail as $key => $itemT){
                    $data['thumbnail'][] = [
                        'link'  => $pathThumbnail . $itemT->image_name,
                        'id'    => $itemT->image_id,
                        'color' => $itemT->rd_type_2,
                    ];
                }
            }

            if($imageBanner->count() > 0) {
                foreach ($imageBanner as $key => $itemB){
                    $data['banner'][] = [
                        'link'  => $pathBanner . $itemB->image_name,
                        'id'    => $itemB->image_id,
                        'color' => $itemB->rd_type_2,
                    ];
                }
            }

            return view($data['view'] , compact('data'));
        }else{
            $error   = 'Không tìm thấy biến thể';
            $redirect = UrlHelper::admin($this->model, 'index',  ['type' => $type]);
            return redirect()->to($redirect)->with('error', $error);
        }
    }

    /**
     * @param ProductVariantRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(ProductVariantRequest $request)
    {
        $id = request()->post('id', 0);
        $type = !empty($request->type) ? $request->type : 'shoes';
        $code = !empty($request->product_code) ? $request->product_code : '';
        $productId = !empty($request->product_id) ? $request->product_id : 0;
        $actionType = request()->post('action_type', 'save');
        if($actionType == 'save'){
            $redirect = UrlHelper::admin('productvariant', 'index', ['type' => $type, 'product_id' => $productId]);
        }else{
            $redirect = UrlHelper::admin('productvariant','create', ['type' => $type, 'product_id' => $productId]);
        }

        $success = 'Đã chỉnh sửa biến thể thành công.';
        $error   = 'Chỉnh sửa biến thể thất bại.';

        $pathAvatar = config('my.path.image_product_variant_avatar_of_module');
        $valueAvatar = config('my.image.value.product_variant.avatar');
        $pathSaveAvatar = $this->model.'_m/avatar';


        $pathThumbnail = config('my.path.image_product_variant_thumbnail_of_module');
        $valueThumbnail = config('my.image.value.product_variant.thumbnail');
        $pathSaveThumbnail = $this->model.'_m/thumbnail';

        $pathBanner = config('my.path.image_product_variant_banner_of_module');
        $valueBanner = config('my.image.value.product_variant.banner');
        $pathSaveBanner = $this->model.'_m/banner';


        $params = $this->productVariantModel->revertAlias(request()->all());
        if($request->related){
            $params['pvariant_related'] = array_diff(array_map('intval', $params['pvariant_related']), [0]);
            $params['pvariant_related'] = $this->productVariantModel->getIds($params['pvariant_related']);
        }

//        $color = $this->productColorModel::query()->where('pcolor_id', $params['pvariant_color'])->first();
//        $size = $this->productSizeModel::query()->where('psize_id', $params['pvariant_size'])->first();
//        if($color && $size){
//            $params['pvariant_code'] = $code .'-'. Str::slug($color->pcolor_code) .'-'. $params['pvariant_sex'] .'-'. Str::slug($size->psize_value);
//        }


        try {
            $variant = $this->productVariantModel::query()->where('pvariant_id', $id)->first();
            $variant->update($params);

            if($request->imageAvatar != null){
                $imageAvatar = $request->imageAvatar;

                /**check image exist*/
                $image = Image::query()->where(['3rd_key' => $id, '3rd_type' => $this->model,  'image_value' => $valueAvatar])->first();
                if($image){
                    ImageHelper::uploadUpdateImage($imageAvatar, $valueAvatar, $image->image_id, $pathSaveAvatar);
                }else{
                    ImageHelper::uploadImage($imageAvatar, $this->model, $id, $valueAvatar, $pathSaveAvatar);
                }
            }

            /** cập nhật delete*/
            $thumbnailIds = json_decode($request->thumbnailIds);
            $bannerIds = json_decode($request->bannerIds);

            if($request->tIds != null && count($request->tIds) > 0){
                $tIds = $request->tIds;
                $arrayDelete = array_diff($thumbnailIds, $tIds);
                if(count($arrayDelete) > 0){
                    $this->imageModel->query()->whereIn('image_id', $arrayDelete)->update(['image_is_deleted' => 'yes']);
                }
            }

            if($request->bIds != null && count($request->bIds) > 0){
                $bIds = $request->bIds;
                $arrayDelete = array_diff($bannerIds, $bIds);
                if(count($arrayDelete) > 0){
                    $this->imageModel->query()->whereIn('image_id', $arrayDelete)->update(['image_is_deleted' => 'yes']);
                }
            }

            /**upload update*/
            if($request->dataThumbnailUpdate != null && count($request->dataThumbnailUpdate) > 0){
                $dataThumbnailUpdate = $request->dataThumbnailUpdate;
                foreach ($dataThumbnailUpdate as $key => $itemThumbnail){
                    ImageHelper::uploadUpdateImage($itemThumbnail, $valueThumbnail, $key, $pathSaveThumbnail);
                }
            }
            if($request->dataBannerUpdate != null && count($request->dataBannerUpdate) > 0){
                $dataBannerUpdate = $request->dataBannerUpdate;
                foreach ($dataBannerUpdate as $key => $itemBanner){
                    ImageHelper::uploadUpdateImage($itemBanner, $valueBanner, $key, $pathSaveBanner);
                }
            }

            /** upload create*/
            if($request->dataThumbnailCreate != null && count($request->dataThumbnailCreate) > 0){
                $dataThumbnailCreate = $request->dataThumbnailCreate;
                foreach ($dataThumbnailCreate as $key => $itemThumbnail){
                    ImageHelper::uploadImage($itemThumbnail, $this->model, $id, $valueThumbnail, $pathSaveThumbnail);

                }
            }
            if($request->dataBannerCreate != null && count($request->dataBannerCreate) > 0){
                $dataBannerCreate = $request->dataBannerCreate;
                foreach ($dataBannerCreate as $key => $itemBanner){
                    ImageHelper::uploadImage($itemBanner, $this->model, $id, $valueBanner, $pathSaveBanner);

                }
            }

            /** update color cho thumnail vả banner*/
            $dataColorUpdate = $request->dataColorUpdate ? $request->dataColorUpdate : [];
            if(count($dataColorUpdate) > 0){
                $this->imageModel->updateColor($dataColorUpdate, $this->model, $id);
            }

            if($request->imageThumbnail != null && count($request->imageThumbnail) > 0){
                $imageThumbnail = $request->imageThumbnail;
                ImageHelper::uploadMultipleImage($imageThumbnail, $this->model, $id, $valueThumbnail, $pathSaveThumbnail);
            }
            if($request->imageBanner != null && count($request->imageBanner) > 0) {
                $imageBanner = $request->imageBanner;
                ImageHelper::uploadMultipleImage($imageBanner, $this->model, $id, $valueBanner, $pathSaveBanner);
            }

            return redirect()->to($redirect)->with('success', $success);
        } catch ( \Exception $e ) {
            $redirect = UrlHelper::admin('productvariant','index', ['type' => $type, 'product_id' => $productId]);
            return redirect()->to($redirect)->with('error', $error);
        }
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete(Request $request)
    {
        $type = \request()->get('type');
        $productId = \request()->get('product_id');
        $type = in_array($type, ['shoes', 'sandals', 'raincoat', 'accessories']) ? $type : 'shoes';
        $ids = request()->post('cid', []);
        $success = 'Tắt biến thể thành công.';
        $error   = 'Tắt biến thể thất bại.';

        $redirect = UrlHelper::admin('productvariant', 'index', ['type' => $type, 'product_id' => $productId]);
        $number = $this->productVariantModel->query()->whereIn('pvariant_id', $ids)->update(['pvariant_is_delete' => 'yes']);
        if($number > 0) {
            return redirect()->to($redirect)->with('success', $success);
        }else{
            return redirect()->to($redirect)->with('error', $error);
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function active(Request $request)
    {
        $type = \request()->get('type');
        $productId = \request()->get('product_id');
        $type = in_array($type, ['shoes', 'sandals', 'raincoat', 'accessories']) ? $type : 'shoes';
        $ids = request()->post('cid', []);
        $success = 'Tắt biến thể thành công.';
        $error   = 'Tắt biến thể thất bại.';

        $redirect = UrlHelper::admin('productvariant', 'index', ['type' => $type, 'product_id' => $productId]);
        $number = $this->productVariantModel->query()->whereIn('pvariant_id', $ids)->update(['pvariant_status_show' => 'yes']);
        if($number > 0) {
            return redirect()->to($redirect)->with('success', $success);
        }else{
            return redirect()->to($redirect)->with('error', $error);
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function inactive(Request $request)
    {
        $type = \request()->get('type');
        $productId = \request()->get('product_id');
        $type = in_array($type, ['shoes', 'sandals', 'raincoat', 'accessories']) ? $type : 'shoes';
        $ids = request()->post('cid', []);
        $success = 'Tắt biến thể thành công.';
        $error   = 'Tắt biến thể thất bại.';

        $redirect = UrlHelper::admin('productvariant', 'index', ['type' => $type, 'product_id' => $productId]);
        $number = $this->productVariantModel->query()->whereIn('pvariant_id', $ids)->update(['pvariant_status_show' => 'no']);
        if($number > 0) {
            return redirect()->to($redirect)->with('success', $success);
        }else{
            return redirect()->to($redirect)->with('error', $error);
        }
    }
}
