<?php

namespace App\Http\Controllers\Backend;

use App\Http\Requests\VoucherSerieRequest;
use App\Models\Image;
use App\Models\AdminUser;
use App\Models\Voucher;
use App\Models\Product;
use App\Models\VoucherSerie;
use App\Models\ProductCategory;
use App\Models\User;
use App\Models\Order;
use App\Helpers\ImageHelper;
use App\Helpers\UrlHelper;
use App\Helpers\StringHelper;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class VoucherSerieController extends BaseController
{
    private $view = '.voucherserie';
    private $model = 'voucherserie';
    private $modelItem = 'voucher';
    private $voucherModel;
    private $voucherserieModel;
    private $productModel;
    private $productCategoryModel;
    private $imageModel;
    private $orderModel;
    private $adminUserModel;
    private $userModel;
    public function __construct()
    {
        $this->voucherModel = new Voucher();
        $this->voucherserieModel = new VoucherSerie();
        $this->productModel = new Product();
        $this->productCategoryModel = new ProductCategory();
        $this->imageModel = new Image();
        $this->adminUserModel = new AdminUser();
        $this->userModel = new User();
        $this->orderModel = new Order();
    }

    /**
     * @return Application|Factory|View
     */
    public function index()
    {
        $data['title'] = 'Quản lý phiếu giảm giá hàng loạt';
        $data['view']  = $this->viewPath . $this->view . '.list';
        $pathAvatar = config('my.path.image_voucher_serie_of_module');
        $valueAvatar = config('my.image.value.voucher_serie.avatar');

        $vouchers = $this->voucherserieModel::query()->get();

        if($vouchers->count() > 0){
            foreach ($vouchers as $key => $item){
                $item->urlAvatar = !empty($item->avatar) ? $pathAvatar . $item->avatar->image_name : '';
            }
        }
        $data['vouchers'] = $vouchers;
        return view($data['view'] , compact('data'));
    }

    /**
     * @return Application|Factory|View
     */
    public function create()
    {
        $user = Auth::guard('admin')->user();
        $data['title'] = 'Quản lý phiếu giảm giá hàng loạt[Thêm]';

        $data['view']  = $this->viewPath . $this->view . '.add';
        $data['productCategories'] = $this->productCategoryModel->get();
        $data['users'] = $this->userModel::query()->orderBy('id', 'DESC')->get();
        return view($data['view'] , compact('data'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(VoucherSerieRequest $request)
    {
        $actionType = request()->post('action_type', 'save');
        if($actionType == 'save'){
            $redirect = UrlHelper::admin($this->model);
        }else{
            $redirect = UrlHelper::admin($this->model,'create');
        }

        $success = 'Đã thêm mới phiếu giảm giá hàng loạt thành công.';
        $error   = 'Thêm mới phiếu giảm giá hàng loạt thất bại.';
        $pathAvatar = config('my.path.image_voucher_serie_of_module');
        $valueAvatar = config('my.image.value.voucher_serie.avatar');
        $pathSave = $this->model.'_m';

        $params = $this->voucherserieModel->revertAlias($request->all());
        $params['product_type'] = !empty($params['product_type']) ? json_encode($params['product_type']) : null;
        $params['product_category'] = !empty($params['product_category']) ? json_encode($params['product_category']) : null;
        $params['product_id'] = !empty($params['product_id']) ? json_encode($params['product_id']) : null;

        /**
         * lưu user được tặng voucher
         */
        $userIds = $request->userIds;
        $arrayUserIds = array();
        if(!empty($userIds) && count($userIds) > 0){
            foreach ($userIds as $key => $itemUser){
                $arrayUserIds[$itemUser] = [
                    'id' => $itemUser,
                    'time' => time(),
                ];
            }
            $params['user_ids'] = json_encode($arrayUserIds);
        }

        try {
            $voucherId = 0;
            $voucher = $this->voucherserieModel::query()->create($params);
            if($voucher){
                $voucherId = $voucher->voucher_serie_id ;
            }

            if($request->imageAvatar != null){
                $imageAvatar = $request->imageAvatar;
                ImageHelper::uploadImage($imageAvatar, $this->model, $voucherId, $valueAvatar, $pathSave);
            }


            /**
             * insert voucher items
             */
            $dataInsertVocher = [];
            $numberCreate = $request->number;
            if($numberCreate && $numberCreate > 0) {
                $arrayItem = $this->voucherModel->revertAlias($request->all());

                $arrayItem['user_ids'] = json_encode($arrayUserIds);
                $arrayItem['serie_id'] = $voucherId;
                $arrayItem['product_type'] = !empty($arrayItem['product_type']) ? json_encode($arrayItem['product_type']) : null;
                $arrayItem['product_category'] = !empty($arrayItem['product_category']) ? json_encode($arrayItem['product_category']) : null;
                $arrayItem['product_id'] = !empty($arrayItem['product_id']) ? json_encode($arrayItem['product_id']) : null;
                for ($i = 0; $i < $numberCreate; $i++) {
                    /**
                     * create code voucher
                     */
                    $arrayItem['voucher_code'] = StringHelper::createCodeVocher($request->character);
                    $dataInsertVocher[] = $arrayItem;
                }
            }
            $this->voucherModel::query()->insert($dataInsertVocher);

            /**
             * get voucher just insert
             */
            $voucherInserted = $this->voucherModel::query()->select('voucher_id')->where('serie_id', $voucherId)->get()->toArray();
            $idVoucherInserted = array_column($voucherInserted, 'voucher_id');

            $pathAvatarItem = config('my.path.image_voucher_of_module');
            $valueAvatarItem = config('my.image.value.voucher.avatar');
            $pathSaveItem = $this->modelItem.'_m';

            if($request->imageAvatar != null){
                $imageAvatar = $request->imageAvatar;
                if(!empty($idVoucherInserted) && count($idVoucherInserted) > 0){
                    foreach ($idVoucherInserted as $key => $itemId){
                        ImageHelper::uploadImage($imageAvatar, $this->modelItem, $itemId, $valueAvatarItem, $pathSaveItem);
                    }
                }
            }

            return redirect()->to($redirect)->with('success', $success);
        } catch ( \Exception $e ) {
            $redirect = UrlHelper::admin($this->model, 'create');
            return redirect()->to($redirect)->with('error', $error);
        }
    }

    /**
     * @param $id
     * @return Application|Factory|View|\Illuminate\Http\RedirectResponse
     */
    public function edit()
    {
        $id = (int) request()->get('id', 0);
        $voucher = $this->voucherserieModel::query()->where('voucher_serie_id', $id)->first();
        $pathAvatar = config('my.path.image_voucher_serie_of_module');
        $valueAvatar = config('my.image.value.voucher_serie.avatar');
        $pathSave = $this->model.'_m';

        if($voucher){
            $data['title'] = 'Quản lý phiếu giảm giá hàng loạt[Sửa]';
            $data['view']  = $this->viewPath . $this->view . '.edit';
            $data['voucher'] = $voucher;
            $data['urlAvatar'] = '';

            $productCategories = [];
            $typeArray = json_decode($voucher->product_type, 1);
            if(!empty($typeArray) && count($typeArray) > 0){
                foreach ($typeArray as $key => $item){
                    $arrayItem = $this->productCategoryModel->findByType($item)->toArray();
                    $productCategories = array_merge($productCategories, $arrayItem);
                }
            }
            $data['productCategories'] = $productCategories;
            $data['products'] = $this->productModel::query()->whereIn('product_group', array_column($productCategories, 'pcat_id'))->get();


            $vouchers = $this->voucherModel::query()->where('serie_id', $id)->get();
            $voucherIds = array_column($vouchers->toArray(), 'voucher_id');

            $data['userVouchers'] = $this->voucherModel::getUserVoucherArray($vouchers);
            $data['orders'] = $this->orderModel::getOrderByVoucherArray($voucherIds);
            $data['users'] = $this->userModel::query()->orderBy('id', 'DESC')->get();
            $data['linkSelectUser'] = UrlHelper::admin($this->model, 'select-user', ['serie' => $id]);

            $imageAvatar  = $this->imageModel->query()->where(['3rd_key' => $id, '3rd_type' => $this->model, 'image_value' => $valueAvatar])->first();
            if($imageAvatar) {
                $data['urlAvatar'] = $pathAvatar . $imageAvatar->image_name;
            }

            return view($data['view'] , compact('data'));
        }else{
            $error   = 'Không tìm thấy phiếu giảm giá hàng loạt';
            $redirect = UrlHelper::admin($this->model);
            return redirect()->to($redirect)->with('error', $error);
        }
    }

    /**
     * @param VoucherSerieRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(VoucherSerieRequest $request)
    {
        $actionType = request()->post('action_type', 'save');
        $id = request()->post('id', 0);

        if($actionType == 'save'){
            $redirect = UrlHelper::admin($this->model);
        }else{
            $redirect = UrlHelper::admin($this->model,'edit', ['id' => $id]);
        }

        $success = 'Cập nhật phiếu giảm giá hàng loạt thành công.';
        $error   = 'Cập nhật phiếu giảm giá hàng loạt thất bại.';
        $pathAvatar = config('my.path.image_voucher_serie_of_module');
        $valueAvatar = config('my.image.value.voucher_serie.avatar');
        $pathSave = $this->model.'_m';

        $params = $this->voucherserieModel->revertAlias(request()->post());
        $params['product_type'] = !empty($params['product_type']) ? json_encode($params['product_type']) : null;
        $params['product_category'] = !empty($params['product_category']) ? json_encode($params['product_category']) : null;
        $params['product_id'] = !empty($params['product_id']) ? json_encode($params['product_id']) : null;


        $voucher = $this->voucherserieModel::query()->where('voucher_serie_id', $id)->first();
        /**
         * lưu user được tặng voucher
         */
        $dataUserVoucher = json_decode($voucher->user_ids, 1);
        $UserIdsOld = !empty($dataUserVoucher && count($dataUserVoucher) > 0) ? array_column($dataUserVoucher, 'id') : [];
        $userIds = !empty($request->userIds) ? $request->userIds  : [];

        $removeIds = array_diff($UserIdsOld, $userIds);

        if(!empty($removeIds) && count($removeIds) > 0){
            foreach ($dataUserVoucher as $key => $itemVoucher){
                if(in_array($itemVoucher['id'], $removeIds)){
                    unset($dataUserVoucher[$key]);
                }
            }
        }

        $addIds = array_diff($userIds, $UserIdsOld);
        if(!empty($addIds) && count($addIds) > 0){
            foreach ($addIds as $key => $itemAddId){
                $dataUserVoucher[] = [
                    'id' => $itemAddId,
                    'time' => time(),
                ];
            }
        }
        if(!empty($dataUserVoucher) && count($dataUserVoucher) > 0){
            $params['user_ids'] = json_encode($dataUserVoucher);
        }


        try {
            $voucher->update($params);

            if($request->imageAvatar != null){
                $imageAvatar = $request->imageAvatar;

                /**check image exist*/
                $image = Image::query()->where(['3rd_key' => $id, '3rd_type' => $this->model,  'image_value' => $valueAvatar])->first();
                if($image){
                    ImageHelper::uploadUpdateImage($imageAvatar, $valueAvatar, $image->image_id, $pathSave);
                }else{
                    ImageHelper::uploadImage($imageAvatar, $this->model, $id, $valueAvatar, $pathSave);
                }
            }


            /**
             * edit voucher item
             */
            $pathAvatarItem = config('my.path.image_voucher_of_module');
            $valueAvatarItem = config('my.image.value.voucher.avatar');
            $pathSaveItem = $this->modelItem.'_m';

            $voucherItems = $this->voucherModel::query()->select('voucher_id')->where('serie_id', $id)->get();
            if(!empty($voucherItems) && $voucherItems->count() > 0) {
                $arrayItem = $this->voucherModel->revertAlias($request->all());
                $arrayItem['user_ids'] = json_encode($dataUserVoucher);
                $arrayItem['product_type'] = !empty($arrayItem['product_type']) ? json_encode($arrayItem['product_type']) : null;
                $arrayItem['product_category'] = !empty($arrayItem['product_category']) ? json_encode($arrayItem['product_category']) : null;
                $arrayItem['product_id'] = !empty($arrayItem['product_id']) ? json_encode($arrayItem['product_id']) : null;

                foreach ($voucherItems as $key => $item)
                {
                    $orders = $this->orderModel::getOrderByVoucher($item->voucher_id);
                    if($orders->count() <= 0){
                        $arrayItem['voucher_code'] = StringHelper::createCodeVocher($request->character);
                        unset($arrayItem['voucher_id']);
                        $item->update($arrayItem);

                        if($request->imageAvatar != null){
                            $imageAvatar = $request->imageAvatar;
                            ImageHelper::uploadImage($imageAvatar, $this->modelItem, $item->voucher_id, $valueAvatarItem, $pathSaveItem);
                        }

                    }
                }
            }


            return redirect()->to($redirect)->with('success', $success);
        } catch ( \Exception $e ) {
            $redirect = UrlHelper::admin($this->model);
            return redirect()->to($redirect)->with('error', $error);
        }
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete(Request $request)
    {
        $ids = request()->post('cid', []);
        $success = 'Xóa phiếu giảm giá hàng loạt thành công.';
        $error   = 'Xóa phiếu giảm giá hàng loạt thất bại.';

        $redirect = UrlHelper::admin($this->model);
        $number = $this->voucherserieModel->query()->whereIn('voucher_serie_id', $ids)->update(['voucher_serie_is_delete' => 'yes']);
        if($number > 0) {
            return redirect()->to($redirect)->with('success', $success);
        }else{
            return redirect()->to($redirect)->with('error', $error);
        }
    }

    /**
     * @param Request $request
     * @return Application|Factory|View
     */
    public function selectUser(Request $request){
        $serie = (int) $_GET['serie'];
        $data['title'] = 'Quản lý phiếu giảm giá hàng loạt [Tặng voucher]';
        $data['view']  = $this->viewPath . $this->view . '.list_user';
        $redirect = UrlHelper::admin($this->model,'edit', ['id' => $serie]);

        $voucherSerie = $this->voucherserieModel::query()->where('voucher_serie_id', $serie)->first();
        if($voucherSerie){
            if(isset($_GET['value'])){
                $value = $_GET['value'];
                $data['users'] = $this->userModel::query()->orderBy('id', 'DESC')
                    ->where('name', 'like' ,'%'.$value.'%')
                    ->orWhere('email', 'like','%'.$value.'%')
                    ->orWhere('phone', 'like','%'.$value.'%')
                    ->paginate(50);
            }else{
                $data['users'] = $this->userModel::query()->orderBy('id', 'DESC')->paginate(500);
            }
            $userIds  = !empty(!empty($voucherSerie) && $voucherSerie->user_ids) ? json_decode($voucherSerie->user_ids, 1) : [];
            $data['userIds'] =  !empty($userIds && count($userIds) > 0) ? array_column($userIds, 'id') : [];
            $data['voucherSerieId'] = $serie;
            return view($data['view'] , compact('data'));
        }else{
            $error   = 'không tìm thấy phiếu giảm giá';
            return redirect()->to($redirect)->with('error', $error);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postSelectUser(Request $request){
        $userIds = request()->post('cid', []);
        $voucherSerieId = (int) request()->post('voucherSerieId', 0);

        $redirect = UrlHelper::admin($this->model, 'select-user', ['serie' => $voucherSerieId]);
        $success = 'Cập nhật thông tin tặng phiếu giảm giá hàng loạt thành công.';
        $error   = 'Cập nhật thông tin tặng phiếu giảm giá hàng loạt thất bại.';

        $voucherSerie = $this->voucherserieModel::query()->where('voucher_serie_id', $voucherSerieId)->first();
        if($voucherSerie){
            $dataUserVoucher = json_decode($voucherSerie->user_ids, 1);
            $UserIdsOld = !empty($dataUserVoucher && count($dataUserVoucher) > 0) ? array_column($dataUserVoucher, 'id') : [];

            $removeIds = array_diff($UserIdsOld, $userIds);

            if(!empty($removeIds) && count($removeIds) > 0){
                foreach ($dataUserVoucher as $key => $itemVoucher){
                    if(in_array($itemVoucher['id'], $removeIds)){
                        unset($dataUserVoucher[$key]);
                    }
                }
            }

            $addIds = array_diff($userIds, $UserIdsOld);
            if(!empty($addIds) && count($addIds) > 0){
                foreach ($addIds as $key => $itemAddId){
                    $dataUserVoucher[] = [
                        'id' => $itemAddId,
                        'time' => time(),
                    ];
                }
            }
            if(!empty($dataUserVoucher) && count($dataUserVoucher) > 0){
                $userIds = json_encode($dataUserVoucher);
            }
            $result = $voucherSerie->update(['user_ids' => $userIds]);

            /**
             * edit voucher item
             */
            $voucherItems = $this->voucherModel::query()->select('voucher_id')->where('serie_id', $voucherSerieId)->get();
            if(!empty($voucherItems) && $voucherItems->count() > 0) {
                $arrayItem = $this->voucherModel->revertAlias($request->all());
                $arrayItem['voucher_code'] = StringHelper::createCodeVocher($request->character);
                $arrayItem['user_ids'] = json_encode($dataUserVoucher);

                foreach ($voucherItems as $key => $item)
                {
                    $orders = $this->orderModel::getOrderByVoucher($item->voucher_id);
                    if($orders->count() <= 0){
                        $arrayItem['voucher_code'] = StringHelper::createCodeVocher($request->character);
                        unset($arrayItem['voucher_id']);
                        $item->update($arrayItem);
                    }
                }
            }

            if($result){
                return redirect()->to($redirect)->with('success', $success);
            }else{
                return redirect()->to($redirect)->with('error', $error);
            }
        }else{
            $error   = 'không tìm thấy phiếu giảm giá';
            return redirect()->to($redirect)->with('error', $error);
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function active(Request $request)
    {
        $ids = request()->post('cid', []);
        $success = 'Bật phiếu giảm giá hàng loạt thành công.';
        $error   = 'Bật phiếu giảm giá hàng loạt thất bại.';

        $redirect = UrlHelper::admin($this->model);
        $number = $this->voucherserieModel->query()->whereIn('voucher_serie_id', $ids)->update(['voucher_serie_status' => 'activated']);
        if($number > 0) {
            return redirect()->to($redirect)->with('success', $success);
        }else{
            return redirect()->to($redirect)->with('error', $error);
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function inactive(Request $request)
    {
        $ids = request()->post('cid', []);
        $success = 'Tắt phiếu giảm giá hàng loạt thành công.';
        $error   = 'Tắt phiếu giảm giá hàng loạt thất bại.';

        $redirect = UrlHelper::admin($this->model);
        $number = $this->voucherserieModel->query()->whereIn('voucher_serie_id', $ids)->update(['voucher_serie_status' => 'inactive']);
        if($number > 0) {
            return redirect()->to($redirect)->with('success', $success);
        }else{
            return redirect()->to($redirect)->with('error', $error);
        }
    }
}
