<?php

namespace App\Http\Controllers\Backend;

use App\Models\Question;
use App\Models\QuestionGroup;
use App\Helpers\UrlHelper;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use App\Http\Requests\QuestionRequest;
use Illuminate\Http\Request;

class QuestionController extends BaseController
{
    private $view = '.question';
    private $model = 'question';
    private $questionModel;
    private $questionGroupModel;
    public function __construct()
    {
        $this->questionModel = new Question();
        $this->questionGroupModel = new QuestionGroup();
    }

    /**
     * @return Application|Factory|View
     */
    public function index()
    {
        $group = \request()->get('group');
        $data['title'] = 'Quản lý câu hỏi';
        $data['view']  = $this->viewPath . $this->view . '.list';
        $data['group'] = $group;

        $items = $this->questionModel::query()->where('group_id', $group)->orderBy('question_number', 'asc')->get();
        if($items->count() > 0){
            foreach ($items as $key => $item){
                $group  = $this->questionGroupModel->query()->where('questiongroup_id', $item->group_id)->first();
                $item->group = !empty($group) ? $group->questiongroup_name : '';
            }
        }
        $data['items'] = $items;
        return view($data['view'] , compact('data'));
    }

    /**
     * @return Application|Factory|View
     */
    public function create()
    {
        $group = \request()->get('group');
        $data['title'] = 'Quản lý câu hỏi: [Thêm]';
        $data['view']  = $this->viewPath . $this->view . '.add';
        $data['items'] = $this->questionModel::query()->where('group_id', $group)->where('parent_id', 0)->get();
        $data['group'] = $group;
        return view($data['view'] , compact('data'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(QuestionRequest $request)
    {
        $group = !empty($request->group) ? $request->group : 0;

        $actionType = request()->post('action_type', 'save');
        if($actionType == 'save'){
            $redirect = UrlHelper::admin($this->model, 'index',['group' => $group]);
        }else{
            $redirect = UrlHelper::admin($this->model,'create', ['group' => $group]);
        }

        $success = 'Đã thêm mới câu hỏi thành công.';
        $error   = 'Thêm mới câu hỏi thất bại.';
        $params = $this->questionModel->revertAlias($request->all());

        if(!empty($params['parent_id'])){
            $params['question_level'] = 2;
        }
        $params['group_id'] = $group;

        try {
            $item = $this->questionModel::query()->create($params);

            return redirect()->to($redirect)->with('success', $success);
        } catch ( \Exception $e ) {
            $redirect = UrlHelper::admin($this->model, 'create', ['group' => $group]);
            return redirect()->to($redirect)->with('error', $error);
        }
    }


    /**
     * @param $id
     * @return Application|Factory|View|\Illuminate\Http\RedirectResponse
     */
    public function edit()
    {
        $group = \request()->get('group');
        $id = (int) request()->get('id', 0);
        $item = $this->questionModel::query()->where('question_id', $id)->first();
        if($item){
            $data['title'] = 'Quản lý câu hỏi: [Sửa]';
            $data['view']  = $this->viewPath . $this->view . '.edit';
            $data['item'] = $item;
            $data['items'] = $this->questionModel::query()->where('group_id', $group)
                                                      ->where('question_id', '<>', $id)
                                                      ->where('parent_id', 0)
                                                      ->get();
            $data['group'] = $group;
            $data['current_question_id'] = $id;

            return view($data['view'] , compact('data'));
        }else{
            $error   = 'Không tìm thấy câu hỏi';
            $redirect = UrlHelper::admin($this->model, 'index', ['group' => $group]);
            return redirect()->to($redirect)->with('error', $error);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(QuestionRequest $request)
    {
        $actionType = request()->post('action_type', 'save');
        $id = request()->post('id', 0);
        $group = !empty($request->group) ? $request->group : 0;

        if($actionType == 'save'){
            $redirect = UrlHelper::admin($this->model, 'index', ['group' => $group]);
        }else{
            $redirect = UrlHelper::admin($this->model,'edit', ['id' => $id, 'group' => $group]);
        }

        $success = 'Cập nhật câu hỏi thành công.';
        $error   = 'Cập nhật câu hỏi thất bại.';

        $params = $this->questionModel->revertAlias(request()->post());
        if(!empty($params['parent_id'])){
            $params['question_level'] = 2;
        }
        $params['group_id'] = $group;

        try {
            if ($params['parent_id'] > 0) {
                $questionChilds = $this->questionModel::parentQuery()->where('parent_id', $id)->get();
                if (count($questionChilds)) {
                    foreach ($questionChilds as $questionChild) {
                        $questionChild->update(['parent_id' => $params['parent_id']]);
                    }
                }
            }
            $this->questionModel::query()->where('question_id', $id)->update($params);
            return redirect()->to($redirect)->with('success', $success);
        } catch ( \Exception $e ) {
            $redirect = UrlHelper::admin($this->model, 'index', ['group' => $group]);
            return redirect()->to($redirect)->with('error', $error);
        }
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete(Request $request)
    {
        $group = !empty($request->group) ? $request->group : 0;
        $ids = request()->post('cid', []);
        $success = 'Xóa câu hỏi thành công.';
        $error   = 'Xóa câu hỏi thất bại.';

        $redirect = UrlHelper::admin($this->model, 'index', ['group' => $group]);
        $number = $this->questionModel->query()->whereIn('question_id', $ids)->update(['question_is_delete' => 'yes']);
        if($number > 0) {
            return redirect()->to($redirect)->with('success', $success);
        }else{
            return redirect()->to($redirect)->with('error', $error);
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function active(Request $request)
    {
        $group = !empty($request->group) ? $request->group : 0;
        $ids = request()->post('cid', []);
        $success = 'Bật câu hỏi thành công.';
        $error   = 'Bật câu hỏi thất bại.';

        $redirect = UrlHelper::admin($this->model, 'index', ['group' => $group]);
        $number = $this->questionModel->query()->whereIn('question_id', $ids)->update(['question_status' => 'activated']);
        if($number > 0) {
            return redirect()->to($redirect)->with('success', $success);
        }else{
            return redirect()->to($redirect)->with('error', $error);
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function inactive(Request $request)
    {
        $group = !empty($request->group) ? $request->group : 0;
        $ids = request()->post('cid', []);
        $success = 'Tắt câu hỏi thành công.';
        $error   = 'Tắt câu hỏi thất bại.';

        $redirect = UrlHelper::admin($this->model, 'index', ['group' => $group]);
        $number = $this->questionModel->query()->whereIn('question_id', $ids)->update(['question_status' => 'inactive']);
        if($number > 0) {
            return redirect()->to($redirect)->with('success', $success);
        }else{
            return redirect()->to($redirect)->with('error', $error);
        }
    }
}
