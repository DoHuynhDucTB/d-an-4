<?php

namespace App\Http\Controllers\Backend;

use App\Http\Requests\BannerGroupRequest;
use App\Models\BannerGroup;
use App\Helpers\UrlHelper;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;

class BannergroupController extends BaseController
{
    private $view = '.bannergroup';
    private $model = 'bannergroup';
    private $bannerGroupModel;
    public function __construct()
    {
        $this->bannerGroupModel = new BannerGroup();
    }

    /**
     * @return Application|Factory|View
     */
    public function index()
    {
        $data['title'] = 'Quản lý nhóm banner';
        $data['view']  = $this->viewPath . $this->view . '.list';

        $groups = $this->bannerGroupModel::query()->get();
        $data['groups'] = $groups;
        return view($data['view'] , compact('data'));
    }

    /**
     * @return Application|Factory|View
     */
    public function create()
    {
        $data['title'] = 'Quản lý nhóm banner[Thêm]';
        $data['view']  = $this->viewPath . $this->view . '.add';
        return view($data['view'] , compact('data'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(BannerGroupRequest $request)
    {
        $actionType = request()->post('action_type', 'save');
        if($actionType == 'save'){
            $redirect = UrlHelper::admin($this->model);
        }else{
            $redirect = UrlHelper::admin($this->model,'create');
        }

        $success = 'Đã thêm mới nhóm banner thành công.';
        $error   = 'Thêm mới nhóm banner thất bại.';

        $params = $this->bannerGroupModel->revertAlias($request->all());


        try {
            $bannerGroup = $this->bannerGroupModel::query()->create($params);

            return redirect()->to($redirect)->with('success', $success);
        } catch ( \Exception $e ) {
            $redirect = UrlHelper::admin($this->model, 'create');
            return redirect()->to($redirect)->with('error', $error);
        }
    }

    /**
     * @param $id
     * @return Application|Factory|View|\Illuminate\Http\RedirectResponse
     */
    public function edit()
    {
        $id = (int) request()->get('id', 0);
        $bannerGroup = $this->bannerGroupModel::query()->where('bangroup_id', $id)->first();
        if($bannerGroup){
            $data['title'] = 'Quản lý nhóm banner[Sửa]';
            $data['view']  = $this->viewPath . $this->view . '.edit';
            $data['bannerGroup'] = $bannerGroup;

            return view($data['view'] , compact('data'));
        }else{
            $error   = 'Không tìm thấy nhóm banner';
            $redirect = UrlHelper::admin($this->model);
            return redirect()->to($redirect)->with('error', $error);
        }
    }

    /**
     * @param BannerGroupRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(BannerGroupRequest $request)
    {
        $actionType = request()->post('action_type', 'save');
        $id = request()->post('id', 0);

        if($actionType == 'save'){
            $redirect = UrlHelper::admin($this->model);
        }else{
            $redirect = UrlHelper::admin($this->model,'edit', ['id' => $id]);
        }

        $success = 'Cập nhật nhóm banner thành công.';
        $error   = 'Cập nhật nhóm banner thất bại.';
        $params = $this->bannerGroupModel->revertAlias(request()->post());

        try {
            $this->bannerGroupModel::query()->where('bangroup_id', $id)->update($params);

            return redirect()->to($redirect)->with('success', $success);
        } catch ( \Exception $e ) {
            $redirect = UrlHelper::admin($this->model);
            return redirect()->to($redirect)->with('error', $error);
        }
    }
}
