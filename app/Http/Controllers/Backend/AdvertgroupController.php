<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\AdvertRequest;
use App\Models\Image;
use App\Models\AdminUser;
use App\Models\AdvertGroup;
use App\Helpers\ImageHelper;
use App\Helpers\UrlHelper;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdvertgroupController extends BaseController
{
    private $view = '.advertgroup';
    private $model = 'advertgroup';
    private $advertGroupModel;
    private $imageModel;
    private $adminUserModel;
    public function __construct()
    {
        $this->advertGroupModel = new AdvertGroup();
        $this->imageModel = new Image();
        $this->adminUserModel = new AdminUser();
    }

    /**
     * @return Application|Factory|View
     */
    public function index()
    {
        $data['title'] = 'Quản lý nhóm quảng cáo ';
        $data['view']  = $this->viewPath . $this->view . '.list';

        $groups = $this->advertGroupModel::query()->get();
        $data['groups'] = $groups;
        return view($data['view'] , compact('data'));
    }

    /**
     * @return Application|Factory|View
     */
    public function create()
    {
        $user = Auth::guard('admin')->user();
        $data['title'] = 'Quản lý nhóm quảng cáo[Thêm]';
        $data['view']  = $this->viewPath . $this->view . '.add';
        $data['adminName']  = $user->username;
        return view($data['view'] , compact('data'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(AdvertRequest $request)
    {
        $actionType = request()->post('action_type', 'save');
        if($actionType == 'save'){
            $redirect = UrlHelper::admin($this->model);
        }else{
            $redirect = UrlHelper::admin($this->model,'create');
        }

        $success = 'Đã thêm mới nhóm quảng cáo thành công.';
        $error   = 'Thêm mới nhóm quảng cáo thất bại.';

        $user = Auth::guard('admin')->user();
        $params = $this->advertGroupModel->revertAlias($request->all());
        $params['adgroup_created_by'] = $user->aduser_id;

        try {
            $adgroup = $this->advertGroupModel::query()->create($params);

            return redirect()->to($redirect)->with('success', $success);
        } catch ( \Exception $e ) {
            $redirect = UrlHelper::admin($this->model, 'create');
            return redirect()->to($redirect)->with('error', $error);
        }
    }

    /**
     * @return Application|Factory|View|\Illuminate\Http\RedirectResponse
     */
    public function copy()
    {
        $ids = request()->post('cid', []);
        $id = isset($ids[0]) ? $ids[0] : 0;
        $user = Auth::guard('admin')->user();

        $adgroup = $this->advertGroupModel::query()->where('adgroup_id', $id)->first();
        if($adgroup){
            $data['title'] = 'Quản lý nhóm quảng cáo[Sao Chép]';
            $data['view']  = $this->viewPath . $this->view . '.copy';
            $data['adminName'] = !empty($creater) ? $creater->username : '';
            $data['adgroup'] = $adgroup;

            return view($data['view'] , compact('data'));
        }else{
            $error   = 'Không tìm thấy nhóm quảng cáo';
            $redirect = UrlHelper::admin($this->model);
            return redirect()->to($redirect)->with('error', $error);
        }
    }


    /**
     * @param $id
     * @return Application|Factory|View|\Illuminate\Http\RedirectResponse
     */
    public function edit()
    {
        $id = (int) request()->get('id', 0);
        $adgroup = $this->advertGroupModel::query()->where('adgroup_id', $id)->first();
        if($adgroup){
            $creater  = $this->adminUserModel::query()->where('aduser_id', $adgroup->adgroup_created_by)->first();
            $data['title'] = 'Quản lý nhóm quảng cáo[Sửa]';
            $data['view']  = $this->viewPath . $this->view . '.edit';
            $data['adgroup'] = $adgroup;
            $data['adminName'] = !empty($creater) ? $creater->username : '';

            return view($data['view'] , compact('data'));
        }else{
            $error   = 'Không tìm thấy nhóm quảng cáo';
            $redirect = UrlHelper::admin($this->model);
            return redirect()->to($redirect)->with('error', $error);
        }
    }

    /**
     * @param AdvertRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(AdvertRequest $request)
    {
        $actionType = request()->post('action_type', 'save');
        $id = request()->post('id', 0);

        if($actionType == 'save'){
            $redirect = UrlHelper::admin($this->model);
        }else{
            $redirect = UrlHelper::admin($this->model,'edit', ['id' => $id]);
        }

        $success = 'Cập nhật nhóm quảng cáo thành công.';
        $error   = 'Cập nhật nhóm quảng cáo thất bại.';
        $params = $this->advertGroupModel->revertAlias(request()->post());

        try {
            $this->advertGroupModel::query()->where('adgroup_id', $id)->update($params);

            return redirect()->to($redirect)->with('success', $success);
        } catch ( \Exception $e ) {
            $redirect = UrlHelper::admin($this->model);
            return redirect()->to($redirect)->with('error', $error);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function duplicate(AdvertRequest $request)
    {
        $user = Auth::guard('admin')->user();
        $actionType = request()->post('action_type', 'save');
        $id = request()->post('id', 0);

        if($actionType == 'save'){
            $redirect = UrlHelper::admin($this->model);
        }else{
            $redirect = UrlHelper::admin($this->model,'edit', ['id' => $id]);
        }

        $success = 'Sao chép nhóm quảng cáo thành công.';
        $error   = 'Sao chép nhóm quảng cáo thất bại.';

        $params = $this->advertGroupModel->revertAlias($request->all());
        $params['advert_created_by'] = $user->aduser_id;
        unset($params['adgroup_id']);

        try {
            $adgroup = $this->advertGroupModel::query()->create($params);

            return redirect()->to($redirect)->with('success', $success);
        } catch ( \Exception $e ) {
            $redirect = UrlHelper::admin($this->model);
            return redirect()->to($redirect)->with('error', $error);
        }
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete(Request $request)
    {
        $ids = request()->post('cid', []);
        $success = 'Xóa quảng cáo thành công.';
        $error   = 'Xóa quảng cáo thất bại.';

        $redirect = UrlHelper::admin($this->model);
        $number = $this->advertGroupModel->query()->whereIn('adgroup_id', $ids)->update(['adgroup_is_delete' => 'yes']);
        if($number > 0) {
            return redirect()->to($redirect)->with('success', $success);
        }else{
            return redirect()->to($redirect)->with('error', $error);
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function active(Request $request)
    {
        $ids = request()->post('cid', []);
        $success = 'Bật nhóm quảng cáo thành công.';
        $error   = 'Bật nhóm quảng cáo thất bại.';

        $redirect = UrlHelper::admin($this->model);
        $number = $this->advertGroupModel->query()->whereIn('adgroup_id', $ids)->update(['adgroup_status' => 'activated']);
        if($number > 0) {
            return redirect()->to($redirect)->with('success', $success);
        }else{
            return redirect()->to($redirect)->with('error', $error);
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function inactive(Request $request)
    {
        $ids = request()->post('cid', []);
        $success = 'Tắt nhóm quảng cáo thành công.';
        $error   = 'Tắt nhóm quảng cáo thất bại.';

        $redirect = UrlHelper::admin($this->model);
        $number = $this->advertGroupModel->query()->whereIn('adgroup_id', $ids)->update(['adgroup_status' => 'inactive']);
        if($number > 0) {
            return redirect()->to($redirect)->with('success', $success);
        }else{
            return redirect()->to($redirect)->with('error', $error);
        }
    }
}
