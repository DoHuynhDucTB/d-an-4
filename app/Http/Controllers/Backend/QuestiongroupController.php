<?php

namespace App\Http\Controllers\Backend;

use App\Models\QuestionGroup;
use App\Http\Requests\QuestionGroupRequest;
use App\Helpers\UrlHelper;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;

class QuestiongroupController extends BaseController
{
    private $view = '.questiongroup';
    private $model = 'questiongroup';
    private $questionGroupModel;
    public function __construct()
    {
        $this->questionGroupModel = new questionGroup();
    }

    /**
     * @return Application|Factory|View
     */
    public function index()
    {
        $data['title'] = 'Quản lý nhóm câu hỏi ';
        $data['view']  = $this->viewPath . $this->view . '.list';

        $groups = $this->questionGroupModel::query()->get();
        $data['groups'] = $groups;
        return view($data['view'] , compact('data'));
    }

    /**
     * @return Application|Factory|View
     */
    public function create()
    {
        $data['title'] = 'Quản lý nhóm câu hỏi[Thêm]';
        $data['view']  = $this->viewPath . $this->view . '.add';
        return view($data['view'] , compact('data'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(questionGroupRequest $request)
    {
        $actionType = request()->post('action_type', 'save');
        if($actionType == 'save'){
            $redirect = UrlHelper::admin($this->model);
        }else{
            $redirect = UrlHelper::admin($this->model,'create');
        }

        $success = 'Đã thêm mới nhóm câu hỏi thành công.';
        $error   = 'Thêm mới nhóm câu hỏi thất bại.';
        $params = $this->questionGroupModel->revertAlias($request->all());

        try {
            $group = $this->questionGroupModel::query()->create($params);

            return redirect()->to($redirect)->with('success', $success);
        } catch ( \Exception $e ) {
            $redirect = UrlHelper::admin($this->model, 'create');
            return redirect()->to($redirect)->with('error', $error);
        }
    }

    /**
     * @param $id
     * @return Application|Factory|View|\Illuminate\Http\RedirectResponse
     */
    public function edit()
    {
        $id = (int) request()->get('id', 0);
        $group = $this->questionGroupModel::query()->where('questiongroup_id', $id)->first();
        if($group){
            $data['title'] = 'Quản lý nhóm câu hỏi[Sửa]';
            $data['view']  = $this->viewPath . $this->view . '.edit';
            $data['group'] = $group;
            return view($data['view'] , compact('data'));
        }else{
            $error   = 'Không tìm thấy nhóm câu hỏi';
            $redirect = UrlHelper::admin($this->model);
            return redirect()->to($redirect)->with('error', $error);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(questionGroupRequest $request)
    {
        $actionType = request()->post('action_type', 'save');
        $id = request()->post('id', 0);

        if($actionType == 'save'){
            $redirect = UrlHelper::admin($this->model);
        }else{
            $redirect = UrlHelper::admin($this->model,'edit', ['id' => $id]);
        }

        $success = 'Cập nhật nhóm câu hỏi thành công.';
        $error   = 'Cập nhật nhóm câu hỏi thất bại.';
        $params = $this->questionGroupModel->revertAlias(request()->post());

        try {
            $this->questionGroupModel::query()->where('questiongroup_id', $id)->update($params);

            return redirect()->to($redirect)->with('success', $success);
        } catch ( \Exception $e ) {
            $redirect = UrlHelper::admin($this->model);
            return redirect()->to($redirect)->with('error', $error);
        }
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete(Request $request)
    {
        $ids = request()->post('cid', []);
        $success = 'Xóa nhóm câu hỏi thành công.';
        $error   = 'Xóa nhóm câu hỏi thất bại.';

        $redirect = UrlHelper::admin($this->model);
        $number = $this->questionGroupModel->query()->whereIn('questiongroup_id', $ids)->update(['questiongroup_is_delete' => 'yes']);
        if($number > 0) {
            return redirect()->to($redirect)->with('success', $success);
        }else{
            return redirect()->to($redirect)->with('error', $error);
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function active(Request $request)
    {
        $ids = request()->post('cid', []);
        $success = 'Bật nhóm câu hỏi thành công.';
        $error   = 'Bật nhóm câu hỏi thất bại.';

        $redirect = UrlHelper::admin($this->model);
        $number = $this->questionGroupModel->query()->whereIn('questiongroup_id', $ids)->update(['questiongroup_status' => 'activated']);
        if($number > 0) {
            return redirect()->to($redirect)->with('success', $success);
        }else{
            return redirect()->to($redirect)->with('error', $error);
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function inactive(Request $request)
    {
        $ids = request()->post('cid', []);
        $success = 'Tắt nhóm câu hỏi thành công.';
        $error   = 'Tắt nhóm câu hỏi thất bại.';

        $redirect = UrlHelper::admin($this->model);
        $number = $this->questionGroupModel->query()->whereIn('questiongroup_id', $ids)->update(['questiongroup_status' => 'inactive']);
        if($number > 0) {
            return redirect()->to($redirect)->with('success', $success);
        }else{
            return redirect()->to($redirect)->with('error', $error);
        }
    }
}
