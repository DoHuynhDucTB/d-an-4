<?php

namespace App\Http\Controllers\Backend;

use App\Models\Menu;
use App\Models\MenuGroup;
use App\Helpers\UrlHelper;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use App\Http\Requests\MenuRequest;
use Illuminate\Http\Request;

class MenuController extends BaseController
{
    private $view = '.menu';
    private $model = 'menu';
    private $menuModel;
    private $menuGroupModel;
    public function __construct()
    {
        $this->menuModel = new Menu();
        $this->menuGroupModel = new MenuGroup();
    }

    /**
     * @return Application|Factory|View
     */
    public function index()
    {
        $group = \request()->get('group');
        $data['title'] = 'Quản lý menu items';
        $data['view']  = $this->viewPath . $this->view . '.list';
        $data['group'] = $group;

        $items = $this->menuModel::query()->where('group_id', $group)->orderBy('menu_number', 'asc')->get();
        if($items->count() > 0){
            foreach ($items as $key => $item){
                $group  = $this->menuGroupModel->query()->where('menugroup_id', $item->group_id)->first();
                $item->group = !empty($group) ? $group->menugroup_name : '';
            }
        }
        $data['items'] = $items;
        return view($data['view'] , compact('data'));
    }

    /**
     * @return Application|Factory|View
     */
    public function create()
    {
        $group = \request()->get('group');
        $data['title'] = 'Quản lý menu items: [Thêm]';
        $data['view']  = $this->viewPath . $this->view . '.add';
        $data['items'] = $this->menuModel::query()->where('group_id', $group)->where('parent_id', 0)->get();
        $data['group'] = $group;
        return view($data['view'] , compact('data'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(MenuRequest $request)
    {
        $group = !empty($request->group) ? $request->group : 0;

        $actionType = request()->post('action_type', 'save');
        if($actionType == 'save'){
            $redirect = UrlHelper::admin($this->model, 'index',['group' => $group]);
        }else{
            $redirect = UrlHelper::admin($this->model,'create', ['group' => $group]);
        }

        $success = 'Đã thêm mới menu items thành công.';
        $error   = 'Thêm mới menu items thất bại.';
        $params = $this->menuModel->revertAlias($request->all());
        $params['group_id'] = $group;

        try {
            $item = $this->menuModel::query()->create($params);

            return redirect()->to($redirect)->with('success', $success);
        } catch ( \Exception $e ) {
            $redirect = UrlHelper::admin($this->model, 'create', ['group' => $group]);
            return redirect()->to($redirect)->with('error', $error);
        }
    }

    /**
     * @return Application|Factory|View|\Illuminate\Http\RedirectResponse
     */
    public function copy()
    {
        $ids = request()->post('cid', []);
        $id = isset($ids[0]) ? $ids[0] : 0;
        $group = \request()->get('group');

        $item = $this->menuModel::query()->where('menu_id', $id)->first();
        if($item){
            $data['title'] = 'Quản lý menu items[Sao Chép]';
            $data['view']  = $this->viewPath . $this->view . '.copy';
            $data['item'] = $item;
            $data['items'] = $this->menuModel::query()->where('group_id', $group)->get();
            $data['group'] = $group;

            return view($data['view'] , compact('data'));
        }else{
            $error   = 'Không tìm thấy menu items';
            $redirect = UrlHelper::admin($this->model, 'index', ['group' => $group]);
            return redirect()->to($redirect)->with('error', $error);
        }
    }


    /**
     * @param $id
     * @return Application|Factory|View|\Illuminate\Http\RedirectResponse
     */
    public function edit()
    {
        $group = \request()->get('group');
        $id = (int) request()->get('id', 0);
        $item = $this->menuModel::query()->where('menu_id', $id)->first();
        if($item){
            $data['title'] = 'Quản lý menu items: [Sửa]';
            $data['view']  = $this->viewPath . $this->view . '.edit';
            $data['item'] = $item;
            $data['items'] = $this->menuModel::query()->where('group_id', $group)
                                                      ->where('menu_id', '<>', $id)
                                                      ->where('parent_id', 0)
                                                      ->get();
            $data['group'] = $group;
            $data['current_menu_id'] = $id;

            return view($data['view'] , compact('data'));
        }else{
            $error   = 'Không tìm thấy menu items';
            $redirect = UrlHelper::admin($this->model, 'index', ['group' => $group]);
            return redirect()->to($redirect)->with('error', $error);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(MenuRequest $request)
    {
        $actionType = request()->post('action_type', 'save');
        $id = request()->post('id', 0);
        $group = !empty($request->group) ? $request->group : 0;

        if($actionType == 'save'){
            $redirect = UrlHelper::admin($this->model, 'index', ['group' => $group]);
        }else{
            $redirect = UrlHelper::admin($this->model,'edit', ['id' => $id, 'group' => $group]);
        }

        $success = 'Cập nhật menu items thành công.';
        $error   = 'Cập nhật menu items thất bại.';

        $params = $this->menuModel->revertAlias(request()->post());
        $params['group_id'] = $group;

        try {
            if ($params['parent_id'] > 0) {
                $menuChilds = $this->menuModel::parentQuery()->where('parent_id', $id)->get();
                if (count($menuChilds)) {
                    foreach ($menuChilds as $menuChild) {
                        $menuChild->update(['parent_id' => $params['parent_id']]);
                    }
                }
            }
            $this->menuModel::query()->where('menu_id', $id)->update($params);
            return redirect()->to($redirect)->with('success', $success);
        } catch ( \Exception $e ) {
            $redirect = UrlHelper::admin($this->model, 'index', ['group' => $group]);
            return redirect()->to($redirect)->with('error', $error);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function duplicate(MenuRequest $request)
    {
        $actionType = request()->post('action_type', 'save');
        $id = request()->post('id', 0);
        $group = !empty($request->group) ? $request->group : 0;

        if($actionType == 'save'){
            $redirect = UrlHelper::admin($this->model, 'index', ['group' => $group]);
        }else{
            $redirect = UrlHelper::admin($this->model,'edit', ['id' => $id, 'group' => $group]);
        }

        $success = 'Sao chép menu items thành công.';
        $error   = 'Sao chép menu items thất bại.';

        $params = $this->menuModel->revertAlias($request->all());
        $params['group_id'] = $group;
        unset($params['menu_id']);

        try {
            $item = $this->menuModel::query()->create($params);

            return redirect()->to($redirect)->with('success', $success);
        } catch ( \Exception $e ) {
            $redirect = UrlHelper::admin($this->model, 'index', ['group' => $group]);
            return redirect()->to($redirect)->with('error', $error);
        }
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete(Request $request)
    {
        $group = !empty($request->group) ? $request->group : 0;
        $ids = request()->post('cid', []);
        $success = 'Xóa menu items thành công.';
        $error   = 'Xóa menu items thất bại.';

        $redirect = UrlHelper::admin($this->model, 'index', ['group' => $group]);
        $number = $this->menuModel->query()->whereIn('menu_id', $ids)->update(['menu_is_delete' => 'yes']);
        if($number > 0) {
            return redirect()->to($redirect)->with('success', $success);
        }else{
            return redirect()->to($redirect)->with('error', $error);
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function active(Request $request)
    {
        $group = !empty($request->group) ? $request->group : 0;
        $ids = request()->post('cid', []);
        $success = 'Bật menu items thành công.';
        $error   = 'Bật menu items thất bại.';

        $redirect = UrlHelper::admin($this->model, 'index', ['group' => $group]);
        $number = $this->menuModel->query()->whereIn('menu_id', $ids)->update(['menu_status' => 'activated']);
        if($number > 0) {
            return redirect()->to($redirect)->with('success', $success);
        }else{
            return redirect()->to($redirect)->with('error', $error);
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function inactive(Request $request)
    {
        $group = !empty($request->group) ? $request->group : 0;
        $ids = request()->post('cid', []);
        $success = 'Tắt menu items thành công.';
        $error   = 'Tắt menu items thất bại.';

        $redirect = UrlHelper::admin($this->model, 'index', ['group' => $group]);
        $number = $this->menuModel->query()->whereIn('menu_id', $ids)->update(['menu_status' => 'inactive']);
        if($number > 0) {
            return redirect()->to($redirect)->with('success', $success);
        }else{
            return redirect()->to($redirect)->with('error', $error);
        }
    }
}
