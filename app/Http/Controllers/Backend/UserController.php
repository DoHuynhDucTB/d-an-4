<?php

namespace App\Http\Controllers\Backend;

use App\Helpers\UrlHelper;
use App\Http\Controllers\Controller;
use App\Models\ProductCare;
use App\Models\ProductMakeUp;
use App\Models\ProductSkin;
use App\Models\User;
use App\Models\Province;
use App\Models\District;
use App\Models\Ward;
use Illuminate\Http\Request;
use App\Http\Requests\UserRequest;
use Illuminate\Support\Arr;

class UserController extends BaseController
{
    private $view = '.user';
    private $model = 'user';
    private $userModel;
    private $provinceModel;
    private $districtModel;
    private $wardModel;
    private $productMakeUpModel;
    private $productCareModel;
    private $productSkinModel;



    public function __construct()
    {
        $this->userModel = new User();
        $this->provinceModel = new Province();
        $this->districtModel = new District();
        $this->wardModel = new Ward();
        $this->productMakeUpModel = new ProductMakeUp();
        $this->productCareModel = new ProductCare();
        $this->productSkinModel = new ProductSkin();
    }


    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        $data['title'] = 'Quản lý người dùng';
        $data['view']  = $this->viewPath . $this->view . '.list';

        $users = $this->userModel::query()->orderBy('id', 'DESC')->paginate(50);
        $data['users'] = $users;
        return view($data['view'] , compact('data'));
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse
     */
    public function edit()
    {
        $productSkinModel = $this->productSkinModel;
        $productMakeUpModel = $this->productMakeUpModel;
        $productCareModel = $this->productCareModel;
        $id = (int) request()->get('id', 0);
        $user = $this->userModel::query()->where('id', $id)->get()->each(function($q) use ($productSkinModel, $productMakeUpModel, $productCareModel){
            $productSkins = $productSkinModel::query()
                ->select('*')
                ->where([
                    'pskin_status' => 'activated',
                ])
                ->whereIn('pskin_id', array_filter(explode(',', $q->pskin_ids)))
                ->get();
            $q->productSkins = $productSkins;

            $productMakeUps = $productMakeUpModel::query()
                ->select('*')
                ->where([
                    'pmakeup_status' => 'activated',
                ])
                ->whereIn('pmakeup_id', array_filter(explode(',', $q->pmakeup_ids)))
                ->get();

            $productCares = $productCareModel::query()
                ->select('*')
                ->where([
                    'pcare_status' => 'activated',
                ])
                ->whereIn('pcare_id', array_filter(explode(',', $q->pcare_ids)))
                ->get();

            $q->productSkins = $productSkins;
            $q->productMakeUps = $productMakeUps;
            $q->productCares = $productCares;
            return $q;
        })->first();
        if($user){
            $data['title'] = 'Quản lý người dùng[Xem]';
            $data['view']  = $this->viewPath . $this->view . '.edit';
            $data['user'] = $user;

            /** ward , district, province*/
            $data['province'] = $this->provinceModel::query()->get();

            return view($data['view'] , compact('data'));
        }else{
            $error   = 'Không tìm thấy người dùng';
            $redirect = UrlHelper::admin($this->model);
            return redirect()->to($redirect)->with('error', $error);
        }
    }


    /**
     * @param UserRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UserRequest $request)
    {
        $actionType = request()->post('action_type', 'save');
        $id = request()->post('id', 0);

        if($actionType == 'save'){
            $redirect = UrlHelper::admin($this->model);
        }else{
            $redirect = UrlHelper::admin($this->model,'edit', ['id' => $id]);
        }

        $success = 'Cập nhật người dùng thành công.';
        $error   = 'Cập nhật người dùng thất bại.';


        $params = [];
        foreach ($this->userModel::ALIAS as $field => $alias) {
            if (($value = Arr::get(request()->post(), $alias)) !== null) {
                Arr::set($params, $field, $value);
            }
        }

        try {
            $this->userModel::query()->where('id', $id)->update($params);

            return redirect()->to($redirect)->with('success', $success);
        } catch ( \Exception $e ) {
            $redirect = UrlHelper::admin($this->model);
            return redirect()->to($redirect)->with('error', $error);
        }
    }


    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete(Request $request)
    {
        $ids = request()->post('cid', []);
        $success = 'Xóa người dùng thành công.';
        $error   = 'Xóa người dùng thất bại.';

        $redirect = UrlHelper::admin($this->model);
        $number = $this->userModel->query()->whereIn('id', $ids)->delete();
        if($number > 0) {
            return redirect()->to($redirect)->with('success', $success);
        }else{
            return redirect()->to($redirect)->with('error', $error);
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function active(Request $request)
    {
        $ids = request()->post('cid', []);
        $success = 'Bật user thành công.';
        $error   = 'Bật user thất bại.';

        $redirect = UrlHelper::admin($this->model);
        $number = $this->userModel->query()->whereIn('id', $ids)->update(['status' => 'activated']);
        if($number > 0) {
            return redirect()->to($redirect)->with('success', $success);
        }else{
            return redirect()->to($redirect)->with('error', $error);
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function inactive(Request $request)
    {
        $ids = request()->post('cid', []);
        $success = 'Tắt user thành công.';
        $error   = 'Tắt user thất bại.';

        $redirect = UrlHelper::admin($this->model);
        $number = $this->userModel->query()->whereIn('id', $ids)->update(['status' => 'inactive']);
        if($number > 0) {
            return redirect()->to($redirect)->with('success', $success);
        }else{
            return redirect()->to($redirect)->with('error', $error);
        }
    }

    /**
     * @param Request $request
     */
    public function getDistrict(Request $request){
        $id = $request->id;
        $data = $this->districtModel::query()->where('province_id', $id)->get();

        $html = '<option value="">Chọn</option>';
        if(!empty($data)){
            foreach ($data as $key => $item){
                $html .= '<option value="' . $item->id . '">'. $item->name . '</option>';
            }
        }

        $response = [
            'html' => $html,
        ];
        return json_encode($response);
    }

    /**
     * @param Request $request
     */
    public function getWard(Request $request){
        $id = $request->id;
        $data = $this->wardModel::query()->where('district_id', $id)->get();

        $html = '<option value="">Chọn</option>';
        if(!empty($data)){
            foreach ($data as $key => $item){
                $html .= '<option value="' . $item->id . '">'. $item->name . '</option>';
            }
        }

        $response = [
            'html' => $html,
        ];
        return json_encode($response);
    }
}
