<?php

namespace App\Http\Controllers\Backend;

use App\Http\Requests\ContactRequest;
use App\Models\Contact;
use App\Helpers\UrlHelper;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;

class ContactController extends BaseController
{
    private $view = '.contact';
    private $model = 'contact';
    private $contactModel;
    public function __construct()
    {
        $this->contactModel = new Contact();
    }

    /**
     * @return Application|Factory|View
     */
    public function index()
    {
        $data['title'] = 'Quản lý liên hệ';
        $data['view']  = $this->viewPath . $this->view . '.list';

        $contacts = $this->contactModel::query()->orderBy('contact_id', 'DESC')->paginate(50);
        $data['contacts'] = $contacts;
        return view($data['view'] , compact('data'));
    }

    /**
     * Lấy danh sach theo $type
     * @return Application|Factory|View
     */
    public function type()
    {
        return view($this->viewPath . $this->view . '.type');
    }

    /**
     * @param $id
     * @return Application|Factory|View|\Illuminate\Http\RedirectResponse
     */
    public function edit()
    {
        $id = (int) request()->get('id', 0);
        $contact = $this->contactModel::query()->where('contact_id', $id)->first();
        if($contact){
            $data['title'] = 'Quản lý liên hệ: [Sửa]';
            $data['view']  = $this->viewPath . $this->view . '.edit';
            $data['contact'] = $contact;

            return view($data['view'] , compact('data'));
        }else{
            $error   = 'Không tìm thấy liên hệ';
            $redirect = UrlHelper::admin($this->model);
            return redirect()->to($redirect)->with('error', $error);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(ContactRequest $request)
    {
        $actionType = request()->post('action_type', 'save');
        $id = request()->post('id', 0);

        if($actionType == 'save'){
            $redirect = UrlHelper::admin($this->model);
        }else{
            $redirect = UrlHelper::admin($this->model,'edit', ['id' => $id]);
        }

        $success = 'Cập nhật liên hệ thành công.';
        $error   = 'Cập nhật liên hệ thất bại.';
        $params = $this->contactModel->revertAlias(request()->post());

        try {
            $this->contactModel::query()->where('contact_id', $id)->update($params);

            return redirect()->to($redirect)->with('success', $success);
        } catch ( \Exception $e ) {
            $redirect = UrlHelper::admin($this->model);
            return redirect()->to($redirect)->with('error', $error);
        }
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete(Request $request)
    {
        $ids = request()->post('cid', []);
        $success = 'Xóa liên hệ thành công.';
        $error   = 'Xóa liên hệ thất bại.';

        $redirect = UrlHelper::admin($this->model);
        $number = $this->contactModel->query()->whereIn('contact_id', $ids)->update(['contact_is_delete' => 'yes']);
        if($number > 0) {
            return redirect()->to($redirect)->with('success', $success);
        }else{
            return redirect()->to($redirect)->with('error', $error);
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function active(Request $request)
    {
        $ids = request()->post('cid', []);
        $success = 'Bật liên hệ thành công.';
        $error   = 'Bật liên hệ thất bại.';

        $redirect = UrlHelper::admin($this->model);
        $number = $this->contactModel->query()->whereIn('contact_id', $ids)->update(['contact_status' => 'activated']);
        if($number > 0) {
            return redirect()->to($redirect)->with('success', $success);
        }else{
            return redirect()->to($redirect)->with('error', $error);
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function inactive(Request $request)
    {
        $ids = request()->post('cid', []);
        $success = 'Tắt liên hệ thành công.';
        $error   = 'Tắt liên hệ thất bại.';

        $redirect = UrlHelper::admin($this->model);
        $number = $this->contactModel->query()->whereIn('contact_id', $ids)->update(['contact_status' => 'inactive']);
        if($number > 0) {
            return redirect()->to($redirect)->with('success', $success);
        }else{
            return redirect()->to($redirect)->with('error', $error);
        }
    }
}
