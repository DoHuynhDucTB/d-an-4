<?php

namespace App\Http\Controllers\Frontend;

use App\Models\AdminUser;
use App\Models\Comment;
use Illuminate\Http\Request;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Laravel\Socialite\Facades\Socialite;
use Illuminate\Support\Facades\Auth;

class CommentController extends BaseController
{
    private $adminUserModel;
    private $commentModel;

    public function __construct()
    {
        parent::__construct();
        $this->adminUserModel = new AdminUser();
        $this->commentModel = new Comment();
    }

    /**
     * @param Request $request
     * @return Application|Factory|View
     */
    public function commentProduct(Request $request)
    {
        $success = 'Cảm ơn Bình luận của bạn.';
        $error   = 'Gửi Bình luận thất bại.';
        $user = Auth::user();
        if($user){
            try {
                $params = $this->commentModel->revertAlias($request->all());
                $params['user_id'] = $user->id;
                $params['comment_level'] = 1;
                $comment = $this->commentModel::query()->create($params);
                return redirect()->back()->with('success', $success);
            } catch ( \Exception $e ) {
                return redirect()->back()->with('error', $error);
            }
        }else{
            return  redirect()->route('login');
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function commentPost(Request $request)
    {
        $success = 'Gửi comment thành công.';
        $error   = 'Gửi comment thất bại.';
        $user = Auth::user();
        if($user){
            try {
                $params = $this->commentModel->revertAlias($request->all());
                $params['user_id'] = $user->id;
                $params['comment_level'] = 1;
                $comment = $this->commentModel::query()->create($params);

                return redirect()->back()->with('success', $success);
            } catch ( \Exception $e ) {
                return redirect()->back()->with('error', $error);
            }
        }else{
            return  redirect()->route('login');
        }
    }
}
