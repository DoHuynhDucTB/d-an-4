<?php

namespace App\Http\Controllers\Frontend;

use App\Models\Banner;
use App\Models\District;
use App\Models\ProductCare;
use App\Models\ProductMakeUp;
use App\Models\ProductSkin;
use App\Models\Province;
use App\Models\User;
use App\Models\Ward;
use Artesaos\SEOTools\Traits\SEOTools;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserController extends BaseController
{
    use SEOTools;
    private $view = '.user';
    private User $userModel;
    private Banner $bannerModel;
    private Province $provinceModel;
    private District $districtModel;
    private Ward $wardModel;
    private ProductSkin $productSkinModel;
    private ProductCare $productCareModel;
    private ProductMakeUp $productMakeupModel;



    public function __construct(
        User $userModel,
        Banner $bannerModel,
        Province $provinceModel,
        District $districtModel,
        Ward $wardModel,
        ProductSkin $productSkinModel,
        ProductCare $productCareModel,
        ProductMakeUp $productMakeupModel
    )
    {
        parent::__construct();
        $this->userModel = $userModel;
        $this->bannerModel = $bannerModel;
        $this->provinceModel = $provinceModel;
        $this->districtModel = $districtModel;
        $this->wardModel = $wardModel;
        $this->productSkinModel = $productSkinModel;
        $this->productCareModel = $productCareModel;
        $this->productMakeupModel = $productMakeupModel;
    }


    public function detail()
    {
        $auth = Auth::guard('web')->user();
        $data['title'] = 'Thông tin tài khoản';
        $this->seo()->setTitle($data['title']);
        $data['view'] = $this->viewPath . $this->view . '.detail';
        $data['provinces'] = $this->provinceModel::parentQuery()->get();
        $data['districts'] = $this->districtModel::parentQuery()->where('province_id', intval($auth->province_id))->get();
        $data['wards'] = $this->wardModel::parentQuery()->where('district_id', intval($auth->district_id ))->get();
        $data['districts2'] = $this->districtModel::parentQuery()->where('province_id', intval($auth->delivery_province_id))->get();
        $data['wards2'] = $this->wardModel::parentQuery()->where('district_id', intval($auth->delivery_district_id ))->get();
        $data['provinces'] = $this->provinceModel::parentQuery()->get();
        $data['productSkins'] = $this->productSkinModel::query()->where('pskin_status', 'activated')->get();
        $data['productCares'] = $this->productCareModel::query()->where('pcare_status', 'activated')->get();
        $data['productMakeups'] = $this->productMakeupModel::query()->where('pmakeup_status', 'activated')->get();

        return view($data['view'] , compact('data'));
    }

    public function update(Request $request)
    {
        /**
         * Validate the form data
         */
        $this->validate($request, [
            'name'      => 'bail|required',
            'phone'     => 'bail|required|regex:/^[0-9]+$/|min:9|max:15',
            'email'     => 'bail|nullable|email|unique:users,email,' . Auth::guard('web')->user()->id,
            'sex'       => 'bail|required|in:male,female',
            'avatar'    => 'bail|file|max:1000|mimes:jpg,jpeg,png',
            'province'  => 'bail|integer',
            'district'  => 'bail|integer',
            'ward'      => 'bail|integer',
            'address'   => 'bail|max:200',
            'password'  => 'bail|nullable|confirmed|between:8,20|regex:/^[a-zA-Z0-9]+$/',
            'delivery_phone' => 'bail|required|regex:/^[0-9]+$/|min:9|max:15',
            'delivery_email' => 'bail|nullable|email',
        ],
        [
            'name.required'      => __('Họ và Tên là bắt buộc'),
            'phone.required'     => __('Số điện thoại không hợp lệ'),
            'phone.regex'        => __('Số điện thoại không hợp lệ'),
            'phone.min'          => __('Số điện thoại không hợp lệ'),
            'phone.max'          => __('Số điện thoại không hợp lệ'),
            'phone.integer'      => __('Số điện thoại không hợp lệ'),
            'email.required'     => __('Email không hợp lệ'),
            'email.email'        => __('Email không hợp lệ'),
            'email.unique'       => __('Email không hợp lệ'),
            'sex.required'       => 'Giới tính là bắt buộc',
            'sex.in'             => 'Giới tính là bắt buộc',
/*            'avatar.file'        => 'Ảnh đại diện phải là "jpg, jpeg, png"',
            'avatar.mimes'       => 'Ảnh đại diện phải là "jpg, jpeg, png"',
            'avatar.max'         => 'Ảnh đại diện quá lớn (tối đa 1M)',
            'address.max'        => 'Địa chỉ phải nhỏ hơn 200 ký tự',*/
            'province.integer'   => __('Tỉnh/Thành phố không hợp lệ'),
            'district.integer'   => __('Quận/Huyện không hợp lệ'),
            'ward.integer'       => __('Phường/Xã không hợp lệ'),
            'password.required'  => __('Mật khẩu là không hợp lệ'),
            'password.confirmed' => __('Xác nhận mật khẩu không chính xác'),
            'password.between'   => __('Mật khẩu là không hợp lệ'),
            'password.regex'     => __('Mật khẩu là không hợp lệ'),

            //
            'delivery_phone.required'     => __('Số điện thoại không hợp lệ'),
            'delivery_phone.regex'        => __('Số điện thoại không hợp lệ'),
            'delivery_phone.min'          => __('Số điện thoại không hợp lệ'),
            'delivery_phone.max'          => __('Số điện thoại không hợp lệ'),
            'delivery_phone.integer'      => __('Số điện thoại không hợp lệ'),
        ]
        );

        $data = [
            'name' => strip_tags($request->get('name')),
            'phone' => strip_tags($request->get('phone')),
            'email' => strip_tags($request->get('email')),
            'sex' => strip_tags($request->get('sex')),
            'province_id' => strip_tags($request->get('province')),
            'district_id' => strip_tags($request->get('district')),
            'ward_id' => strip_tags($request->get('ward')),
            'address' => strip_tags($request->get('address')),
            'date_of_birth' => strip_tags($request->get('date_of_birth')),
            'pskin_ids' => $this->productMakeupModel->getIds($request->get('skin')),
            'pcare_ids' => $this->productMakeupModel->getIds($request->get('care')),
            'pmakeup_ids' => $this->productMakeupModel->getIds($request->get('make-up')),

            'delivery_name' => strip_tags($request->get('delivery_name')),
            'delivery_phone' => strip_tags($request->get('delivery_phone')),
            'delivery_email' => strip_tags($request->get('delivery_email')),
            'delivery_ward_id' => strip_tags($request->get('delivery_ward')),
            'delivery_province_id' => strip_tags($request->get('delivery_province')),
            'delivery_district_id' => strip_tags($request->get('delivery_district')),
            'delivery_address' => strip_tags($request->get('delivery_address')),
        ];

        if (trim($request->get('password')) != '') {
            $data['password'] = Hash::make($request->post('password'));
        }

        if (in_array(Auth::guard('web')->user()->type, ['google', 'system'])) {
            unset($data['email']);
        }

/*        if ($request->avatar) {
            $originalName = $request->avatar->getClientOriginalName();
            $name = pathinfo($originalName, PATHINFO_FILENAME) .'-'. time() . "." .  pathinfo($originalName, PATHINFO_EXTENSION);
            $request->avatar->storeAs('user_m/avatar', $name, 'image');
            $data['avatar'] = $name;

            // remove old avatar

            $pathOldAvatar = public_path() .
                Str::replaceFirst('/public', '', config('my.path.image_user_of_module')) .
                config('my.image.value.user.avatar') .'/'.
                Auth::guard('web')->user()->avatar;
            if (is_file($pathOldAvatar)) {
                unlink($pathOldAvatar);
            }
        }*/

        if ($this->userModel::parentQuery()->where(['id' => Auth::guard('web')->user()->id])->update($data)) {
            return back()->with('success', __('Cập nhật thành công'));
        }

        return back()->with('error', __('Cập nhật thất bại'));
    }

    public function registerForm()
    {
        $this->seo()->setTitle('Đăng ký');
        $data['view'] = $this->viewPath . $this->view . '.register';
        $data['provinces'] = $this->provinceModel::parentQuery()->get();
        $data['productSkins'] = $this->productSkinModel::query()->where('pskin_status', 'activated')->get();
        $data['productCares'] = $this->productCareModel::query()->where('pcare_status', 'activated')->get();
        $data['productMakeups'] = $this->productMakeupModel::query()->where('pmakeup_status', 'activated')->get();
        return view($data['view'] , compact('data'));
    }


    public function register(Request $request)
    {
        $this->validate($request, [
            'name'      => 'bail|required',
            'email'     => 'bail|required|email|unique:users,email',
            'phone'     => 'bail|required|regex:/^[0-9]+$/|min:9|max:15',
            'password'  => 'bail|required|confirmed|between:8,20|regex:/^[a-zA-Z0-9]+$/',
        ],
        [
            'email.required'     => __('Email không hợp lệ'),
            'email.email'        => __('Email không hợp lệ'),
            'email.unique'       => __('Email đã tồn tại'),
            'name.required'      => __('Họ và Tên là bắt buộc'),
            'phone.required'     => __('Số điện thoại không hợp lệ'),
            'phone.regex'        => __('Số điện thoại không hợp lệ'),
            'phone.min'          => __('Số điện thoại không hợp lệ'),
            'phone.max'          => __('Số điện thoại không hợp lệ'),
            'password.required'  => __('Mật khẩu là không hợp lệ'),
            'password.confirmed' => __('Xác nhận mật khẩu không chính xác'),
            'password.between'   => __('Mật khẩu là không hợp lệ'),
            'password.regex'     => __('Mật khẩu là không hợp lệ'),
        ]);

        $sex        = $request->post('name');
        $pskinIds   = is_array($request->post('skin')) ? $this->bannerModel->getIds($request->post('skin')) : '';
        $pcareIds   = is_array($request->post('care')) ? $this->bannerModel->getIds($request->post('care')) : '';
        $pmakeupIds = is_array($request->post('make-up')) ? $this->bannerModel->getIds($request->post('make-up')) : '';

        $data = [
            'name'          => $request->post('name'),
            'email'         => $request->post('email'),
            'phone'         => $request->post('phone'),
            'status'        => 'activated',
            'type'          => 'system',
            'date_of_birth' => $request->post('date_of_birth'),
            'sex'           => in_array($sex, $this->userModel::VALUE_DEFAULT_OF_FIELDS['sex']) ? $sex : current($this->userModel::VALUE_DEFAULT_OF_FIELDS['sex']),
            'password'      => Hash::make($request->post('password')),
            'ward_id'       => intval($request->post('ward')),
            'province_id'   => intval($request->post('province')),
            'district_id'   => intval($request->post('district')),
            'pcare_ids'     => $pcareIds,
            'pskin_ids'     => $pskinIds,
            'pmakeup_ids'   => $pmakeupIds,
            'address'       => $request->post('address'),
        ];
        $User = $this->userModel::parentQuery()->create($data);
        if ($User) {
            return redirect()->route('login')->with('success', __('Bạn đã tạo tài khoản thành công, vùi lòng đăng nhập'));
        }else{
            return back()->with('error', __('Tạo tài khoản thất bại, vui lòng liên hệ với quản trị viên.'));
        }
    }

    public function delivery()
    {
        $auth = Auth::guard('web')->user();
        $data['title'] = 'Thông tin giao hàng';
        $this->seo()->setTitle($data['title']);

        $data['provinces'] = $this->provinceModel::parentQuery()->get();
        $data['districts'] = $this->districtModel::parentQuery()->where('province_id', intval($auth->delivery_province_id))->get();
        $data['wards'] = $this->wardModel::parentQuery()->where('district_id', intval($auth->delivery_district_id ))->get();
        $data['view'] = $this->viewPath . $this->view . '.delivery';

        return view($data['view'] , compact('data'));
    }


    public function updateDelivery(Request $request)
    {
        /**
         * Validate the form data
         */
        $this->validate($request, [
            'delivery_name'      => 'bail|required',
            'delivery_phone'     => 'bail|required|regex:/^[0-9]+$/|min:9|max:15',
            'delivery_email'     => 'bail|required|email',
            'delivery_province'  => 'bail|integer',
            'delivery_district'  => 'bail|integer',
            'delivery_ward'      => 'bail|integer',
            'delivery_address'   => 'bail|max:200',
        ],
            [
                'delivery_name.required'      => 'Họ và Tên là bắt buộc ',
                'delivery_phone.required'     => 'Số điện thoại là bắt buộc',
                'delivery_phone.regex'        => 'Số điện thoại không hợp lệ',
                'delivery_phone.min'          => 'Số điện thoại không hợp lệ',
                'delivery_phone.max'          => 'Số điện thoại không hợp lệ',
                'delivery_email.required'     => 'Email là bắt buộc',
                'delivery_email.email'        => 'Email không hợp lệ',
                'delivery_address.max'        => 'Địa chỉ phải nhỏ hơn 200 ký tự',
                'delivery_province.integer'   => 'Tỉnh/Thành phố không hợp lệ',
                'delivery_district.integer'   => 'Quận/Huyện không hợp lệ',
                'delivery_ward.integer'       => 'Phường/Xã không hợp lệ',
            ]
        );

        $data = [
            'delivery_name' => strip_tags($request->get('delivery_name')),
            'delivery_phone' => strip_tags($request->get('delivery_phone')),
            'delivery_email' => strip_tags($request->get('delivery_email')),
            'delivery_province_id' => strip_tags($request->get('delivery_province')),
            'delivery_district_id' => strip_tags($request->get('delivery_district')),
            'delivery_ward_id' => strip_tags($request->get('delivery_ward')),
            'delivery_address' => strip_tags($request->get('delivery_address')),
        ];

        if ($this->userModel::parentQuery()->where(['id' => Auth::guard('web')->user()->id])->update($data)) {
            return back()->with('success', 'Cập nhật thành công.');
        }

        return back()->with('error', 'Cập nhật thất bại.');
    }
}
