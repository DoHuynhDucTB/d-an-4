<?php

namespace App\Http\Controllers\Frontend;

use App\Models\AdminUser;
use App\Models\Banner;
use App\Models\Config;
use App\Models\Contact;
use App\Http\Requests\Frontend\ContactRequest;
use Artesaos\SEOTools\Traits\SEOTools;
use Illuminate\Http\Request;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;

class ContactController extends BaseController
{
    use SEOTools;

    private $view = '.contact';
    private $adminUserModel;
    private $bannerModel;
    private $configModel;
    private $contactModel;

    public function __construct()
    {
        parent::__construct();
        $this->adminUserModel = new AdminUser();
        $this->bannerModel = new Banner();
        $this->configModel = new Config();
        $this->contactModel = new Contact();
    }

    /**
     * @param Request $request
     * @return Application|Factory|View
     */
    public function index(Request $request)
    {
        $data['title'] = 'Liên Hệ';
        $data['view']  = $this->viewPath . $this->view . '.index';

        $this->seo()->setTitle($data['title']);

        /**config*/
        $googleMapValue = $this->configModel::getConfig('google_map');
        $addressValue   = $this->configModel::getConfig('address');
        $phoneValue     = $this->configModel::getConfig('phone');
        $emailValue     = $this->configModel::getConfig('email');
        $titleContactValue  = $this->configModel::getConfig('title_contact');
        $workingDayValue    = $this->configModel::getConfig('working_day');
        $workTimeValue      = $this->configModel::getConfig('work_time');

        $data['googleMapValue']     = $googleMapValue;
        $data['addressValue']       = $addressValue;
        $data['phoneValue']         = $phoneValue;
        $data['emailValue']         = $emailValue;
        $data['titleContactValue']  = $titleContactValue;
        $data['workingDayValue']    = $workingDayValue;
        $data['workTimeValue']      = $workTimeValue;

        return view($data['view'] , compact('data'));
    }

    /**
     * @param ContactRequest $request
     * @return Application|Factory|View
     */
    public function contact(ContactRequest $request)
    {
        $success = 'Gửi liên hệ thành công.';
        $error   = 'Gửi liên hệ thất bại.';
        $params = $this->contactModel->revertAlias(request()->post());
        $redirect = route('contactInfo');

        try {
            $contact = $this->contactModel::query()->create($params);
            return redirect()->to($redirect)->with('success', $success);
        } catch ( \Exception $e ) {
            return redirect()->to($redirect)->with('error', $error);
        }
    }

}
