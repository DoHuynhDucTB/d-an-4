<?php

namespace App\Http\Controllers\Frontend;

use App\Helpers\ArrayHelper;
use App\Helpers\HtmlHelper;
use App\Helpers\LanguageHelper;
use App\Models\Config;
use App\Models\Producer;
use App\Models\Product;
use App\Models\Image;
use App\Models\Banner;
use App\Models\ProductCare;
use App\Models\ProductCategory;
use App\Models\ProductCollection;
use App\Models\Comment;
use App\Models\ProductColor;
use App\Models\ProductMakeUp;
use App\Models\ProductNutritions;
use App\Models\ProductOdorous;
use App\Models\ProductSize;
use App\Models\ProductSkin;
use App\Models\User;
use Artesaos\SEOTools\Traits\SEOTools;
use Artesaos\SEOTools\Facades\OpenGraph;
use Illuminate\Http\Request;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Str;

class ProductController extends BaseController
{
    use SEOTools;

    private $view = '.product';
    private $model = 'product';
    private $productModel;
    private $imageModel;
    private $bannerModel;
    private $productCollectionModel;
    private $configModel;
    private $commentModel;
    private $userModel;
    private $productOdorousModel;
    private $productNutritionModel;
    private $productCategoryModel;
    private $producerModel;
    private $productColorModel;
    private $productSizeModel;
    private $productSkinModel;
    private $productCareModel;
    private $productMakeUpModel;

    public function __construct()
    {
        parent::__construct();
        $this->productModel = new Product();
        $this->imageModel = new Image();
        $this->bannerModel = new Banner();
        $this->configModel = new Config();
        $this->productCollectionModel = new ProductCollection();
        $this->commentModel = new Comment();
        $this->userModel = new User();
        $this->productOdorousModel = new ProductOdorous();
        $this->productNutritionModel = new ProductNutritions();
        $this->productCategoryModel = new ProductCategory();
        $this->producerModel = new Producer();
        $this->productColorModel = new ProductColor();
        $this->productSizeModel = new ProductSize();
        $this->productSkinModel = new ProductSkin();
        $this->productCareModel = new ProductCare();
        $this->productMakeUpModel = new ProductMakeUp();
    }

    /**
     * @param Request $request
     * @return Application|Factory|View
     */
    public function index(Request $request)
    {
        $data['title'] = 'Danh sách sản phẩm';
        $data['view']  = $this->viewPath . $this->view . '.list';

        /*
        |-------------------------------------------------------
        | FILTER
        |-------------------------------------------------------
         */
        $query = $this->productModel::query()->where('product_status_show', 'yes');
        $query->when(request()->get('skin') > 0, function ($q) {
            $condition = intval(request()->get('skin')) > 0 ? '=' : '<>';
            return $q->join(PRODUCT_USING_SKINS_TBL, PRODUCT_USING_SKINS_TBL . '.product_id', PRODUCT_TBL . '.product_id')
                ->where(PRODUCT_USING_SKINS_TBL . '.pskin_id', $condition, request()->get('skin'));
        });

        $query->when(in_array(request()->get('sex'), ['male', 'female']), function ($q) {
            return $q->where('product_sex', request()->get('sex'));
        });
        $query->when(request()->get('collection') > 0, function ($q) {
            $condition = intval(request()->get('collection')) > 0 ? '=' : '<>';
            return $q->join(PRODUCT_USING_COLLECTIONS_TBL, PRODUCT_USING_COLLECTIONS_TBL . '.product_id', PRODUCT_TBL . '.product_id')
                ->where(PRODUCT_USING_COLLECTIONS_TBL . '.pcollection_id', $condition, request()->get('collection'));
        });
        $query->when(request()->get('odorous') > 0, function ($q) {
            return $q->join(PRODUCT_USING_ODOROUS_TBL, PRODUCT_USING_ODOROUS_TBL . '.product_id', PRODUCT_TBL . '.product_id')
                ->where(PRODUCT_USING_ODOROUS_TBL . '.podo_id', request()->get('odorous'));
        });
        $query->when(request()->get('nutrition') > 0, function ($q) {
            return $q->join(PRODUCT_USING_NUTRITIONS_TBL, PRODUCT_USING_NUTRITIONS_TBL . '.product_id', PRODUCT_TBL . '.product_id')
                ->where(PRODUCT_USING_NUTRITIONS_TBL . '.pnutri_id', request()->get('nutrition'));
        });

        $query->when(request()->get('color') > 0, function ($q) {
            $condition = intval(request()->get('color')) > 0 ? '=' : '<>';
            return $q->join(PRODUCT_USING_COLOR_TBL, PRODUCT_USING_COLOR_TBL . '.product_id', PRODUCT_TBL . '.product_id')
                ->where(PRODUCT_USING_COLOR_TBL . '.pcolor_id', $condition, request()->get('color'));
        });

        $query->when(request()->get('care') > 0, function ($q) {
            $condition = intval(request()->get('care')) > 0 ? '=' : '<>';
            return $q->join(PRODUCT_USING_CARES_TBL, PRODUCT_USING_CARES_TBL . '.product_id', PRODUCT_TBL . '.product_id')
                ->where(PRODUCT_USING_CARES_TBL . '.pcare_id', $condition, request()->get('care'));
        });

        $query->when(request()->get('makeup') > 0, function ($q) {
            $condition = intval(request()->get('makeup')) > 0 ? '=' : '<>';
            return $q->join(PRODUCT_USING_MAKEUP_TBL, PRODUCT_USING_MAKEUP_TBL . '.product_id', PRODUCT_TBL . '.product_id')
                ->where(PRODUCT_USING_MAKEUP_TBL . '.pmakeup_id', $condition, request()->get('makeup'));
        });

        $query->when(request()->get('size') > 0, function ($q) {
            $condition = intval(request()->get('size')) > 0 ? '=' : '<>';
            return $q->join(PRODUCT_USING_SIZE_TBL, PRODUCT_USING_SIZE_TBL . '.product_id', PRODUCT_TBL . '.product_id')
                ->where(PRODUCT_USING_SIZE_TBL . '.psize_id', $condition, request()->get('size'));
        });

        $query->when(request()->get('category') > 0, function ($q) {
            $condition = intval(request()->get('category')) > 0 ? '=' : '<>';
            return $q->where('pcat_id', $condition, request()->get('category'));
        });

        $query->when(request()->get('producer') > 0, function ($q) {
            $condition = intval(request()->get('producer')) > 0 ? '=' : '<>';
            return $q->where('producer', $condition, request()->get('producer'));
        });

        $query->when(request()->get('min_price') > 0, function ($q) {
            return $q->whereRaw( "IF(product_new_price > 0 , product_new_price, product_price - product_discount) >= " . request()->get('min_price'));
        });

        $query->when(request()->get('max_price') > 0, function ($q) {
            return $q->whereRaw( "IF(product_new_price > 0 , product_new_price, product_price - product_discount) <= " . request()->get('max_price'));
        });


        // order by
        $query->when(!empty(request()->get('order_by')), function ($q) {
            $orderBy = request()->get('order_by');
            $values = explode(':', $orderBy);
            if (in_array($values[0], ['price', 'name']) && in_array($values[1], ['asc', 'desc'])) {
                if ($values[0] == 'name') {
                    return $q->orderBy('product_name', $values[1]);
                } else {
                    return $q->orderBy('price', $values[1]);
                }
            }else{
                return $q->orderBy('price', 'asc');
            }
        });

        $query->when(empty(request()->get('order_by')), function ($q) {
            if (app()->getLocale() == 'vi') {
                return $q->orderBy('product_name', 'asc');
            } else {
                $language = app()->getLocale();
                return $q->orderBy("product_name_$language", 'asc');
            }

        });

        $query->when(request()->get('cat') > 0, function ($q) {
            $condition = intval(request()->get('cat')) > 0 ? '=' : '<>';
            return $q->where('pcat_id', $condition, request()->get('cat'));
        });



        $products = $query->selectRaw('DISTINCT ' . PRODUCT_TBL . '.`product_id`')
                            ->select(PRODUCT_TBL . '.*')
                            ->selectRaw('IF(product_new_price > 0, product_new_price, product_price - product_discount) AS price')
                            ->with('avatar')
                            ->paginate(30);

        $data['products'] = $products;
        $this->seo()->setTitle('Sản phẩm');
        $this->seo()->setDescription($this->configModel::getConfig('meta_description'));
        $this->seo()->metatags()->setKeywords(explode(',', $this->configModel::getConfig('meta_keyword')));

/*

            $data['productOdorous'] = $this->productOdorousModel::parentQuery()
                            ->where(['podo_status' => 'activated', 'podo_is_delete' => 'no'])
                            ->orderBy('podo_name', 'asc')->get();

            $data['productNutritions'] = $this->productNutritionModel::parentQuery()
                            ->where(['pnutri_status' => 'activated', 'pnutri_is_delete' => 'no'])
                            ->orderBy('pnutri_name', 'asc')->get();

            $data['productCategories'] = $this->productCategoryModel->findByType('cosmetics');

            $data['producers'] = $this->producerModel::parentQuery()->where([
                'producer_is_delete' => 'no',
                'producer_status' => 'activated',
            ])->orderBy('producer_name', 'asc')->get();

            $data['productCategories'] = $this->productCategoryModel->findByType('shoes');
            $data['productColors'] = $this->productColorModel::parentQuery()
                                            ->where([
                                                'pcolor_status' => 'activated',
                                                'pcolor_is_delete' => 'no',
                                            ])->orderBy('pcolor_code', 'asc')->get();

            $data['productSizes'] = $this->productSizeModel::parentQuery()
                ->where([
                    'psize_status' => 'activated',
                    'psize_is_delete' => 'no',
                ])->orderBy('psize_code', 'asc')->get();*/

        $data['productSkins'] = $this->productSkinModel::parentQuery()->where(['pskin_status' => 'activated', 'pskin_is_delete' => 'no'])->get();
        $productCategoryParent = $this->productCategoryModel::parentQuery()->select('*')->where('pcat_id', 3)->first();
        $data['productCategories'] =  $this->productCategoryModel::parentQuery()->select('*')
            ->orderBy('left', 'ASC')
            ->whereBetween('left', [$productCategoryParent->left, $productCategoryParent->right])
            ->where('level', '>', 0)
            ->where('right', '>', 0)
            ->where('pcat_id', '<>', 3)
            ->with('avatar')
            ->get();

        $data['productCollections'] = $this->productCollectionModel::parentQuery()
                                        ->where(['pcollection_status' => 'activated', 'pcollection_is_delete' => 'no'])
                                        ->orderBy('pcollection_name', 'asc')->get();

        $data['productCares'] = $this->productCareModel::parentQuery()
                                    ->where(['pcare_status' => 'activated', 'pcare_is_delete' => 'no'])
                                    ->orderBy('pcare_name', 'asc')->get();

        $data['productMakeups'] = $this->productMakeUpModel::parentQuery()
            ->where(['pmakeup_status' => 'activated', 'pmakeup_is_delete' => 'no'])
            ->orderBy('pmakeup_name', 'asc')->get();

        $data['phone'] = $this->configModel::getConfig('phone_number');

        return view($data['view'] , compact('data'));
    }


    /**
     * @param Request $request
     * @return Application|Factory|View
     */
    public function detail(Request $request, $slug, $id)
    {
        $data['title'] = 'Chi tiết sản phẩm';
        $data['view'] = $this->viewPath . $this->view . '.detail';

        $pathAvatar = config('my.path.image_product_avatar_of_module');
        $pathThumbnail = config('my.path.image_product_thumbnail_of_module');
        $pathBanner = config('my.path.image_product_banner_of_module');

        $product = $this->productModel::query()
            ->select('*')
            ->selectRaw('IF(product_new_price > 0, product_new_price, product_price - product_discount) AS price')
            ->where('product_id', $id)
            ->where('product_status_show', 'yes')
            ->first();

        if ($product) {
            $this->seo()->setTitle($product->product_name);
            $this->seo()->setDescription($product->product_meta_description);
            $this->seo()->metatags()->setKeywords($product->product_meta_keywords);

            $product->pcatName = !empty($product->pcat) ? $product->pcat->pcat_name : '';
            $product->slug = $slug;

            $imageAvatar = $product->avatar;
            $imageThumbnail = $product->thumbnail()->get();
            $imageBanner = $product->banner()->get();


            /*
            |-------------------------------------------------------
            | SẢN PHẦM LIÊN QUAN, BÁN CHẠY
            |-------------------------------------------------------
             */
            $arrayRelated = !empty($product->product_related) ? array_diff(array_map('intval', explode(',', $product->product_related)), [0]) : [];
            $productRelated = $this->productModel::query()
                ->select(['*'])
                ->selectRaw('IF(product_new_price > 0, product_new_price, product_price - product_discount) AS price')
                ->where('product_status_show', 'yes')
                ->whereIn('product_id', $arrayRelated)
                ->whereNotIn('product_id', [$id])
                ->leftJoin(IMAGE_TBL, '3rd_key', 'product_id')
                ->where(['3rd_type' => 'product', 'image_value' => config('my.image.value.product.avatar'), 'image_status' => 'activated'])
                ->limit(12)
                ->get();

/*            $productSelling = $this->productModel::query()
                ->select('*')
                ->selectRaw('IF(product_new_price > 0, product_new_price, product_price - product_discount) AS price')
                ->where('product_status_show', 'yes')
                ->where('product_is_selling', 'yes')
                ->whereNotIn('product_id', [$id])
                ->leftJoin(IMAGE_TBL, '3rd_key', 'product_id')
                ->where(['3rd_type' => 'product', 'image_value' => config('my.image.value.product.avatar'), 'image_status' => 'activated'])
                ->limit(12)
                ->get();*/

            /*
            |-------------------------------------------------------
            | COMMENT
            |-------------------------------------------------------
             */
            /*$comments = $this->commentModel::query()
                ->selectRaw('DISTINCT ' . COMMENT_TBL . '.`comment_id`')
                ->select([COMMENT_TBL . '.*', 'name', 'avatar'])
                ->join(USER_TBL, USER_TBL . '.id', COMMENT_TBL . '.user_id')
                ->where(['comment_status' => 'approved', 'comment_type' => 'product', 'comment_3rd_id' => $id, 'comment_level' => 1])
                ->orderBy('comment_id', 'desc')
                ->paginate(50);
            if (count($comments)) {
                $commentParentIds = [];
                foreach ($comments as $item) {
                    $commentParentIds[] = $item->comment_id;
                }
                // Lấy comment con
                $data['childrenComments'] = $this->commentModel::query()
                    ->selectRaw('DISTINCT ' . COMMENT_TBL . '.`comment_id`')
                    ->select([COMMENT_TBL . '.*', 'name', 'avatar'])
                    ->join(USER_TBL, USER_TBL . '.id', COMMENT_TBL . '.user_id')
                    ->where(['comment_status' => 'approved', 'comment_type' => 'product', 'comment_level' => 2, 'comment_3rd_id' => $id])
                    ->whereIn('comment_parent_id', $commentParentIds)
                    ->orderBy('comment_id', 'desc')
                    ->get();
            }*/

            $data['product'] = $product;
            //$data['comments'] = $comments;
            $data['pathAvatar'] = $pathAvatar;
            $data['pathThumbnail'] = $pathThumbnail;
            $data['pathBanner'] = $pathBanner;

            $data['imageAvatar'] = $imageAvatar;
            $data['imageThumbnail'] = $imageThumbnail;
            $data['imageBanner'] = $imageBanner;

            $data['productRelated'] = $productRelated;
            /*$data['productSelling'] = $productSelling;*/

            $data['phone'] = $this->configModel::getConfig('phone_number');
            $data['share_url'] = route('detailProduct', ['slug' => $slug, 'id' => $product->product_id]);


            OpenGraph::setTitle($product->product_name)
                ->setDescription($product->product_meta_description)
                ->setType('article')
                ->setUrl(route('detailProduct', ['slug' => $slug, 'id' => $product->product_id]))
                ->addImage(config('my.path.image_product_avatar_of_module') . $product->avatar->image_name);

            return view($data['view'], compact('data'));
        }else {
            abort(404);
        }
    }

    public function modal(Request $request){
        $id = $request->id;

        $product = $this->productModel::query()
            ->select('*')
            ->selectRaw('IF(product_new_price > 0, product_new_price, product_price - product_discount) AS price')
            ->where('product_id', $id)
            ->where('product_status_show', 'yes')
            ->first();


        $html = [];
        if ($product) {
            $product->pcatName = !empty($product->pcat) ? $product->pcat->pcat_name : '';
            $product->slug = Str::slug($product->product_name);

            $html = HtmlHelper::htmlModal($product);
        }

        $response = [
            'html' => $html
        ];
        return json_encode($response);
    }


    /**
     * @return Application|Factory|View
     */
    public function productPhilosophy()
    {
        $data['view']  = $this->viewPath . $this->view . '.philosophy';
        $this->seo()->setTitle('Triết lý sản phẩm');
        $this->seo()->setDescription($this->configModel::getConfig('meta_description'));
        $this->seo()->metatags()->setKeywords(explode(',', $this->configModel::getConfig('meta_keyword')));

        $bannerMain = $this->bannerModel::parentQuery()
            ->where(['banner_is_delete' => 'no', 'banner_status' => 'activated', 'bangroup_id' => 8])
            ->with('avatar')
            ->first();
        $bannerDetails = $this->bannerModel::parentQuery()
            ->where(['banner_is_delete' => 'no', 'banner_status' => 'activated', 'bangroup_id' => 9])
            ->with('avatar')
            ->orderBy('banner_sorted', 'asc')
            ->get();

        $data['bannerMain'] = $bannerMain;
        $data['bannerDetails'] = $bannerDetails;
        return view($data['view'] , compact('data'));
    }
}
