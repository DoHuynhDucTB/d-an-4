<?php

namespace App\Http\Controllers\Frontend;


use App\Helpers\ArrayHelper;
use App\Models\Agency;
use App\Models\Banner;
use App\Models\Block;
use App\Models\Config;
use App\Models\Product;
use App\Models\ProductCategory;
use App\Models\ProductColor;
use App\Models\ProductSize;
use App\Models\ProductVariant;
use Artesaos\SEOTools\Traits\SEOTools;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;

class LocationController extends BaseController
{
    use SEOTools;
    /**
     * @var Agency
     */
    private Agency $agencyModel;

    /**
     * @var Config
     */
    private Config $configModel;

    /**
     * @var Banner
     */
    private Banner $bannerModel;

    /**
     * LocationController constructor.
     * @param Agency $agencyModel
     * @param Config $configModel
     * @param Banner $bannerModel
     */
    public function __construct(
        Agency $agencyModel,
        Config $configModel,
        Banner $bannerModel
    )
    {
        parent::__construct();
        $this->agencyModel = $agencyModel;
        $this->configModel = $configModel;
        $this->bannerModel = $bannerModel;
    }

    public function index()
    {
        $this->seo()->setTitle('Location');
        $this->seo()->setDescription($this->configModel::getConfig('meta_description'));
        $this->seo()->metatags()->setKeywords(explode(',', $this->configModel::getConfig('meta_keyword')));

        $banner = $this->bannerModel::query()->isActivated()->where('banner_id', 55)->first();
        $agencyByCountry = $this->agencyModel::query()
            ->select(['agency_country'])
            ->where('agency_status', 'activated')
            ->groupBy('agency_country')
            ->orderBy('agency_country')
            ->get();
        $countrySelected = ArrayHelper::valueAsKey($agencyByCountry->toArray(), 'agency_country');

        $countryArr = [0];
        foreach ($countrySelected as $key => $country) {
            $countryArr[] = $key;
        }


        $agencies = $this->agencyModel::query()
            ->select(['agency_country', 'agency_province'])
            ->where('agency_status', 'activated')
            ->whereIn('agency_country', $countryArr)
            ->with('province')
            ->get();

        $agencyInfo = [];
        foreach ($agencies as $item) {
            $agency = $this->agencyModel::query()
                ->where('agency_status', 'activated')
                ->where('agency_province', $item->agency_province)
                ->with('shops')
                ->get();
            $agencyInfo[$item->agency_country][$item->agency_province] = [
                                                                                    'province' => $item,
                                                                                    'agencies' => $agency,
                                                                                ];
        }

        return \view($this->viewPath . 'location.index', [
            'banner' => $banner,
            'countrySelected' =>$countrySelected,
            'agencyInfo' =>$agencyInfo,
        ]);
    }

}
