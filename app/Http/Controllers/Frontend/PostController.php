<?php

namespace App\Http\Controllers\Frontend;

use App\Models\Config;
use App\Models\Post;
use App\Models\Image;
use App\Models\Banner;
use App\Models\PostGroup;
use App\Models\AdminUser;
use App\Models\Comment;
use App\Models\User;
use Artesaos\SEOTools\Traits\SEOTools;
use Illuminate\Http\Request;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;

class PostController extends BaseController
{
    use SEOTools;

    private $view = '.post';
    private $model = 'post';
    private $postModel;
    private $imageModel;
    private $bannerModel;
    private $postGroupModel;
    private $adminUserModel;
    private $commentModel;
    private $userModel;
    private $configModel;

    public function __construct()
    {
        parent::__construct();
        $this->postModel = new Post();
        $this->imageModel = new Image();
        $this->bannerModel = new Banner();
        $this->postGroupModel = new PostGroup();
        $this->adminUserModel = new AdminUser();
        $this->commentModel = new Comment();
        $this->userModel = new User();
        $this->configModel = new Config();
    }

    /**
     * @param Request $request
     * @return Application|Factory|View
     */
    public function index(Request $request)
    {
        $data['view']  = $this->viewPath . $this->view . '.list';
        $pathAvatar = config('my.path.image_post_avatar_of_module');

        $query = $this->postModel::query()->where('post_group', 70);
        $query->when(!empty(request()->get('searching')), function ($q) {
            $searching = request()->get('searching');
            return $q->where('post_name', 'like', "%$searching%")->orWhere('post_content', 'like', "%$searching%");
        });
        $posts = $query->where(['post_status' => 'activated'])
                        ->with('avatar')
                        ->paginate(25);


        $this->seo()->setTitle('Tin tức');
        $this->seo()->setDescription($this->configModel::getConfig('meta_description'));
        $this->seo()->metatags()->setKeywords(explode(',', $this->configModel::getConfig('meta_keyword')));

        $data['posts'] = $posts;
        return view($data['view'] , compact('data'));
    }

    /**
     * @param Request $request
     * @return Application|Factory|View
     */
    public function detail(Request $request)
    {
        $id = $request->id;
        $data['title'] = 'Chi tiết bài viết';
        $data['view']  = $this->viewPath . $this->view . '.detail';

        $pathAvatar = config('my.path.image_post_avatar_of_module');
        $valueAvatar = config('my.image.value.post.avatar');

        $post = $this->postModel::query()->where('post_id', $id)->where('post_status', 'activated')->first();

        if($post){
            $this->seo()->setTitle($post->post_name);
            $this->seo()->setDescription($post->post_meta_description);
            $this->seo()->metatags()->setKeywords($post->post_meta_keywords);

            $post->urlAvatar = !empty($post->avatar) ? $pathAvatar . $post->avatar->image_name : '';


            /** bài viết liên quan, bán chạy */
            $arrayRelated = !empty($post->post_related) ? array_diff(array_map('intval', explode(',', $post->post_related)), [0]) : [];
            $postRelated = $this->postModel::query()->whereIn('post_id', $arrayRelated)->get();
            if($postRelated->count() > 0){
                foreach ($postRelated as $key => $item){
                    $item->urlAvatar = !empty($item->avatar) ? $pathAvatar . $item->avatar->image_name : '';
                }
            }

            /**comment*/
            $comments = $this->commentModel::query()->where(['comment_type' => 'post', 'comment_3rd_id' => $id])->get();
            if($comments->count() > 0){
                foreach ($comments as $key => $comment){
                    $user = $this->userModel::query()->where('id', $comment->user_id)->first();
                    $comment->userName = $user->name;
                    $comment->userAvatar = $user->avatar;
                }
            }

            $data['post'] = $post;
            $data['comments'] = $comments;
            $data['pathAvatar'] = $pathAvatar;
            $data['postRelated'] = $postRelated;

            return view($data['view'] , compact('data'));
        }else{
            abort(404);
        }
    }


    public function getShoesNews()
    {
        $data['posts'] = $this->postModel::query()->where(['post_status' => 'activated', 'post_group' => 71])->get();
        $data['view']  = $this->viewPath . $this->view . '.shoes_news';
        return view($data['view'] , compact('data'));
    }
}
