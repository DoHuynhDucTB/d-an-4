<?php

namespace App\Http\Controllers\Frontend;


use App\Helpers\ArrayHelper;
use App\Models\Banner;
use App\Models\Block;
use App\Models\Config;
use App\Models\District;
use App\Models\Product;
use App\Models\ProductCategory;
use App\Models\ProductColor;
use App\Models\ProductSize;
use App\Models\ProductVariant;
use Artesaos\SEOTools\Traits\SEOTools;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Session;

class IndexController extends BaseController
{
    use SEOTools;
    /**
     * @var Banner
     */
    private Banner $bannerModel;

    /**
     * @var Product $productModel
     */
    private Product $productModel;

    /**
     * @var Config
     */
    private Config $configModel;


    /**
     * @var Block
     */
    private Block $blockModel;

    /**
     * @var ProductCategory
     */
    private ProductCategory $productCategoryModel;


    /**
     * @var ProductColor
     */
    private ProductColor $productColorModel;

    /**
     * @var ProductSize
     */
    private ProductSize $productSizeModel;

    /**
     * @var ProductVariant
     */
    private ProductVariant $productVariantModel;

    /**
     * IndexController constructor.
     * @param Banner $bannerModel
     * @param Product $productModel
     * @param Config $configModel
     * @param Block $blockModel
     * @param ProductCategory $productCategoryModel
     * @param ProductColor $productColorModel
     * @param ProductSize $productSizeModel
     */
    public function __construct(
        Banner $bannerModel,
        Product $productModel,
        Config $configModel,
        Block $blockModel,
        ProductCategory $productCategoryModel,
        ProductColor $productColorModel,
        ProductSize $productSizeModel,
        ProductVariant $productVariantModel
    )
    {
        parent::__construct();
        $this->bannerModel = $bannerModel;
        $this->productModel = $productModel;
        $this->configModel = $configModel;
        $this->blockModel = $blockModel;
        $this->productCategoryModel = $productCategoryModel;
        $this->productColorModel = $productColorModel;
        $this->productSizeModel = $productSizeModel;
        $this->productVariantModel = $productVariantModel;
    }


    /**
     * @return Application|Factory|View
     */
    public function index()
    {
        $this->seo()->setTitle($this->configModel::getConfig('meta_title'));
        $this->seo()->setDescription($this->configModel::getConfig('meta_description'));
        $this->seo()->metatags()->setKeywords(explode(',', $this->configModel::getConfig('meta_keyword')));
        $blocks = $this->blockModel->findByStatus('activated');

        return \view(
            $this->viewPath . 'index.index',
            [
                'blocks' => $blocks
            ]
        );
    }


    /**
     * @return Application|Factory|View
     */
    public function shop()
    {
        $this->seo()->setTitle('Shop');
        $this->seo()->setDescription($this->configModel::getConfig('meta_description'));
        $this->seo()->metatags()->setKeywords(explode(',', $this->configModel::getConfig('meta_keyword')));

        $colors = request()->get('color');
        $data['selectedColors'] = $colorArr = array_filter(explode('-', $colors));

        $data['productCount'] = $this->productModel::parentQuery()
            ->select(['product_type'])->selectRaw('COUNT(*) as count')
            ->where(['product_is_delete' => 'no', 'prodouct_show_shop' => 'yes'])
            ->orderBy('product_type', 'asc')
            ->groupBy('product_type')
            ->get();

        $productCategories = $this->productCategoryModel::query()
            ->where(['is_show_shoppage' => 'yes', 'pcat_status' => 'activated'])
            ->join(PRODUCT_TBL, 'product_group', 'pcat_id')
            ->with('productsOfShopPage.variants')
            ->with('productsOfShopPage.variants.avatar')
            ->with('avatar')
            ->with('bannerFeature')
            ->with('bannerFeature.avatar')
            ->when(is_array($colorArr) && count($colorArr), function ($q) use ($colorArr) {
                $variantArr = [0];
                $variants = $this->productVariantModel::query()->select('product_id')->whereIn('pvariant_color', $colorArr)->get();
                foreach ($variants as $variant) {
                    $variantArr[] = $variant->product_id;
                }
                $q->whereIn('product_id', $variantArr);
            })
            ->when(request()->get('size'), function ($q) {
                $variantArr = [0];
                $variants = $this->productVariantModel::query()
                    ->select('product_id')
                    ->where('pvariant_size', intval(request()->get('size')))
                    ->where('pvariant_sex', request()->get('sex'))
                    ->get();
                foreach ($variants as $variant) {
                    $variantArr[] = $variant->product_id;
                }
                $q->whereIn('product_id', $variantArr);
            })->when(request()->get('min-price') > 0 && request()->get('max-price') > 0, function ($q) {
                $variantArr = [0];
                $variants = $this->productVariantModel::query()
                    ->select('product_id')
                    ->whereRaw( "IF(pvariant_new_price > 0 , pvariant_new_price, pvariant_price - pvariant_discount) >= " . request()->get('min-price') . '000')
                    ->whereRaw( "IF(pvariant_new_price > 0 , pvariant_new_price, pvariant_price - pvariant_discount) <= " . request()->get('max-price') . '000')
                    ->get();
                foreach ($variants as $variant) {
                    $variantArr[] = $variant->product_id;
                }
                $q->whereIn('product_id', $variantArr);
            })
            ->get();

        $productColors = $this->productColorModel::parentQuery()
            ->select(['pcolor_hex', 'pcolor_id'])
            ->where('pcolor_is_delete', 'no')
            ->where('pcolor_status', 'activated')
            ->get();
        $data['productColors'] = $productColors;
        $data['productColorArr'] = ArrayHelper::valueAsKey($productColors->toArray(), 'pcolor_id');

        $data['productSizes'] = $this->productSizeModel::parentQuery()
            ->select(['psize_id', 'psize_code', 'psize_sex'])
            ->where('psize_is_delete', 'no')
            ->where('psize_status', 'activated')
            ->orderBy('psize_code', 'asc')
            ->get();

        $productCategoryArr = [];
        foreach ($productCategories as $category) {
            /**
             * @var Product $products
             */
            $products = $category->productsOfShopPage;
            if ($products->count() > 0) {
                $productFirst = $products->first();
                $productCategoryArr[$productFirst->product_type . '-' . $category->right]['product-category'] = $category;
                $productCategoryArr[$productFirst->product_type . '-' . $category->right]['product'] = $products;
            }
        }
        krsort($productCategoryArr);
//        /dd($productCategoryArr);
        $data['productCategories'] = $productCategoryArr;
        return \view(
            $this->viewPath . 'index.shop',
            compact('data')
        );
    }

/*    public function importProvince()
    {
        $row = 1;
        if (($handle = fopen("D:\province.csv", "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {

                District::parentQuery()->create([
                    'id' => $data[0],
                    'name' => $data[1],
                    'province_id' => $data[2],
                    'gso_id' => '0'
                ]);
            }
            fclose($handle);
        }
        dd(__METHOD__);
    }*/
}
