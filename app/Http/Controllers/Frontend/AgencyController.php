<?php

namespace App\Http\Controllers\Frontend;

use App\Models\AdminUser;
use App\Models\Config;
use App\Models\Contact;
use Artesaos\SEOTools\Traits\SEOTools;
use Illuminate\Http\Request;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;

class AgencyController extends BaseController
{
    use SEOTools;

    private $view = '.agency';
    private $adminUserModel;
    private $bannerModel;
    private $configModel;
    private $contactModel;

    public function __construct()
    {
        parent::__construct();
        $this->adminUserModel = new AdminUser();
        $this->configModel = new Config();
    }

    /**
     * @param Request $request
     * @return Application|Factory|View
     */
    public function index(Request $request)
    {
        $data['title'] = 'Đại lý phân phối của Viba';
        $data['view']  = $this->viewPath . $this->view . '.index';

        $this->seo()->setTitle($data['title']);
        $googleMap   = $this->configModel::query()->where('conf_key', 'google_map')->first();
        $data['google_map'] = $googleMap->conf_value;

        return view($data['view'] , compact('data'));
    }
}
