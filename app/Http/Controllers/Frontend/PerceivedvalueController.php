<?php
namespace App\Http\Controllers\Frontend;

use App\Models\Banner;
use App\Models\PerceivedValue;
use Artesaos\SEOTools\Traits\SEOTools;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;

class PerceivedvalueController extends BaseController
{
    use SEOTools;
    /**
     * @var PerceivedValue
     */
    private PerceivedValue $perceivedValueModel;

    /**
     * @var Banner
     */
    private Banner $bannerModel;

    private $view = '.perceivedvalue';

    /**
     * PerceivedvalueController constructor.
     * @param PerceivedValue $perceivedValueModel
     * @param Banner $bannerModel
     */
    public function __construct(PerceivedValue $perceivedValueModel,  Banner $bannerModel)
    {
        $this->perceivedValueModel = $perceivedValueModel;
        $this->bannerModel = $bannerModel;
    }

    /**
     * @return Application|Factory|View
     */
    public function index()
    {
        $data['view']  = $this->viewPath . 'perceived-value.index';
        $this->seo()->setTitle('Khách hàng nói về chúng tôi');

        $bannerMain = $this->bannerModel::parentQuery()
            ->where(['banner_is_delete' => 'no', 'banner_status' => 'activated', 'bangroup_id' => 12])
            ->with('avatar')
            ->first();
        $bannerDetails = $this->bannerModel::parentQuery()
            ->where(['banner_is_delete' => 'no', 'banner_status' => 'activated', 'bangroup_id' => 13])
            ->with('avatar')
            ->get();

        $data['bannerMain'] = $bannerMain;
        $data['bannerDetails'] = $bannerDetails;
        return view($data['view'] , compact('data'));
    }

}
