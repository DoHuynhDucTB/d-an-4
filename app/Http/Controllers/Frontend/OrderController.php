<?php

namespace App\Http\Controllers\Frontend;

use App\Helpers\ArrayHelper;
use App\Helpers\OrderHelper;
use App\Helpers\ProductHelper;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\Product;
use App\Models\Image;
use App\Models\Banner;
use App\Models\ProductColor;
use App\Models\ProductSize;
use App\Models\Ward;
use App\Models\Province;
use App\Models\District;
use App\Models\User;
use App\Models\Config;
use App\Helpers\HtmlHelper;
use App\Helpers\CartHelper;
use App\Http\Requests\Frontend\ContactRequest;
use Artesaos\SEOTools\Traits\SEOTools;
use Illuminate\Http\Request;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class OrderController extends BaseController
{
    use SEOTools;
    private $view = '.order';
    private $orderModel;
    private $orderItemModel;
    private $productModel;
    private $imageModel;
    private $bannerModel;
    private $userModel;
    private $wardModel;
    private $provinceModel;
    private $districtModel;
    private $productColorModel;
    private $productSizeModel;
    private $configModel;

    public function __construct()
    {
        parent::__construct();
        $this->orderModel = new Order();
        $this->orderItemModel = new OrderItem();
        $this->productModel = new Product();
        $this->imageModel = new Image();
        $this->bannerModel = new Banner();
        $this->userModel = new User();
        $this->wardModel = new Ward();
        $this->provinceModel = new Province();
        $this->districtModel = new District();
        $this->productColorModel = new ProductColor();
        $this->productSizeModel = new ProductSize();
        $this->configModel = new Config();
    }

    public function index()
    {
        $data['view'] = $this->viewPath . $this->view . '.index';
        $data['title'] = 'Đơn hàng của tôi';

        $this->seo()->setTitle($data['title']);

        /**
         * Banner
         */
/*        $pathCategoryBanner = config('my.path.image_banner_of_module');
        $imageCategory = $this->bannerModel->getBannerAllPage();
        $imageCategoryUrl = $pathCategoryBanner . $imageCategory->avatar->image_name;
        $data['imageCategoryUrl'] = $imageCategoryUrl;*/

        $data['orders'] = $this->orderModel::parentQuery()->where(['ord_is_deleted' => 'no', 'user_id' => Auth::user()->id])->paginate(20);

        return view($data['view'] , compact('data'));
    }

    /**
     * @param Request $request
     * @return Application|Factory|View
     */
    public function add(Request $request)
    {

        if(!$request->session()->has('cart')){
            $request->session()->put('cart', []);
        }

        $id = (int)$request->id;
        $quantity = (int)$request->quantity;

        $cart = $request->session()->get('cart');
        $product = $this->productModel::query()->where('product_id', $id)->first();
        $valueAvatar = config('my.image.value.product.avatar');
        $pathAvatar = config('my.path.image_product_avatar_of_module');
        $imageAvatar  = $this->imageModel->query()->where(['3rd_key' => $id, '3rd_type' => 'product', 'image_value' => $valueAvatar])->first();


        $itemIds = array_column($cart, 'id');
        /**
         * nếu cart chưa có sản phẩm đó thì thêm vào, nếu cart đã có sản phẩm đó thì cập nhật số lượng
         */
        $price = !empty($product->product_new_price) ? $product->product_new_price : $product->product_price - $product->product_discount;
        $item = [
            'id' => $id,
            'name' => $product->product_name,
            'slug' => Str::slug($product->product_name),
            'quantity' => $quantity,
            'price' => $price,
            'sub_total_price' => $price * $quantity,
            'type' => $product->product_type,
            'status' => $product->product_status,
            'image' => !empty($imageAvatar) ? $pathAvatar . $imageAvatar->image_name : '',
        ];

        if(!in_array($id, $itemIds)){
            $cart[$id] = $item;
        }else{
            $cart[$id]['quantity'] += $quantity;
            $cart[$id]['sub_total_price'] = $cart[$id]['price'] * $cart[$id]['quantity'];
        }


        /**
         * tính tổng tiền
         */
        $totalPrice = 0;
        $total = 0;
        if(!empty($cart) && count($cart) > 0){
            foreach ($cart as $key => $item){
                $totalPrice += $item['sub_total_price'];
            }
        }

        $total = $totalPrice;
        $request->session()->put('total_price', $totalPrice);
        $request->session()->put('total', $total);
        $request->session()->put('cart', $cart);

        $htmlListHeader = HtmlHelper::htmlListHeader($cart);
        $htmlList = CartHelper::htmlCart($cart);
        $htmlTotal = CartHelper::htmlTotal($totalPrice);

        $textMessage = "Thêm sản phẩm $product->product_name vào giỏ hàng thành công";
        if($product->product_type == 'shoes'){
            $textMessage = "Thêm sản phẩm $product->product_name vào giỏ hàng thành công";
        }

        $response = [
            'countCart' => count($cart),
            'htmlListHeader' => $htmlListHeader,
            'htmlList' => $htmlList,
            'htmlTotal' => $htmlTotal,
            'total' => number_format($total, 0, '.', ','),
            'textMessage' => $textMessage,
            'redirect' => $request->get('redirect', null),
        ];
        return json_encode($response);
    }

    /**
     * @param Request $request
     * @return Application|Factory|View
     */
    public function update(Request $request)
    {
        $id = (int)$request->id;
        $quantity = (int)$request->quantity;

        $cart = $request->session()->get('cart') ? $request->session()->get('cart') : [];
        $name = $cart[$id]['name'];
        $cart[$id]['quantity'] = $quantity > 0 ? intval($quantity) : 1;
        if ($cart[$id]['quantity'] < 1) $cart[$id]['quantity'] = 1;
        $cart[$id]['sub_total_price'] = $cart[$id]['price'] * $quantity;


        /**
         * tính tổng tiền
         */
        $totalPrice = 0;
        $total = 0;
        if(!empty($cart) && count($cart) > 0){
            foreach ($cart as $key => $item){
                $totalPrice += $item['sub_total_price'];
            }
        }

        $total = $totalPrice;
        $request->session()->put('total_price', $totalPrice);
        $request->session()->put('total', $total);
        $request->session()->put('cart', $cart);

        $htmlListHeader = HtmlHelper::htmlListHeader($cart);
        $htmlList = CartHelper::htmlCart($cart);
        $htmlTotal = CartHelper::htmlTotal($totalPrice);

        $response = [
            'countCart' => count($cart),
            'htmlListHeader' => $htmlListHeader,
            'htmlList' => $htmlList,
            'htmlTotal' => $htmlTotal,
            'total' => number_format($total, 0, '.', ','),
            'textMessage' => "Cập nhật sản phẩm $name trong giỏ hàng thành công"
        ];
        return json_encode($response);
    }


    /**
     * @param Request $request
     * @return Application|Factory|View
     */
    public function remove(Request $request)
    {
        $id = $request->id;
        $cart = $request->session()->get('cart') ? $request->session()->get('cart') : [];
        $name = $cart[$id]['name'];
        $itemIds = array_column($cart, 'id');
        if(in_array($id, $itemIds)){
            unset($cart[$id]);
        }

        /**
         * tính tổng tiền
         */
        $totalPrice = 0;
        $total = 0;
        if(!empty($cart) && count($cart) > 0){
            foreach ($cart as $key => $item){
                $totalPrice += $item['sub_total_price'];
            }
        }
        $total = $totalPrice;
        $request->session()->put('total_price', $totalPrice);
        $request->session()->put('total', $total);
        $request->session()->put('cart', $cart);

        $htmlListHeader = HtmlHelper::htmlListHeader($cart);
        $htmlList = CartHelper::htmlCart($cart);
        $htmlTotal = CartHelper::htmlTotal($totalPrice);

        $response = [
            'countCart' => count($cart),
            'htmlListHeader' => $htmlListHeader,
            'htmlList' => $htmlList,
            'htmlTotal' => $htmlTotal,
            'total' => number_format($total, 0, '.', ','),
            'textMessage'   => "Xóa sản phẩm $name trong giỏ hàng thành công"
        ];
        return json_encode($response);
    }

    /**
     * @param Request $request
     * @return Application|Factory|View
     */
    public function delete(Request $request)
    {

    }


    /**
     * @param Request $request
     */
    public function shoppingCart(Request $request){

        $data['title'] = 'Giỏ Hàng';
        $data['view']  = $this->viewPath . $this->view . '.shopping-cart';
        $this->seo()->setTitle($data['title']);

        /*
         |---------------------------------------------------------------------------------------
         | Lấy thông tin product được lưu trong session, và kiểm tra lại trong thông tin trong DB
         | để lấy ra thông tin mới nhất.
         |---------------------------------------------------------------------------------------
         */
        $data['cart'] = Session()->get('cart') ?? [];
        $data['totalPriceCart'] = Session()->get('total_price') ?? 0;
        $data['totalCart'] = Session()->get('total') ?? 0;
        $productsOnDb = $this->productModel->findProductsAreStocking(array_keys($data['cart']));
        $productInfo = ProductHelper::compareProductsInSessionsWithProductsOnDb($productsOnDb);
        $data['cart'] = $productInfo['products'];
        $data['totalPriceCart'] = $productInfo['total_price'];
        $data['totalCart'] = $productInfo['total'];


        return view($data['view'], compact('data'));
    }


    /**
     * @param Request $request
     * @return Application|Factory|View
     */
    public function checkout(Request $request){
        $data['title'] = 'Thanh Toán';
        $data['view']  = $this->viewPath . $this->view . '.checkout';

        /** user */
        $user = Auth::guard('web')->user();
        $this->seo()->setTitle($data['title']);

        /** ward , district, province*/
        $data['provinces'] = $this->provinceModel::query()->get();
        $data['districts'] = $this->districtModel::parentQuery()->where('province_id', intval($user->province_id))->get();
        $data['wards'] = $this->wardModel::parentQuery()->where('district_id', intval($user->district_id ))->get();
        $data['districts2'] = $this->districtModel::parentQuery()->where('province_id', intval($user->delivery_province_id))->get();
        $data['wards2'] = $this->wardModel::parentQuery()->where('district_id', intval($user->delivery_district_id ))->get();

        /** order */
        $data['cart'] = $request->session()->get('cart') ? $request->session()->get('cart') : [];
        $data['totalPriceCart'] = $request->session()->get('total_price') ? $request->session()->get('total_price') : 0;
        $data['totalCart'] = $request->session()->get('total') ? $request->session()->get('total') : 0;

        $productsOnDb = $this->productModel->findProductsAreStocking(array_keys($data['cart']));
        $productInfo = ProductHelper::compareProductsInSessionsWithProductsOnDb($productsOnDb);
        $data['cart'] = $productInfo['products'];
        $data['totalPriceCart'] = $productInfo['total_price'];
        $data['totalCart'] = $productInfo['total'];

        if (count($data['cart']) < 1) {
            return redirect()->route('shopping-cart')->with('error', 'Không thể mua khi không có sản phẩm nào trong giỏ hàng');
        }


        /**config*/
        $stk = $this->configModel::query()->where('conf_key', 'thong-tin-ngan-hang')->first();
        $data['stkValue'] = !empty($stk) ? $stk->conf_value : '';

        return view($data['view'], compact('data'));

    }

    /**
     * @param Request $request
     */
    public function checkoutPost(Request $request){
        //dd($request->post());
        $rules = [
            'fullName' => 'bail|required',
            'phone' => 'bail|required|regex:/^[0-9]+$/|min:9|max:15',
            'email' => 'bail|required|email',
            'provinceId' => 'bail|required|integer',
            'districtId' => 'bail|required|integer',
            'wardId' => 'bail|required|integer',
            'addressDetail' => 'bail|required|max:200',
//            'g-recaptcha-response' => 'required|captcha'
        ];
        if ($request->post('delivery') == 'on') {
            $rules['fullNameTwo'] = 'bail|required';
            $rules['provinceIdTwo'] = 'bail|required|integer';
            $rules['districtIdTwo'] = 'bail|required|integer';
            $rules['wardIdTwo'] = 'bail|required|integer';
            $rules['phoneTwo'] = 'bail|required|regex:/^[0-9]+$/|min:9|max:15';
            $rules['emailTwo'] = 'bail|required|email';
            $rules['addressDetailTwo'] = 'bail|required|max:200';
        }
        $this->validate($request, $rules,
            [
                'fullName.required'     => 'Họ và Tên là bắt buộc ',
                'phone.required'        => 'Số điện thoại là bắt buộc',
                'phone.regex'           => 'Số điện thoại không hợp lệ',
                'phone.min'             => 'Số điện thoại không hợp lệ',
                'phone.max'             => 'Số điện thoại không hợp lệ',
                'phone.integer'         => 'Số điện thoại phải là số và không có khoảng trắng',
                'email.required'        => 'Email là bắt buộc',
                'email.email'           => 'Email không hợp lệ',
                'addressDetail.max'     => 'Địa chỉ phải nhỏ hơn 200 ký tự',
                'provinceId.required'   => 'Tỉnh/Thành phố không hợp lệ',
                'provinceId.integer'    => 'Tỉnh/Thành phố không hợp lệ',
                'districtId.integer'    => 'Quận/Huyện không hợp lệ',
                'districtId.required'   => 'Quận/Huyện không hợp lệ',
                'wardId.integer'        => 'Phường/Xã không hợp lệ',
                'wardId.required'       => 'Phường/Xã không hợp lệ',
                'addressDetail.required'=> 'Địa chi là bắt buộc',


                'fullNameTwo.required'     => 'Họ và Tên (thông tin nhận hàng) là bắt buộc ',
                'phoneTwo.required'        => 'Số điện thoại (thông tin nhận hàng) là bắt buộc',
                'phoneTwo.regex'           => 'Số điện thoại (thông tin nhận hàng) không hợp lệ',
                'phoneTwo.min'             => 'Số điện thoại (thông tin nhận hàng) không hợp lệ',
                'phoneTwo.max'             => 'Số điện thoại (thông tin nhận hàng) không hợp lệ',
                'phoneTwo.integer'         => 'Số điện thoại (thông tin nhận hàng) phải là số và không có khoảng trắng',
                'emailTwo.required'        => 'Email (thông tin nhận hàng) là bắt buộc',
                'emailTwo.email'           => 'Email (thông tin nhận hàng) không hợp lệ',
                'addressDetailTwo.max'     => 'Địa chỉ (thông tin nhận hàng) phải nhỏ hơn 200 ký tự',
                'provinceIdTwo.required'   => 'Tỉnh/Thành phố (thông tin nhận hàng) không hợp lệ',
                'provinceIdTwo.integer'    => 'Tỉnh/Thành phố (thông tin nhận hàng) không hợp lệ',
                'districtIdTwo.integer'    => 'Quận/Huyện (thông tin nhận hàng) không hợp lệ',
                'districtIdTwo.required'   => 'Quận/Huyện (thông tin nhận hàng) không hợp lệ',
                'wardIdTwo.integer'        => 'Phường/Xã (thông tin nhận hàng) không hợp lệ',
                'wardIdTwo.required'       => 'Phường/Xã (thông tin nhận hàng) không hợp lệ',
                'addressDetailTwo.required'=> 'Địa chi (thông tin nhận hàng) là bắt buộc',
                /*'g-recaptcha-response.required'    => 'Captcha là bắt buộc',
                'g-recaptcha-response.captcha'     => 'Captcha là bắt buộc',*/
            ]
        );


        $data['title'] = 'Đặt Hàng Thành Công';
        $data['view']  = $this->viewPath . $this->view . '.success';
        $data['view-email']  = $this->viewPath . $this->view . '.email';
        $cart = $request->session()->get('cart') ?? [];
        $this->seo()->setTitle('Đặt Hàng Thành Công');



        /*
        |-------------------------------------------------------
        | Lấy những cấu hình cần thiết cho việc gửi mail
        |-------------------------------------------------------
         */
        $valuePhone = $this->configModel::getConfig('phone', '');
        $valueTimeWork = $this->configModel::getConfig('work_time', '');
        $configOrderMailValue = $this->configModel::getConfig('email-xac-nhan-don-hang', '');
        $arrayEmailSend = explode(',', $configOrderMailValue);


        /*
        |-------------------------------------------------------
        | User
        |-------------------------------------------------------
         */
        /**
         * @var User $user
         */
        $user = Auth::guard('web')->user();
        $userId = $user->id;

        if ($request->post('delivery') == 'on') {
            $this->userModel::parentQuery()->where(['id' => $user->id])->update([
                'delivery_name'     => $request->post('fullNameTwo'),
                'delivery_phone'    => $request->post('phoneTwo'),
                'delivery_email'    => $request->post('emailTwo'),
                'delivery_address'  => $request->post('addressDetailTwo'),
                'delivery_ward_id'  => $request->post('wardIdTwo'),
                'delivery_province_id' => $request->post('provinceIdTwo'),
                'delivery_district_id' => $request->post('districtIdTwo')
            ]);
        }

//        /*
//        |-------------------------------------------------------
//        | Get banner
//        |-------------------------------------------------------
//         */
//        $pathCategoryBanner         = config('my.path.image_banner_of_module');
//        $imageCategory              = $this->bannerModel->getBannerAllPage();
//        $imageCategoryUrl           = $pathCategoryBanner . $imageCategory->avatar->image_name;
//        $data['imageCategoryUrl']   = $imageCategoryUrl;


        /*
        |-------------------------------------------------------
        | Kiểm tra trạng thái còn hàng của sản phẩm trong cart.
        | Khi tất cả sản phẩm hết hàng sẽ redirect trang checkout
        |-------------------------------------------------------
         */
        $products     = $this->productModel->findProductsAreStocking(array_keys($cart));
        $productInfo  = ProductHelper::compareProductsInSessionsWithProductsOnDb($products);
        $cart         = $productInfo['products'];
        if (count($cart) < 1) {
            return redirect()->route('checkout')->with('error', 'Đặt hàng thất bại');
        }

        /*
        |-------------------------------------------------------
        | Create order
        |-------------------------------------------------------
         */
        $dataInsert = $this->orderModel->revertAlias(request()->post());
        $dataInsert['ord_status']       = 'new';
        $dataInsert['product_type']     = 'all';
        $dataInsert['user_id']          = $userId;
        $dataInsert['ord_total_cost']   = $productInfo['total'];
        $dataInsert['ord_quantity']     = $productInfo['quantity'];

        if ($request->post('delivery') !== 'on') {
            $dataInsert['order_full_name_two'] = $request->post('fullName');
            $dataInsert['order_phone_two'] = $request->post('phone');
            $dataInsert['order_email_two'] = $request->post('email');
            $dataInsert['ord_note_two'] = $request->post('note');
            $dataInsert['ord_address_detail_two'] = $request->post('addressDetail');
            $dataInsert['ward_id_two'] = $request->post('wardId');
            $dataInsert['province_id_two'] = $request->post('provinceId');
            $dataInsert['district_id_two'] = $request->post('districtId');
        }

        $order = $this->orderModel::query()->create($dataInsert);
        $ordId = $order->ord_id;
        $data['code'] = $order->ord_id;
        $order->update(['ord_name' => OrderHelper::getOrderName($ordId)]);

        /*
        |-------------------------------------------------------
        | Create order item and send mail
        |-------------------------------------------------------
         */
        if($order){
            $orderItemInserts = [];
            $productsArr = ArrayHelper::valueAsKey($products->toArray(), 'product_id');
            foreach ($cart as $key => $item){
                $price       = $productsArr[$item['id']]['price'];
                $orderItemdata = [
                    'ordi_historical_cost'  => $price,
                    'ordi_quantity'         => $item['quantity'],
                    'ordi_total_cost'       => $price * $item['quantity'],
                    'user_id'               => $userId,
                    'product_id'            => $item['id'],
                    'pcolor_id'             => @$item['colorId'] ? intval($item['colorId']) : 0,
                    'psize_id'              => @$item['sizeId'] ? intval($item['sizeId']) : 0,
                    'ord_id'                => $ordId,
                ];
                $orderItemInserts[] = $orderItemdata;
            }
            $this->orderItemModel::parentQuery()->insert($orderItemInserts);

            // Send email
            $dataEmail = [
                'products'  => $productsArr,
                'web'       => \request()->root(),
                'phone'     => $valuePhone,
                'timeWork'  => $valueTimeWork,
                'order'     => $order,
                'orderItems'=> $orderItemInserts,
                'user'      => $user,
                'colors'    => $this->productColorModel::query()->get(),
                'sizes'     => $this->productSizeModel::query()->get(),
                'provinces' => $this->provinceModel::parentQuery()->whereIn('id', [$order->province_id, $order->province_id_two])->get(),
                'wards'     => $this->wardModel::parentQuery()->whereIn('id', [$order->ward_id, $order->ward_id_two])->get(),
                'districts' => $this->districtModel::parentQuery()->whereIn('id', [$order->district_id , $order->district_id_two])->get(),
            ];

            try {
                Mail::send($data['view-email'], $dataEmail, function ($message) use($user, $arrayEmailSend){
                    $message->from(env('MAIL_USERNAME'), \request()->root());
                    $message->subject('XÁC NHẬN ĐƠN HÀNG');

                    $message->to(\request()->post('email'), \request()->post('name'));
                    if(!empty($arrayEmailSend) && count($arrayEmailSend) > 0){
                        foreach ($arrayEmailSend as $key => $itemSendMail){
                            $message->to($itemSendMail, 'Quản trị viên');
                        }
                    }
                });
            } catch (\Exception $e) {
            }


            // Remove product from cart
            $request->session()->put('total_price', 0);
            $request->session()->put('total', 0);
            $request->session()->put('cart', []);
            return view($data['view'], compact('data'));
        }else{
            return redirect()->back()->with('error', 'Đặt hàng thất bại');
        }
    }

    /**
     * @param Request $request
     * @return Application|Factory|View
     */
    public function addWish(Request $request)
    {
        /** user */
        $userId = Auth::user()->id;
        $user = $this->userModel::query()->where('id', $userId)->first();
        $wish = !empty($user->product_wishlists) ? array_diff(array_map('intval', explode(',', $user->product_wishlists)), [0]) : [];

        /** product */
        $productId = (int)$request->id;
        $product = $this->productModel::query()->where('product_id', $productId)->first();
        $name = !empty($product) ? $product->product_name : '';

        if(!in_array($productId, $wish) && !empty($product)){
            $wish[] = $productId;
        }

        $data = ',' . implode(',', $wish) . ',';
        $user->update(['product_wishlists' => $data]);

        $response = [
            'textMessage' => "Thêm sản phẩm $name vào danh sách yêu thích thành công"
        ];
        return json_encode($response);
    }

    /**
     * @param Request $request
     * @return Application|Factory|View
     */
    public function removeWish(Request $request)
    {
        /** user */
        $userId = Auth::user()->id;
        $user = $this->userModel::query()->where('id', $userId)->first();
        $wish = !empty($user->product_wishlists) ? array_diff(array_map('intval', explode(',', $user->product_wishlists)), [0]) : [];

        /** product */
        $productId = (int)$request->id;
        $product = $this->productModel::query()->where('product_id', $productId)->first();
        $name = !empty($product) ? $product->product_name : '';

        if(in_array($productId, $wish) && !empty($product)){
            if(!empty($wish)){
                foreach ($wish as $key => $item){
                    if($item == $productId){
                        unset($wish[$key]);
                    }
                }
            }
        }

        $data = ',' . implode(',', $wish) . ',';
        $user->update(['product_wishlists' => $data]);

        $htmlListWish = HtmlHelper::htmlListWish($wish);
        $response = [
            'htmlListWish' => $htmlListWish,
            'textMessage' => "Xóa sản phẩm $name trong giỏ hàng thành công"
        ];
        return json_encode($response);
    }

    /**
     * @param Request $request
     * @return Application|Factory|View
     */
    public function wishList(Request $request){
        $data['title'] = 'Sản Phẩm Yêu Thích';
        $data['view']  = $this->viewPath . $this->view . '.wish-list';
        $this->seo()->setTitle($data['title']);

        /** user */
        $userId = Auth::user()->id;
        $user = $this->userModel::query()->where('id', $userId)->first();
        $wish = !empty($user->product_wishlists) ? array_diff(array_map('intval', explode(',', $user->product_wishlists)), [0]) : [];

        /** banner */
        $pathCategoryBanner = config('my.path.image_banner_of_module');
        $imageCategory = $this->bannerModel->getBannerAllPage();
        $imageCategoryUrl = $pathCategoryBanner . $imageCategory->avatar->image_name;
        $data['imageCategoryUrl'] = $imageCategoryUrl;

        $data['wish'] = HtmlHelper::htmlListWish($wish);
        return view($data['view'], compact('data'));
    }

    /**
     * @param Request $request
     */
    public function getDistrict(Request $request){
        $id = $request->id;
        $data = $this->districtModel::query()->where('province_id', $id)->get();

        $html = '<option value="">'.__('Quận/Huyện').'</option>';
        if(!empty($data)){
            foreach ($data as $key => $item){
                $html .= '<option value="' . $item->id . '">'. $item->name . '</option>';
            }
        }

        $response = [
            'html' => $html,
        ];
        return json_encode($response);
    }

    /**
     * @param Request $request
     */
    public function getWard(Request $request){
        $id = $request->id;
        $data = $this->wardModel::query()->where('district_id', $id)->get();

        $html = '<option value="">'.__('Phường/Xã').'</option>';
        if(!empty($data)){
            foreach ($data as $key => $item){
                $html .= '<option value="' . $item->id . '">'. $item->name . '</option>';
            }
        }

        $response = [
            'html' => $html,
        ];
        return json_encode($response);
    }


    /**
     * Tra cứu đơn hàng
     */
    public function lookup()
    {
        $this->seo()->setTitle('Tra cứu đơn hàng');
        $data['view']  = $this->viewPath . $this->view . '.lookup';

        $auth = Auth::guard('web')->user();
        $data['order'] = $this->orderModel::query()->where(['user_id' => $auth->id])->find(\request()->get('ord'));
        if ($data['order']) {
            $order = $data['order'];
            $data['ward'] = $this->wardModel::query()->where('id', $order->ward_id)->first();
            $data['district'] = $this->districtModel::query()->where('id', $order->district_id)->first();
            $data['province'] = $this->provinceModel::query()->where('id', $order->province_id)->first();
            $data['ward2'] = $this->wardModel::query()->where('id', $order->ward_id_two)->first();
            $data['district2'] = $this->districtModel::query()->where('id', $order->district_id_two)->first();
            $data['province2'] = $this->provinceModel::query()->where('id', $order->province_id_two)->first();

            $data['productColors'] = $this->productColorModel::query()->get();
            $data['productSizes'] = $this->productSizeModel::query()->get();
            $data['items'] = $order->items;
        }


        /**
         * Banner
         */
        /*$pathCategoryBanner = config('my.path.image_banner_of_module');
        $imageCategory = $this->bannerModel->getBannerAllPage();
        $imageCategoryUrl = $pathCategoryBanner . $imageCategory->avatar->image_name;
        $data['imageCategoryUrl'] = $imageCategoryUrl;*/

        return view($data['view'], compact('data'));
    }

    /**
     * @param Request $request
     */
    public function sendMail(Request $request){

        $view  = $this->viewPath . $this->view . '.email';
        $data = [
            'a' => 'aaaaaa',
            'b' => 'bbbbbb'
        ];
        Mail::send($view, $data, function ($message){
            $message->from(env('MAIL_USERNAME'), ' trai thai binh');
            $message->to('dohuynhduc384858@gmail.com', ' trai 17');
            $message->subject('công danh nam tử còn vương nợ');
        });
    }

}
