<?php
namespace App\Http\Controllers\Frontend;

use App\Helpers\LanguageHelper;
use Illuminate\Http\RedirectResponse;

class LanguageController extends BaseController
{
    /**
     * @param $language
     * @return RedirectResponse
     */
    public function setLanguage($language): RedirectResponse
    {
        $language = LanguageHelper::getAllowedLanguage($language);
        request()->session()->put('current_language', $language);

        return back();
    }

}
