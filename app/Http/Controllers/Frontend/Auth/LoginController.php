<?php

namespace App\Http\Controllers\Frontend\Auth;

use App\Http\Controllers\Frontend\BaseController;
use App\Models\Banner;
use Artesaos\SEOTools\Traits\SEOTools;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;

class LoginController extends BaseController
{
    use SEOTools;
    /**
     * @var string
     */
    private string $homePage = '/';

    /**
     * @var string
     */
    private string $loginForm = '/login';

    private $bannerModel;

    public function __construct()
    {
        $this->bannerModel = new Banner();
        $this->middleware('guest')->except(['logout', 'logoutOtherDevices']);

    }

    /**
     * @param Request $request
     * @return Application|RedirectResponse|Redirector
     * @throws ValidationException
     */
    public function login(Request $request)
    {
        /**
         * Validate the form data
         */
        $this->validate($request, [
            'email'     => 'bail|required|email',
            'password'  => 'bail|required'
        ],[
            'email.required'    => __('Email không hợp lệ'),
            'email.email'       => __('Email không hợp lệ'),
            'password.required' => __('Mật khẩu là bắt buộc')
        ]);
        /**
         * Verify user
         */
        $credentials = $request->only('email', 'password');
        //$rememberMe  = $request->get('remember-me') ? true : false;
        if (Auth::attempt(array_merge($credentials, ['status' => 'activated', 'type' => 'system']), true)) {
            $request->session()->regenerate();
            return redirect()->intended()->with('success', __('Đăng nhập thành công'));
        }else{
            return redirect()->route('login')->with('error', __('Email hoặc mật khẩu không đúng'));
        }
    }

    /**
     * @return RedirectResponse
     */
    public function logout(){
        Auth::guard('web')->logout();
        return redirect()->to('/')->with('success', 'Đăng xuất thành công');
    }

    /**
     * @return Application|Factory|View|RedirectResponse|Redirector
     */
    public function loginForm()
    {
        $this->seo()->setTitle('Đăng nhập');
        return view('frontend.login.login');
    }


    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function logoutOtherDevices(Request $request)
    {
        $password = $request->post('password', null);
        if (Hash::check($password, $request->user()->password)) {
            if (Auth::logoutOtherDevices($password)) {
                return redirect('/');
            }
        }

        return back();
    }

}
