<?php

use App\Http\Controllers\Frontend\Auth\ForgotPasswordController;
use App\Http\Controllers\Frontend\Auth\ResetPasswordController;
use App\Http\Controllers\Frontend\UserController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Frontend\Auth\LoginController;
use App\Http\Controllers\Frontend\LoginGoogleController;
use App\Http\Controllers\Frontend\LoginFacebookController;
use App\Http\Controllers\Frontend\IndexController;
use App\Http\Controllers\Frontend\ProductController;
use App\Http\Controllers\Frontend\PostController;
use App\Http\Controllers\Frontend\ContactController;
use App\Http\Controllers\Frontend\AgencyController;
use App\Http\Controllers\Frontend\CommentController;
use App\Http\Controllers\Frontend\OrderController;
use App\Http\Controllers\Frontend\LanguageController;
use App\Http\Controllers\Frontend\LocationController;
use App\Http\Controllers\Frontend\Auth\VerificationController;
use App\Http\Controllers\Frontend\Auth\ConfirmPasswordController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


/*
|--------------------------------------------------------------------------
| FAKE DATA
|--------------------------------------------------------------------------
 */
Route::prefix('fake')->group(function () {
    /**
     * Posts
     */
    //Route::get('/posts', [FakeDataController::class, 'fakePost']);
    //Route::get('/posts-not-save-to-database', [FakeDataController::class, 'fakePostNotSaveToDatabase']);

    //Route::get('/categories', [FakeDataController::class, 'fakeCategories']);
});

/*
|--------------------------------------------------------------------------
| HOME PAGE
|--------------------------------------------------------------------------
 */
Route::get('/', [IndexController::class, 'index'])->name('home');
Route::get('/shop.html', [IndexController::class, 'shop'])->name('shop');



/*
|--------------------------------------------------------------------------
| SOCIAL
|--------------------------------------------------------------------------
 */
Route::get('/login.html', [LoginController::class, 'loginForm'])->name('login');
Route::post('/login', [LoginController::class, 'login']);
Route::get('/logout.html', [LoginController::class, 'logout'])->name('logout');
Route::get('/register.html', [UserController::class, 'registerForm'])->name('register');
Route::post('/register', [UserController::class, 'register']);
//Route::get('/logout-other-devices', [LoginController::class, 'logoutOtherDevices'])->middleware('auth')->name('logout.other.devices');


Route::get('/social/login-google.html', [LoginGoogleController::class, 'redirectToGoogle'])->name('social.login.google');
Route::get('/social/login-google-callback.html', [LoginGoogleController::class, 'handleGoogleCallback']);
Route::get('/social/login-facebook.html', [LoginFacebookController::class, 'redirectToFacebook'])->name('social.login.facebook');
Route::get('/social/login-facebook-callback.html', [LoginFacebookController::class, 'handleFacebookCallback']);

/**
 * Email Verification Route(s)
 */
/*Route::get('/email/verify', [VerificationController::class, 'show'])->name('verification.notice');
Route::post('/email/resend', [VerificationController::class, 'resend'])->name('verification.resend');
Route::get('/email/verify/{id}/{hash}', [VerificationController::class, 'verify'])->name('verification.verify');*/


/**
 * Confirm password
 * */
/*Route::get('/password/confirm', [ConfirmPasswordController::class, 'showConfirmForm'])->name( 'password.confirm');
Route::post('/password/confirm', [ConfirmPasswordController::class, 'confirm']);*/

/**
 * Password Reset Route(S)
 */
/*Route::get('/forgot-password', [ForgotPasswordController::class, 'showLinkRequestForm'])->middleware('guest')->name( 'password.request');
Route::post('/forgot-password', [ForgotPasswordController::class, 'sendResetLinkEmail'])->middleware('guest')->name( 'password.email');
Route::get('/reset-password/{token}', [ResetPasswordController::class, 'showResetForm'])->middleware('guest')->name('password.reset');
Route::post('/reset-password', [ResetPasswordController::class, 'reset'])->middleware('guest')->name('password.update');*/

Route::get('/thong-tin-tai-khoan.html', [UserController::class, 'detail'])->name('detailUser')->middleware('auth:web');
Route::post('/cap-nhat-thong-tin-tai-khoan.html', [UserController::class, 'update'])->name('updateUser')->middleware('auth:web');
Route::get('/thong-tin-giao-hang.html', [UserController::class, 'delivery'])->name('deliveryUser')->middleware('auth:web');
Route::post('/cap-nhat-thong-tin-giao-hang.html', [UserController::class, 'updateDelivery'])->name('updateDeliveryUser')->middleware('auth:web');
Route::get('/tra-cuu-don-hang.html', [OrderController::class, 'lookup'])->name('lookupOrder')->middleware('auth:web');
Route::get('/don-hang-cua-toi.html', [OrderController::class, 'index'])->name('indexOrder')->middleware('auth:web');


Route::get('/san-pham/danh-sach', [ProductController::class, 'index'])->name('listProduct');
Route::get('/san-pham/chi-tiet/{slug}/{id}', [ProductController::class, 'detail'])->name('detailProduct');
Route::get('/san-pham/modal', [ProductController::class, 'modal'])->name('modal');
Route::get('/triet-ly-san-pham', [ProductController::class, 'productPhilosophy'])->name('productPhilosophy');

Route::get('/bai-viet/danh-sach', [PostController::class, 'index'])->name('listPost');
Route::get('/bai-viet/{slug}/{id}', [PostController::class, 'detail'])->name('detailPost');

Route::get('/lien-he', [ContactController::class, 'index'])->name('contactInfo');
Route::post('/lien-he', [ContactController::class, 'contact'])->name('contactSubmit');

Route::get('/dai-ly', [AgencyController::class, 'index'])->name('agencyInfo');

Route::post('/comment-product', [CommentController::class, 'commentProduct'])->name('comment-product')->middleware('auth:web');
Route::post('/comment-post', [CommentController::class, 'commentPost'])->name('comment-post');

Route::get('/add-cart', [OrderController::class, 'add'])->name('add-cart');
Route::get('/update-cart', [OrderController::class, 'update'])->name('update-cart');
Route::get('/remove-item-cart', [OrderController::class, 'remove'])->name('remove-item-cart');
Route::get('/shopping-cart', [OrderController::class, 'shoppingCart'])->name('shopping-cart');
Route::get('/checkout', [OrderController::class, 'checkout'])->name('checkout')->middleware('auth:web');
Route::post('/checkout-post', [OrderController::class, 'checkoutPost'])->name('checkout-post');
Route::get('/send-mail', [OrderController::class, 'sendMail'])->name('send-mail');

Route::get('/get-district', [OrderController::class, 'getDistrict'])->name('get-district');
Route::get('/get-ward', [OrderController::class, 'getWard'])->name('get-ward');

Route::get('/add-wish', [OrderController::class, 'addWish'])->name('add-wish')->middleware('auth:web');
Route::get('/remove-item-wish', [OrderController::class, 'removeWish'])->name('remove-wish')->middleware('auth:web');
Route::get('/wish-list', [OrderController::class, 'wishList'])->name('wish-list')->middleware('auth:web');

Route::get('/set-language-{language}', [LanguageController::class, 'setLanguage'])->where('language', '(' . config('app.locales') . ')')->name('set-language');


//
//Route::get('/import-province', [IndexController::class, 'importProvince']);

// Location page
Route::get('/location.html', [LocationController::class, 'index'])->name('location');
