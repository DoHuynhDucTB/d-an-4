<?php

use App\Http\Controllers\Backend\Auth\LoginController;
use Illuminate\Support\Facades\Route;

if (!defined('ADMIN_ROUTE')) {
    define('ADMIN_ROUTE', 'admins');
}

Route::group(['prefix' => ADMIN_ROUTE], function () {
    Route::get('/caching.system', [\App\Http\Controllers\Backend\SystemController::class, 'configCache']);
    /**
     * Login Route(s)
     */
    Route::get('/login.htm', [LoginController::class, 'loginForm'])->name(ADMIN_ROUTE . '.login');
    Route::post('/login.htm', [LoginController::class, 'login']);
    Route::get('/logout.htm', [LoginController::class, 'logout'])->name(ADMIN_ROUTE . '.logout');

    /*
    |-------------------------------------------------------
    | PRODUCT
    |-------------------------------------------------------
    */
    Route::post('/product/store', [\App\Http\Controllers\Backend\ProductController::class, 'store'])->middleware('auth:admin');
    Route::post('/product/update', [\App\Http\Controllers\Backend\ProductController::class, 'update'])->middleware('auth:admin');
    Route::post('/product/copy', [\App\Http\Controllers\Backend\ProductController::class, 'copy'])->middleware('auth:admin');
    Route::post('/product/duplicate', [\App\Http\Controllers\Backend\ProductController::class, 'duplicate'])->middleware('auth:admin');
    Route::post('/product/active', [\App\Http\Controllers\Backend\ProductController::class, 'active'])->middleware('auth:admin');
    Route::post('/product/inactive', [\App\Http\Controllers\Backend\ProductController::class, 'inactive'])->middleware('auth:admin');
    Route::post('/product/delete', [\App\Http\Controllers\Backend\ProductController::class, 'delete'])->middleware('auth:admin');
    Route::get('/product/index', [\App\Http\Controllers\Backend\ProductController::class, 'index'])->middleware('auth:admin');
    Route::get('/product/create', [\App\Http\Controllers\Backend\ProductController::class, 'create'])->middleware('auth:admin');
    Route::get('/product/edit', [\App\Http\Controllers\Backend\ProductController::class, 'edit'])->middleware('auth:admin');

    /**color*/
    Route::post('/productcolor/store', [\App\Http\Controllers\Backend\ProductColorController::class, 'store'])->middleware('auth:admin');
    Route::post('/productcolor/update', [\App\Http\Controllers\Backend\ProductColorController::class, 'update'])->middleware('auth:admin');
    Route::post('/productcolor/copy', [\App\Http\Controllers\Backend\ProductColorController::class, 'copy'])->middleware('auth:admin');
    Route::post('/productcolor/duplicate', [\App\Http\Controllers\Backend\ProductColorController::class, 'duplicate'])->middleware('auth:admin');
    Route::post('/productcolor/active', [\App\Http\Controllers\Backend\ProductColorController::class, 'active'])->middleware('auth:admin');
    Route::post('/productcolor/inactive', [\App\Http\Controllers\Backend\ProductColorController::class, 'inactive'])->middleware('auth:admin');
    Route::post('/productcolor/delete', [\App\Http\Controllers\Backend\ProductColorController::class, 'delete'])->middleware('auth:admin');
    Route::get('/productcolor/index', [\App\Http\Controllers\Backend\ProductColorController::class, 'index'])->middleware('auth:admin');
    Route::get('/productcolor/create', [\App\Http\Controllers\Backend\ProductColorController::class, 'create'])->middleware('auth:admin');
    Route::get('/productcolor/edit', [\App\Http\Controllers\Backend\ProductColorController::class, 'edit'])->middleware('auth:admin');
    Route::get('/productcolor/list', [\App\Http\Controllers\Backend\ProductColorController::class, 'list'])->middleware('auth:admin');


    /**size*/
    Route::post('/productsize/store', [\App\Http\Controllers\Backend\ProductSizeController::class, 'store'])->middleware('auth:admin');
    Route::post('/productsize/update', [\App\Http\Controllers\Backend\ProductSizeController::class, 'update'])->middleware('auth:admin');
    Route::post('/productsize/copy', [\App\Http\Controllers\Backend\ProductSizeController::class, 'copy'])->middleware('auth:admin');
    Route::post('/productsize/duplicate', [\App\Http\Controllers\Backend\ProductSizeController::class, 'duplicate'])->middleware('auth:admin');
    Route::post('/productsize/active', [\App\Http\Controllers\Backend\ProductSizeController::class, 'active'])->middleware('auth:admin');
    Route::post('/productsize/inactive', [\App\Http\Controllers\Backend\ProductSizeController::class, 'inactive'])->middleware('auth:admin');
    Route::post('/productsize/delete', [\App\Http\Controllers\Backend\ProductSizeController::class, 'delete'])->middleware('auth:admin');
    Route::get('/productsize/index', [\App\Http\Controllers\Backend\ProductSizeController::class, 'index'])->middleware('auth:admin');
    Route::get('/productsize/create', [\App\Http\Controllers\Backend\ProductSizeController::class, 'create'])->middleware('auth:admin');
    Route::get('/productsize/edit', [\App\Http\Controllers\Backend\ProductSizeController::class, 'edit'])->middleware('auth:admin');

    /**nutritions*/
    Route::post('/productnutritions/store', [\App\Http\Controllers\Backend\ProductNutritionsController::class, 'store'])->middleware('auth:admin');
    Route::post('/productnutritions/update', [\App\Http\Controllers\Backend\ProductNutritionsController::class, 'update'])->middleware('auth:admin');
    Route::post('/productnutritions/copy', [\App\Http\Controllers\Backend\ProductNutritionsController::class, 'copy'])->middleware('auth:admin');
    Route::post('/productnutritions/duplicate', [\App\Http\Controllers\Backend\ProductNutritionsController::class, 'duplicate'])->middleware('auth:admin');
    Route::post('/productnutritions/active', [\App\Http\Controllers\Backend\ProductNutritionsController::class, 'active'])->middleware('auth:admin');
    Route::post('/productnutritions/inactive', [\App\Http\Controllers\Backend\ProductNutritionsController::class, 'inactive'])->middleware('auth:admin');
    Route::post('/productnutritions/delete', [\App\Http\Controllers\Backend\ProductNutritionsController::class, 'delete'])->middleware('auth:admin');
    Route::get('/productnutritions/index', [\App\Http\Controllers\Backend\ProductNutritionsController::class, 'index'])->middleware('auth:admin');
    Route::get('/productnutritions/create', [\App\Http\Controllers\Backend\ProductNutritionsController::class, 'create'])->middleware('auth:admin');
    Route::get('/productnutritions/edit', [\App\Http\Controllers\Backend\ProductNutritionsController::class, 'edit'])->middleware('auth:admin');

    /**odorous*/
    Route::post('/productodorous/store', [\App\Http\Controllers\Backend\ProductOdorousController::class, 'store'])->middleware('auth:admin');
    Route::post('/productodorous/update', [\App\Http\Controllers\Backend\ProductOdorousController::class, 'update'])->middleware('auth:admin');
    Route::post('/productodorous/copy', [\App\Http\Controllers\Backend\ProductOdorousController::class, 'copy'])->middleware('auth:admin');
    Route::post('/productodorous/duplicate', [\App\Http\Controllers\Backend\ProductOdorousController::class, 'duplicate'])->middleware('auth:admin');
    Route::post('/productodorous/active', [\App\Http\Controllers\Backend\ProductOdorousController::class, 'active'])->middleware('auth:admin');
    Route::post('/productodorous/inactive', [\App\Http\Controllers\Backend\ProductOdorousController::class, 'inactive'])->middleware('auth:admin');
    Route::post('/productodorous/delete', [\App\Http\Controllers\Backend\ProductOdorousController::class, 'delete'])->middleware('auth:admin');
    Route::get('/productodorous/index', [\App\Http\Controllers\Backend\ProductOdorousController::class, 'index'])->middleware('auth:admin');
    Route::get('/productodorous/create', [\App\Http\Controllers\Backend\ProductOdorousController::class, 'create'])->middleware('auth:admin');
    Route::get('/productodorous/edit', [\App\Http\Controllers\Backend\ProductOdorousController::class, 'edit'])->middleware('auth:admin');

    /**odorous*/
    Route::post('/productcollection/store', [\App\Http\Controllers\Backend\ProductCollectionController::class, 'store'])->middleware('auth:admin');
    Route::post('/productcollection/update', [\App\Http\Controllers\Backend\ProductCollectionController::class, 'update'])->middleware('auth:admin');
    Route::post('/productcollection/copy', [\App\Http\Controllers\Backend\ProductCollectionController::class, 'copy'])->middleware('auth:admin');
    Route::post('/productcollection/duplicate', [\App\Http\Controllers\Backend\ProductCollectionController::class, 'duplicate'])->middleware('auth:admin');
    Route::post('/productcollection/active', [\App\Http\Controllers\Backend\ProductCollectionController::class, 'active'])->middleware('auth:admin');
    Route::post('/productcollection/inactive', [\App\Http\Controllers\Backend\ProductCollectionController::class, 'inactive'])->middleware('auth:admin');
    Route::post('/productcollection/delete', [\App\Http\Controllers\Backend\ProductCollectionController::class, 'delete'])->middleware('auth:admin');
    Route::get('/productcollection/index', [\App\Http\Controllers\Backend\ProductCollectionController::class, 'index'])->middleware('auth:admin');
    Route::get('/productcollection/create', [\App\Http\Controllers\Backend\ProductCollectionController::class, 'create'])->middleware('auth:admin');
    Route::get('/productcollection/edit', [\App\Http\Controllers\Backend\ProductCollectionController::class, 'edit'])->middleware('auth:admin');

    /**skin*/
    Route::post('/productskin/store', [\App\Http\Controllers\Backend\ProductSkinController::class, 'store'])->middleware('auth:admin');
    Route::post('/productskin/update', [\App\Http\Controllers\Backend\ProductSkinController::class, 'update'])->middleware('auth:admin');
    Route::post('/productskin/active', [\App\Http\Controllers\Backend\ProductSkinController::class, 'active'])->middleware('auth:admin');
    Route::post('/productskin/inactive', [\App\Http\Controllers\Backend\ProductSkinController::class, 'inactive'])->middleware('auth:admin');
    Route::post('/productskin/delete', [\App\Http\Controllers\Backend\ProductSkinController::class, 'delete'])->middleware('auth:admin');
    Route::get('/productskin/index', [\App\Http\Controllers\Backend\ProductSkinController::class, 'index'])->middleware('auth:admin');
    Route::get('/productskin/create', [\App\Http\Controllers\Backend\ProductSkinController::class, 'create'])->middleware('auth:admin');
    Route::get('/productskin/edit', [\App\Http\Controllers\Backend\ProductSkinController::class, 'edit'])->middleware('auth:admin');

    /**care*/
    Route::post('/productcare/store', [\App\Http\Controllers\Backend\ProductCareController::class, 'store'])->middleware('auth:admin');
    Route::post('/productcare/update', [\App\Http\Controllers\Backend\ProductCareController::class, 'update'])->middleware('auth:admin');
    Route::post('/productcare/active', [\App\Http\Controllers\Backend\ProductCareController::class, 'active'])->middleware('auth:admin');
    Route::post('/productcare/inactive', [\App\Http\Controllers\Backend\ProductCareController::class, 'inactive'])->middleware('auth:admin');
    Route::post('/productcare/delete', [\App\Http\Controllers\Backend\ProductCareController::class, 'delete'])->middleware('auth:admin');
    Route::get('/productcare/index', [\App\Http\Controllers\Backend\ProductCareController::class, 'index'])->middleware('auth:admin');
    Route::get('/productcare/create', [\App\Http\Controllers\Backend\ProductCareController::class, 'create'])->middleware('auth:admin');
    Route::get('/productcare/edit', [\App\Http\Controllers\Backend\ProductCareController::class, 'edit'])->middleware('auth:admin');

    /**make up*/
    Route::post('/productmakeup/store', [\App\Http\Controllers\Backend\ProductMakeUpController::class, 'store'])->middleware('auth:admin');
    Route::post('/productmakeup/update', [\App\Http\Controllers\Backend\ProductMakeUpController::class, 'update'])->middleware('auth:admin');
    Route::post('/productmakeup/active', [\App\Http\Controllers\Backend\ProductMakeUpController::class, 'active'])->middleware('auth:admin');
    Route::post('/productmakeup/inactive', [\App\Http\Controllers\Backend\ProductMakeUpController::class, 'inactive'])->middleware('auth:admin');
    Route::post('/productmakeup/delete', [\App\Http\Controllers\Backend\ProductMakeUpController::class, 'delete'])->middleware('auth:admin');
    Route::get('/productmakeup/index', [\App\Http\Controllers\Backend\ProductMakeUpController::class, 'index'])->middleware('auth:admin');
    Route::get('/productmakeup/create', [\App\Http\Controllers\Backend\ProductMakeUpController::class, 'create'])->middleware('auth:admin');
    Route::get('/productmakeup/edit', [\App\Http\Controllers\Backend\ProductMakeUpController::class, 'edit'])->middleware('auth:admin');


    Route::post('/productvariant/store', [\App\Http\Controllers\Backend\ProductVariantController::class, 'store'])->middleware('auth:admin');
    Route::post('/productvariant/update', [\App\Http\Controllers\Backend\ProductVariantController::class, 'update'])->middleware('auth:admin');
    Route::post('/productvariant/active', [\App\Http\Controllers\Backend\ProductVariantController::class, 'active'])->middleware('auth:admin');
    Route::post('/productvariant/inactive', [\App\Http\Controllers\Backend\ProductVariantController::class, 'inactive'])->middleware('auth:admin');
    Route::post('/productvariant/delete', [\App\Http\Controllers\Backend\ProductVariantController::class, 'delete'])->middleware('auth:admin');
    Route::get('/productvariant/index', [\App\Http\Controllers\Backend\ProductVariantController::class, 'index'])->middleware('auth:admin');
    Route::get('/productvariant/create', [\App\Http\Controllers\Backend\ProductVariantController::class, 'create'])->middleware('auth:admin');
    Route::get('/productvariant/edit', [\App\Http\Controllers\Backend\ProductVariantController::class, 'edit'])->middleware('auth:admin');


    /**producer */
    Route::post('/producer/store', [\App\Http\Controllers\Backend\ProducerController::class, 'store'])->middleware('auth:admin');
    Route::post('/producer/update', [\App\Http\Controllers\Backend\ProducerController::class, 'update'])->middleware('auth:admin');
    Route::post('/producer/copy', [\App\Http\Controllers\Backend\ProducerController::class, 'copy'])->middleware('auth:admin');
    Route::post('/producer/duplicate', [\App\Http\Controllers\Backend\ProducerController::class, 'duplicate'])->middleware('auth:admin');
    Route::post('/producer/active', [\App\Http\Controllers\Backend\ProducerController::class, 'active'])->middleware('auth:admin');
    Route::post('/producer/inactive', [\App\Http\Controllers\Backend\ProducerController::class, 'inactive'])->middleware('auth:admin');
    Route::post('/producer/delete', [\App\Http\Controllers\Backend\ProducerController::class, 'delete'])->middleware('auth:admin');
    Route::get('/producer/index', [\App\Http\Controllers\Backend\ProducerController::class, 'index'])->middleware('auth:admin');
    Route::get('/producer/create', [\App\Http\Controllers\Backend\ProducerController::class, 'create'])->middleware('auth:admin');
    Route::get('/producer/edit', [\App\Http\Controllers\Backend\ProducerController::class, 'edit'])->middleware('auth:admin');

    /**post */
    Route::post('/post/store', [\App\Http\Controllers\Backend\PostController::class, 'store'])->middleware('auth:admin');
    Route::post('/post/update', [\App\Http\Controllers\Backend\PostController::class, 'update'])->middleware('auth:admin');
    Route::post('/post/copy', [\App\Http\Controllers\Backend\PostController::class, 'copy'])->middleware('auth:admin');
    Route::post('/post/duplicate', [\App\Http\Controllers\Backend\PostController::class, 'duplicate'])->middleware('auth:admin');
    Route::post('/post/active', [\App\Http\Controllers\Backend\PostController::class, 'active'])->middleware('auth:admin');
    Route::post('/post/inactive', [\App\Http\Controllers\Backend\PostController::class, 'inactive'])->middleware('auth:admin');
    Route::post('/post/delete', [\App\Http\Controllers\Backend\PostController::class, 'delete'])->middleware('auth:admin');
    Route::get('/post/index', [\App\Http\Controllers\Backend\PostController::class, 'index'])->middleware('auth:admin');
    Route::get('/post/create', [\App\Http\Controllers\Backend\PostController::class, 'create'])->middleware('auth:admin');
    Route::get('/post/edit', [\App\Http\Controllers\Backend\PostController::class, 'edit'])->middleware('auth:admin');

    /**post group */
    Route::post('/postgroup/store', [\App\Http\Controllers\Backend\PostgroupController::class, 'store'])->middleware('auth:admin');
    Route::post('/postgroup/update', [\App\Http\Controllers\Backend\PostgroupController::class, 'update'])->middleware('auth:admin');
    Route::post('/postgroup/copy', [\App\Http\Controllers\Backend\PostgroupController::class, 'copy'])->middleware('auth:admin');
    Route::post('/postgroup/duplicate', [\App\Http\Controllers\Backend\PostgroupController::class, 'duplicate'])->middleware('auth:admin');
    Route::post('/postgroup/active', [\App\Http\Controllers\Backend\PostgroupController::class, 'active'])->middleware('auth:admin');
    Route::post('/postgroup/inactive', [\App\Http\Controllers\Backend\PostgroupController::class, 'inactive'])->middleware('auth:admin');
    Route::post('/postgroup/delete', [\App\Http\Controllers\Backend\PostgroupController::class, 'delete'])->middleware('auth:admin');
    Route::get('/postgroup/index', [\App\Http\Controllers\Backend\PostgroupController::class, 'index'])->middleware('auth:admin');
    Route::get('/postgroup/create', [\App\Http\Controllers\Backend\PostgroupController::class, 'create'])->middleware('auth:admin');
    Route::get('/postgroup/edit', [\App\Http\Controllers\Backend\PostgroupController::class, 'edit'])->middleware('auth:admin');

    /**advert */
    Route::post('/advert/store', [\App\Http\Controllers\Backend\AdvertController::class, 'store'])->middleware('auth:admin');
    Route::post('/advert/update', [\App\Http\Controllers\Backend\AdvertController::class, 'update'])->middleware('auth:admin');
    Route::post('/advert/copy', [\App\Http\Controllers\Backend\AdvertController::class, 'copy'])->middleware('auth:admin');
    Route::post('/advert/duplicate', [\App\Http\Controllers\Backend\AdvertController::class, 'duplicate'])->middleware('auth:admin');
    Route::post('/advert/active', [\App\Http\Controllers\Backend\AdvertController::class, 'active'])->middleware('auth:admin');
    Route::post('/advert/inactive', [\App\Http\Controllers\Backend\AdvertController::class, 'inactive'])->middleware('auth:admin');
    Route::post('/advert/delete', [\App\Http\Controllers\Backend\AdvertController::class, 'delete'])->middleware('auth:admin');
    Route::get('/advert/index', [\App\Http\Controllers\Backend\AdvertController::class, 'index'])->middleware('auth:admin');
    Route::get('/advert/create', [\App\Http\Controllers\Backend\AdvertController::class, 'create'])->middleware('auth:admin');
    Route::get('/advert/edit', [\App\Http\Controllers\Backend\AdvertController::class, 'edit'])->middleware('auth:admin');

    /**banner */
    Route::post('/banner/sort', [\App\Http\Controllers\Backend\BannerController::class, 'sort'])->middleware('auth:admin');
    Route::post('/banner/store', [\App\Http\Controllers\Backend\BannerController::class, 'store'])->middleware('auth:admin');
    Route::post('/banner/update', [\App\Http\Controllers\Backend\BannerController::class, 'update'])->middleware('auth:admin');
    Route::post('/banner/copy', [\App\Http\Controllers\Backend\BannerController::class, 'copy'])->middleware('auth:admin');
    Route::post('/banner/duplicate', [\App\Http\Controllers\Backend\BannerController::class, 'duplicate'])->middleware('auth:admin');
    Route::post('/banner/active', [\App\Http\Controllers\Backend\BannerController::class, 'active'])->middleware('auth:admin');
    Route::post('/banner/inactive', [\App\Http\Controllers\Backend\BannerController::class, 'inactive'])->middleware('auth:admin');
    Route::post('/banner/delete', [\App\Http\Controllers\Backend\BannerController::class, 'delete'])->middleware('auth:admin');
    Route::get('/banner/index', [\App\Http\Controllers\Backend\BannerController::class, 'index'])->middleware('auth:admin');
    Route::get('/banner/create', [\App\Http\Controllers\Backend\BannerController::class, 'create'])->middleware('auth:admin');
    Route::get('/banner/edit', [\App\Http\Controllers\Backend\BannerController::class, 'edit'])->middleware('auth:admin');

    /**banner group*/
    Route::post('/bannergroup/store', [\App\Http\Controllers\Backend\BannergroupController::class, 'store'])->middleware('auth:admin');
    Route::post('/bannergroup/update', [\App\Http\Controllers\Backend\BannergroupController::class, 'update'])->middleware('auth:admin');
    Route::post('/bannergroup/copy', [\App\Http\Controllers\Backend\BannergroupController::class, 'copy'])->middleware('auth:admin');
    Route::post('/bannergroup/duplicate', [\App\Http\Controllers\Backend\BannergroupController::class, 'duplicate'])->middleware('auth:admin');
    Route::post('/bannergroup/active', [\App\Http\Controllers\Backend\BannergroupController::class, 'active'])->middleware('auth:admin');
    Route::post('/bannergroup/inactive', [\App\Http\Controllers\Backend\BannergroupController::class, 'inactive'])->middleware('auth:admin');
    Route::post('/bannergroup/delete', [\App\Http\Controllers\Backend\BannergroupController::class, 'delete'])->middleware('auth:admin');
    Route::get('/bannergroup/index', [\App\Http\Controllers\Backend\BannergroupController::class, 'index'])->middleware('auth:admin');
    Route::get('/bannergroup/create', [\App\Http\Controllers\Backend\BannergroupController::class, 'create'])->middleware('auth:admin');
    Route::get('/bannergroup/edit', [\App\Http\Controllers\Backend\BannergroupController::class, 'edit'])->middleware('auth:admin');

    /**advert group */
    Route::post('/advertgroup/store', [\App\Http\Controllers\Backend\AdvertgroupController::class, 'store'])->middleware('auth:admin');
    Route::post('/advertgroup/update', [\App\Http\Controllers\Backend\AdvertgroupController::class, 'update'])->middleware('auth:admin');
    Route::post('/advertgroup/copy', [\App\Http\Controllers\Backend\AdvertgroupController::class, 'copy'])->middleware('auth:admin');
    Route::post('/advertgroup/duplicate', [\App\Http\Controllers\Backend\AdvertgroupController::class, 'duplicate'])->middleware('auth:admin');
    Route::post('/advertgroup/active', [\App\Http\Controllers\Backend\AdvertgroupController::class, 'active'])->middleware('auth:admin');
    Route::post('/advertgroup/inactive', [\App\Http\Controllers\Backend\AdvertgroupController::class, 'inactive'])->middleware('auth:admin');
    Route::post('/advertgroup/delete', [\App\Http\Controllers\Backend\AdvertgroupController::class, 'delete'])->middleware('auth:admin');
    Route::get('/advertgroup/index', [\App\Http\Controllers\Backend\AdvertgroupController::class, 'index'])->middleware('auth:admin');
    Route::get('/advertgroup/create', [\App\Http\Controllers\Backend\AdvertgroupController::class, 'create'])->middleware('auth:admin');
    Route::get('/advertgroup/edit', [\App\Http\Controllers\Backend\AdvertgroupController::class, 'edit'])->middleware('auth:admin');

    /**menu group */
    Route::post('/menugroup/store', [\App\Http\Controllers\Backend\MenugroupController::class, 'store'])->middleware('auth:admin');
    Route::post('/menugroup/update', [\App\Http\Controllers\Backend\MenugroupController::class, 'update'])->middleware('auth:admin');
    Route::post('/menugroup/active', [\App\Http\Controllers\Backend\MenugroupController::class, 'active'])->middleware('auth:admin');
    Route::post('/menugroup/inactive', [\App\Http\Controllers\Backend\MenugroupController::class, 'inactive'])->middleware('auth:admin');
    Route::post('/menugroup/delete', [\App\Http\Controllers\Backend\MenugroupController::class, 'delete'])->middleware('auth:admin');
    Route::get('/menugroup/index', [\App\Http\Controllers\Backend\MenugroupController::class, 'index'])->middleware('auth:admin');
    Route::get('/menugroup/create', [\App\Http\Controllers\Backend\MenugroupController::class, 'create'])->middleware('auth:admin');
    Route::get('/menugroup/edit', [\App\Http\Controllers\Backend\MenugroupController::class, 'edit'])->middleware('auth:admin');

    /** menu */
    Route::post('/menu/store', [\App\Http\Controllers\Backend\MenuController::class, 'store'])->middleware('auth:admin');
    Route::post('/menu/update', [\App\Http\Controllers\Backend\MenuController::class, 'update'])->middleware('auth:admin');
    Route::post('/menu/copy', [\App\Http\Controllers\Backend\MenuController::class, 'copy'])->middleware('auth:admin');
    Route::post('/menu/duplicate', [\App\Http\Controllers\Backend\MenuController::class, 'duplicate'])->middleware('auth:admin');
    Route::post('/menu/active', [\App\Http\Controllers\Backend\MenuController::class, 'active'])->middleware('auth:admin');
    Route::post('/menu/inactive', [\App\Http\Controllers\Backend\MenuController::class, 'inactive'])->middleware('auth:admin');
    Route::post('/menu/delete', [\App\Http\Controllers\Backend\MenuController::class, 'delete'])->middleware('auth:admin');
    Route::get('/menu/index', [\App\Http\Controllers\Backend\MenuController::class, 'index'])->middleware('auth:admin');
    Route::get('/menu/create', [\App\Http\Controllers\Backend\MenuController::class, 'create'])->middleware('auth:admin');
    Route::get('/menu/edit', [\App\Http\Controllers\Backend\MenuController::class, 'edit'])->middleware('auth:admin');

    /** Admin group */
    Route::post('/admingroup/active', [\App\Http\Controllers\Backend\AdmingroupController::class, 'active'])->middleware('auth:admin');
    Route::post('/admingroup/inactive', [\App\Http\Controllers\Backend\AdmingroupController::class, 'inactive'])->middleware('auth:admin');
    Route::post('/admingroup/delete', [\App\Http\Controllers\Backend\AdmingroupController::class, 'delete'])->middleware('auth:admin');
    Route::post('/admingroup/save', [\App\Http\Controllers\Backend\AdmingroupController::class, 'save'])->middleware('auth:admin');

    /**contact*/
    Route::post('/contact/update', [\App\Http\Controllers\Backend\ContactController::class, 'update'])->middleware('auth:admin');
    Route::post('/contact/active', [\App\Http\Controllers\Backend\ContactController::class, 'active'])->middleware('auth:admin');
    Route::post('/contact/inactive', [\App\Http\Controllers\Backend\ContactController::class, 'inactive'])->middleware('auth:admin');
    Route::post('/contact/delete', [\App\Http\Controllers\Backend\ContactController::class, 'delete'])->middleware('auth:admin');
    Route::get('/contact/index', [\App\Http\Controllers\Backend\ContactController::class, 'index'])->middleware('auth:admin');
    Route::get('/contact/edit', [\App\Http\Controllers\Backend\ContactController::class, 'edit'])->middleware('auth:admin');


    /**order*/
    Route::post('/order/update', [\App\Http\Controllers\Backend\OrderController::class, 'update'])->middleware('auth:admin');
    Route::post('/order/delete', [\App\Http\Controllers\Backend\OrderController::class, 'delete'])->middleware('auth:admin');
    Route::get('/order/index', [\App\Http\Controllers\Backend\OrderController::class, 'index'])->middleware('auth:admin');
    Route::get('/order/edit', [\App\Http\Controllers\Backend\OrderController::class, 'edit'])->middleware('auth:admin');
    Route::post('/order/exportOrder', [\App\Http\Controllers\Backend\OrderController::class, 'exportOrder'])->middleware('auth:admin');
    Route::post('/order/exportCustomer', [\App\Http\Controllers\Backend\OrderController::class, 'exportCustomer'])->middleware('auth:admin');
    Route::get('/order/statistic', [\App\Http\Controllers\Backend\OrderController::class, 'statistic'])->middleware('auth:admin')->name('statistic');

    /**comment */
    Route::post('/comment/store', [\App\Http\Controllers\Backend\CommentController::class, 'store'])->middleware('auth:admin');
    Route::post('/comment/update', [\App\Http\Controllers\Backend\CommentController::class, 'update'])->middleware('auth:admin');
    Route::post('/comment/active', [\App\Http\Controllers\Backend\CommentController::class, 'active'])->middleware('auth:admin');
    Route::post('/comment/inactive', [\App\Http\Controllers\Backend\CommentController::class, 'inactive'])->middleware('auth:admin');
    Route::get('/comment/index', [\App\Http\Controllers\Backend\CommentController::class, 'index'])->middleware('auth:admin');
    Route::get('/comment/edit', [\App\Http\Controllers\Backend\CommentController::class, 'edit'])->middleware('auth:admin');

    /** user */
    Route::post('/user/active', [\App\Http\Controllers\Backend\UserController::class, 'active'])->middleware('auth:admin');
    Route::post('/user/inactive', [\App\Http\Controllers\Backend\UserController::class, 'inactive'])->middleware('auth:admin');
    Route::get('/user/index', [\App\Http\Controllers\Backend\UserController::class, 'index'])->middleware('auth:admin');
    Route::get('/user/edit', [\App\Http\Controllers\Backend\UserController::class, 'edit'])->middleware('auth:admin');
    Route::post('/user/update', [\App\Http\Controllers\Backend\UserController::class, 'update'])->middleware('auth:admin');
    Route::post('/user/delete', [\App\Http\Controllers\Backend\UserController::class, 'delete'])->middleware('auth:admin');
    Route::get('/user/get-district', [\App\Http\Controllers\Backend\UserController::class, 'getDistrict'])->name('get-district');
    Route::get('/user/get-ward', [\App\Http\Controllers\Backend\UserController::class, 'getWard'])->name('get-ward');
    /*
    |-------------------------------------------------------
    | PERCEIVED VALUE
    |-------------------------------------------------------
    */
    Route::get('/perceivedvalue/index', [\App\Http\Controllers\Backend\PerceivedvalueController::class, 'index'])->middleware('auth:admin');
    Route::get('/perceivedvalue/detail', [\App\Http\Controllers\Backend\PerceivedvalueController::class, 'detail'])->middleware('auth:admin');
    Route::post('/perceivedvalue/save', [\App\Http\Controllers\Backend\PerceivedvalueController::class, 'save'])->middleware('auth:admin');
    Route::post('/perceivedvalue/inactive', [\App\Http\Controllers\Backend\PerceivedvalueController::class, 'inactive'])->middleware('auth:admin');
    Route::post('/perceivedvalue/active', [\App\Http\Controllers\Backend\PerceivedvalueController::class, 'active'])->middleware('auth:admin');
    Route::post('/perceivedvalue/delete', [\App\Http\Controllers\Backend\PerceivedvalueController::class, 'delete'])->middleware('auth:admin');
    Route::post('/perceivedvalue/sort', [\App\Http\Controllers\Backend\PerceivedvalueController::class, 'sort'])->middleware('auth:admin');


    /*
    |-------------------------------------------------------
    | SYSTEM
    |-------------------------------------------------------
    */
    Route::get('/system/configcache', [\App\Http\Controllers\Backend\SystemController::class, 'configCache'])->middleware('auth:admin');
    Route::get('/system/configclear', [\App\Http\Controllers\Backend\SystemController::class, 'configClear'])->middleware('auth:admin');

    /*
    |-------------------------------------------------------
    | CONFIG
    |-------------------------------------------------------
    */
    Route::get('/config/index', [\App\Http\Controllers\Backend\ConfigController::class, 'index'])->middleware('auth:admin');
    Route::get('/config/detail', [\App\Http\Controllers\Backend\ConfigController::class, 'detail'])->middleware('auth:admin');
    Route::post('/config/save', [\App\Http\Controllers\Backend\ConfigController::class, 'save'])->middleware('auth:admin');
    Route::get('/config/savecache', [\App\Http\Controllers\Backend\ConfigController::class, 'saveCache'])->middleware('auth:admin');
    Route::get('/config/clearcache', [\App\Http\Controllers\Backend\ConfigController::class, 'clearCache'])->middleware('auth:admin');


    /*
    |-------------------------------------------------------
    | DASHBOARD
    |-------------------------------------------------------
    */
    Route::get('/index/index', [\App\Http\Controllers\Backend\IndexController::class, 'index'])->name(ADMIN_ROUTE . '.dashboard')->middleware('auth:admin');
    Route::get('', [\App\Http\Controllers\Backend\IndexController::class, 'index'])->name(ADMIN_ROUTE . '.dashboard')->middleware('auth:admin');


    /*
    |-------------------------------------------------------
    | ADMIN MENU
    |-------------------------------------------------------
    */
    Route::get('/adminmenu/index', [\App\Http\Controllers\Backend\AdminmenuController::class, 'index'])->middleware('auth:admin');
    Route::get('/adminmenu/detail', [\App\Http\Controllers\Backend\AdminmenuController::class, 'detail'])->middleware('auth:admin');
    Route::post('/adminmenu/active', [\App\Http\Controllers\Backend\AdminmenuController::class, 'active'])->middleware('auth:admin');
    Route::post('/adminmenu/inactive', [\App\Http\Controllers\Backend\AdminmenuController::class, 'inactive'])->middleware('auth:admin');
    Route::post('/adminmenu/save', [\App\Http\Controllers\Backend\AdminmenuController::class, 'save'])->middleware('auth:admin');

    /*
    |-------------------------------------------------------
    | ADMIN USER
    |-------------------------------------------------------
    */
    Route::get('/adminuser/index', [\App\Http\Controllers\Backend\AdminuserController::class, 'index'])->middleware('auth:admin');
    Route::get('/adminuser/detail', [\App\Http\Controllers\Backend\AdminuserController::class, 'detail'])->middleware('auth:admin');
    Route::post('/adminuser/active', [\App\Http\Controllers\Backend\AdminuserController::class, 'active'])->middleware('auth:admin');
    Route::post('/adminuser/inactive', [\App\Http\Controllers\Backend\AdminuserController::class, 'inactive'])->middleware('auth:admin');
    Route::post('/adminuser/save', [\App\Http\Controllers\Backend\AdminuserController::class, 'save'])->middleware('auth:admin');
    Route::post('/adminuser/delete', [\App\Http\Controllers\Backend\AdminuserController::class, 'delete'])->middleware('auth:admin');

    /*
    |-------------------------------------------------------
    | PRODUCT CATEGORY
    |-------------------------------------------------------
    */
    Route::get('/productcategory/index', [\App\Http\Controllers\Backend\ProductcategoryController::class, 'index'])->middleware('auth:admin');
    Route::get('/productcategory/type', [\App\Http\Controllers\Backend\ProductcategoryController::class, 'type'])->middleware('auth:admin');
    Route::get('/productcategory/detail', [\App\Http\Controllers\Backend\ProductcategoryController::class, 'detail'])->middleware('auth:admin');
    Route::get('/productcategory/delete', [\App\Http\Controllers\Backend\ProductcategoryController::class, 'delete'])->middleware('auth:admin');
    Route::post('/productcategory/save', [\App\Http\Controllers\Backend\ProductcategoryController::class, 'save'])->middleware('auth:admin');

    /*
    |-------------------------------------------------------
    | BLOCK
    |-------------------------------------------------------
    */
    Route::get('/block/index', [\App\Http\Controllers\Backend\BlockController::class, 'index'])->middleware('auth:admin');
    Route::post('/block/inactive', [\App\Http\Controllers\Backend\BlockController::class, 'inactive'])->middleware('auth:admin');
    Route::post('/block/active', [\App\Http\Controllers\Backend\BlockController::class, 'active'])->middleware('auth:admin');
    Route::post('/block/sort', [\App\Http\Controllers\Backend\BlockController::class, 'sort'])->middleware('auth:admin');
    Route::post('/block/store', [\App\Http\Controllers\Backend\BlockController::class, 'store'])->middleware('auth:admin');
    Route::post('/block/update', [\App\Http\Controllers\Backend\BlockController::class, 'update'])->middleware('auth:admin');
    Route::get('/block/create', [\App\Http\Controllers\Backend\BlockController::class, 'create'])->middleware('auth:admin');
    Route::get('/block/edit', [\App\Http\Controllers\Backend\BlockController::class, 'edit'])->middleware('auth:admin');

    /**block group*/
    Route::post('/blockgroup/store', [\App\Http\Controllers\Backend\BlockgroupController::class, 'store'])->middleware('auth:admin');
    Route::post('/blockgroup/update', [\App\Http\Controllers\Backend\BlockgroupController::class, 'update'])->middleware('auth:admin');
    Route::post('/blockgroup/active', [\App\Http\Controllers\Backend\BlockgroupController::class, 'active'])->middleware('auth:admin');
    Route::post('/blockgroup/inactive', [\App\Http\Controllers\Backend\BlockgroupController::class, 'inactive'])->middleware('auth:admin');
    Route::get('/blockgroup/index', [\App\Http\Controllers\Backend\BlockgroupController::class, 'index'])->middleware('auth:admin');
    Route::get('/blockgroup/create', [\App\Http\Controllers\Backend\BlockgroupController::class, 'create'])->middleware('auth:admin');
    Route::get('/blockgroup/edit', [\App\Http\Controllers\Backend\BlockgroupController::class, 'edit'])->middleware('auth:admin');


    /**menu group */
    Route::post('/questiongroup/store', [\App\Http\Controllers\Backend\QuestiongroupController::class, 'store'])->middleware('auth:admin');
    Route::post('/questiongroup/update', [\App\Http\Controllers\Backend\QuestiongroupController::class, 'update'])->middleware('auth:admin');
    Route::post('/questiongroup/active', [\App\Http\Controllers\Backend\QuestiongroupController::class, 'active'])->middleware('auth:admin');
    Route::post('/questiongroup/inactive', [\App\Http\Controllers\Backend\QuestiongroupController::class, 'inactive'])->middleware('auth:admin');
    Route::post('/questiongroup/delete', [\App\Http\Controllers\Backend\QuestiongroupController::class, 'delete'])->middleware('auth:admin');
    Route::get('/questiongroup/index', [\App\Http\Controllers\Backend\QuestiongroupController::class, 'index'])->middleware('auth:admin');
    Route::get('/questiongroup/create', [\App\Http\Controllers\Backend\QuestiongroupController::class, 'create'])->middleware('auth:admin');
    Route::get('/questiongroup/edit', [\App\Http\Controllers\Backend\QuestiongroupController::class, 'edit'])->middleware('auth:admin');

    /** menu */
    Route::post('/question/store', [\App\Http\Controllers\Backend\QuestionController::class, 'store'])->middleware('auth:admin');
    Route::post('/question/update', [\App\Http\Controllers\Backend\QuestionController::class, 'update'])->middleware('auth:admin');
    Route::post('/question/copy', [\App\Http\Controllers\Backend\QuestionController::class, 'copy'])->middleware('auth:admin');
    Route::post('/question/duplicate', [\App\Http\Controllers\Backend\QuestionController::class, 'duplicate'])->middleware('auth:admin');
    Route::post('/question/active', [\App\Http\Controllers\Backend\QuestionController::class, 'active'])->middleware('auth:admin');
    Route::post('/question/inactive', [\App\Http\Controllers\Backend\QuestionController::class, 'inactive'])->middleware('auth:admin');
    Route::post('/question/delete', [\App\Http\Controllers\Backend\QuestionController::class, 'delete'])->middleware('auth:admin');
    Route::get('/question/index', [\App\Http\Controllers\Backend\QuestionController::class, 'index'])->middleware('auth:admin');
    Route::get('/question/create', [\App\Http\Controllers\Backend\QuestionController::class, 'create'])->middleware('auth:admin');
    Route::get('/question/edit', [\App\Http\Controllers\Backend\QuestionController::class, 'edit'])->middleware('auth:admin');


    /** menu */
    Route::post('/subscribe/store', [\App\Http\Controllers\Backend\SubscribeController::class, 'store'])->middleware('auth:admin');
    Route::post('/subscribe/update', [\App\Http\Controllers\Backend\SubscribeController::class, 'update'])->middleware('auth:admin');
    Route::post('/subscribe/copy', [\App\Http\Controllers\Backend\SubscribeController::class, 'copy'])->middleware('auth:admin');
    Route::post('/subscribe/duplicate', [\App\Http\Controllers\Backend\SubscribeController::class, 'duplicate'])->middleware('auth:admin');
    Route::post('/subscribe/active', [\App\Http\Controllers\Backend\SubscribeController::class, 'active'])->middleware('auth:admin');
    Route::post('/subscribe/inactive', [\App\Http\Controllers\Backend\SubscribeController::class, 'inactive'])->middleware('auth:admin');
    Route::post('/subscribe/delete', [\App\Http\Controllers\Backend\SubscribeController::class, 'delete'])->middleware('auth:admin');
    Route::get('/subscribe/index', [\App\Http\Controllers\Backend\SubscribeController::class, 'index'])->middleware('auth:admin');
    Route::get('/subscribe/create', [\App\Http\Controllers\Backend\SubscribeController::class, 'create'])->middleware('auth:admin');
    Route::get('/subscribe/edit', [\App\Http\Controllers\Backend\SubscribeController::class, 'edit'])->middleware('auth:admin');
    Route::post('/subscribe/export', [\App\Http\Controllers\Backend\SubscribeController::class, 'exportExcel'])->name('export-subscribe')->middleware('auth:admin');

    /** product info */
    Route::post('/productinfo/delete', [\App\Http\Controllers\Backend\ProductInfoController::class, 'delete'])->middleware('auth:admin');
    Route::get('/productinfo/index', [\App\Http\Controllers\Backend\ProductInfoController::class, 'index'])->middleware('auth:admin');
    Route::post('/productinfo/export', [\App\Http\Controllers\Backend\ProductInfoController::class, 'exportExcel'])->name('export-product-info')->middleware('auth:admin');
    Route::get('/productinfo/sort', [\App\Http\Controllers\Backend\ProductInfoController::class, 'sort'])->name('sort-product-info')->middleware('auth:admin');


    /**
     * voucher
     */
    Route::get('/voucher/index', [\App\Http\Controllers\Backend\VoucherController::class, 'index'])->middleware('auth:admin');
    Route::post('/voucher/inactive', [\App\Http\Controllers\Backend\VoucherController::class, 'inactive'])->middleware('auth:admin');
    Route::post('/voucher/active', [\App\Http\Controllers\Backend\VoucherController::class, 'active'])->middleware('auth:admin');
    Route::post('/voucher/store', [\App\Http\Controllers\Backend\VoucherController::class, 'store'])->middleware('auth:admin');
    Route::post('/voucher/update', [\App\Http\Controllers\Backend\VoucherController::class, 'update'])->middleware('auth:admin');
    Route::get('/voucher/create', [\App\Http\Controllers\Backend\VoucherController::class, 'create'])->middleware('auth:admin');
    Route::get('/voucher/edit', [\App\Http\Controllers\Backend\VoucherController::class, 'edit'])->middleware('auth:admin');
    Route::post('/voucher/delete', [\App\Http\Controllers\Backend\VoucherController::class, 'delete'])->middleware('auth:admin');
    Route::get('/voucher/select-user', [\App\Http\Controllers\Backend\VoucherController::class, 'selectUser'])->name('voucher-list-user')->middleware('auth:admin');
    Route::post('/voucher/post-select-user', [\App\Http\Controllers\Backend\VoucherController::class, 'postSelectUser'])->middleware('auth:admin');

    Route::get('/voucher/get-product-category', [\App\Http\Controllers\Backend\VoucherController::class, 'getProductCategory'])->middleware('auth:admin');
    Route::get('/voucher/get-product', [\App\Http\Controllers\Backend\VoucherController::class, 'getProduct'])->middleware('auth:admin');

    /**
     * voucher series
     */
    Route::get('/voucherserie/index', [\App\Http\Controllers\Backend\VoucherSerieController::class, 'index'])->middleware('auth:admin');
    Route::post('/voucherserie/inactive', [\App\Http\Controllers\Backend\VoucherSerieController::class, 'inactive'])->middleware('auth:admin');
    Route::post('/voucherserie/active', [\App\Http\Controllers\Backend\VoucherSerieController::class, 'active'])->middleware('auth:admin');
    Route::post('/voucherserie/store', [\App\Http\Controllers\Backend\VoucherSerieController::class, 'store'])->middleware('auth:admin');
    Route::post('/voucherserie/update', [\App\Http\Controllers\Backend\VoucherSerieController::class, 'update'])->middleware('auth:admin');
    Route::get('/voucherserie/create', [\App\Http\Controllers\Backend\VoucherSerieController::class, 'create'])->middleware('auth:admin');
    Route::get('/voucherserie/edit', [\App\Http\Controllers\Backend\VoucherSerieController::class, 'edit'])->middleware('auth:admin');
    Route::post('/voucherserie/delete', [\App\Http\Controllers\Backend\VoucherSerieController::class, 'delete'])->middleware('auth:admin');
    Route::get('/voucherserie/select-user', [\App\Http\Controllers\Backend\VoucherSerieController::class, 'selectUser'])->name('voucher-serie-list-user')->middleware('auth:admin');
    Route::post('/voucherserie/post-select-user', [\App\Http\Controllers\Backend\VoucherSerieController::class, 'postSelectUser'])->middleware('auth:admin');


    /**
     * agency
     */
    Route::get('/agency/index', [\App\Http\Controllers\Backend\AgencyController::class, 'index'])->middleware('auth:admin');
    Route::post('/agency/inactive', [\App\Http\Controllers\Backend\AgencyController::class, 'inactive'])->middleware('auth:admin');
    Route::post('/agency/active', [\App\Http\Controllers\Backend\AgencyController::class, 'active'])->middleware('auth:admin');
    Route::post('/agency/store', [\App\Http\Controllers\Backend\AgencyController::class, 'store'])->middleware('auth:admin');
    Route::post('/agency/update', [\App\Http\Controllers\Backend\AgencyController::class, 'update'])->middleware('auth:admin');
    Route::get('/agency/create', [\App\Http\Controllers\Backend\AgencyController::class, 'create'])->middleware('auth:admin');
    Route::get('/agency/edit', [\App\Http\Controllers\Backend\AgencyController::class, 'edit'])->middleware('auth:admin');
    Route::post('/agency/delete', [\App\Http\Controllers\Backend\AgencyController::class, 'delete'])->middleware('auth:admin');
    Route::get('/agency/get-province', [\App\Http\Controllers\Backend\AgencyController::class, 'getProvince'])->middleware('auth:admin');
    Route::get('/agency/get-district', [\App\Http\Controllers\Backend\AgencyController::class, 'getDistrict'])->middleware('auth:admin');
    Route::get('/agency/get-ward', [\App\Http\Controllers\Backend\AgencyController::class, 'getWard'])->middleware('auth:admin');



    /**
     * shop
     */
    Route::get('/shop/index', [\App\Http\Controllers\Backend\ShopController::class, 'index'])->middleware('auth:admin');
    Route::post('/shop/inactive', [\App\Http\Controllers\Backend\ShopController::class, 'inactive'])->middleware('auth:admin');
    Route::post('/shop/active', [\App\Http\Controllers\Backend\ShopController::class, 'active'])->middleware('auth:admin');
    Route::post('/shop/store', [\App\Http\Controllers\Backend\ShopController::class, 'store'])->middleware('auth:admin');
    Route::post('/shop/update', [\App\Http\Controllers\Backend\ShopController::class, 'update'])->middleware('auth:admin');
    Route::get('/shop/create', [\App\Http\Controllers\Backend\ShopController::class, 'create'])->middleware('auth:admin');
    Route::get('/shop/edit', [\App\Http\Controllers\Backend\ShopController::class, 'edit'])->middleware('auth:admin');
    Route::post('/shop/delete', [\App\Http\Controllers\Backend\ShopController::class, 'delete'])->middleware('auth:admin');





    $namespace = 'App\Http\Controllers\Backend\\';
    Route::get('/{controller?}/{action?}', function ($controller = 'index', $action = 'index') use ($namespace) {
        $controllerName = ucfirst($controller);
        $class          = $namespace . $controllerName.'Controller';
        $controller     = new $class();
        return $controller->{$action}();
    })->where(['controller' => '[a-z0-9]+', 'action' => '[a-z0-9]+'])->name(ADMIN_ROUTE)->middleware('auth:admin');
});




