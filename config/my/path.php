<?php
/*
|--------------------------------------------------------------------------
| Path Name
|--------------------------------------------------------------------------
|
| Tất cả path về images hoặc các files khác sẽ được khai báo ở đây.
|
*/
return [
    /*
    |--------------------------------------------------------------------------
    | Path của module banner
    |--------------------------------------------------------------------------
    */
    'image_banner_of_module' => '/public/upload/banner_m/',


    /*
    |--------------------------------------------------------------------------
    | Path của module agency
    |--------------------------------------------------------------------------
    */
    'image_agency_of_module' => '/public/upload/agency_m/',


    /*
   |--------------------------------------------------------------------------
   | Path của module shop
   |--------------------------------------------------------------------------
   */
    'image_shop_of_module' => '/public/upload/shop_m/',


    /*
    |--------------------------------------------------------------------------
    | Path của module perceivedvalue
    |--------------------------------------------------------------------------
    */
    'image_perceivedvalue_of_module' => '/public/upload/perceivedvalue_m/',

    /*
    |--------------------------------------------------------------------------
    | Path của module advert
    |--------------------------------------------------------------------------
    */
    'image_advert_of_module' => '/public/upload/advert_m/',

    /*
    |--------------------------------------------------------------------------
    | Path của module voucher
    |--------------------------------------------------------------------------
    */
    'image_voucher_of_module' => '/public/upload/voucher_m/',

    /*
    |--------------------------------------------------------------------------
    | Path của module voucher series
    |--------------------------------------------------------------------------
    */
    'image_voucher_serie_of_module' => '/public/upload/voucherserie_m/',


    /*
    |--------------------------------------------------------------------------
    | Path của module product
    |--------------------------------------------------------------------------
    */
    'image_product_avatar_of_module' => '/public/upload/product_m/avatar/',
    'image_product_thumbnail_of_module' => '/public/upload/product_m/thumbnail/',
    'image_product_banner_of_module' => '/public/upload/product_m/banner/',
    'image_product_detail_of_module' => '/public/upload/content/',

    /*
    |--------------------------------------------------------------------------
    | Path của module product variant
    |--------------------------------------------------------------------------
    */
    'image_product_variant_avatar_of_module' => '/public/upload/productvariant_m/avatar/',
    'image_product_variant_thumbnail_of_module' => '/public/upload/productvariant_m/thumbnail/',
    'image_product_variant_banner_of_module' => '/public/upload/productvariant_m/banner/',
    'image_product_variant_detail_of_module' => '/public/upload/content/',

    /*
    |--------------------------------------------------------------------------
    | Path của module post
    |--------------------------------------------------------------------------
    */
    'image_post_avatar_of_module' => '/public/upload/post_m/avatar/',
    'image_post_detail_of_module' => '/public/upload/content/',

    /*
    |--------------------------------------------------------------------------
    | Path của module postgroup
    |--------------------------------------------------------------------------
    */
    'image_postgroup_of_module' => '/public/upload/postgroup_m/',

    /*
    |--------------------------------------------------------------------------
    | Path của module producer
    |--------------------------------------------------------------------------
    */
    'image_producer_of_module' => '/public/upload/producer_m/',

    /*
    |--------------------------------------------------------------------------
    | Path của module user
    |--------------------------------------------------------------------------
    */
    'image_user_of_module' => '/public/upload/user_m/',

    /*
    |--------------------------------------------------------------------------
    | Path của module post
    |--------------------------------------------------------------------------
    */
    'image_product_cate_avatar_of_module' => '/public/upload/productcategory_m/avatar/',

];
