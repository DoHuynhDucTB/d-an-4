var mainTable = 'table.js-main-table';
var mainForm = 'form#admin-form';
var mainAction = 'div.js-admin-action';
var checkboxItem = mainTable + ' input.checkItem';

/**
 * Checkbox all
 */
$(mainTable + ' input.checkAll').click(function (){
    var isCheck = $(this).is(':checked');
    if (isCheck == true) {
        $(checkboxItem).prop( "checked", true );
    }else{
        $(checkboxItem).prop( "checked", false );
    }
});


/**
 * edit
 */
$(mainAction + ' .js-edit').click(function () {
    var url = $(this).data('url');
    var httpQuery = $(this).data('http-query');
    httpQuery = (httpQuery) ? httpQuery : '';
    var items = $(checkboxItem);
    $.each(items, function( index, item ) {
        var isCheck = $(this).is(':checked');
        if (isCheck == true) {
            window.location.replace(url + '?id=' + $(this).val() + httpQuery);
            return false;
        }
    });
});

/**
 * multiple delete
 */
$(mainAction + ' .js-deletes').click(function () {
    var url = $(this).data('url');
    if(!confirm("Bạn có chắc chắn muốn xóa? Dữ liệu sẽ không thể phục hồi!!!")) {
        return false;
    }
    $(mainForm).attr('action', url);
    $(mainForm).submit();
});


/**
 * multiple post data
 */
$(mainAction + ' .js-posts').click(function () {
    var url = $(this).data('url');
    $(mainForm).attr('action', url);
    $(mainForm).submit();
});


/**
 * multiple post edit by action type
 */
$(mainAction + ' .js-post-edit').click(function () {
    var url = $(this).data('url');
    var actionType = $(this).data('action-type');
    $(mainForm).attr('action', url);
    $(mainForm + " input[name='action_type']").attr('value', actionType);
    $(mainForm).submit();
});

/**
 * multiple post copy by action type
 */
$(mainAction + ' .js-post-copy').click(function () {
    var url = $(this).data('url');
    var actionType = $(this).data('action-type');
    $(mainForm).attr('action', url);
    $(mainForm + " input[name='action_type']").attr('value', actionType);
    $(mainForm).submit();
});


/**
 * multiple post add by action type
 */
$(mainAction + ' .js-post-add').click(function () {
    var url = $(this).data('url');
    var actionType = $(this).data('action-type');
    $(mainForm).attr('action', url);
    $(mainForm + " input[name='action_type']").attr('value', actionType);
    $(mainForm).submit();
});
