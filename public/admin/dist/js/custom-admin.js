$( document ).ready( function( ) {
    $.ajaxSetup({'cache':true});

    CKEDITOR.replaceAll( 'js-editor' );

    if ($("#editor_short").length > 0) {
        CKEDITOR.replace('editor_short', {
            height: '600px',
        });
    }
    if ($("#editor").length > 0) {
        CKEDITOR.replace('editor', {
            height: '600px',
        });
    }
    if ($("#editor_short_en").length > 0) {
        CKEDITOR.replace('editor_short_en', {
            height: '600px',
        });
    }
    if ($("#editor_en").length > 0) {
        CKEDITOR.replace('editor_en', {
            height: '600px',
        });
    }

    $( "input[name='type']" ).on('change', function() {
        var value = this.value;

        if(value == 'shoes'){
            $('.tr-shoes').css("display", "block");
            $('.tr-cosmetic').css("display", "none");
        }
        if(value == 'cosmetic'){
            $('.tr-shoes').css("display", "none");
            $('.tr-cosmetic').css("display", "block");
        }
    });

    // /** upload ảnh*/
    // if ($(".input-images-1").length > 0) {
    //     $('.input-images-1').imageUploader({
    //         imagesInputName: 'imageSmall',
    //         preloadedInputName: 'old'
    //     });
    // }
    //
    // if ($(".input-images-2").length > 0) {
    //     $('.input-images-2').imageUploader({
    //         imagesInputName: 'imageBig',
    //         preloadedInputName: 'old'
    //     });
    // }

    $('.add-image').click(function(e) {
        e.preventDefault();

        var check = $('.box-image').find('.box-item');
        var temp = 1;
        if(check.length > 0){
            var last = $(".box-image .box-item").last()
            temp = parseInt(last.data('index')) + 1;
        }

        var html = '<div class="box-item" data-index="'+temp+'">'
                        +'<div class="row">'
                            +'<div class="col-md-4">'
                                +'<input type="file" name="imageThumbnail[]" class="dropify"/>'
                            +'</div>'
                            +'<div class="col-md-4">'
                                +'<input type="file" name="imageBanner[]" class="dropify" />'
                            +'</div>'
                            +'<div class="col-md-1">'
                                +'<span class="delete-image"><i class="glyphicon glyphicon-remove"></i></span>'
                            +'</div>'
                        +'</div>'
                    +'</div>';

        $('.box-image').append(html);
        $('.dropify').dropify();
    });

    $(document.body).on('click', '.delete-image', function() {
        var item = $(this).closest('.box-item');
        item.remove();
    });

    /**
     * remove wish list
     */
    $(document.body).on('change', '#provinceId', function(event) {
        event.preventDefault();
        var id = $(this).val();

        var data = {};
        data.id = id;
        $.ajax({
            method: "GET",
            url: "/admins/user/get-district",
            data: data,
            success : function(response){
                var response = JSON.parse(response);
                var html = response.html;

                $('#districtId option').remove();
                $('#districtId').append(html);
            }
        });
    });

    /**
     * remove wish list
     */
    $(document.body).on('change', '#districtId', function(event) {
        event.preventDefault();
        var id = $(this).val();

        var data = {};
        data.id = id;

        $.ajax({
            method: "GET",
            url: "/admins/user/get-ward",
            data: data,
            success : function(response){
                var response = JSON.parse(response);
                var html = response.html;

                $('#wardId option').remove();
                $('#wardId').append(html);
            }
        });
    });

    /* Single table*/
    $('.time-statistic').daterangepicker({
        singleDatePicker: true,
        showDropdowns: true,
        "cancelClass": "btn-secondary",
        locale: {
            format: 'DD-MM-YYYY'
        }
    }, function(start, end, label) {
        var years = moment().diff(start, 'years');
    });

    /**time-export-filter*/
    $('.time-export-filter').daterangepicker({
        autoUpdateInput: false,
        singleDatePicker: true,
        showDropdowns: true,
    }, function(chosen_date) {
        $('.time-export-filter').val(chosen_date.format('DD-MM-YYYY'));
    });

    $(document.body).on('change', '.sort-param', function(event) {
        var search = $('.sort-search').val();
        var group = $('.sort-group').val();
        var color = $('.sort-color').val();
        var sex = $('.sort-sex').val();
        var size = $('.sort-size').val();

        var data = {};
        data.search = search;
        data.group = group;
        data.color = color;
        data.sex = sex;
        data.size = size;

        $.ajax({
            method: "GET",
            url: "/admins/productinfo/sort",
            data: data,
            success : function(response){
                var response = JSON.parse(response);
                var html = response.html;

                $("#admin-form table tbody tr").remove();
                $("#admin-form table tbody").html(html);
            }
        });
    });


    $(document.body).on('change', '#selectCountry', function(event) {
        event.preventDefault();
        var id = $(this).val();

        var data = {};
        data.id = id;
        $.ajax({
            method: "GET",
            url: "/admins/agency/get-province",
            data: data,
            success : function(response){
                var response = JSON.parse(response);
                var html = response.html;

                $('#selectProvince option').remove();
                $('#selectProvince').append(html);
            }
        });
    });


    $(document.body).on('change', '#selectProvince', function(event) {
        event.preventDefault();
        var id = $(this).val();

        var data = {};
        data.id = id;
        $.ajax({
            method: "GET",
            url: "/admins/agency/get-district",
            data: data,
            success : function(response){
                var response = JSON.parse(response);
                var html = response.html;

                $('#selectDistrict option').remove();
                $('#selectDistrict').append(html);
            }
        });
    });


    $(document.body).on('change', '#selectDistrict', function(event) {
        event.preventDefault();
        var id = $(this).val();

        var data = {};
        data.id = id;

        $.ajax({
            method: "GET",
            url: "/admins/agency/get-ward",
            data: data,
            success : function(response){
                var response = JSON.parse(response);
                var html = response.html;

                $('#selectWard option').remove();
                $('#selectWard').append(html);
            }
        });
    });


    $(document.body).on('change', '#SelectProductType', function(event) {
        event.preventDefault();
        var type = $(this).val();

        var data = {};
        data.type = type;

        $.ajax({
            method: "GET",
            url: "/admins/voucher/get-product-category",
            data: data,
            success : function(response){
                var response = JSON.parse(response);
                var html = response.html;

                $('#SelectProductCategory option').remove();
                $('#SelectProductCategory').append(html);
            }
        });
    });

    $(document.body).on('change', '#SelectProductCategory', function(event) {
        event.preventDefault();
        var pcatId = $(this).val();

        var data = {};
        data.pcatId = pcatId;

        $.ajax({
            method: "GET",
            url: "/admins/voucher/get-product",
            data: data,
            success : function(response){
                var response = JSON.parse(response);
                var html = response.html;

                $('#SelectProduct option').remove();
                $('#SelectProduct').append(html);
            }
        });
    });

    $(document.body).on('click', '.remove-item-cart', function(event) {
        var tr = $(this).closest('tr');
        tr.remove();
    });
});
