<?php
const DS = DIRECTORY_SEPARATOR;

/*
|-------------------------------------------------------
| ROOT ACCOUNT
|-------------------------------------------------------
*/
const ROOT_USER_IDS = [1];
const ROOT_GROUP_IDS = [1];


/*
|-------------------------------------------------------
| DATABASE TABLE
|-------------------------------------------------------
 */
const ADMIN_MENU_TBL = 'admin_menus';
const ADMIN_USER_TBL = 'admin_users';
const ADMIN_GROUP_TBL = 'admin_groups';
const PRODUCT_TBL = 'products';
const PRODUCT_CATEGORY_TBL = 'product_categories';
const PRODUCT_COLOR_TBL = 'product_colors';
const PRODUCT_SIZE_TBL = 'product_sizes';
const PRODUCT_ODOROUS_TBL = 'product_odorous';
const PRODUCT_COLLECTION_TBL = 'product_collections';
const PRODUCT_NUTRITIONS_TBL = 'product_nutritions';
const PRODUCT_USING_COLOR_TBL = 'product_using_colors';
const PRODUCT_USING_SIZE_TBL = 'product_using_sizes';
const PRODUCT_USING_ODOROUS_TBL = 'product_using_odorous';
const PRODUCT_USING_NUTRITIONS_TBL = 'product_using_nutritions';
const PRODUCT_USING_COLLECTIONS_TBL = 'product_using_collections';
const PRODUCT_USING_SKINS_TBL = 'product_using_skins';
const PRODUCT_USING_CARES_TBL = 'product_using_cares';
const PRODUCT_USING_MAKEUP_TBL = 'product_using_makeups';
const PRODUCT_SKIN_TBL = 'product_skins';
const PRODUCT_CARE_TBL = 'product_cares';
const PRODUCT_MAKE_UP_TBL = 'product_makeups';
const PRODUCT_VARIANT_TBL = 'product_variants';
const MENU_GROUP_TBL = 'menu_groups';
const MENU_TBL = 'menus';
const CONFIG_TBL = 'configs';
const QUESTION_GROUP_TBL = 'question_groups';
const QUESTION_TBL = 'questions';

const PRODUCER_TBL = 'producers';
const POST_TBL = 'posts';
const POST_GROUP_TBL = 'post_groups';
const ADVERT_TBL = 'adverts';
const CONTACT_TBL = 'contacts';
const ADVERT_GROUP_TBL = 'advert_groups';
const IMAGE_TBL = 'images';
const BANNER_TBL = 'banners';
const BANNER_GROUP_TBL = 'banner_groups';
const PERCEIVED_VALUE_TBL = 'perceived_values';
const USER_TBL = 'users';
const BLOCK_TBL = 'blocks';
const BLOCK_GROUP_TBL = 'block_groups';
const ORDER_TBL = 'orders';
const ORDER_ITEM_TBL = 'order_items';
const WARD_TBL = 'wards';
const DISTRICT_TBL = 'districts';
const PROVINCE_TBL = 'provinces';
const COMMENT_TBL = 'comments';
const SUBSCRIBE_TBL = 'subscribes';
const PRODUCT_INFO_TBL = 'product_infos';
const VOUCHER_TBL = 'vouchers';
const VOUCHER_SERIE_TBL = 'voucher_series';
const AGENCY_TBL = 'agencies';
const SHOP_TBL = 'shops';


/*
|-------------------------------------------------------
| ADMINISTRATOR
|-------------------------------------------------------
 */
const ADMIN_ROUTE = 'admins';
const ADMIN_CSS_AND_JAVASCRIPT_VERSION = 117;
const ADMIN_CSS_AND_JAVASCRIPT_PATH = '/public/admin/';

/*
|-------------------------------------------------------
| FRONTEND
|-------------------------------------------------------
 */
const FRONTEND_ROUTE = 'release';
const FRONTEND_CSS_AND_JAVASCRIPT_VERSION = 211;
const FRONTEND_CSS_AND_JAVASCRIPT_PATH = '/public/release2/';

/*
|-------------------------------------------------------
| SESSION KEY
|-------------------------------------------------------
*/
const SESSION_SUCCESS_KEY = 'success_41led9myproductbeat2686new78';
const SESSION_ERROR_KEY = 'error_12slow6061save99moment45mecentury';


/*
|-------------------------------------------------------
| URL
|-------------------------------------------------------
*/
const PUBLIC_URL = '/public/';
const ADMIN_URL = PUBLIC_URL . 'admin/';
const ADMIN_DIST_URL = ADMIN_URL . 'dist/';
const ADMIN_DIST_ICON_URL = ADMIN_DIST_URL . 'icons/';

/*
|-------------------------------------------------------
| META TAGS
|-------------------------------------------------------
*/
const META_TITLE = 'Giày nam nữ Đẹp, chất liệu cao chính hãng, Giá Tốt Nhất';
const META_DESCRIPTION = 'Giày nam nữ thời trang cao cấp chính hãng, mẫu mã đẹp, giá tốt nhất ✓ Giao hàng tận nơi trên toàn quốc ✓ Tích điểm,đổi quà miễn phí ✓ Mua sắm online đảm bảo';
const META_KEYWORD = 'giày nữ, giày nữ đẹp, giày nữ thời trang, giày nữ giảm giá, giay nu, giay nu dep, giay nu thoi trang, giay nu giam gia';


/*
|-------------------------------------------------------
| ORDER STATUS
|-------------------------------------------------------
*/
const ORDER_STATUS = [
    'new' => 'Mới đặt',
    'pending' => 'Đang xử lý',
    'processing' => 'Đang giao hàng',
    'paid' => 'Đã thanh toán',
    'cancelled' => 'Đơn hàng hủy',
];
