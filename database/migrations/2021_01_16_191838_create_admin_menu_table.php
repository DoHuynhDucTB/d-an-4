<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdminMenuTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_menus', function (Blueprint $table) {
            $table->id('admenu_id');
            $table->string('name')->collation('utf8_unicode_ci');
            $table->string('controller')->collation('utf8_unicode_ci');
            $table->string('action')->collation('utf8_unicode_ci');
            $table->enum('status', ['activated', 'inactive'])->default('inactive')->collation('utf8_unicode_ci');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin_menus');
    }
}
