<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdminUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_users', function (Blueprint $table) {
            $table->id('aduser_id');
            $table->string('name')->collation('utf8_unicode_ci');
            $table->string('username')->collation('utf8_unicode_ci')->unique();
            $table->string('email')->collation('utf8_unicode_ci')->unique();
            $table->string('avatar')->collation('utf8_unicode_ci');
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password')->collation('utf8_unicode_ci');
            $table->enum('status', ['activated', 'inactive'])->default('inactive')->collation('utf8_unicode_ci');
            $table->enum('is_deleted', ['yes', 'no'])->default('yes')->collation('utf8_unicode_ci');
            $table->rememberToken()->collation('utf8_unicode_ci');
            $table->timestamps();
            $table->integer('adgroup_id')->default(0)->comment('Id của bảng user group')->unique();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin_users');
    }
}
