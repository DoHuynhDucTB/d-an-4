<?php

namespace Database\Factories;

use App\Models\Category;
use App\Models\Post;
use Illuminate\Database\Eloquent\Factories\Factory;

class PostFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Post::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->name,
            'content' => $this->faker->text,
            'created_at' => now(),
            'updated_at' => now(),
            'category_id' => function (array $attributes) {
                return Category::all()->random(1)->first()->id;
            },
        ];
    }


    /**
     * Set up default value for 'status'
     * @return PostFactory
     */
    public function status()
    {
        return $this->state(function (array $attributes) {
            return [
                'status' => 'activated',
            ];
        });
    }

    /**
     * Configure the model factory.
     *
     * @return $this
     */
    public function configure()
    {
        return $this->afterMaking(function (Post $user) {

            //
        })->afterCreating(function (Post $user) {
            //
        });
    }
}
