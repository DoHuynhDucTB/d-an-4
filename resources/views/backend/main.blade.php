<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <title>@php echo strtoupper($_SERVER['HTTP_HOST']); @endphp - Administrator</title>
    <!-- Favicon -->
    <link rel="shortcut icon" href="/public/favicon_48x48.ico">
    <link rel="icon" href="/public/favicon_48x48.ico" type="image/x-icon">

    @include('backend.elements.css')
</head>

<body>
<!-- HK Wrapper -->
<div class="hk-wrapper hk-vertical-nav">
    <!-- Preloader -->
{{--    <div class="preloader-it">--}}
{{--        <div class="loader-pendulums"></div>--}}
{{--    </div>--}}
    <!-- Top Navbar -->
    @include('backend.elements.navbar_top')
    <!-- /Top Navbar -->
    <!-- Vertical Nav -->
    @include('backend.elements.menu_left')
    <div id="hk_nav_backdrop" class="hk-nav-backdrop"></div>
    <!-- /Vertical Nav -->

    <!-- Setting Panel -->
    <div class="hk-settings-panel">
        <div class="nicescroll-bar position-relative">
            <div class="settings-panel-wrap">
                <div class="settings-panel-head">
                    <img class="brand-img d-inline-block align-top" src="{{asset('admin/dist/img/logo-light.png')}}" alt="brand" />
                    <a href="javascript:void(0);" id="settings_panel_close" class="settings-panel-close"><span class="feather-icon"><i data-feather="x"></i></span></a>
                </div>
                <hr>
                <h6 class="mb-5">Layout</h6>
                <p class="font-14">Choose your preferred layout</p>
                <div class="layout-img-wrap">
                    <div class="row">
                        <a href="javascript:void(0);" class="col-6 mb-30 active">
                            <img class="rounded opacity-70" src="{{asset('admin/dist/img/layout1.png')}}" alt="layout">
                            <i class="zmdi zmdi-check"></i>
                        </a>
                        <a href="dashboard2.html" class="col-6 mb-30">
                            <img class="rounded opacity-70" src="{{asset('admin/dist/img/layout2.png')}}" alt="layout">
                            <i class="zmdi zmdi-check"></i>
                        </a>
                        <a href="dashboard3.html" class="col-6">
                            <img class="rounded opacity-70" src="{{asset('admin/dist/img/layout3.png')}}" alt="layout">
                            <i class="zmdi zmdi-check"></i>
                        </a>
                    </div>
                </div>
                <hr>
                <h6 class="mb-5">Navigation</h6>
                <p class="font-14">Menu comes in two modes: dark & light</p>
                <div class="button-list hk-nav-select mb-10">
                    <button type="button" id="nav_light_select" class="btn btn-outline-light btn-sm btn-wth-icon icon-wthot-bg"><span class="icon-label"><i class="fa fa-sun-o"></i> </span><span class="btn-text">Light Mode</span></button>
                    <button type="button" id="nav_dark_select" class="btn btn-outline-primary btn-sm btn-wth-icon icon-wthot-bg"><span class="icon-label"><i class="fa fa-moon-o"></i> </span><span class="btn-text">Dark Mode</span></button>
                </div>
                <hr>
                <h6 class="mb-5">Top Nav</h6>
                <p class="font-14">Choose your liked color mode</p>
                <div class="button-list hk-navbar-select mb-10">
                    <button type="button" id="navtop_light_select" class="btn btn-outline-primary btn-sm btn-wth-icon icon-wthot-bg"><span class="icon-label"><i class="fa fa-sun-o"></i> </span><span class="btn-text">Light Mode</span></button>
                    <button type="button" id="navtop_dark_select" class="btn btn-outline-light btn-sm btn-wth-icon icon-wthot-bg"><span class="icon-label"><i class="fa fa-moon-o"></i> </span><span class="btn-text">Dark Mode</span></button>
                </div>
                <hr>
                <div class="d-flex justify-content-between align-items-center">
                    <h6>Scrollable Header</h6>
                    <div class="toggle toggle-sm toggle-simple toggle-light toggle-bg-primary scroll-nav-switch"></div>
                </div>
                <button id="reset_settings" class="btn btn-primary btn-block btn-reset mt-30">Reset</button>
            </div>
        </div>
        <img class="d-none" src="{{asset('admin/dist/img/logo-light.png')}}" alt="brand" />
        <img class="d-none" src="{{asset('admin/dist/img/logo-dark.png')}}" alt="brand" />
    </div>
    <!-- /Setting Panel -->

    <!-- Main Content -->
    <div class="hk-pg-wrapper">

        <!-- Container -->
        <div class="container-fluid">
            @include('backend.elements.notify')
            @yield('content')
        </div>
        <!-- /Container -->
        <div class="clearfix"></div>
        <!-- Footer -->
        <div class="hk-footer-wrap container-fluid">
            <section class="hk-sec-wrapper">
                <div class="row">
                    <div class="col-md-12">
                        <p>@php echo date('Y'); @endphp © @php echo $_SERVER['HTTP_HOST']; @endphp. Developed by HTH.</p>
                    </div>
                </div>
            </section>

        </div>
        <!-- /Footer -->

    </div>
    <!-- /Main Content -->

</div>
<!-- /HK Wrapper -->
<input type="hidden" class="js-admin-route" value="{{ADMIN_ROUTE}}">

@include('backend.elements.javascript')

</body>
</html>
