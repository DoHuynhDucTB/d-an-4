<?php
$title = isset($data['title']) ? $data['title'] : '';
?>

@extends('backend.main')

@section('content')
    @include('backend.elements.content_action', ['title' => $title, 'action' => 'backend.elements.actions.agency.agency_add'])
    <form method="POST" id="admin-form" action="{{app('UrlHelper')::admin('agency', 'store')}}" enctype="multipart/form-data">
        <input type="hidden" name="action_type">
        @csrf
        <input type="hidden" name="typeSubmit" value="0">
        <div class="row">
            <div class="col-xl-12">
                <section class="hk-sec-wrapper">
                    <div class="row">
                        <nav id="nav-tabs">
                            <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                <a class="nav-item nav-link active" id="nav-vi-tab" data-toggle="tab" href="#nav-vi" role="tab" aria-controls="nav-vi" aria-selected="true">Thông tin (Tiếng Việt)</a>
                                <a class="nav-item nav-link" id="nav-en-tab" data-toggle="tab" href="#nav-en" role="tab" aria-controls="nav-en" aria-selected="false">Thông tin (Tiếng Anh)</a>
                                <a class="nav-item nav-link" id="nav-upload-tab" data-toggle="tab" href="#nav-upload" role="tab" aria-controls="nav-upload" aria-selected="false">Upload ảnh</a>
                            </div>
                        </nav>
                        <div class="tab-content" id="nav-tabContent">
                            <div class="tab-pane fade show active" id="nav-vi" role="tabpanel" aria-labelledby="nav-vi-tab">
                                <div class="col-xl-12">
                                    <section class="hk-sec-wrapper">
                                        <div class="row">
                                            <div class="col-sm">
                                                <div class="table-wrap">
                                                    <div class="table-responsive">
                                                        <table class="table mb-0">
                                                            <tbody>
                                                                <tr>
                                                                    <th scope="row">Tên <span class="red">*</span></th>
                                                                    <td><input type="text" name="name" class="form-control form-control-sm" placeholder="Tên"></td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Nội dung</th>
                                                                    <td><textarea id="editor" name="description" class="form-control" placeholder="Nhập nội dung"></textarea></td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Bật</th>
                                                                    <td>
                                                                        <div class="form-check form-check-inline">
                                                                            <input class="form-check-input" type="radio" name="status" value="activated" checked="">
                                                                            <label class="form-check-label" >Có</label>
                                                                        </div>
                                                                        <div class="form-check form-check-inline">
                                                                            <input class="form-check-input" type="radio" name="status" value="inactive">
                                                                            <label class="form-check-label">Không</label>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Quốc gia</th>
                                                                    <td>
                                                                        <select id="selectCountry" name="country" class="form-control custom-select form-control custom-select-sm mt-15">
                                                                            <option value="">--- Chọn quốc gia ---</option>
                                                                            <option value="1">Việt Nam</option>
                                                                            <option value="2">Nhật Bản</option>
                                                                            <option value="3">Thái Lan</option>
                                                                        </select>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Tỉnh/Thành Phố</th>
                                                                    <td>
                                                                        <select id="selectProvince" name="province" class="form-control custom-select form-control custom-select-sm mt-15">
                                                                            <option value="">--- Chọn tỉnh thành ---</option>
                                                                        </select>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Quận/Huyện</th>
                                                                    <td>
                                                                        <select id="selectDistrict" name="district" class="form-control custom-select form-control custom-select-sm mt-15">
                                                                            <option value="">--- Chọn quận/ huyện ---</option>
                                                                        </select>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Xã/Phường</th>
                                                                    <td>
                                                                        <select id="selectWard" name="ward" class="form-control custom-select form-control custom-select-sm mt-15">
                                                                            <option value="">--- Chọn xã phường ---</option>
                                                                        </select>
                                                                    </td>
                                                                </tr>
{{--                                                                <tr>--}}
{{--                                                                    <th scope="row">Địa chỉ</th>--}}
{{--                                                                    <td><textarea name="address" class="form-control" rows="10"></textarea></td>--}}
{{--                                                                </tr>--}}
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="nav-en" role="tabpanel" aria-labelledby="nav-en-tab">
                                <div class="col-xl-12">
                                    <section class="hk-sec-wrapper">
                                        <div class="row">
                                            <div class="col-sm">
                                                <div class="table-wrap">
                                                    <div class="table-responsive">
                                                        <table class="table mb-0">
                                                            <tbody>
                                                                <tr>
                                                                    <th scope="row">Tên</th>
                                                                    <td><input type="text" name="nameEn" class="form-control form-control-sm" placeholder="Tên"></td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Nội dung</th>
                                                                    <td><textarea id="editor_en" name="descriptionEn" class="form-control" placeholder="Nhập nội dung"></textarea></td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="nav-upload" role="tabpanel" aria-labelledby="nav-upload-tab">
                                <div class="col-xl-12">
                                    <section class="hk-sec-wrapper">
                                        <h5 class="hk-sec-title">Ảnh đại diện</h5>
                                        <input type="file" name="imageAvatar" class="dropify" />
                                    </section>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </form>
@endsection
