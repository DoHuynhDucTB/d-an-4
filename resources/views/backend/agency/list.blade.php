<?php
/**
 * @var \App\Helpers\UrlHelper $urlHelper
 */
$urlHelper = app('UrlHelper');
$agencies = !empty($data['agencies']) ? $data['agencies'] : [];
$title = isset($data['title']) ? $data['title'] : '';
?>

@extends('backend.main')

@section('content')
    @include('backend.elements.content_action', ['title' => $title, 'action' => 'backend.elements.actions.agency.agency_list'])
    <div class="row">
        <div class="col-xl-12">
            <section class="hk-sec-wrapper">
                <div class="row">
                    <div class="col-sm">
                        <div class="table-wrap">
                            <form action="#" method="post" id="admin-form">
                                @csrf
                                <table id="datable_1" class="table table-hover w-100 display pb-30 js-main-table">
                                    <thead>
                                    <tr>
                                        <th><input type="checkbox" class="checkAll" name="checkAll" value=""></th>
                                        <th>#</th>
                                        <th>Ảnh</th>
                                        <th>Đối tác</th>
                                        <th>Cửa hàng</th>
                                        <th>Ngày tạo</th>
                                        <th>Bật</th>
                                        <th>Id</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(!empty($agencies) && count($agencies) > 0)
                                        @foreach($agencies as $key => $item)

                                            <?php
                                            $statusName = $item->agency_status == 'activated' ? 'Bật' : 'Tắt';
                                            ?>

                                            <tr>
                                                <th><input type="checkbox" class="checkItem" name="cid[]" value="{{ $item->agency_id }}"></th>
                                                <td>{{ $key + 1 }}</td>
                                                <td>
                                                    @if(!empty($item->urlAvatar))
                                                        <img class="lazy" src="{{ asset($item->urlAvatar) }}">
                                                    @endif
                                                </td>
                                                <td><a href="@php echo $urlHelper::admin('agency', 'edit')."?id=$item->agency_id" @endphp">{{ $item->agency_name }}</a></td>
                                                <td>
                                                    <a href="{{app('UrlHelper')->admin('shop', 'index?agency='.$item->agency_id)}}">
                                                        <image src="{{ asset('public/admin/dist/icons/go_items.png') }}"></image>
                                                    </a>
                                                </td>
                                                <td>{{ $item->agency_created_at}}</td>
                                                <td>{{ $statusName }}</td>
                                                <td>{{ $item->agency_id }}</td>
                                            </tr>
                                        @endforeach
                                    @endif
                                    </tbody>
                                </table>
                            </form>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
@endsection
