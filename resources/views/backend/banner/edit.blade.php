<?php
$banner = !empty($data['banner']) ? $data['banner'] : null;
$title = !empty($data['title']) ? $data['title'] : '';
$urlAvatar = !empty($data['urlAvatar']) ? $data['urlAvatar'] : '';
$groups = !empty($data['groups']) ? $data['groups'] : null;
?>

@extends('backend.main')

@section('content')
    @include('backend.elements.content_action', ['title' => $title, 'action' => 'backend.elements.actions.banner.banner_edit'])
    <form method="POST" id="admin-form" action="{{app('UrlHelper')::admin('banner', 'update')}}" enctype="multipart/form-data">
        <input type="hidden" name="action_type">
        <input type="hidden" name="id" value="{{$banner->banner_id}}">
        <input type="hidden" name="task" value="update">
        @csrf
        <div class="row">
            <div class="col-xl-12">
                <section class="hk-sec-wrapper">
                    <div class="row">
                        <nav id="nav-tabs">
                            <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                <a class="nav-item nav-link active" id="nav-vi-tab" data-toggle="tab" href="#nav-vi" role="tab" aria-controls="nav-vi" aria-selected="true">Thông tin (Tiếng Việt)</a>
                                <a class="nav-item nav-link" id="nav-en-tab" data-toggle="tab" href="#nav-en" role="tab" aria-controls="nav-en" aria-selected="false">Thông tin (Tiếng Anh)</a>
                                <a class="nav-item nav-link" id="nav-upload-tab" data-toggle="tab" href="#nav-upload" role="tab" aria-controls="nav-upload" aria-selected="false">Upload ảnh</a>
                            </div>
                        </nav>
                        <div class="tab-content" id="nav-tabContent">
                            <div class="tab-pane fade show active" id="nav-vi" role="tabpanel" aria-labelledby="nav-vi-tab">
                                <div class="col-xl-12">
                                    <section class="hk-sec-wrapper">
                                        <div class="row">
                                            <div class="col-sm">
                                                <div class="table-wrap">
                                                    <div class="table-responsive">
                                                        <table class="table mb-0">
                                                            <tbody>
                                                                <tr>
                                                                    <th scope="row">Tên banner <span class="red">*</span></th>
                                                                    <td><input type="text" name="name" class="form-control form-control-sm" value="{{ $banner->banner_name }}" placeholder="Tên"></td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Url</th>
                                                                    <td><input type="text" name="url" class="form-control form-control-sm" placeholder="Url" value="{{ $banner->banner_url }}"></td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Thứ tự</th>
                                                                    <td><input type="number" name="sorted" class="form-control form-control-sm" placeholder="" value="{{ $banner->banner_sorted }}"></td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Màu sắc</th>
                                                                    <td>
                                                                        <input name="colorHex" class="form-control" type="color" id="colorHex" value="{{ $banner->banner_color_hex }}">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Bật</th>
                                                                    <td>
                                                                        <div class="form-check form-check-inline">
                                                                            <input class="form-check-input" type="radio" name="status" value="activated" @if($banner->banner_status == 'activated') checked @endif>
                                                                            <label class="form-check-label" >Có</label>
                                                                        </div>
                                                                        <div class="form-check form-check-inline">
                                                                            <input class="form-check-input" type="radio" name="status" value="inactive" @if($banner->banner_status == 'inactive') checked @endif>
                                                                            <label class="form-check-label">Không</label>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Nhóm banner</th>
                                                                    <td>
                                                                        <select name="groupId" class="form-control custom-select form-control custom-select-sm mt-15">
                                                                            @if($groups->count() > 0)
                                                                                <option value="">Chọn nhóm</option>
                                                                                @foreach($groups as $key => $item)
                                                                                    <option value="{{ $item->bangroup_id }}" @if($banner->bangroup_id == $item->bangroup_id) selected @endif >{{ $item->bangroup_name }}</option>
                                                                                @endforeach
                                                                            @endif
                                                                        </select>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Mô tả</th>
                                                                    <td><textarea id="editor_short" name="description" class="form-control" placeholder="Nhập mô tả">{!! $banner->banner_description !!}</textarea></td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="nav-en" role="tabpanel" aria-labelledby="nav-en-tab">
                                <div class="col-xl-12">
                                    <section class="hk-sec-wrapper">
                                        <div class="row">
                                            <div class="col-sm">
                                                <div class="table-wrap">
                                                    <div class="table-responsive">
                                                        <table class="table mb-0">
                                                            <tbody>
                                                            <tr>
                                                                <th scope="row">Tên banner <span class="red">*</span></th>
                                                                <td><input value="{{ $banner->banner_name_en}}" type="text" name="nameEn" class="form-control form-control-sm" placeholder="Tên"></td>
                                                            </tr>
                                                            <tr>
                                                                <th scope="row">Mô tả</th>
                                                                <td><textarea name="descriptionEn" class="form-control js-editor" placeholder="Nhập mô tả">{{ $banner->banner_description_en}}</textarea></td>
                                                            </tr>
                                                            <tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="nav-upload" role="tabpanel" aria-labelledby="nav-upload-tab">
                                <div class="col-xl-12">
                                    <section class="hk-sec-wrapper">
                                        <h5 class="hk-sec-title">Ảnh đại diện</h5>
                                        <input type="file" name="imageAvatar" class="dropify" @if(!empty($urlAvatar)) data-default-file="{{ asset($urlAvatar) }}" @endif/>
                                    </section>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </form>
@endsection
