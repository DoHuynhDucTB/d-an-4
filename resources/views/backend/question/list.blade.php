<?php
$questions = !empty($data['items']) ? $data['items'] : [];
$title = isset($data['title']) ? $data['title'] : '';
$group = $data['group'];

?>

@extends('backend.main')

@section('content')
    @include('backend.elements.content_action', ['title' => $title, 'action' => 'backend.elements.actions.question.question_list'])
    <div class="row">
        <div class="col-xl-12">
            <section class="hk-sec-wrapper">
                <div class="row">
                    <div class="col-sm">
                        <div class="table-wrap">
                            <form action="#" method="post" id="admin-form">
                                <input type="hidden" name="group" value="{{$group}}">
                                @csrf
                                <table id="datable_1" class="table table-hover w-100 display pb-30 js-main-table">
                                    <thead>
                                    <tr>
                                        <th><input type="checkbox" class="checkAll" name="checkAll" value=""></th>
                                        <th>#</th>
                                        <th>Tên câu hỏi</th>
                                        <th>Cấp câu hỏi</th>
                                        <th>Tên nhóm câu hỏi</th>
                                        <th>Thứ tự</th>
                                        <th>Ngày tạo</th>
                                        <th>Ngày sửa</th>
                                        <th>Bật</th>
                                        <th>Id</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(!empty($questions) && count($questions) > 0)
                                        @foreach($questions as $key => $question)
                                            <?php
                                            $status = $question->question_status == 'activated' ? 'Bật' : 'Tắt';
                                            ?>
                                            <tr>
                                                <th><input type="checkbox" class="checkItem" name="cid[]" value="{{ $question->question_id }}"></th>
                                                <td>{{ $key + 1 }}</td>
                                                <td><a href="{{app('UrlHelper')::admin('question', 'edit', ['group' => $group, 'id' => $question->question_id])}}">{{ $question->question_name }}</a></td>
                                                <td>{{ $question->question_level }}</td>
                                                <td>{{ $question->group }}</td>
                                                <td>{{ $question->question_number }}</td>
                                                <td>{{ $question->question_created_at }}</td>
                                                <td>{{ $question->question_updated_at }}</td>
                                                <td>{{ $status }}</td>
                                                <td>{{ $question->question_id }}</td>
                                            </tr>
                                        @endforeach
                                    @endif
                                    </tbody>
                                </table>
                            </form>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
@endsection
