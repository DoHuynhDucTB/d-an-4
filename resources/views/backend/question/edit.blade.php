<?php
$title = isset($data['title']) ? $data['title'] : '';
$items = !empty($data['items']) ? $data['items'] : [];
$item = !empty($data['item']) ? $data['item'] : [];
$group = $data['group'];
?>

@extends('backend.main')

@section('content')
    @include('backend.elements.content_action', ['title' => $title, 'action' => 'backend.elements.actions.question.question_edit'])
    <form method="POST" id="admin-form" action="{{app('UrlHelper')::admin('question', 'update')}}" enctype="multipart/form-data">
        <input type="hidden" name="action_type">
        <input type="hidden" name="id" value="{{$item->question_id}}">
        <input type="hidden" name="group" value="{{$group}}">
        <input type="hidden" name="task" value="update">
        @csrf
        <div class="row">
            <div class="col-xl-12">
                <section class="hk-sec-wrapper">
                    <div class="row product-module">
                        <nav id="nav-tabs">
                            <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                <a class="nav-item nav-link active" id="nav-vi-tab" data-toggle="tab" href="#nav-vi" role="tab" aria-controls="nav-vi" aria-selected="true">Thông tin (Tiếng Việt)</a>
                                <a class="nav-item nav-link" id="nav-en-tab" data-toggle="tab" href="#nav-en" role="tab" aria-controls="nav-en" aria-selected="true">Thông tin (Tiếng Anh)</a>
                            </div>
                        </nav>
                        <div class="tab-content" id="nav-tabContent">
                            <div class="tab-pane fade show active" id="nav-vi" role="tabpanel" aria-labelledby="nav-vi-tab">
                                <div class="col-xl-12">
                                    <section class="hk-sec-wrapper">
                                        <div class="row">
                                            <div class="col-sm">
                                                <div class="table-wrap">
                                                    <div class="table-responsive">
                                                        <table class="table mb-0">
                                                            <tbody>
                                                            <tr>
                                                                <th scope="row">Tên question<span class="red">*</span></th>
                                                                <td><input type="text" name="name" class="form-control form-control-sm" placeholder="Tên" value="{{ $item->question_name }}"></td>
                                                            </tr>
                                                            <tr>
                                                                <th scope="row">Câu trả lời</th>
                                                                <td><textarea id="editor_short" name="answer" class="form-control form-control-sm" placeholder="Câu trả lời">{!! $item->question_answer !!}</textarea>
                                                            </tr>
                                                            <tr>
                                                                <th scope="row">Thứ tự</th>
                                                                <td><input type="number" name="number" class="form-control form-control-sm" placeholder="" value="{{ $item->question_number }}"></td>
                                                            </tr>
                                                            <tr>
                                                                <th scope="row">question cha</th>
                                                                <td>
                                                                    <select class="form-control form-control-sm" name="parentId">
                                                                        <option value="0">--Chọn question cha--</option>
                                                                        @if($items->count() > 0)
                                                                            @foreach($items as $key => $itemquestion)
                                                                                <option value="{{ $itemquestion->question_id }}" @if($itemquestion->question_id == $item->parent_id ) selected @endif>{{ $itemquestion->question_name }}</option>
                                                                            @endforeach
                                                                        @endif
                                                                    </select>
                                                                    <span style="color: darkred; font-size: 10pt">(Không chọn mặc định sẽ là question cha)</span>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th scope="row">Bật</th>
                                                                <td>
                                                                    <div class="form-check form-check-inline">
                                                                        <input class="form-check-input" type="radio" name="status" value="activated" @if($item->question_status == 'activated') checked @endif>
                                                                        <label class="form-check-label" >Có</label>
                                                                    </div>
                                                                    <div class="form-check form-check-inline">
                                                                        <input class="form-check-input" type="radio" name="status" value="inactive" @if($item->question_status == 'inactive') checked @endif>
                                                                        <label class="form-check-label">Không</label>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="nav-en" role="tabpanel" aria-labelledby="nav-en-tab">
                                <div class="col-xl-12">
                                    <section class="hk-sec-wrapper">
                                        <div class="row">
                                            <div class="col-sm">
                                                <div class="table-wrap">
                                                    <div class="table-responsive">
                                                        <table class="table mb-0">
                                                            <tbody>
                                                            <tr>
                                                                <th scope="row">Tên question</th>
                                                                <td><input type="text" name="nameEn" value="{{ $item->question_name_en }}" class="form-control form-control-sm" placeholder="Tên"></td>
                                                            </tr>
                                                            <tr>
                                                                <th scope="row">Câu trả lời</th>
                                                                <td><textarea id="editor_short_en" name="answerEn" class="form-control form-control-sm" placeholder="Câu trả lời">{!! $item->question_answer_en !!}</textarea>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </form>
@endsection
