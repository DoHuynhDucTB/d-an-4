<?php
$vouchers  = !empty(!empty($voucher) && $voucher->user_ids) ? json_decode($voucher->user_ids, 1) : [];
$userIds =  !empty($vouchers && count($vouchers) > 0) ? array_column($vouchers, 'id') : [];
?>


<div class="modal fade bd-example-modal-lg" id="modal-list-users"  tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body">
                <div class="row">
                    @if(!empty($users) && count($users) > 0)
                        @foreach($users as $key => $item)
                            <div class="col-xl-3">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" value="{{ $item->id }}" name="userIds[]"
                                           @if(in_array($item->id, $userIds)) checked @endif
                                    >
                                    <label class="form-check-label" for="flexCheckDefault">
                                        {{ $item->name }}
                                    </label>
                                </div>
                            </div>
                        @endforeach
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
