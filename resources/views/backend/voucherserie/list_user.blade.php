<?php
/**
 * @var \App\Helpers\UrlHelper $urlHelper
 */
$urlHelper = app('UrlHelper');
$users = !empty($data['users']) ? $data['users'] : [];
$voucherSerieId = !empty($data['voucherSerieId']) ? $data['voucherSerieId'] : 0;
$userIds = !empty($data['userIds']) ? $data['userIds'] : [];
$title = isset($data['title']) ? $data['title'] : '';
?>

@extends('backend.main')

@section('content')
    @include('backend.elements.content_action', ['title' => $title, 'action' => 'backend.elements.actions.voucherserie.list_user'])
    <div class="row">
        <div class="col-xl-12">
            <section class="hk-sec-wrapper">
                <div class="row">
                    <form action="{{ route('voucher-serie-list-user') }}"  method="get" style="width: 100%;">
                        @csrf
                        <div class="col-xl-6">
                            <div class="row">
                                <div class="col-xl-8">
                                    <input class="form-control" type="text" name="value" value="" placeholder="Tên, Email, Số điện thoại">
                                    <input type="hidden" name="serie" value="{{ $voucherSerieId }}">
                                </div>
                                <div class="col-xl-4">
                                    <button class='btn btn-outline-light' type="submit">Tìm Kiếm</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div><br>
                <div class="row">
                    <div class="col-sm">
                        <div class="table-wrap">
                            <form action="#" method="post" id="admin-form">
                                <input type="hidden" name="voucherSerieId" value="{{ $voucherSerieId }}">
                                @csrf
                                <div class="table-responsive">
                                    <table class="table table-hover w-100 display pb-30 js-main-table">
                                        <thead>
                                        <tr>
                                            <th><input type="checkbox" class="checkAll" name="checkAll" value=""></th>
                                            <th>#</th>
                                            <th>Tên người dùng</th>
                                            <th>Điện thoại</th>
                                            <th>Email</th>
                                            <th>Ngày khởi tạo</th>
                                            <th>Id</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if(!empty($users) && count($users) > 0)
                                            @foreach($users as $key => $item)
                                                <tr>
                                                    <th><input type="checkbox" class="checkItem" name="cid[]" @if(in_array($item->id, $userIds)) checked @endif value="{{ $item->id }}"></th>
                                                    <td>{{ $key + 1 }}</td>
                                                    <td>{{ $item->name }}</td>
                                                    <td>{{ $item->phone }}</td>
                                                    <td>{{ $item->email }}</td>
                                                    <td>{{ $item->created_at }}</td>
                                                    <td>{{ $item->id }}</td>
                                                </tr>
                                            @endforeach
                                        @endif
                                        </tbody>
                                    </table>
                                </div>

                                <?php $id = $voucherSerieId; ?>
                                {{ $users->links('backend.elements.pagination', compact('id')) }}
                            </form>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
@endsection
