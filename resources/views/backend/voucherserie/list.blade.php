<?php
/**
 * @var \App\Helpers\UrlHelper $urlHelper
 */
$urlHelper = app('UrlHelper');
$vouchers = !empty($data['vouchers']) ? $data['vouchers'] : [];
$title = isset($data['title']) ? $data['title'] : '';
?>

@extends('backend.main')

@section('content')
    @include('backend.elements.content_action', ['title' => $title, 'action' => 'backend.elements.actions.voucherserie.voucherserie_list'])
    <div class="row">
        <div class="col-xl-12">
            <section class="hk-sec-wrapper">
                <div class="row">
                    <div class="col-sm">
                        <div class="table-wrap">
                            <form action="#" method="post" id="admin-form">
                                @csrf
                                <table id="datable_1" class="table table-hover w-100 display pb-30 js-main-table">
                                    <thead>
                                    <tr>
                                        <th><input type="checkbox" class="checkAll" name="checkAll" value=""></th>
                                        <th>#</th>
                                        <th>Mã giảm giá</th>
                                        <th>Tiêu đề</th>
                                        <th>Số Lượng</th>
                                        <th>Giảm giá</th>
                                        <th>% giảm</th>
                                        <th>Ngày tạo</th>
                                        <th>Ngày hết hạn</th>
                                        <th>Đã sử dụng</th>
                                        <th>Bật</th>
                                        <th>Id</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(!empty($vouchers) && count($vouchers) > 0)
                                        @foreach($vouchers as $key => $item)
                                            <tr>
                                                <th><input type="checkbox" class="checkItem" name="cid[]" value="{{ $item->voucher_serie_id }}"></th>
                                                <td>{{ $key + 1 }}</td>
                                                <td><a href="@php echo $urlHelper::admin('voucherserie', 'edit')."?id=$item->voucher_serie_id" @endphp">{{ $item->voucher_serie_code }}</a></td>
                                                <td><a href="@php echo $urlHelper::admin('voucherserie', 'edit')."?id=$item->voucher_serie_id" @endphp">{{ $item->voucher_serie_name }}</a></td>
                                                <td>
                                                    <a href="{{app('UrlHelper')->admin('voucher', 'index?serie='.$item->voucher_serie_id)}}">
                                                        <image src="{{ asset('public/admin/dist/icons/go_items.png') }}"></image>
                                                    </a>
                                                </td>
                                                <td>{{ $item->voucher_serie_money }}</td>
                                                <td>{{ $item->voucher_serie_percent }}</td>
                                                <td>{{ $item->voucher_serie_created_at }}</td>
                                                <td>{{ $item->voucher_serie_to }}</td>
                                                <td>{{ $item->order_number_used }}</td>
                                                <td>{{ $item->voucher_serie_status }}</td>
                                                <td>{{ $item->voucher_serie_id }}</td>
                                            </tr>
                                        @endforeach
                                    @endif
                                    </tbody>
                                </table>
                            </form>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
@endsection
