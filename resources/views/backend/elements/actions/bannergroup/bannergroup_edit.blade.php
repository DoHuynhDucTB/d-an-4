<a data-url='{{app('UrlHelper')::admin('bannergroup', 'update')}}' class='js-post-edit' data-action-type="save">
    <button class='btn btn-outline-light btn-sm'><i class='glyphicon glyphicon-plus'></i> Lưu</button>
</a>
<a data-url='{{app('UrlHelper')::admin('bannergroup', 'update')}}' class="js-post-edit" data-action-type="save_edit">
    <button class='btn btn-outline-light btn-sm'><i class='glyphicon glyphicon-pencil'></i> Lưu và Sửa</button>
</a>
<a href='{{app('UrlHelper')::admin('bannergroup', 'index')}}'>
    <button class='btn btn-outline-light btn-sm'><i class='glyphicon glyphicon-remove'></i> Thoát</button>
</a>
