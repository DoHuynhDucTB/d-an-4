<a data-url='{{app('UrlHelper')::admin('voucherserie', 'post-select-user')}}' class="js-posts">
    <button class='btn btn-outline-light btn-sm'><i class='glyphicon glyphicon-ok'></i> Chọn</button>
</a>
<a href='{{app('UrlHelper')::admin('voucherserie', 'edit', ['id' => $voucherSerieId])}}'>
    <button class='btn btn-outline-light btn-sm'><i class='glyphicon glyphicon-remove'></i> Thoát</button>
</a>
