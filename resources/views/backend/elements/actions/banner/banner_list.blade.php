<a href='{{app('UrlHelper')::admin('banner', 'create')}}' class='button-add'>
    <button class='btn btn-outline-light btn-sm'><i class='glyphicon glyphicon-plus'></i> Thêm</button>
</a>
<a data-url='{{app('UrlHelper')::admin('banner', 'edit')}}' class="js-edit">
    <button class='btn btn-outline-light btn-sm'><i class='glyphicon glyphicon-pencil'></i> Sửa</button>
</a>
{{--<a data-url='{{app('UrlHelper')::admin('banner', 'delete')}}' class="js-deletes">--}}
{{--    <button class='btn btn-outline-light btn-sm'><i class='glyphicon glyphicon-remove'></i> Xóa</button>--}}
{{--</a>--}}
<a data-url='{{app('UrlHelper')::admin('banner', 'copy')}}' class="js-posts">
    <button class='btn btn-outline-light btn-sm'><i class='glyphicon glyphicon-file'></i> Sao chép</button>
</a>
<a data-url='{{app('UrlHelper')::admin('banner', 'sort')}}' class="js-posts">
    <button class='btn btn-outline-light btn-sm'><i class='glyphicon ti-split-v'></i> Sắp xếp</button>
</a>
<a data-url='{{app('UrlHelper')::admin('banner', 'active')}}' class="js-posts">
    <button class='btn btn-outline-light btn-sm'><i class='glyphicon glyphicon-ok'></i> Bật</button>
</a>
<a data-url='{{app('UrlHelper')::admin('banner', 'inactive')}}' class="js-posts">
    <button class='btn btn-outline-light btn-sm'><i class='glyphicon glyphicon-off'></i> Tắt</button>
</a>
