<a href='{{app('UrlHelper')::admin('menu', 'create', ['group' => $group])}}' class='button-add'>
    <button class='btn btn-outline-light btn-sm'><i class='glyphicon glyphicon-plus'></i> Thêm</button>
</a>
<a data-url='{{app('UrlHelper')::admin('menu', 'edit')}}' data-http-query="&group={{$group}}" class="js-edit">
    <button class='btn btn-outline-light btn-sm'><i class='glyphicon glyphicon-pencil'></i> Sửa</button>
</a>
{{--<a data-url='{{app('UrlHelper')::admin('menu', 'delete')}}' class="js-deletes">--}}
{{--    <button class='btn btn-outline-light btn-sm'><i class='glyphicon glyphicon-remove'></i> Xóa</button>--}}
{{--</a>--}}
<a data-url='{{app('UrlHelper')::admin('menu', 'copy')}}' data-http-query="&group={{$group}}" class="js-posts">
    <button class='btn btn-outline-light btn-sm'><i class='glyphicon glyphicon-file'></i> Sao chép</button>
</a>
<a data-url='{{app('UrlHelper')::admin('menu', 'active')}}' class="js-posts">
    <button class='btn btn-outline-light btn-sm'><i class='glyphicon glyphicon-ok'></i> Bật</button>
</a>
<a data-url='{{app('UrlHelper')::admin('menu', 'inactive')}}' class="js-posts">
    <button class='btn btn-outline-light btn-sm'><i class='glyphicon glyphicon-off'></i> Tắt</button>
</a>
<a href='{{app('UrlHelper')::admin('menu', 'index', ['group' => $group])}}'>
    <button class='btn btn-outline-light btn-sm'><i class='glyphicon glyphicon-remove'></i> Thoát</button>
</a>
