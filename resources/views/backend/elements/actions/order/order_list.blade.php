<a data-url='{{app('UrlHelper')::admin('order', 'edit')}}' class="js-edit">
    <button class='btn btn-outline-light btn-sm'><i class='glyphicon glyphicon-pencil'></i> Sửa</button>
</a>
<a data-url='{{app('UrlHelper')::admin('order', 'delete')}}' class="js-deletes">
    <button class='btn btn-outline-light btn-sm'><i class='glyphicon glyphicon-remove'></i> Xóa</button>
</a>
<a data-url='{{app('UrlHelper')::admin('order', 'exportOrder')}}' class="js-posts">
    <button class='btn btn-outline-light btn-sm'><i class='glyphicon glyphicon-save'></i> Xuất excel đơn hàng</button>
</a>
<a data-url='{{app('UrlHelper')::admin('order', 'exportCustomer')}}' class="js-posts">
    <button class='btn btn-outline-light btn-sm'><i class='glyphicon glyphicon-save'></i> Xuất excel khách hàng</button>
</a>
