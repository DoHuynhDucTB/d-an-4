<a data-url='{{app('UrlHelper')::admin('voucher', 'post-select-user')}}' class="js-posts">
    <button class='btn btn-outline-light btn-sm'><i class='glyphicon glyphicon-ok'></i> Chọn</button>
</a>
<a href='{{app('UrlHelper')::admin('voucher', 'edit', ['id' => $voucherId])}}'>
    <button class='btn btn-outline-light btn-sm'><i class='glyphicon glyphicon-remove'></i> Thoát</button>
</a>
