<a href='{{app('UrlHelper')::admin('productvariant', 'create', ['type' => $type, 'product_id' => $productId])}}' class='button-add'>
    <button class='btn btn-outline-light btn-sm'><i class='glyphicon glyphicon-plus'></i> Thêm</button>
</a>
<a data-url='{{app('UrlHelper')::admin('productvariant', 'edit',['type' => $type, 'product_id' => $productId])}}' data-http-query="&type={{$type}}" class="js-edit">
    <button class='btn btn-outline-light btn-sm'><i class='glyphicon glyphicon-pencil'></i> Sửa</button>
</a>
<a data-url='{{app('UrlHelper')::admin('productvariant', 'delete',['type' => $type, 'product_id' => $productId])}}' class="js-deletes">
    <button class='btn btn-outline-light btn-sm'><i class='glyphicon glyphicon-remove'></i> Xóa</button>
</a>
<a data-url='{{app('UrlHelper')::admin('productvariant', 'active',['type' => $type, 'product_id' => $productId])}}' class="js-posts">
    <button class='btn btn-outline-light btn-sm'><i class='glyphicon glyphicon-ok'></i> Bật</button>
</a>
<a data-url='{{app('UrlHelper')::admin('productvariant', 'inactive', ['type' => $type, 'product_id' => $productId])}}' class="js-posts">
    <button class='btn btn-outline-light btn-sm'><i class='glyphicon glyphicon-off'></i> Tắt</button>
</a>
<a href='{{app('UrlHelper')::admin('productvariant', 'index', ['type' => $type, 'product_id' => $productId])}}'>
    <button class='btn btn-outline-light btn-sm'><i class='glyphicon glyphicon-remove'></i> Thoát</button>
</a>
