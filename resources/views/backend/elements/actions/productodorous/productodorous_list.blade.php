<a href='{{app('UrlHelper')::admin('productodorous', 'create')}}' class='button-add'>
    <button class='btn btn-outline-light btn-sm'><i class='glyphicon glyphicon-plus'></i> Thêm</button>
</a>
<a data-url='{{app('UrlHelper')::admin('productodorous', 'edit')}}' class="js-edit">
    <button class='btn btn-outline-light btn-sm'><i class='glyphicon glyphicon-pencil'></i> Sửa</button>
</a>
<a data-url='{{app('UrlHelper')::admin('productodorous', 'delete')}}' class="js-deletes">
    <button class='btn btn-outline-light btn-sm'><i class='glyphicon glyphicon-remove'></i> Xóa</button>
</a>
{{--<a data-url='{{app('UrlHelper')::admin('productodorous', 'copy')}}' class="js-posts">--}}
{{--    <button class='btn btn-outline-light btn-sm'><i class='glyphicon glyphicon-file'></i> Sao chép</button>--}}
{{--</a>--}}
<a data-url='{{app('UrlHelper')::admin('productodorous', 'active')}}' class="js-posts">
    <button class='btn btn-outline-light btn-sm'><i class='glyphicon glyphicon-ok'></i> Bật</button>
</a>
<a data-url='{{app('UrlHelper')::admin('productodorous', 'inactive')}}' class="js-posts">
    <button class='btn btn-outline-light btn-sm'><i class='glyphicon glyphicon-off'></i> Tắt</button>
</a>
