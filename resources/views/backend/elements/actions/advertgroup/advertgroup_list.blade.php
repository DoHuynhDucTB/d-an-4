<a href='{{app('UrlHelper')::admin('advertgroup', 'create')}}' class='button-add'>
    <button class='btn btn-outline-light btn-sm'><i class='glyphicon glyphicon-plus'></i> Thêm</button>
</a>
<a data-url='{{app('UrlHelper')::admin('advertgroup', 'edit')}}' class="js-edit">
    <button class='btn btn-outline-light btn-sm'><i class='glyphicon glyphicon-pencil'></i> Sửa</button>
</a>
{{--<a data-url='{{app('UrlHelper')::admin('advertgroup', 'delete')}}' class="js-deletes">--}}
{{--    <button class='btn btn-outline-light btn-sm'><i class='glyphicon glyphicon-remove'></i> Xóa</button>--}}
{{--</a>--}}
<a data-url='{{app('UrlHelper')::admin('advertgroup', 'copy')}}' class="js-posts">
    <button class='btn btn-outline-light btn-sm'><i class='glyphicon glyphicon-file'></i> Sao chép</button>
</a>
<a data-url='{{app('UrlHelper')::admin('advertgroup', 'active')}}' class="js-posts">
    <button class='btn btn-outline-light btn-sm'><i class='glyphicon glyphicon-ok'></i> Bật</button>
</a>
<a data-url='{{app('UrlHelper')::admin('advertgroup', 'inactive')}}' class="js-posts">
    <button class='btn btn-outline-light btn-sm'><i class='glyphicon glyphicon-off'></i> Tắt</button>
</a>
