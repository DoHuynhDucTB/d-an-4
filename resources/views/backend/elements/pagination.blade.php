@if ($paginator->hasPages())

    <div class="row">
        <div class="col-sm-12 col-md-5">
            <div class="dataTables_info" id="datable_1_info" role="status" aria-live="polite">Showing 1 to 10 of {{$paginator->count()}} entries</div>
        </div>
        <div class="col-sm-12 col-md-7">
            <div class="dataTables_paginate paging_simple_numbers" id="datable_1_paginate" style="float: right;">
                <ul class="pagination">
                    @if ($paginator->onFirstPage())
                        <li class="paginate_button page-item previous disabled" id="datable_1_previous"><a href="#" aria-controls="datable_1" tabindex="0" class="page-link">Previous</a></li>
                    @else
                        @if(!empty($id))
                            <li class="paginate_button page-item previous" id="datable_1_previous"><a href="{{ $paginator->previousPageUrl().'&id='.$id }} " aria-controls="datable_1" tabindex="0" class="page-link">Previous</a></li>
                        @else
                            <li class="paginate_button page-item previous" id="datable_1_previous"><a href="{{ $paginator->previousPageUrl() }}" aria-controls="datable_1" tabindex="0" class="page-link">Previous</a></li>
                        @endif
                    @endif


                    @foreach ($elements as $element)
                        @if (is_string($element))
                            <li class="paginate_button page-item "><a href="" aria-controls="datable_1" data-dt-idx="2" tabindex="0" class="page-link">{{ $element }}</a></li>
                        @endif
                        @if (is_array($element))
                            @foreach ($element as $page => $url)
                                @if ($page == $paginator->currentPage())
                                    <li class="paginate_button page-item active"><a href="" aria-controls="datable_1" tabindex="0" class="page-link">{{ $page }}</a></li>
                                @else
                                    @if(!empty($id))
                                        <li class="paginate_button page-item"><a href="{{ $url.'&id='.$id }}" aria-controls="datable_1" tabindex="0" class="page-link">{{ $page }}</a></li>
                                    @else
                                        <li class="paginate_button page-item"><a href="{{ $url }}" aria-controls="datable_1" tabindex="0" class="page-link">{{ $page }}</a></li>
                                    @endif
                                @endif
                            @endforeach
                        @endif
                    @endforeach


                    @if ($paginator->hasMorePages())
                            @if(!empty($id))
                                <li class="paginate_button page-item next" id="datable_1_next"><a href="{{ $paginator->nextPageUrl().'?&='.$id }}" aria-controls="datable_1" tabindex="0" class="page-link">Next</a></li>
                            @else
                                <li class="paginate_button page-item next" id="datable_1_next"><a href="{{ $paginator->nextPageUrl() }}" aria-controls="datable_1" tabindex="0" class="page-link">Next</a></li>
                            @endif
                    @else
                        <li class="paginate_button page-item next disabled" id="datable_1_next"><a href="#" aria-controls="datable_1" tabindex="0" class="page-link">Next</a></li>
                    @endif
                </ul>
            </div>
        </div>
    </div>
@endif
