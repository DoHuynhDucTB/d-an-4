<?php
/**
 * @var \App\Models\AdminMenu $adminMenuModel
 */
$adminMenuModel = app('AdminMenu');
$parentMenus    = $adminMenuModel->getMenuByUser('parent');
if (count($parentMenus) < 1) {
    return false;
}

/**
 * Childrens menu
 */
$childrenMenus  = $adminMenuModel->getMenuByUser('children');
$childrenMenuArr = [];
if (count($parentMenus) > 0) {
    /**
     * @var \App\Helpers\ArrayHelper $arrayHelper
     */
    $arrayHelper = app('ArrayHelper');
    foreach ($childrenMenus as $childrenMenu) {
        $childrenMenuArr[$childrenMenu->parent][] = $childrenMenu->toArray();
    }
}
$urlHelper = app('UrlHelper');
?>

<nav class="hk-nav hk-nav-dark">
    <a href="javascript:void(0);" id="hk_nav_close" class="hk-nav-close"><span class="feather-icon"><i data-feather="x"></i></span></a>
    <div class="nicescroll-bar">
        <div class="navbar-nav-wrap">
            <ul class="navbar-nav flex-column">
            @foreach($parentMenus as $parentMenu)
                @php
                /**
                * @var \App\Models\AdminMenu $parentMenu
                * @var \App\Helpers\UrlHelper $urlHelper
                */
                $menuUrl  = $urlHelper::admin($parentMenu->controller, $parentMenu->action);
                $iconUrl  = $urlHelper::adminIcon($parentMenu->icon);
                $menuName = $parentMenu->name;
                $menuId = $parentMenu->admenu_id;
                $newChildrenMenuArr = @$childrenMenuArr[$menuId];
                $toggleValue = ($newChildrenMenuArr) ? 'collapse' : '';
                @endphp
                <li class="nav-item">
                    <a class="nav-link" href="{{$menuUrl}}" data-toggle="{{$toggleValue}}" data-target="#menu_{{$menuId}}">
                        <i style="margin-top: 12px;"><img width="20px" src="{{$iconUrl}}"></i>
                        <span class="nav-link-text">{{$menuName}}</span>
                    </a>

                @php
                if ($newChildrenMenuArr){
                @endphp
                <ul style="background-color: #283644" id="menu_{{$menuId}}" class="nav flex-column collapse collapse-level-1">
                    <li class="nav-item">
                        <ul class="nav flex-column">
                            @foreach($newChildrenMenuArr as $value)
                                <li class="nav-item">
                                    <a style="font-weight: bolder" class="nav-link" href="{{$urlHelper::admin($value['controller'], $value['action'])}}">{{$value['name']}}</a>
                                </li>
                            @endforeach
                        </ul>
                    </li>
                </ul>
                @php
                    }
                @endphp
            @endforeach
                </li>
            </ul>
        </div>
    </div>
</nav>
