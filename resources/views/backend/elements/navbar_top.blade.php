<?php
$logo = app('Banner')::query()->where('bangroup_id', 6)->orderBy('banner_id', 'desc')->first();
?>
<nav class="navbar navbar-expand-xl navbar-light fixed-top hk-navbar">
    <a id="navbar_toggle_btn" class="navbar-toggle-btn nav-link-hover" href="javascript:void(0);"><span class="feather-icon"><i data-feather="menu"></i></span></a>
    <a class="navbar-brand" href="{{route(ADMIN_ROUTE . '.dashboard')}}">
        <img class="brand-img d-inline-block" width="30px" src="{{config('my.path.image_banner_of_module') . $logo->avatar->image_name}}" alt="brand" />
    </a>
    <ul class="navbar-nav hk-navbar-content">
        <li class="nav-item dropdown dropdown-authentication">
            <a class="nav-link dropdown-toggle no-caret" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <div class="media">
                    <div class="media-img-wrap">
                        <div class="avatar">
                            <img src="{{@asset(ADMIN_CSS_AND_JAVASCRIPT_PATH . 'dist/icons/avatar-default.png')}}" alt="user" class="avatar-img rounded-circle">
                        </div>
                        <span class="badge badge-success badge-indicator"></span>
                    </div>
                    <div class="media-body">
                        <span>
                            @auth('admin')
                                {{\Illuminate\Support\Facades\Auth::guard('admin')->user()->name}}
                            @endauth
                            <i class="zmdi zmdi-chevron-down"></i>
                        </span>
                    </div>
                </div>
            </a>
            <div class="dropdown-menu dropdown-menu-right" data-dropdown-in="flipInX" data-dropdown-out="flipOutX">
                <a class="dropdown-item" href="profile.html"><i class="dropdown-icon zmdi zmdi-account"></i><span>Profile</span></a>
                <a class="dropdown-item" href="#"><i class="dropdown-icon zmdi zmdi-settings"></i><span>Settings</span></a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="{{@route(ADMIN_ROUTE . '.logout')}}"><i class="dropdown-icon zmdi zmdi-power"></i><span>Log out</span></a>
            </div>
        </li>
    </ul>
</nav>
