<section class="hk-sec-wrapper" style="margin-top: 15px">
    <div class="hk-pg-header js-admin-action" style="margin-bottom: 0px;">
        <div>
            <h4 class="hk-pg-title" style="color:#003366; font-weight: bold">
                {{ $title }}
            </h4>
        </div>
        <div class="d-flex">
            @if(isset($action)) @include($action) @endif
        </div>
    </div>
</section>
