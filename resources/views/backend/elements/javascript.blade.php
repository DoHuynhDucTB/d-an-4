@php
    $javascriptArr = [
              'vendors/jquery/dist/jquery.min.js',
              'vendors/popper.js/dist/umd/popper.min.js',
              'vendors/bootstrap/dist/js/bootstrap.min.js',
              'vendors/jasny-bootstrap/dist/js/jasny-bootstrap.min.js',
              'vendors/jquery-image-upload/src/image-uploader.js',
              'vendors/ckeditor/ckeditor.js',
              'vendors/dropify/dist/js/dropify.min.js',
              'vendors/dropzone/dist/dropzone.js',
              'dist/js/jquery.slimscroll.js',
              'dist/js/dropdown-bootstrap-extended.js',
              'dist/js/feather.min.js',
              'vendors/jquery-toggles/toggles.min.js',
              'dist/js/toggle-data.js',
              'dist/js/init.js',
              'dist/js/custom-admin.js',
              'dist/js/form-file-upload-data.js',
              'vendors/moment/min/moment.min.js',
              'vendors/daterangepicker/daterangepicker.js',
              'dist/js/daterangepicker-data.js',
              'vendors/datatables.net/js/jquery.dataTables.min.js',
              'vendors/datatables.net-bs4/js/dataTables.bootstrap4.min.js',
              'vendors/datatables.net-dt/js/dataTables.dataTables.min.js',
              'vendors/datatables.net-buttons/js/dataTables.buttons.min.js',
              'vendors/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js',
              'vendors/datatables.net-buttons/js/buttons.flash.min.js',
              'vendors/jszip/dist/jszip.min.js',
              'vendors/pdfmake/build/pdfmake.min.js',
              'vendors/pdfmake/build/vfs_fonts.js',
              'vendors/datatables.net-buttons/js/buttons.html5.min.js',
              'vendors/datatables.net-buttons/js/buttons.print.min.js',
              'vendors/datatables.net-responsive/js/dataTables.responsive.min.js',
              'vendors/bootstrap-confirm/bootstrap-confirm-delete.js',
              'dist/js/dataTables-data.js',
              'vendors/select2/dist/js/select2.full.min.js',
              'vendors/jquery.lazy/jquery.lazy.js',
              'vendors/jquery.lazy/jquery.lazy.min.js',
              'vendors/jquery.lazy/jquery.lazy.plugins.js',
              'vendors/jquery.lazy/jquery.lazy.plugins.min.js',
              'dist/js/select2-data.js',
              'dist/js/my/style.js',
          ];
@endphp

@section('javascript_tag')
    @foreach($javascriptArr as $javascript)
        <script src="{{@asset(ADMIN_CSS_AND_JAVASCRIPT_PATH . $javascript. '?v=' . ADMIN_CSS_AND_JAVASCRIPT_VERSION)}}"></script>
    @endforeach
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>
        $('img.js-lazy-loading').each(function( index ) {
            var dataSrc = $(this).data('src');
            if (dataSrc) {
                $(this).attr('src', dataSrc);
            }
        });
    </script>
    <script>
        $( function() {
            $( ".datepicker" ).datepicker({dateFormat: 'yy-mm-dd'});
        } );
    </script>
@show
