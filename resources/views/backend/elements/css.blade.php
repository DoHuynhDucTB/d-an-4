@php
  $cssArr = [
            'vendors/jquery-toggles/css/toggles.css',
            'vendors/daterangepicker/daterangepicker.css',
            'vendors/jquery-toggles/css/themes/toggles-light.css',
            'vendors/dropify/dist/css/dropify.min.css',
            'vendors/dropzone/dist/dropzone.css',
            'vendors/jquery-image-upload/src/image-uploader.css',
            'vendors/datatables.net-dt/css/jquery.dataTables.min.css',
            'vendors/datatables.net-responsive-dt/css/responsive.dataTables.min.css',
            'vendors/select2/dist/css/select2.min.css',
            'vendors/bootstrap-confirm/bootstrap-confirm-delete.css',
            'dist/css/style.css',
            'dist/css/custom-admin.css',
        ];
@endphp

@section('css_tag')
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    @foreach($cssArr as $css)
        <link href="{{@asset(ADMIN_CSS_AND_JAVASCRIPT_PATH . $css. '?v=' . ADMIN_CSS_AND_JAVASCRIPT_VERSION)}}" rel="stylesheet" type="text/css">
    @endforeach
@show
