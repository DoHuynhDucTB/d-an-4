<?php
$title = isset($data['title']) ? $data['title'] : '';
$contact = !empty($data['contact']) ? $data['contact'] : [];
?>

@extends('backend.main')

@section('content')
    @include('backend.elements.content_action', ['title' => $title, 'action' => 'backend.elements.actions.contact.contact_edit'])
    <form method="POST" id="admin-form" action="{{app('UrlHelper')::admin('contact', 'update')}}" enctype="multipart/form-data">
        <input type="hidden" name="action_type">
        <input type="hidden" name="id" value="{{$contact->contact_id}}">
        <input type="hidden" name="task" value="update">
        @csrf
        <div class="row">
            <div class="col-xl-12">
                <section class="hk-sec-wrapper">
                    <div class="row product-module">
                        <nav id="nav-tabs">
                            <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                <a class="nav-item nav-link active" id="nav-vi-tab" data-toggle="tab" href="#nav-vi" role="tab" aria-controls="nav-vi" aria-selected="true">Thông tin (Tiếng Việt)</a>
                            </div>
                        </nav>
                        <div class="tab-content" id="nav-tabContent">
                            <div class="tab-pane fade show active" id="nav-vi" role="tabpanel" aria-labelledby="nav-vi-tab">
                                <div class="col-xl-12">
                                    <section class="hk-sec-wrapper">
                                        <div class="row">
                                            <div class="col-sm">
                                                <div class="table-wrap">
                                                    <div class="table-responsive">
                                                        <table class="table mb-0">
                                                            <tbody>
                                                            <tr>
                                                                <th scope="row">Tiêu đề</th>
                                                                <td><input type="text" name="subject" class="form-control form-control-sm" value="{{ $contact->contact_subject }}"></td>
                                                            </tr>
                                                            <tr>
                                                                <th scope="row">Người liên hệ</th>
                                                                <td><input type="text" name="name" class="form-control form-control-sm" placeholder="Tên" value="{{ $contact->contact_name }}"></td>
                                                            </tr>
                                                            <tr>
                                                                <th scope="row">Số điện thoại</th>
                                                                <td><input type="text" name="phoneNumber" class="form-control form-control-sm" value="{{ $contact->contact_phone_number }}"></td>
                                                            </tr>
                                                            <tr>
                                                                <th scope="row">Email</th>
                                                                <td><input type="text" name="email" class="form-control form-control-sm" value="{{ $contact->contact_email }}"></td>
                                                            </tr>
                                                            <tr>
                                                                <th scope="row">Nội Dung</th>
                                                                <td><textarea type="text" name="content" class="form-control">{!! $contact->contact_content !!}</textarea>
                                                            </tr>
                                                            <tr>
                                                                <th scope="row">Bật</th>
                                                                <td>
                                                                    <div class="form-check form-check-inline">
                                                                        <input class="form-check-input" type="radio" name="status" value="activated" @if($contact->contact_status == 'activated') checked @endif>
                                                                        <label class="form-check-label" >Có</label>
                                                                    </div>
                                                                    <div class="form-check form-check-inline">
                                                                        <input class="form-check-input" type="radio" name="status" value="inactive" @if($contact->contact_status == 'inactive') checked @endif>
                                                                        <label class="form-check-label">Không</label>
                                                                    </div>
                                                                </td>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </form>
@endsection
