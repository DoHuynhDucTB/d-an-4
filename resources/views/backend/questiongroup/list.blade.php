<?php
/**
 * @var \App\Helpers\UrlHelper $urlHelper
 */
$groups = !empty($data['groups']) ? $data['groups'] : [];
$urlHelper = app('UrlHelper');
$title = isset($data['title']) ? $data['title'] : '';
?>

@extends('backend.main')

@section('content')
    @include('backend.elements.content_action', ['title' => $title, 'action' => 'backend.elements.actions.questiongroup.questiongroup_list'])
    <div class="row">
        <div class="col-xl-12">
            <section class="hk-sec-wrapper">
                <div class="row">
                    <div class="col-sm">
                        <div class="table-wrap">
                            <form action="#" method="post" id="admin-form">
                                @csrf
                                <table id="datable_1" class="table table-hover w-100 display pb-30 js-main-table">
                                    <thead>
                                    <tr>
                                        <th><input type="checkbox" class="checkAll" name="checkAll" value=""></th>
                                        <th>#</th>
                                        <th>Tên nhóm câu hỏi</th>
                                        <th>Xem chi tiết nhóm câu hỏi</th>
                                        <th>Ngày tạo</th>
                                        <th>Ngày sửa</th>
                                        <th>Trạng thái:Bật/Tắt</th>
                                        <th>Id</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(!empty($groups) && count($groups) > 0)
                                        @foreach($groups as $key => $item)
                                            <?php
                                            $status = $item->questiongroup_status == 'activated' ? 'Bật' : 'Tắt';
                                            ?>
                                            <tr>
                                                <th><input type="checkbox" class="checkItem" name="cid[]" value="{{ $item->questiongroup_id }}"></th>
                                                <td>{{ $key + 1 }}</td>
                                                <td><a href="@php echo $urlHelper::admin('questiongroup', 'edit')."?id=$item->questiongroup_id" @endphp">{{ $item->questiongroup_name }}</a></td>
                                                <td>
                                                    <a href="{{app('UrlHelper')->admin('question', 'index?group='.$item->questiongroup_id)}}">
                                                        <image src="{{ asset('public/admin/dist/icons/go_items.png') }}"></image>
                                                    </a>
                                                </td>
                                                <td>{{ $item->questiongroup_created_at}}</td>
                                                <td>{{ $item->questiongroup_updated_at}}</td>
                                                <td>{{ $status}}</td>
                                                <td>{{ $item->questiongroup_id }}</td>
                                            </tr>
                                        @endforeach
                                    @endif
                                    </tbody>
                                </table>
                            </form>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
@endsection
