@extends('backend.main')
@section('content')
<div class="row" style="margin-top: 15px">
    <div class="col-xl-12">
        <section class="hk-sec-wrapper">
            <div class="row">
                @foreach($data['parentMenus'] as $menu)
                    <div class="col-md-3" style="text-align: center;padding: 20px;">
                        <div class="thumbnail">
                            <a href="{{app('UrlHelper')::admin($menu->controller, $menu->action)}}">
                                <img src="{{ADMIN_DIST_ICON_URL . $menu->icon}}" alt="" >
                                <div class="caption">
                                    <p>{{$menu->name}}</p>
                                </div>
                            </a>
                        </div>
                    </div>
                @endforeach
            </div>
        </section>
    </div>
</div>
@endsection
