<?php
$shop = !empty($data['shop']) ? $data['shop'] : null;
$title = !empty($data['title']) ? $data['title'] : '';
$urlAvatar = !empty($data['urlAvatar']) ? $data['urlAvatar'] : '';
$agency = !empty($data['agency']) ? $data['agency'] : 0;
?>

@extends('backend.main')

@section('content')
    @include('backend.elements.content_action', ['title' => $title, 'action' => 'backend.elements.actions.shop.shop_edit'])
    <form method="POST" id="admin-form" action="{{app('UrlHelper')::admin('shop', 'update')}}" enctype="multipart/form-data">
        <input type="hidden" name="action_type">
        <input type="hidden" name="id" value="{{$shop->shop_id}}">
        <input type="hidden" name="agency" value="{{$agency}}">
        @csrf
        <div class="row">
            <div class="col-xl-12">
                <section class="hk-sec-wrapper">
                    <div class="row">
                        <nav id="nav-tabs">
                            <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                <a class="nav-item nav-link active" id="nav-vi-tab" data-toggle="tab" href="#nav-vi" role="tab" aria-controls="nav-vi" aria-selected="true">Thông tin (Tiếng Việt)</a>
                                <a class="nav-item nav-link" id="nav-en-tab" data-toggle="tab" href="#nav-en" role="tab" aria-controls="nav-en" aria-selected="false">Thông tin (Tiếng Anh)</a>
                                <a class="nav-item nav-link" id="nav-upload-tab" data-toggle="tab" href="#nav-upload" role="tab" aria-controls="nav-upload" aria-selected="false">Upload ảnh</a>
                            </div>
                        </nav>
                        <div class="tab-content" id="nav-tabContent">
                            <div class="tab-pane fade show active" id="nav-vi" role="tabpanel" aria-labelledby="nav-vi-tab">
                                <div class="col-xl-12">
                                    <section class="hk-sec-wrapper">
                                        <div class="row">
                                            <div class="col-sm">
                                                <div class="table-wrap">
                                                    <div class="table-responsive">
                                                        <table class="table mb-0">
                                                            <tbody>
                                                            <tr>
                                                                <th scope="row">Tên <span class="red">*</span></th>
                                                                <td><input type="text" name="name" class="form-control form-control-sm" placeholder="Tên" value="{{ $shop->shop_name }}"></td>
                                                            </tr>
                                                            <tr>
                                                                <th scope="row">Nội dung</th>
                                                                <td><textarea id="editor" name="description" class="form-control" placeholder="Nhập nội dung">{!! $shop->shop_description !!}</textarea></td>
                                                            </tr>
                                                            <tr>
                                                                <th scope="row">Bật</th>
                                                                <td>
                                                                    <div class="form-check form-check-inline">
                                                                        <input class="form-check-input" type="radio" name="status" value="activated" @if($shop->shop_status == 'activated') checked @endif>
                                                                        <label class="form-check-label" >Có</label>
                                                                    </div>
                                                                    <div class="form-check form-check-inline">
                                                                        <input class="form-check-input" type="radio" name="status" value="inactive"  @if($shop->shop_status == 'inactive') checked @endif>
                                                                        <label class="form-check-label">Không</label>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th scope="row">Giờ mở cửa/ đóng cửa</th>
                                                                <td><input type="text" name="time" class="form-control form-control-sm"  value="{{ $shop->shop_time }}" placeholder=""></td>
                                                            </tr>
                                                            <tr>
                                                                <th scope="row">Hotline</th>
                                                                <td><input type="text" name="hotline" class="form-control form-control-sm" value="{{ $shop->shop_hotline }}" placeholder=""></td>
                                                            </tr>
                                                            <tr>
                                                                <th scope="row">Địa chỉ</th>
                                                                <td><textarea name="address" class="form-control" rows="10" style="font-size: 0.875rem;">{!! $shop->shop_address !!}</textarea></td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="nav-en" role="tabpanel" aria-labelledby="nav-en-tab">
                                <div class="col-xl-12">
                                    <section class="hk-sec-wrapper">
                                        <div class="row">
                                            <div class="col-sm">
                                                <div class="table-wrap">
                                                    <div class="table-responsive">
                                                        <table class="table mb-0">
                                                            <tbody>
                                                            <tr>
                                                                <th scope="row">Tên</th>
                                                                <td><input type="text" name="nameEn" class="form-control form-control-sm" value="{{ $shop->shop_name }}" placeholder="Tên"></td>
                                                            </tr>
                                                            <tr>
                                                                <th scope="row">Nội dung</th>
                                                                <td><textarea id="editor_en" name="descriptionEn" class="form-control" placeholder="Nhập nội dung">{!! $shop->shop_description_en !!}</textarea></td>
                                                            </tr>
                                                            <tr>
                                                                <th scope="row">Giờ mở cửa/ đóng cửa</th>
                                                                <td><input type="text" name="timeEn" class="form-control form-control-sm"  value="{{ $shop->shop_time_en }}" placeholder=""></td>
                                                            </tr>
                                                            <tr>
                                                                <th scope="row">Hotline</th>
                                                                <td><input type="text" name="hotlineEn" class="form-control form-control-sm" value="{{ $shop->shop_hotline_en }}" placeholder=""></td>
                                                            </tr>
                                                            <tr>
                                                                <th scope="row">Địa chỉ</th>
                                                                <td><textarea name="addressEn" class="form-control" rows="10" style="font-size: 0.875rem;">{!! $shop->shop_address_en !!}</textarea></td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="nav-upload" role="tabpanel" aria-labelledby="nav-upload-tab">
                                <div class="col-xl-12">
                                    <section class="hk-sec-wrapper">
                                        <h5 class="hk-sec-title">Ảnh đại diện</h5>
                                        <input type="file" name="imageAvatar" class="dropify" @if(!empty($urlAvatar)) data-default-file="{{ asset($urlAvatar) }}" @endif/>
                                    </section>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </form>
@endsection
