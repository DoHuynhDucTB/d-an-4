<?php
/**
 * @var \App\Helpers\UrlHelper $urlHelper
 */
$urlHelper = app('UrlHelper');
$shops = !empty($data['shops']) ? $data['shops'] : [];
$title = isset($data['title']) ? $data['title'] : '';
$agency = !empty($data['agency']) ? $data['agency'] : 0;
?>

@extends('backend.main')

@section('content')
    @include('backend.elements.content_action', ['title' => $title, 'action' => 'backend.elements.actions.shop.shop_list'])
    <div class="row">
        <div class="col-xl-12">
            <section class="hk-sec-wrapper">
                <div class="row">
                    <div class="col-sm">
                        <div class="table-wrap">
                            <form action="#" method="post" id="admin-form">
                                <input type="hidden" name="agency" value="{{$agency}}">
                                @csrf
                                <table id="datable_1" class="table table-hover w-100 display pb-30 js-main-table">
                                    <thead>
                                    <tr>
                                        <th><input type="checkbox" class="checkAll" name="checkAll" value=""></th>
                                        <th>#</th>
                                        <th>Ảnh</th>
                                        <th>Đối tác</th>
                                        <th>Ngày tạo</th>
                                        <th>Bật</th>
                                        <th>Id</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(!empty($shops) && count($shops) > 0)
                                        @foreach($shops as $key => $item)

                                            <?php
                                            $statusName = $item->shop_status == 'activated' ? 'Bật' : 'Tắt';
                                            ?>

                                            <tr>
                                                <th><input type="checkbox" class="checkItem" name="cid[]" value="{{ $item->shop_id }}"></th>
                                                <td>{{ $key + 1 }}</td>
                                                <td>
                                                    @if(!empty($item->urlAvatar))
                                                        <img class="lazy" src="{{ asset($item->urlAvatar) }}">
                                                    @endif
                                                </td>
                                                <td><a href="{{app('UrlHelper')::admin('shop', 'edit', ['agency' => $agency, 'id' => $item->shop_id])}}" @endphp">{{ $item->shop_name }}</a></td>
                                                <td>{{ $item->shop_created_at}}</td>
                                                <td>{{ $statusName }}</td>
                                                <td>{{ $item->shop_id }}</td>
                                            </tr>
                                        @endforeach
                                    @endif
                                    </tbody>
                                </table>
                            </form>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
@endsection
