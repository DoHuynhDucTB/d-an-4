<?php
/**
 * @var \App\Helpers\UrlHelper $urlHelper
 */
$urlHelper = app('UrlHelper');
$infos = !empty($data['infos']) ? $data['infos'] : [];
$title = isset($data['title']) ? $data['title'] : '';

$colors = !empty($data['colors']) ? $data['colors'] : [];
$sizes = !empty($data['sizes']) ? $data['sizes'] : [];
?>

@extends('backend.main')

@section('content')
    @include('backend.elements.content_action', ['title' => $title, 'action' => 'backend.elements.actions.productinfo.productinfo_list'])
    <div class="row">
        <div class="col-xl-12">
            <section class="hk-sec-wrapper">
                <div class="row">
                    <form action="{{ route('export-product-info') }}"  method="post" id="export-product-info" style="width: 100%;">
                        @csrf
                        <div class="col-xl-12">
                            <div class="row">
                                <div class="col-xl-2">
                                    <input type="text" value="" name="search" class="form-control form-control-sm sort-param sort-search" placeholder="search">
                                </div>
                                <div class="col-xl-2">
                                    <select name="group" class="form-control custom-select form-control custom-select-sm sort-param sort-group">
                                        <option value="">--- Chọn nhóm---</option>
                                        <option value="shoes">giày</option>
                                        <option value="sandals">dép</option>
                                        <option value="raincoat">áo mưa</option>
                                        <option value="accessories">phụ kiện</option>
                                    </select>
                                </div>
                                <div class="col-xl-2">
                                    <select name="color" class="form-control custom-select form-control custom-select-sm sort-param sort-color">
                                        <option value="">--- Chọn màu---</option>
                                        @if(count($colors) > 0)
                                            @foreach($colors as $key => $color)
                                                <option value="{{ $color->pcolor_id }}">{{ $color->pcolor_code}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                                <div class="col-xl-2">
                                    <select name="sex" class="form-control custom-select form-control custom-select-sm sort-param sort-sex">
                                        <option value="">--- Chọn giới tính---</option>
                                        <option value="male">Nam</option>
                                        <option value="female">Nữ-</option>
                                    </select>
                                </div>
                                <div class="col-xl-2">
                                    <select name="size" class="form-control custom-select form-control custom-select-sm sort-param sort-size">
                                        <option value="">--- Chọn size---</option>
                                        @if(count($sizes) > 0)
                                            @foreach($sizes as $key => $size)
                                                <option value="{{ $size->psize_id }}">{{ $size->psize_value }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                                <div class="col-xl-2">
                                    <button class='btn btn-outline-light btn-sm' type="submit">Xuất excel</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div><br>
                <div class="row">
                    <div class="col-sm">
                        <div class="table-wrap">
                            <form action="#" method="post" id="admin-form">
                                @csrf
                                <table class="table table-hover w-100 display pb-30 js-main-table">
                                    <thead>
                                    <tr>
                                        <th><input type="checkbox" class="checkAll" name="checkAll" value=""></th>
                                        <th>#</th>
                                        <th>Email</th>
                                        <th>Mã sản phẩm chung</th>
                                        <th>Tên sản phẩm chung</th>
                                        <th>Mã biến thể</th>
                                        <th>Tên biến thể</th>
                                        <th>Màu</th>
                                        <th>Giới tính</th>
                                        <th>Size</th>
                                        <th>Ngày tạo</th>
                                        <th>Id</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(!empty($infos) && count($infos) > 0)
                                        @foreach($infos as $key => $item)
                                            <tr>
                                                <th><input type="checkbox" class="checkItem" name="cid[]" value="{{ $item->id }}"></th>
                                                <td>{{ $key + 1 }}</td>
                                                <td>{{ $item->email }}</td>
                                                <td>{{ $item->product_general_code }}</td>
                                                <td>{{ $item->product_general_name }}</td>
                                                <td>{{ $item->product_variant_code }}</td>
                                                <td>{{ $item->product_variant_name }}</td>
                                                <td>{{ $item->color_name }}</td>
                                                <td>{{ $item->sex }}</td>
                                                <td>{{ $item->size_name }}</td>
                                                <td>{{ $item->created_at}}</td>
                                                <td>{{ $item->id }}</td>
                                            </tr>
                                        @endforeach
                                    @endif
                                    </tbody>
                                </table>
                                {{ $infos->links('backend.elements.pagination') }}
                            </form>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
@endsection

