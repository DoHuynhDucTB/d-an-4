<?php
$logo = app('Banner')::query()->where('bangroup_id', 6)->orderBy('banner_id', 'desc')->first();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <title>@php echo strtoupper($_SERVER['HTTP_HOST']); @endphp - Login</title>

    <!-- Toggles CSS -->
    <link href="{{@asset(ADMIN_CSS_AND_JAVASCRIPT_PATH . 'vendors/jquery-toggles/css/toggles.css')}}" rel="stylesheet" type="text/css">
    <link href="{{@asset(ADMIN_CSS_AND_JAVASCRIPT_PATH . 'vendors/jquery-toggles/css/themes/toggles-light.css')}}" rel="stylesheet" type="text/css">

    <!-- Custom CSS -->
    <link href="{{@asset(ADMIN_CSS_AND_JAVASCRIPT_PATH . 'dist/css/style.css')}}" rel="stylesheet" type="text/css">
</head>
<body>

<!-- HK Wrapper -->
<div class="hk-wrapper">
    <!-- Main Content -->
    <div class="hk-pg-wrapper hk-auth-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-xl-12 pa-0">
                    <div class="auth-form-wrap pt-xl-0 pt-70">
                        <div class="auth-form w-xl-30 w-lg-55 w-sm-75 w-100" style="padding: 15px; background-color: #fff">
                            <a class="auth-brand text-center d-block mb-20" href="#">
                                <img class="brand-img" src="{{asset(config('my.path.image_banner_of_module') . $logo->avatar->image_name)}}" alt="brand"/>
                            </a>
                            <form method="post" action="{{route(ADMIN_ROUTE . '.login', []) }}">
                                @if ( session('errors'))
                                    <div class="alert alert-danger alert-block">
                                        <button type="button" class="close" data-dismiss="alert">×</button>
                                        <strong>{{ session('errors')->first() }}</strong>
                                    </div>
                                @endif
                                <h1 class="display-4 text-center mb-10">Login to Admin</h1>
                                <div class="form-group">
                                    <input name="username" class="form-control" placeholder="Tên đăng nhập" type="text">
                                </div>
                                <div class="form-group">
                                    <div class="input-group">
                                        <input class="form-control" placeholder="Mật khẩu" type="password" name="password">
                                        <div class="input-group-append">
                                            <span class="input-group-text"><span class="feather-icon"><i data-feather="eye-off"></i></span></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="custom-control custom-checkbox mb-25">
                                    <input class="custom-control-input" name="remember-me" id="remember-me" type="checkbox" checked>
                                    <label class="custom-control-label font-14" for="remember-me">Lưu thông tin đăng nhập</label>
                                </div>
                                <button class="btn btn-primary btn-block" type="submit">Đăng nhập</button>
                                @csrf
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /Main Content -->
</div>
<!-- /HK Wrapper -->

<!-- JavaScript -->

<!-- jQuery -->
<script src="{{@asset(ADMIN_CSS_AND_JAVASCRIPT_PATH . 'vendors/jquery/dist/jquery.min.js')}}"></script>

<!-- Bootstrap Core JavaScript -->
<script src="{{@asset(ADMIN_CSS_AND_JAVASCRIPT_PATH . 'vendors/popper.js/dist/umd/popper.min.js')}}"></script>
<script src="{{@asset(ADMIN_CSS_AND_JAVASCRIPT_PATH . 'vendors/bootstrap/dist/js/bootstrap.min.js')}}"></script>

<!-- Slimscroll JavaScript -->
<script src="{{@asset(ADMIN_CSS_AND_JAVASCRIPT_PATH . 'dist/js/jquery.slimscroll.js')}}"></script>

<!-- Fancy Dropdown JS -->
<script src="{{@asset(ADMIN_CSS_AND_JAVASCRIPT_PATH . 'dist/js/dropdown-bootstrap-extended.js')}}"></script>

<!-- FeatherIcons JavaScript -->
<script src="{{@asset(ADMIN_CSS_AND_JAVASCRIPT_PATH . 'dist/js/feather.min.js')}}"></script>

<!-- Init JavaScript -->
<script src="{{@asset(ADMIN_CSS_AND_JAVASCRIPT_PATH . 'dist/js/init.js')}}"></script>
</body>
</html>
