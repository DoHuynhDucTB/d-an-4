<?php
/**
 * @var \App\Helpers\UrlHelper $urlHelper
 */
$urlHelper = app('UrlHelper');
$subscribes = !empty($data['subscribes']) ? $data['subscribes'] : [];
$title = isset($data['title']) ? $data['title'] : '';
?>

@extends('backend.main')

@section('content')
    @include('backend.elements.content_action', ['title' => $title, 'action' => 'backend.elements.actions.subscribe.subscribe_list'])
    <div class="row">
        <div class="col-xl-12">
            <section class="hk-sec-wrapper">
                <div class="row">
                    <form action="{{ route('export-subscribe') }}"  method="post" id="export-newsletter" style="width: 100%;">
                        @csrf
                        <div class="col-xl-6">
                            <div class="row">
                                <div class="col-xl-5">
                                    <input class="form-control time-export-filter" type="text" name="time" value="">
                                </div>
                                <div class="col-xl-4">
                                    <select class="form-control custom-select d-block w-100" name="status">
                                        <option value="">Chọn</option>
                                        <option value="activated">Bật</option>
                                        <option value="inactive">Tắt</option>
                                    </select>
                                </div>
                                <div class="col-xl-3">
                                    <button class='btn btn-outline-light' type="submit">Xuất excel</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div><br>
                <div class="row">
                    <div class="col-sm">
                        <div class="table-wrap">
                            <form action="#" method="post" id="admin-form">
                                @csrf
                                <table id="datable_1" class="table table-hover w-100 display pb-30 js-main-table">
                                    <thead>
                                    <tr>
                                        <th><input type="checkbox" class="checkAll" name="checkAll" value=""></th>
                                        <th>#</th>
                                        <th>Email</th>
                                        <th>Tên</th>
                                        <th>Ngày tạo</th>
                                        <th>Trạng thái</th>
                                        <th>Id</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(count($subscribes) > 0)
                                        @foreach($subscribes as $key => $item)
                                            <tr>
                                                <th><input type="checkbox" class="checkItem" name="cid[]" value="{{ $item->subscribe_id }}"></th>
                                                <td>{{ $key + 1 }}</td>
                                                <td><a href="@php echo $urlHelper::admin('subscribe', 'edit')."?id=$item->subscribe_id" @endphp">{{ $item->subscribe_email }}</a></td>
                                                <td>{{$item->subscribe_name }}</td>
                                                <td>{{$item->subscribe_created_at }}</td>
                                                <td>{{$item->subscribe_status }}</td>
                                                <td>{{$item->subscribe_id}}</td>
                                            </tr>
                                        @endforeach
                                    @endif
                                    </tbody>
                                </table>
                            </form>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
@endsection
