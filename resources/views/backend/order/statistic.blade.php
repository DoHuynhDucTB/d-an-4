<?php
/**
 * @var \App\Helpers\UrlHelper $urlHelper
 */
$urlHelper = app('UrlHelper');
$title = isset($data['title']) ? $data['title'] : '';

$contacts = !empty($data['contacts']) ? $data['contacts'] : [];

$user = !empty($data['user']) ? $data['user'] : [];
$userFacebook = !empty($data['userFacebook']) ? $data['userFacebook'] : [];
$userGoogle = !empty($data['userGoogle']) ? $data['userGoogle'] : [];
$userEmail = !empty($data['userEmail']) ? $data['userEmail'] : [];

$order = !empty($data['order']) ? $data['order'] : [];
$orderNew = !empty($data['orderNew']) ? $data['orderNew'] : [];
$orderPending = !empty($data['orderPending']) ? $data['orderPending'] : [];
$orderProcessing = !empty($data['orderProcessing']) ? $data['orderProcessing'] : [];
$orderPaid = !empty($data['orderPaid']) ? $data['orderPaid'] : [];
$orderCancelled = !empty($data['orderCancelled']) ? $data['orderCancelled'] : [];
$orderSumPaid = $data['orderSumPaid'];

$orderLastest = !empty($data['orderLastest']) ? $data['orderLastest'] : [];
$orderItems = !empty($data['orderItems']) ? $data['orderItems'] : [];

$fromTime = $data['fromTime'];
$toTime = $data['toTime'];

?>

@extends('backend.main')

@section('content')
    @include('backend.elements.content_action', ['title' => $title])

    <section class="hk-sec-wrapper" style="margin-top: 15px">
        <form method="GET" enctype="multipart/form-data" action="{{ route('statistic') }}">
            @csrf
            <div class="row">
                <div class="col-md-4">
                    <input class="form-control time-statistic" type="text" name="fromTime" value="{{ $fromTime }}" />
                </div>
                <div class="col-md-4">
                    <input class="form-control time-statistic" type="text" name="toTime" value="{{ $toTime }}" />
                </div>
                <div class="col-md-4">
                    <button class="btn btn-primary" type="submit">Xem thống kê</button>
                </div>
            </div>
        </form>
    </section>

    <div class="row">

        <div class="col-md-4">
            <section class="hk-sec-wrapper" style="margin-top: 15px">
                <div class="media align-items-center">
                    <div class="d-flex media-img-wrap mr-15">
                        <div class="avatar avatar-sm">
							<span class="avatar-text avatar-text-inv-primary">
                                <span class="initial-wrap"><span><i class="zmdi zmdi-money"></i></span></span>
                            </span>
                        </div>
                    </div>
                    <div class="media-body">
                        <span class="d-block text-dark text-cap[italize text-truncate ">THÀNH TIỀN ĐƠN HÀNG HOÀN THÀNH</span>
                        <span class="d-block  text-truncate ">{{ number_format($orderSumPaid, 0, '.', ',') }} đ</span>
                    </div>
                </div>
            </section>
        </div>
        <div class="col-md-4">
            <section class="hk-sec-wrapper" style="margin-top: 15px">
                <div class="media align-items-center">
                    <div class="d-flex media-img-wrap mr-15">
                        <div class="avatar avatar-sm">
							<span class="avatar-text avatar-text-inv-primary">
                                <span class="initial-wrap"><span><i class="zmdi zmdi-shopping-cart"></i></span></span>
                            </span>
                        </div>
                    </div>
                    <div class="media-body">
                        <span class="d-block text-dark text-capitalize text-truncate ">SỐ LƯỢNG ĐƠN HÀNG HOÀN THÀNH</span>
                        <span class="d-block  text-truncate ">{{ $orderPaid->count() }}</span>
                    </div>
                </div>
            </section>
        </div>
        <div class="col-md-4">
            <section class="hk-sec-wrapper" style="margin-top: 15px">
                <div class="media align-items-center">
                    <div class="d-flex media-img-wrap mr-15">
                        <div class="avatar avatar-sm">
							<span class="avatar-text avatar-text-inv-primary">
                                <span class="initial-wrap"><span><i class="zmdi zmdi-shopping-cart"></i></span></span>
                            </span>
                        </div>
                    </div>
                    <div class="media-body">
                        <span class="d-block text-dark text-capitalize text-truncate ">TỔNG SỐ ĐƠN HÀNG</span>
                        <span class="d-block  text-truncate ">{{ $order->count() }}</span>
                    </div>
                </div>
            </section>
        </div>
        <div class="col-md-4">
            <section class="hk-sec-wrapper" style="margin-top: 15px">
                <div class="media align-items-center">
                    <div class="d-flex media-img-wrap mr-15">
                        <div class="avatar avatar-sm">
							<span class="avatar-text avatar-text-inv-primary">
                                <span class="initial-wrap"><span><i class="zmdi zmdi-shopping-cart"></i></span></span>
                            </span>
                        </div>
                    </div>
                    <div class="media-body">
                        <span class="d-block text-dark text-capitalize text-truncate ">ĐƠN HÀNG MỚI ĐẶT</span>
                        <span class="d-block  text-truncate ">{{ $orderNew->count()  }}</span>
                    </div>
                </div>
            </section>
        </div>
        <div class="col-md-4">
            <section class="hk-sec-wrapper" style="margin-top: 15px">
                <div class="media align-items-center">
                    <div class="d-flex media-img-wrap mr-15">
                        <div class="avatar avatar-sm">
							<span class="avatar-text avatar-text-inv-primary">
                                <span class="initial-wrap"><span><i class="zmdi zmdi-shopping-cart"></i></span></span>
                            </span>
                        </div>
                    </div>
                    <div class="media-body">
                        <span class="d-block text-dark text-capitalize text-truncate ">ĐƠN HÀNG ĐANG GIAO</span>
                        <span class="d-block  text-truncate ">{{ $orderProcessing->count() }}</span>
                    </div>
                </div>
            </section>
        </div>
        <div class="col-md-4">
            <section class="hk-sec-wrapper" style="margin-top: 15px">
                <div class="media align-items-center">
                    <div class="d-flex media-img-wrap mr-15">
                        <div class="avatar avatar-sm">
							<span class="avatar-text avatar-text-inv-primary">
                                <span class="initial-wrap"><span><i class="zmdi zmdi-shopping-cart"></i></span></span>
                            </span>
                        </div>
                    </div>
                    <div class="media-body">
                        <span class="d-block text-dark text-capitalize text-truncate ">ĐƠN HÀNG HỦY</span>
                        <span class="d-block  text-truncate ">{{ $orderCancelled->count() }}</span>
                    </div>
                </div>
            </section>
        </div>
        <div class="col-md-4">
            <section class="hk-sec-wrapper" style="margin-top: 15px">
                <div class="media align-items-center">
                    <div class="d-flex media-img-wrap mr-15">
                        <div class="avatar avatar-sm">
							<span class="avatar-text avatar-text-inv-primary">
                                <span class="initial-wrap"><span><i class="zmdi zmdi-account"></i></span></span>
                            </span>
                        </div>
                    </div>
                    <div class="media-body">
                        <span class="d-block text-dark text-capitalize text-truncate ">USER ĐĂNG KÝ</span>
                        <span class="d-block  text-truncate ">{{ $user->count() }}</span>
                    </div>
                </div>
            </section>
        </div>
        <div class="col-md-4">
            <section class="hk-sec-wrapper" style="margin-top: 15px">
                <div class="media align-items-center">
                    <div class="d-flex media-img-wrap mr-15">
                        <div class="avatar avatar-sm">
							<span class="avatar-text avatar-text-inv-primary">
                                <span class="initial-wrap"><span><i class="zmdi zmdi-account"></i></span></span>
                            </span>
                        </div>
                    </div>
                    <div class="media-body">
                        <span class="d-block text-dark text-capitalize text-truncate ">DÙNG FACEBOOK</span>
                        <span class="d-block  text-truncate ">{{ $userFacebook->count() }}</span>
                    </div>
                </div>
            </section>
        </div>
        <div class="col-md-4">
            <section class="hk-sec-wrapper" style="margin-top: 15px">
                <div class="media align-items-center">
                    <div class="d-flex media-img-wrap mr-15">
                        <div class="avatar avatar-sm">
							<span class="avatar-text avatar-text-inv-primary">
                                <span class="initial-wrap"><span><i class="zmdi zmdi-account"></i></span></span>
                            </span>
                        </div>
                    </div>
                    <div class="media-body">
                        <span class="d-block text-dark text-capitalize text-truncate ">DÙNG GOOGLE</span>
                        <span class="d-block  text-truncate ">{{ $userGoogle->count() }}</span>
                    </div>
                </div>
            </section>
        </div>
        <div class="col-md-4">
            <section class="hk-sec-wrapper" style="margin-top: 15px">
                <div class="media align-items-center">
                    <div class="d-flex media-img-wrap mr-15">
                        <div class="avatar avatar-sm">
							<span class="avatar-text avatar-text-inv-primary">
                                <span class="initial-wrap"><span><i class="zmdi zmdi-account"></i></span></span>
                            </span>
                        </div>
                    </div>
                    <div class="media-body">
                        <span class="d-block text-dark text-capitalize text-truncate ">DÙNG MAIL</span>
                        <span class="d-block  text-truncate ">{{ $userEmail->count() }}</span>
                    </div>
                </div>
            </section>
        </div>
        <div class="col-md-4">
            <section class="hk-sec-wrapper" style="margin-top: 15px">
                <div class="media align-items-center">
                    <div class="d-flex media-img-wrap mr-15">
                        <div class="avatar avatar-sm">
							<span class="avatar-text avatar-text-inv-primary">
                                <span class="initial-wrap"><span><i class="zmdi zmdi-account"></i></span></span>
                            </span>
                        </div>
                    </div>
                    <div class="media-body">
                        <span class="d-block text-dark text-capitalize text-truncate ">THÔNG TIN LIÊN HỆ</span>
                        <span class="d-block  text-truncate ">{{ $contacts->count() }}</span>
                    </div>
                </div>
            </section>
        </div>

    </div>

    <div class="row">
        <div class="col-xl-12">
            <section class="hk-sec-wrapper">
                <div class="row">
                    <div class="col-sm">
                        <div class="table-wrap">
                            <h5>TOP 5 MẶT HÀNG BÁN CHẠY NHẤT</h5>
                            <table class="table table-hover w-100 display pb-30 js-main-table">
                                <thead>
                                <tr>
                                    <th>Mã Hàng</th>
                                    <th>Tên Hàng</th>
                                    <th>Số lượng</th>
                                    <th>Thành Tiền</th>
                                </thead>
                                <tbody>
                                @if(!empty($orderItems) && count($orderItems) > 0)
                                    @foreach($orderItems as $key => $item)
                                        @php
                                            $product = \App\Models\Product::query()->where('product_id', $item['product_id'])->first();
                                            $productCode = !empty($product) ? $product->product_code : '';
                                            $productName = !empty($product) ? $product->product_name : '';
                                        @endphp
                                        <tr>
                                            <td>{{ $productCode }}</td>
                                            <td>{{ $productName }}</td>
                                            <td>{{ $item['quantity'] }}</td>
                                            <td>{{ number_format($item['total'], 0, '.', ',') }} đ</td>
                                        </tr>
                                    @endforeach
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm">
                        <div class="table-wrap">
                            <h5>TOP 10 ĐƠN HÀNG GẦN NHẤT</h5>
                            <table class="table table-hover w-100 display pb-30 js-main-table">
                                <thead>
                                <tr>
                                    <th>Đơn hàng</th>
                                    <th>Khách hàng</th>
                                    <th>Ngày đặt hàng</th>
                                    <th>Doanh thu</th>
                                    <th>Trạng thái</th>
                                </thead>
                                <tbody>
                                @if(!empty($orderLastest) && count($orderLastest) > 0)
                                    @foreach($orderLastest as $key => $item)
                                        <tr>
                                            <td>{{ $item->ord_id }}</td>
                                            <td>{{ $item->userName }}</td>
                                            <td>{{ date_format( $item->ord_created_at,"m/d/Y" ) }}</td>
                                            <td>{{ number_format($item->ord_total_cost, 0, '.', ',') }} đ</td>
                                            <td><p class="order order-{{ $item->ord_status }}">{{ \App\Models\Order::STATUS[$item->ord_status]  }}</p></td>
                                        </tr>
                                    @endforeach
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
@endsection
