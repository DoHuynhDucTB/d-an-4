<?php
/**
 * @var \App\Helpers\UrlHelper $urlHelper
 */
$urlHelper = app('UrlHelper');
$orders = !empty($data['orders']) ? $data['orders'] : [];
$title = isset($data['title']) ? $data['title'] : '';
?>

@extends('backend.main')

@section('content')
    @include('backend.elements.content_action', ['title' => $title, 'action' => 'backend.elements.actions.order.order_list'])
    <div class="row">
        <div class="col-xl-12">
            <section class="hk-sec-wrapper">
                <div class="row">
                    <div class="col-sm">
                        <div class="table-wrap">
                            <form action="#" method="post" id="admin-form">
                                @csrf
                                <div class="table-responsive">
                                    <table class="table table-hover w-100 display pb-30 js-main-table">
                                    <thead>
                                    <tr>
                                        <th><input type="checkbox" class="checkAll" name="checkAll" value=""></th>
                                        <th>#</th>
                                        <th>Số đơn hàng</th>
                                        <th>Tên khách hàng</th>
                                        <th>Địa chỉ</th>
                                        <th>Email</th>
                                        <th>Điện thoại</th>
                                        <th>Ngày tạo</th>
                                        <th>Trạng thái</th>
                                        <th>PT thanh toán</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(!empty($orders) && count($orders) > 0)
                                        @foreach($orders as $key => $item)
                                            <tr>
                                                <th><input type="checkbox" class="checkItem" name="cid[]" value="{{ $item->ord_id }}"></th>
                                                <td>{{ $key + 1 }}</td>
                                                <td><a href="@php echo $urlHelper::admin('order', 'edit')."?id=$item->ord_id" @endphp">{{ $item->ord_name }}</a></td>
                                                <td>{{ $item->order_full_name }}</td>
                                                <td>{{ $item->ord_address_detail . ', ' .  $item->ward_name . ', ' .  $item->district_name . ', ' .  $item->province_name}}</td>
                                                <td style="max-width:150px;width:150px;white-space: normal;word-break: break-all">{{ $item->order_email }}</td>
                                                <td>{{ $item->order_phone }}</td>
                                                <td>{{ date_format( $item->ord_created_at,"m/d/Y" ) }}</td>
                                                <td><p class="order order-{{ $item->ord_status }}">{{ \App\Models\Order::STATUS[$item->ord_status]  }}</p></td>
                                                <td>{{ \App\Models\Order::PAYMENT[$item->payment_method]  }}</td>
                                            </tr>
                                        @endforeach
                                    @endif
                                    </tbody>
                                </table>
                                    {{ $orders->links('backend.elements.pagination') }}
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
@endsection
