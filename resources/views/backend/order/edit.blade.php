<?php
$order = !empty($data['order']) ? $data['order'] : null;

/**
 * order
 */
$orderFullName = !empty($order->order_full_name_two) ? $order->order_full_name_two : $order->order_full_name;
$orderAddressDetail = !empty($order->ord_address_detail_two) ? $order->ord_address_detail_two : $order->ord_address_detail;
$orderEmail = !empty($order->order_email_two) ? $order->order_email_two : $order->order_email;
$orderPhone = !empty($order->order_phone_two) ? $order->order_phone_two : $order->order_phone;

$orderWard = \App\Models\Ward::query()->where('id', $order->ward_id)->first();
$orderWardName = $orderWard->name ?? '';
$orderDistrict = \App\Models\District::query()->where('id', $order->district_id)->first();
$orderDistrictName =  $orderDistrict->name ?? '';
$orderProvince = \App\Models\Province::query()->where('id', $order->province_id)->first();
$orderProvinceName = $orderProvince->name ?? '';

$orderWard2 = \App\Models\Ward::query()->where('id', $order->ward_id_two)->first();
$orderWardName2 = $orderWard2->name ?? '';
$orderDistrict2 = \App\Models\District::query()->where('id', $order->district_id_two)->first();
$orderDistrictName2 = $orderDistrict2->name ?? '';
$orderProvince2 = \App\Models\Province::query()->where('id', $order->province_id_two)->first();
$orderProvinceName2 = $orderProvince2->name ?? '';

$arrayStatus = !empty($data['status']) ? $data['status'] : null;
$title = !empty($data['title']) ? $data['title'] : '';
$paymentTitle = $order->payment_method = 'cod' ? 'Thanh toán tiền mặt khi nhận hàng (COD)' : 'Thanh toán chuyển khoản';
$items = !empty($data['items']) ? $data['items'] : null;


$provinces = !empty($data['provinces']) ? $data['provinces'] : null;
$districts = !empty($data['districts']) ? $data['districts'] : null;
$wards = !empty($data['wards']) ? $data['wards'] : null;


$receivable = ($order->ord_total_cost - $order->money_paid) > 0 ? abs($order->ord_total_cost - $order->money_paid) : 0;
$refund = ($order->ord_total_cost - $order->money_paid) < 0 ? abs($order->ord_total_cost - $order->money_paid) : 0;
?>

@extends('backend.main')

@section('content')
    @include('backend.elements.content_action', ['title' => $title, 'action' => 'backend.elements.actions.order.order_edit'])
    <form method="POST" id="admin-form" action="{{app('UrlHelper')::admin('order', 'update')}}" enctype="multipart/form-data">
        <input type="hidden" name="action_type">
        <input type="hidden" name="id" value="{{ $order->ord_id }}">
        @csrf
        <div class="row">
            <div class="col-xl-12">
                <section class="hk-sec-wrapper">
                    <div class="row">
                        <div class="col-xl-12">
                            <h5>Mã đơn hàng: {{ $order->ord_code }}</h5><br>
                            <h5>THÔNG TIN TỔNG QUAN</h5>
                            <hr>

                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Trạng Thái</label>
                                <div class="col-sm-3">
                                    <select class="form-control form-control-sm" name="status">
                                        @if(!empty($arrayStatus) && count($arrayStatus) > 0)
                                            @foreach($arrayStatus as $key => $status)
                                                <option value="{{ $key }}" @if($key == $order->ord_status) selected @endif>{{ $status }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>

                            <table class="table table-bordered">
                                <thead class="thead-light">
                                    <tr>
                                        <th scope="col">THÔNG TIN KHÁCH HÀNG</th>
                                        <th scope="col">THÔNG TIN GIAO HÀNG	</th>
                                        <th scope="col">THANH TOÁN & VẬN CHUYỂN</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            Tên khách hàng: <h6>{{ $order->order_full_name }}</h6><br>
                                            Địa chỉ: <h6>{{ $order->ord_address_detail }}</h6><br>
                                            Tỉnh/ Thành Phố: <h6>{{ $orderProvinceName }}</h6><br>
                                            Quận/ Huyện: <h6>{{ $orderDistrictName }}</h6><br>
                                            Xã /Phường: <h6>{{ $orderWardName }}</h6><br>
                                            Email: <h6>{{ $order->order_email }}</h6><br>
                                            Điện thoại: <h6>{{ $order->order_phone }}</h6><br>
                                        </td>
                                        <td>
                                            Tên khách hàng: <br>
                                            <input type="text" name="fullNameTwo" class="form-control" value="{{ $order->order_full_name_two }}"><br>
                                            Địa chỉ: <br>
                                            <textarea class="form-control" name="addressDetailTwo">{{ $order->ord_address_detail_two }}</textarea><br>
                                            Tỉnh/ Thành Phố: <br>
                                            <select class="form-control form-control-sm" id="selectProvince" name="provinceIdTwo">
                                                <option value="">Chọn tỉnh thành</option>
                                                @if(!empty($provinces) && count($provinces) > 0)
                                                    @foreach($provinces as $key => $item)
                                                        <option value="{{ $item->id }}" @if($item->id == $order->province_id_two) selected @endif>{{ $item->name }}</option>
                                                    @endforeach
                                                @endif
                                            </select><br>
                                            Quận/ Huyện: <br>
                                            <select class="form-control form-control-sm" id="selectDistrict" name="districtIdTwo">
                                                <option value="">Chọn tỉnh quận/huyện</option>
                                                @if(!empty($districts) && count($districts) > 0)
                                                    @foreach($districts as $key => $item)
                                                        <option value="{{ $item->id }}" @if($item->id == $order->district_id_two) selected @endif>{{ $item->name }}</option>
                                                    @endforeach
                                                @endif
                                            </select><br>
                                            Xã /Phường: <br>
                                            <select class="form-control form-control-sm" id="selectWard" name="wardIdTwo">
                                                <option value="">Chọn tỉnh xã/phường</option>
                                                @if(!empty($wards) && count($wards) > 0)
                                                    @foreach($wards as $key => $item)
                                                        <option value="{{ $item->id }}" @if($item->id == $order->ward_id_two) selected @endif>{{ $item->name }}</option>
                                                    @endforeach
                                                @endif
                                            </select><br>
                                            Email: <br>
                                            <input type="text" name="emailTwo" class="form-control" value="{{ $order->order_email_two }}"><br>
                                            Điện thoại: <br>
                                            <input type="text" name="phoneTwo" class="form-control" value="{{ $order->order_phone_two }}"><br>
                                        </td>
                                        <td>
                                            <h5>THANH TOÁN</h5>
                                            <h6>{{ $paymentTitle }}</h6>
                                            <hr>
                                            <h5>VẬN CHUYỂN</h5>
                                            <h6></h6>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>

                            <h5>THÔNG TIN ĐẶT HÀNG</h5>
                            @if($items->count() > 0)
                                <table class="table table-bordered">
                                    <thead class="thead-light">
                                    <tr>
                                        <th scope="col">ID</th>
                                        <th scope="col">Tên sản phẩm</th>
                                        <th scope="col">Giá sản phẩm</th>
                                        <th scope="col" width="15%">Số lượng</th>
                                        <th scope="col" width="5%">Xóa</th>
                                        <th scope="col">Thành tiền</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($items as $key => $item)
                                            @php
                                            $product = \App\Models\Product::query()->where('product_id', $item->product_id)->first();
                                            $productName = !empty($product->product_name) ? $product->product_name : '';
                                            @endphp
                                            <tr>
                                                <td>{{ $item->ordi_id }}</td>
                                                <td>{{ $productName }}</td>
                                                <td>{{ number_format($item->ordi_historical_cost, 0, '.', ',') }} đ</td>
                                                <td><input type="number" name="orderItemId[{{ $item->ordi_id }}]" class="form-control" value="{{ $item->ordi_quantity }}"></td>
                                                <td><i class="glyphicon glyphicon-remove remove-item-cart"></i></td>
                                                <td>{{ number_format($item->ordi_total_cost, 0, '.', ',') }} đ</td>
                                            </tr>
                                        @endforeach
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td>Tổng tiền hàng: {{ number_format($order->ord_total_cost, 0, '.', ',') }} đ</td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td>Tổng phí vận chuyển:</td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td>Thành tiền: <span style="color: crimson; font-weight: bold;">{{ number_format($order->ord_total_cost, 0, '.', ',') }} đ</span></td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td>Tiền đã thanh toán: <input type="number" name="moneyPaid" value="{{ $order->money_paid }}"> đ<br>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td>Tiền phải thu thêm: <span style="color: crimson; font-weight: bold;">{{ number_format($receivable, 0, '.', ',') }} đ</span></td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td>Tiền phải trả lại cho khách: <span style="color: crimson; font-weight: bold;">{{ number_format($refund, 0, '.', ',') }} đ</span></td>
                                        </tr>
                                    </tbody>
                                </table>
                            @endif

                        </div>
                    </div>
                </section>
            </div>
        </div>
    </form>
@endsection
