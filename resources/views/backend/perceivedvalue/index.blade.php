<?php
$perceivedValues = !empty($data['perceivedValues']) ? $data['perceivedValues'] : (object)[];
$title = isset($data['title']) ? $data['title'] : '';
?>
@extends('backend.main')
@section('content')
    @include('backend.elements.content_action', ['title' => $title, 'action' => 'backend.elements.actions.perceivedvalue.perceivedvalue_index'])
    <div class="row">
        <div class="col-xl-12">
            <section class="hk-sec-wrapper">
                <div class="row">
                    <div class="col-sm">
                        <div class="table-wrap">
                            <form action="#" method="post" id="admin-form">
                                @csrf
                                <table id="datable_1" class="table table-hover w-100 display pb-30 js-main-table">
                                    <thead>
                                    <tr>
                                        <th><input type="checkbox" class="checkAll" name="checkAll" value=""></th>
                                        <th>#</th>
                                        <th>Cảm nhận</th>
                                        <th>Họ tên khách hàng</th>
                                        <th>Vị trí</th>
                                        <th>Trạng thái</th>
                                        <th>ID</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(!empty($perceivedValues) && count($perceivedValues) > 0)
                                        @foreach($perceivedValues as $key => $item)
                                            <tr>
                                                <th><input type="checkbox" class="checkItem" name="cid[]" value="{{ $item->pervalue_id }}"></th>
                                                <td>{{ $key + 1 }}</td>
                                                <td>{{ $item->pervalue_description}}</td>
                                                <td><a href="{{app('UrlHelper')::admin('perceivedvalue', 'detail', ['id' => $item->pervalue_id])}}">{{ $item->pervalue_fullname }}</a></td>
                                                <td><input style="width: 40px;" type="number" name="sort[]" value="{{ $item->pervalue_sorted }}"><span style="display: none">{{ $item->pervalue_sorted }}</span></td>
                                                <td>{{ $item->pervalue_status }}</td>
                                                <td>{{ $item->pervalue_id }}</td>
                                            </tr>
                                        @endforeach
                                    @endif
                                    </tbody>
                                </table>
                            </form>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
@endsection
