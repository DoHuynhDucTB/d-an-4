<?php
$title = isset($data['title']) ? $data['title'] : '';
$perceivedValue = $data['perceivedValue'];
$urlAvatar = !empty($data['urlAvatar']) ? $data['urlAvatar'] : '';
?>
@extends('backend.main')

@section('content')
    @include('backend.elements.content_action', ['title' => $title, 'action' => 'backend.elements.actions.perceivedvalue.perceivedvalue_detail'])
    <div class="row">
        <div class="col-xl-12">
            <section class="hk-sec-wrapper">
                <div class="row">
                    <div class="col-sm">
                        <div class="table-wrap">
                            <form action="#" method="post" id="admin-form" enctype="multipart/form-data">
                                <input type="hidden" name="id" value="{{@$perceivedValue->pervalue_id ?? 0}}">
                                <input type="hidden" name="action_type" value="save">
                                @csrf
                                <div class="form-group row">
                                    <label for="name" class="col-sm-2 col-form-label">Họ tên khách hàng</label>
                                    <div class="col-sm-10">
                                        <input name="pervalue_fullname" class="form-control" id="pervalue_fullname" value="{{$perceivedValue->pervalue_fullname ?? old('pervalue_fullname')}}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="description" class="col-sm-2 col-form-label">Cảm nhận<span class="text-danger">*</span></label>
                                    <div class="col-sm-10">
                                        <textarea class="form-control" name="pervalue_description" id="pervalue_description">{{$perceivedValue->pervalue_description ?? old('pervalue_description')}}</textarea>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="description" class="col-sm-2 col-form-label">Cảm nhận (Tiếng Anh)</label>
                                    <div class="col-sm-10">
                                        <textarea class="form-control" name="pervalue_description_en" id="pervalue_description_en">{{$perceivedValue->pervalue_description_en ?? old('pervalue_description_en')}}</textarea>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-form-label col-sm-2 pt-0">Trạng thái</label>
                                    <div class="col-sm-10">
                                        <select class="form-control custom-select d-block w-100" name="pervalue_status" id="pervalue_status">
                                            <option value="">Choose...</option>
                                            @foreach(['activated', 'inactive'] as $item)
                                                @if($item == (@$perceivedValue->pervalue_status ?? old('pervalue_status')))
                                                    <option value="{{$item}}" selected="selected">{{$item}}</option>
                                                @else
                                                    <option value="{{$item}}">{{$item}}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Hình ảnh</label>
                                    <div class="col-sm-10">
                                        <input type="file" name="imageAvatar" class="dropify" @if(!empty($urlAvatar)) data-default-file="{{ asset($urlAvatar) }}" @endif/>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
@endsection

@section('javascript_tag')
    @parent
    <script>
        /**
         * Khi Checked menu cha
         */
        $("input[data-menu-type='parent']").click(function (){
            var isCheck = $(this).is(':checked');
            var parentId = $(this).attr('id');

            /**
             * Bỏ checked menu con
             */
            if (isCheck == false) {
                $("input[data-parent='"+parentId+"']").prop( "checked", false );
            }
        });

        /**
         * Khi Checked menu con
         */
        $("input[data-menu-type='children']").click(function (){
            var isCheck = $(this).is(':checked');
            var parentId = $(this).data('parent');

            /**
             * Checked menu cha
             */
            if (isCheck == true) {
                $("input#" + parentId).prop( "checked", true );
            }
        });
    </script>
@endsection
