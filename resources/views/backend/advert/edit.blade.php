<?php
$adminName = $data['adminName'];
$advert = !empty($data['advert']) ? $data['advert'] : null;
$title = !empty($data['title']) ? $data['title'] : '';
$urlAvatar = !empty($data['urlAvatar']) ? $data['urlAvatar'] : '';
$groups = !empty($data['groups']) ? $data['groups'] : null;
?>

@extends('backend.main')

@section('content')
    @include('backend.elements.content_action', ['title' => $title, 'action' => 'backend.elements.actions.advert.advert_edit'])
    <form method="POST" id="admin-form" action="{{app('UrlHelper')::admin('advert', 'update')}}" enctype="multipart/form-data">
        <input type="hidden" name="action_type">
        <input type="hidden" name="id" value="{{$advert->advert_id}}">
        <input type="hidden" name="task" value="update">
        @csrf
        <div class="row">
            <div class="col-xl-12">
                <section class="hk-sec-wrapper">
                    <div class="row">
                        <nav id="nav-tabs">
                            <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                <a class="nav-item nav-link active" id="nav-vi-tab" data-toggle="tab" href="#nav-vi" role="tab" aria-controls="nav-vi" aria-selected="true">Thông tin (Tiếng Việt)</a>
                                <a class="nav-item nav-link" id="nav-en-tab" data-toggle="tab" href="#nav-en" role="tab" aria-controls="nav-en" aria-selected="true">Thông tin (Tiếng Anh)</a>
                                <a class="nav-item nav-link" id="nav-upload-tab" data-toggle="tab" href="#nav-upload" role="tab" aria-controls="nav-upload" aria-selected="false">Upload ảnh</a>
                            </div>
                        </nav>
                        <div class="tab-content" id="nav-tabContent">
                            <div class="tab-pane fade show active" id="nav-vi" role="tabpanel" aria-labelledby="nav-vi-tab">
                                <div class="col-xl-12">
                                    <section class="hk-sec-wrapper">
                                        <div class="row">
                                            <div class="col-sm">
                                                <div class="table-wrap">
                                                    <div class="table-responsive">
                                                        <table class="table mb-0">
                                                            <tbody>
                                                            <tr>
                                                                <th scope="row">Được tạo bởi</th>
                                                                <td>{{ $adminName }}</td>
                                                            </tr>
                                                            <tr>
                                                                <th scope="row">Tên quảng cáo <span class="red">*</span></th>
                                                                <td><input type="text" name="name" class="form-control form-control-sm" value="{{ $advert->advert_name }}" placeholder="Tên quảng cáo"></td>
                                                            </tr>
                                                            <tr>
                                                                <th scope="row">Bật</th>
                                                                <td>
                                                                    <div class="form-check form-check-inline">
                                                                        <input class="form-check-input" type="radio" name="status" value="activated" @if($advert->advert_status == 'activated') checked @endif>
                                                                        <label class="form-check-label" >Có</label>
                                                                    </div>
                                                                    <div class="form-check form-check-inline">
                                                                        <input class="form-check-input" type="radio" name="status" value="inactive" @if($advert->advert_status == 'inactive') checked @endif>
                                                                        <label class="form-check-label">Không</label>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th scope="row">Url</th>
                                                                <td><input type="text" name="url" class="form-control form-control-sm" placeholder="Url" value="{{ $advert->advert_url }}"></td>
                                                            </tr>
                                                            <tr>
                                                                <th scope="row">Nhóm quảng cáo</th>
                                                                <td>
                                                                    <select name="groupId" class="form-control custom-select form-control custom-select-sm mt-15">
                                                                        @if($groups->count() > 0)
                                                                                <option value="">Chọn nhóm</option>
                                                                            @foreach($groups as $key => $item)
                                                                                <option value="{{ $item->adgroup_id }}" @if($advert->group_id == $item->adgroup_id) selected @endif >{{ $item->adgroup_name }}</option>
                                                                            @endforeach
                                                                        @endif
                                                                    </select>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th scope="row">Thứ tự</th>
                                                                <td><input type="number" name="number" class="form-control form-control-sm" placeholder="" value="{{ $advert->advert_number }}"></td>
                                                            </tr>
                                                            <tr>
                                                                <th scope="row">Lượt xem</th>
                                                                <td><input type="number" name="view" class="form-control form-control-sm" placeholder="" value="{{ $advert->advert_view }}"></td>
                                                            </tr>
                                                            <tr>
                                                                <th scope="row">Mô tả</th>
                                                                <td><textarea id="editor_short" name="description" class="form-control" placeholder="Nhập mô tả">{!! $advert->advert_description !!}</textarea></td>
                                                            </tr>
                                                            <tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="nav-en" role="tabpanel" aria-labelledby="nav-en-tab">
                                <div class="col-xl-12">
                                    <section class="hk-sec-wrapper">
                                        <div class="row">
                                            <div class="col-sm">
                                                <div class="table-wrap">
                                                    <div class="table-responsive">
                                                        <table class="table mb-0">
                                                            <tbody>
                                                            <tr>
                                                                <th scope="row">Tên quảng cáo</th>
                                                                <td><input type="text" name="nameEn" class="form-control form-control-sm" placeholder="Tên quảng cáo" value="{{ $advert->advert_name_en }}"></td>
                                                            </tr>
                                                            <tr>
                                                                <th scope="row">Mô tả</th>
                                                                <td><textarea id="editor_short_en" name="descriptionEn" class="form-control" placeholder="Nhập mô tả">{{ $advert->advert_description_en }}</textarea></td>
                                                            </tr>
                                                            <tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="nav-upload" role="tabpanel" aria-labelledby="nav-upload-tab">
                                <div class="col-xl-12">
                                    <section class="hk-sec-wrapper">
                                        <h5 class="hk-sec-title">Ảnh đại diện</h5>
                                        <input type="file" name="imageAvatar" class="dropify" @if(!empty($urlAvatar)) data-default-file="{{ asset($urlAvatar) }}" @endif/>
                                    </section>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </form>
@endsection
