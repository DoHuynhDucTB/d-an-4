<?php
$comment = !empty($data['comment']) ? $data['comment'] : null;
$title = !empty($data['title']) ? $data['title'] : '';
?>

@extends('backend.main')

@section('content')
    @include('backend.elements.content_action', ['title' => $title, 'action' => 'backend.elements.actions.comment.comment_edit'])
    <form method="POST" id="admin-form" action="{{app('UrlHelper')::admin('comment', 'update')}}" enctype="multipart/form-data">
        <input type="hidden" name="action_type">
        <input type="hidden" name="id" value="{{$comment->comment_id}}">
        @csrf
        <div class="row">
            <div class="col-xl-12">
                <section class="hk-sec-wrapper">
                    <div class="row">
                        <div class="col-xl-12">

                            <section class="hk-sec-wrapper">
                                <div class="row">
                                    <div class="col-sm">
                                        <div class="table-wrap">
                                            <div class="table-responsive">
                                                <table class="table mb-0">
                                                    <tbody>
                                                    <tr>
                                                        <th scope="row">Bật</th>
                                                            <td>
                                                                <div class="form-check form-check-inline">
                                                                    <input class="form-check-input" type="radio" name="status" value="approved" @if($comment->comment_status == 'approved') checked @endif>
                                                                    <label class="form-check-label" >Cho phép</label>
                                                                </div>
                                                                <div class="form-check form-check-inline">
                                                                    <input class="form-check-input" type="radio" name="status" value="unapproved" @if($comment->comment_status == 'unapproved') checked @endif>
                                                                    <label class="form-check-label">Không cho phép</label>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    <th scope="row">Xếp Hạng</th>
                                                    <td>
                                                        <div class="row">
                                                            <div class="col-sm-3">
                                                                <select class="form-control custom-select-sm" name="rating">
                                                                    <option value="">Chọn</option>
                                                                    <option value="1" @if($comment->comment_rating == 1) selected @endif>1</option>
                                                                    <option value="2" @if($comment->comment_rating == 2) selected @endif>2</option>
                                                                    <option value="3" @if($comment->comment_rating == 3) selected @endif>3</option>
                                                                    <option value="4" @if($comment->comment_rating == 4) selected @endif>4</option>
                                                                    <option value="5" @if($comment->comment_rating == 5) selected @endif>5</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    </tr>
                                                        <tr>
                                                            <th scope="row">Mô tả</th>
                                                            <td><textarea {{--id="editor" --}}name="content" class="form-control" placeholder="">{!! $comment->comment_content !!}</textarea></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>

                        </div>
                    </div>
                </section>
            </div>
        </div>
    </form>
@endsection
