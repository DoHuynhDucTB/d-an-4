<?php
/**
 * @var \App\Helpers\UrlHelper $urlHelper
 */
$urlHelper = app('UrlHelper');
$posts = !empty($data['posts']) ? $data['posts'] : [];
$title = isset($data['title']) ? $data['title'] : '';
?>

@extends('backend.main')

@section('content')
    @include('backend.elements.content_action', ['title' => $title, 'action' => 'backend.elements.actions.post.post_list'])
    <div class="row">
        <div class="col-xl-12">
            <section class="hk-sec-wrapper">
                <div class="row">
                    <div class="col-sm">
                        <div class="table-wrap">
                            <form action="#" method="post" id="admin-form">
                                @csrf
                                <table id="datable_1" class="table table-hover w-100 display pb-30 js-main-table">
                                    <thead>
                                    <tr>
                                        <th><input type="checkbox" class="checkAll" name="checkAll" value=""></th>
                                        <th>#</th>
                                        <th>Ảnh</th>
                                        <th>Tên bài viết</th>
                                        <th>Nhóm</th>
                                        <th>Url</th>
                                        <th>Ngày tạo</th>
                                        <th>Bật</th>
                                        <th>Id</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(!empty($posts) && count($posts) > 0)
                                        @foreach($posts as $key => $item)
                                            <tr>
                                                <th><input type="checkbox" class="checkItem" name="cid[]" value="{{ $item->post_id }}"></th>
                                                <td>{{ $key + 1 }}</td>
                                                <td>
                                                    @if(!empty($item->urlAvatar))
                                                        <img class="lazy" src="{{ asset($item->urlAvatar) }}">
                                                    @endif
                                                </td>
                                                <td><a href="@php echo $urlHelper::admin('post', 'edit')."?id=$item->post_id" @endphp">{{ $item->post_name }}</a></td>
                                                <td>{{ $item->groupName }}</td>
                                                <td><a href="{{ route('detailPost', ['slug' => $item->slug, 'id' => $item->post_id, 'slug' => \Illuminate\Support\Str::slug($item->post_name)]) }}" target="_blank">{{ route('detailPost', ['slug' => $item->slug, 'id' => $item->post_id, 'slug' => \Illuminate\Support\Str::slug($item->post_name)], false) }}</a></td>
                                                <td>{{ $item->post_created_at}}</td>
                                                <td>{{ $item->post_status }}</td>
                                                <td>{{ $item->post_id }}</td>
                                            </tr>
                                        @endforeach
                                    @endif
                                    </tbody>
                                </table>
                            </form>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
@endsection
