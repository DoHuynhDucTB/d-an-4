<?php
$adminName = $data['adminName'];
$post = !empty($data['post']) ? $data['post'] : null;
$posts = !empty($data['posts']) ? $data['posts'] : null;
$groups = !empty($data['groups']) ? $data['groups'] : null;
$arrayRelated = !empty($data['arrayRelated']) ? $data['arrayRelated'] : [];
$title = !empty($data['title']) ? $data['title'] : '';
$urlAvatar = !empty($data['urlAvatar']) ? $data['urlAvatar'] : '';
?>

@extends('backend.main')

@section('content')
    @include('backend.elements.content_action', ['title' => $title, 'action' => 'backend.elements.actions.post.post_edit'])
    <form method="POST" id="admin-form" action="{{app('UrlHelper')::admin('post', 'update')}}" enctype="multipart/form-data">
        <input type="hidden" name="action_type">
        <input type="hidden" name="id" value="{{$post->post_id}}">
        @csrf
        <div class="row">
            <div class="col-xl-12">
                <section class="hk-sec-wrapper">
                    <div class="row">
                        <nav id="nav-tabs">
                            <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                <a class="nav-item nav-link active" id="nav-vi-tab" data-toggle="tab" href="#nav-vi" role="tab" aria-controls="nav-vi" aria-selected="true">Thông tin (Tiếng Việt)</a>
                                <a class="nav-item nav-link" id="nav-en-tab" data-toggle="tab" href="#nav-en" role="tab" aria-controls="nav-en" aria-selected="false">Thông tin (Tiếng Anh)</a>
                                <a class="nav-item nav-link" id="nav-setting-tab" data-toggle="tab" href="#nav-setting" role="tab" aria-controls="nav-setting" aria-selected="false">Cấu Hình</a>
                                <a class="nav-item nav-link" id="nav-upload-tab" data-toggle="tab" href="#nav-upload" role="tab" aria-controls="nav-upload" aria-selected="false">Upload ảnh</a>
                                <a class="nav-item nav-link" id="nav-related-tab" data-toggle="tab" href="#nav-related" role="tab" aria-controls="nav-related" aria-selected="false">Bài viết liên quan</a>
                            </div>
                        </nav>
                        <div class="tab-content" id="nav-tabContent">
                            <div class="tab-pane fade show active" id="nav-vi" role="tabpanel" aria-labelledby="nav-vi-tab">
                                <div class="col-xl-12">
                                    <section class="hk-sec-wrapper">
                                        <div class="row">
                                            <div class="col-sm">
                                                <div class="table-wrap">
                                                    <div class="table-responsive">
                                                        <table class="table mb-0">
                                                            <tbody>
                                                            <tr>
                                                                <th scope="row">Được tạo bởi</th>
                                                                <td>{{ $adminName }}</td>
                                                            </tr>
                                                            <tr>
                                                                <th scope="row">Tên bài viết <span class="red">*</span></th>
                                                                <td><input type="text" name="name" class="form-control form-control-sm" value="{{ $post->post_name }}" placeholder="Tên bài viết"></td>
                                                            </tr>
                                                            <tr>
                                                                <th scope="row">Nhóm bài viết</th>
                                                                <td>
                                                                    <select class="form-control form-control-sm" name="group">
                                                                        <option value="">Chọn nhóm bài viết</option>
                                                                        @if($groups->count() > 0)
                                                                            @foreach($groups as $key => $group)
                                                                                <option value="{{ $group->postgroup_id }}" @if($post->post_group == $group->postgroup_id) selected @endif>{{ $group->postgroup_name }}</option>
                                                                            @endforeach
                                                                        @endif
                                                                    </select>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th scope="row">Mô tả</th>
                                                                <td><textarea {{--id="editor_short"--}} name="description" class="form-control" placeholder="Nhập mô tả">{!! $post->post_description !!}</textarea></td>
                                                            </tr>
                                                            <tr>
                                                                <th scope="row">Nội dung</th>
                                                                <td><textarea id="editor" name="content" class="form-control" placeholder="Nhập nội dung">{!! $post->post_content !!}</textarea></td>
                                                            </tr>
                                                            <tr>
                                                                <th scope="row">Meta Title</th>
                                                                <td><input type="text" name="metaTitle" class="form-control form-control-sm" placeholder="" value="{{ $post->post_meta_title }}"></td>
                                                            </tr>
                                                            <tr>
                                                                <th scope="row">Meta Keywords</th>
                                                                <td><input type="text" name="metaKeywords" class="form-control form-control-sm" placeholder="" value="{{ $post->post_meta_keywords }}"></td>
                                                            </tr>
                                                            <tr>
                                                                <th scope="row">Meta Description</th>
                                                                <td><input type="text" name="metaDescription" class="form-control form-control-sm" placeholder="" value="{{ $post->post_meta_description }}"></td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="nav-en" role="tabpanel" aria-labelledby="nav-en-tab">
                                <div class="col-xl-12">
                                    <section class="hk-sec-wrapper">
                                        <div class="row">
                                            <div class="col-sm">
                                                <div class="table-wrap">
                                                    <div class="table-responsive">
                                                        <table class="table mb-0">
                                                            <tbody>
                                                            <tr>
                                                                <th scope="row">Tên Bài viết</th>
                                                                <td><input type="text" name="nameEn" class="form-control form-control-sm" placeholder="Tên Bài viết" value="{{ $post->post_name_en }}"></td>
                                                            </tr>
                                                            <tr>
                                                                <th scope="row">Mô tả</th>
                                                                <td><textarea {{--id="editor_short_en"--}} name="descriptionEn" class="form-control" placeholder="Nhập mô tả">{!! $post->post_description_en !!}</textarea></td>
                                                            </tr>
                                                            <tr>
                                                                <th scope="row">Nội dung</th>
                                                                <td><textarea id="editor_en" name="contentEn" class="form-control" placeholder="Nhập nội dung">{!! $post->post_content_en !!}</textarea></td>
                                                            </tr>
                                                            <tr>
                                                                <th scope="row">Meta Title</th>
                                                                <td><input type="text" name="metaTitleEn" class="form-control form-control-sm" placeholder="" value="{{ $post->post_meta_title_en }}"></td>
                                                            </tr>
                                                            <tr>
                                                                <th scope="row">Meta Keywords</th>
                                                                <td><input type="text" name="metaKeywordsEn" class="form-control form-control-sm" placeholder="" value="{{ $post->post_meta_keywords_en }}"></td>
                                                            </tr>
                                                            <tr>
                                                                <th scope="row">Meta Description</th>
                                                                <td><input type="text" name="metaDescriptionEn" class="form-control form-control-sm" placeholder="" value="{{ $post->post_meta_description_en }}"></td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="nav-setting" role="tabpanel" aria-labelledby="nav-setting-tab">
                                <div class="col-xl-12">
                                    <section class="hk-sec-wrapper">
                                        <div class="row">
                                            <div class="col-sm">
                                                <div class="table-wrap">
                                                    <div class="table-responsive">
                                                        <table class="table mb-0">
                                                            <tbody>
                                                            <tr>
                                                                <th scope="row">Bật</th>
                                                                <td>
                                                                    <div class="form-check form-check-inline">
                                                                        <input class="form-check-input" type="radio" name="status" value="activated" @if($post->post_status == 'activated') checked @endif>
                                                                        <label class="form-check-label" >Có</label>
                                                                    </div>
                                                                    <div class="form-check form-check-inline">
                                                                        <input class="form-check-input" type="radio" name="status" value="inactive"  @if($post->post_status == 'inactive') checked @endif>
                                                                        <label class="form-check-label">Không</label>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th scope="row">Bài viết nổi bật</th>
                                                                <td>
                                                                    <div class="form-check form-check-inline">
                                                                        <input class="form-check-input" type="radio" name="isHot" value="yes" @if($post->post_is_hot == 'yes') checked @endif>
                                                                        <label class="form-check-label">Có</label>
                                                                    </div>
                                                                    <div class="form-check form-check-inline">
                                                                        <input class="form-check-input" type="radio" name="isHot" value="no" @if($post->post_is_hot == 'no') checked @endif>
                                                                        <label class="form-check-label">Không</label>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th scope="row">Bài viết mới</th>
                                                                <td>
                                                                    <div class="form-check form-check-inline">
                                                                        <input class="form-check-input" type="radio" name="isNew" value="yes" @if($post->post_is_new == 'yes') checked @endif>
                                                                        <label class="form-check-label">Có</label>
                                                                    </div>
                                                                    <div class="form-check form-check-inline">
                                                                        <input class="form-check-input" type="radio" name="isNew" value="no" @if($post->post_is_new == 'no') checked @endif>
                                                                        <label class="form-check-label">Không</label>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th scope="row">Bài viết xem nhiều</th>
                                                                <td>
                                                                    <div class="form-check form-check-inline">
                                                                        <input class="form-check-input" type="radio" name="isView" value="yes" @if($post->post_is_view == 'yes') checked @endif>
                                                                        <label class="form-check-label">Có</label>
                                                                    </div>
                                                                    <div class="form-check form-check-inline">
                                                                        <input class="form-check-input" type="radio" name="isView" value="no" @if($post->post_is_view == 'no') checked @endif>
                                                                        <label class="form-check-label">Không</label>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="nav-upload" role="tabpanel" aria-labelledby="nav-upload-tab">
                                <div class="col-xl-12">
                                    <section class="hk-sec-wrapper">
                                        <h5 class="hk-sec-title">Ảnh đại diện</h5>
                                        <input type="file" name="imageAvatar" class="dropify" @if(!empty($urlAvatar)) data-default-file="{{ asset($urlAvatar) }}" @endif/>
                                    </section>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="nav-related" role="tabpanel" aria-labelledby="nav-related-tab">
                                <div class="col-xl-12">
                                    <select class="select2 select2-multiple" name="related[]" multiple="multiple" data-placeholder="Choose">
                                        @if($post->count() > 0)
                                            @foreach($posts as $key => $itemPost)
                                                @php
                                                    $itemId = $itemPost->post_id;
                                                @endphp
                                                <option value="{{ $itemId }}" @if(in_array($itemId,$arrayRelated )) selected @endif>{{ $itemPost->post_name }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </form>
@endsection
