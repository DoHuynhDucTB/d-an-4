<?php
$producer = !empty($data['producer']) ? $data['producer'] : null;
$title = !empty($data['title']) ? $data['title'] : '';
$urlAvatar = !empty($data['urlAvatar']) ? $data['urlAvatar'] : '';
?>

@extends('backend.main')

@section('content')
    @include('backend.elements.content_action', ['title' => $title, 'action' => 'backend.elements.actions.producer.producer_edit'])
    <form method="POST" id="admin-form" action="{{app('UrlHelper')::admin('producer', 'update')}}" enctype="multipart/form-data">
        <input type="hidden" name="action_type">
        <input type="hidden" name="id" value="{{$producer->producer_id}}">
        @csrf
        <div class="row">
            <div class="col-xl-12">
                <section class="hk-sec-wrapper">
                    <div class="row">
                        <nav id="nav-tabs">
                            <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                <a class="nav-item nav-link active" id="nav-vi-tab" data-toggle="tab" href="#nav-vi" role="tab" aria-controls="nav-vi" aria-selected="true">Thông tin (Tiếng Việt)</a>
                                <a class="nav-item nav-link" id="nav-en-tab" data-toggle="tab" href="#nav-en" role="tab" aria-controls="nav-en" aria-selected="true">Thông tin (Tiếng Anh)</a>
                                <a class="nav-item nav-link" id="nav-upload-tab" data-toggle="tab" href="#nav-upload" role="tab" aria-controls="nav-upload" aria-selected="false">Upload ảnh</a>
                            </div>
                        </nav>
                        <div class="tab-content" id="nav-tabContent">
                            <div class="tab-pane fade show active" id="nav-vi" role="tabpanel" aria-labelledby="nav-vi-tab">
                                <div class="col-xl-12">
                                    <section class="hk-sec-wrapper">
                                        <div class="row">
                                            <div class="col-sm">
                                                <div class="table-wrap">
                                                    <div class="table-responsive">
                                                        <table class="table mb-0">
                                                            <tbody>
                                                            <tr>
                                                                <th scope="row">Tên nhà sản xuất <span class="red">*</span></th>
                                                                <td><input type="text" name="name" class="form-control form-control-sm" placeholder="Tên" value="{{ $producer->producer_name}}"></td>
                                                            </tr>
                                                            <tr>
                                                                <th scope="row">Mã</th>
                                                                <td><input type="text" name="code" class="form-control form-control-sm" placeholder="Mã" value="{{ $producer->producer_code }}"></td>
                                                            </tr>
                                                            <tr>
                                                                <th scope="row">Thuộc loại sp</th>
                                                                <td>
                                                                    <select class="form-control custom-select d-block w-100" name="type">
                                                                        <option value="shoes" @if($producer->producer_type == 'shoes') selected @endif>Giày</option>
                                                                        <option value="cosmetic" @if($producer->producer_type == 'cosmetic') selected @endif >Mỹ phẩm</option>
                                                                    </select>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th scope="row">Bật</th>
                                                                <td>
                                                                    <div class="form-check form-check-inline">
                                                                        <input class="form-check-input" type="radio" name="status" value="activated" @if($producer->producer_status = 'activated') checked @endif>
                                                                        <label class="form-check-label" >Có</label>
                                                                    </div>
                                                                    <div class="form-check form-check-inline">
                                                                        <input class="form-check-input" type="radio" name="status" value="inactive" @if($producer->producer_status = 'inactive') checked @endif>
                                                                        <label class="form-check-label">Không</label>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th scope="row">Mô tả</th>
                                                                <td><textarea id="editor_short" name="description" class="form-control" placeholder="Nhập mô tả">{!! $producer->producer_description !!}</textarea></td>
                                                            </tr>
                                                            <tr>
                                                                <th scope="row">Thông tin bảo hành</th>
                                                                <td><textarea id="editor" name="content" class="form-control" placeholder="Nhập thông tin bảo hành">{!! $producer->producer_content !!}</textarea></td>
                                                            </tr>
                                                            <tr>
                                                                <th scope="row">Meta Title</th>
                                                                <td><input type="text" name="metaTitle" class="form-control form-control-sm" placeholder="" value="{{ $producer->producer_meta_title }}"></td>
                                                            </tr>
                                                            <tr>
                                                                <th scope="row">Meta Keywords</th>
                                                                <td><input type="text" name="metaKeywords" class="form-control form-control-sm" placeholder="" value="{{ $producer->producer_meta_keywords }}"></td>
                                                            </tr>
                                                            <tr>
                                                                <th scope="row">Meta Description</th>
                                                                <td><input type="text" name="metaDescription" class="form-control form-control-sm" placeholder="" value="{{ $producer->post_meta_description }}"></td>
                                                            </tr>
                                                            <tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                </div>
                            </div>
                            <div class="tab-pane fade show" id="nav-en" role="tabpanel" aria-labelledby="nav-en-tab">
                                <div class="col-xl-12">
                                    <section class="hk-sec-wrapper">
                                        <div class="row">
                                            <div class="col-sm">
                                                <div class="table-wrap">
                                                    <div class="table-responsive">
                                                        <table class="table mb-0">
                                                            <tbody>
                                                            <tr>
                                                                <th scope="row">Tên nhà sản xuất</th>
                                                                <td><input type="text" name="nameEn" class="form-control form-control-sm" placeholder="Tên" value="{{ $producer->producer_name_en}}"></td>
                                                            </tr>
                                                            <tr>
                                                                <th scope="row">Mã</th>
                                                                <td><input type="text" name="codeEn" class="form-control form-control-sm" placeholder="Mã" value="{{ $producer->producer_code_en}}"></td>
                                                            </tr>
                                                            <tr>
                                                                <th scope="row">Mô tả</th>
                                                                <td><textarea id="editor_short_en" name="descriptionEn" class="form-control" placeholder="Nhập mô tả">{{ $producer->producer_description_en}}</textarea></td>
                                                            </tr>
                                                            <tr>
                                                                <th scope="row">Nội Dung</th>
                                                                <td><textarea id="editor_en" name="contentEn" class="form-control" placeholder="Nhập thông tin bảo hành">{{ $producer->producer_content_en}}</textarea></td>
                                                            </tr>
                                                            <tr>
                                                                <th scope="row">Meta Title</th>
                                                                <td><input type="text" name="metaTitleEn" class="form-control form-control-sm" placeholder="" value="{{ $producer->producer_meta_title_en}}"></td>
                                                            </tr>
                                                            <tr>
                                                                <th scope="row">Meta Keywords</th>
                                                                <td><input type="text" name="metaKeywordsEn" class="form-control form-control-sm" placeholder=""  value="{{ $producer->producer_meta_keywords_en}}"></td>
                                                            </tr>
                                                            <tr>
                                                                <th scope="row">Meta Description</th>
                                                                <td><input type="text" name="metaDescriptionEn" class="form-control form-control-sm" placeholder="" value="{{ $producer->producer_meta_description_en}}"></td>
                                                            </tr>
                                                            <tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="nav-upload" role="tabpanel" aria-labelledby="nav-upload-tab">
                                <div class="col-xl-12">
                                    <section class="hk-sec-wrapper">
                                        <h5 class="hk-sec-title">Ảnh đại diện</h5>
                                        <input type="file" name="imageAvatar" class="dropify" @if(!empty($urlAvatar)) data-default-file="{{ asset($urlAvatar) }}" @endif/>
                                    </section>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </form>
@endsection
