<?php
$producers = !empty($data['producers']) ? $data['producers'] : [];
$title = isset($data['title']) ? $data['title'] : '';
?>

@extends('backend.main')

@section('content')
    @include('backend.elements.content_action', ['title' => $title, 'action' => 'backend.elements.actions.producer.producer_list'])
    <div class="row">
        <div class="col-xl-12">
            <section class="hk-sec-wrapper">
                <div class="row">
                    <div class="col-sm">
                        <div class="table-wrap">
                            <form action="#" method="post" id="admin-form">
                                @csrf
                                <table id="datable_1" class="table table-hover w-100 display pb-30 js-main-table">
                                    <thead>
                                    <tr>
                                        <th><input type="checkbox" class="checkAll" name="checkAll" value=""></th>
                                        <th>#</th>
                                        <th>Ảnh</th>
                                        <th>Tên nhà sản xuất</th>
                                        <th>Mã</th>
                                        <th>Thuộc loại sp</th>
                                        <th>Ngày tạo</th>
                                        <th>Bật</th>
                                        <th>Id</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(!empty($producers) && count($producers) > 0)
                                        @foreach($producers as $key => $item)
                                            <tr>
                                                <th><input type="checkbox" class="checkItem" name="cid[]" value="{{ $item->producer_id }}"></th>
                                                <td>{{ $key + 1 }}</td>
                                                <td>
                                                    @if(!empty($item->urlAvatar))
                                                        <img class="lazy" data-src="{{ asset($item->urlAvatar) }}">
                                                    @endif
                                                </td>
                                                <td><a href="{{app('UrlHelper')::admin('producer', 'edit', ['id' => $item->producer_id])}}">{{ $item->producer_name }}</a></td>
                                                <td>{{ $item->producer_code }}</td>
                                                <td>{{ $item->producer_type == 'shoes' ? 'Giày' : 'Mỹ phẩm' }}</td>
                                                <td>{{ $item->producer_created_at}}</td>
                                                <td>{{ $item->producer_status}}</td>
                                                <td>{{ $item->producer_id }}</td>
                                            </tr>
                                        @endforeach
                                    @endif
                                    </tbody>
                                </table>
                            </form>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
@endsection
