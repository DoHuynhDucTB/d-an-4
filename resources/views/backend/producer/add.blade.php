<?php
$title = isset($data['title']) ? $data['title'] : '';
?>

@extends('backend.main')

@section('content')
    @include('backend.elements.content_action', ['title' => $title, 'action' => 'backend.elements.actions.producer.producer_add'])
    <form method="POST" id="admin-form" action="{{app('UrlHelper')::admin('producer', 'store')}}" enctype="multipart/form-data">
        <input type="hidden" name="action_type">
        @csrf
        <input type="hidden" name="typeSubmit" value="0">
        <div class="row">
            <div class="col-xl-12">
                <section class="hk-sec-wrapper">
                    <div class="row">
                        <nav id="nav-tabs">
                            <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                <a class="nav-item nav-link active" id="nav-vi-tab" data-toggle="tab" href="#nav-vi" role="tab" aria-controls="nav-vi" aria-selected="true">Thông tin (Tiếng Việt)</a>
                                <a class="nav-item nav-link" id="nav-en-tab" data-toggle="tab" href="#nav-en" role="tab" aria-controls="nav-en" aria-selected="true">Thông tin (Tiếng Anh)</a>
                                <a class="nav-item nav-link" id="nav-upload-tab" data-toggle="tab" href="#nav-upload" role="tab" aria-controls="nav-upload" aria-selected="false">Upload ảnh</a>
                            </div>
                        </nav>
                        <div class="tab-content" id="nav-tabContent">
                            <div class="tab-pane fade show active" id="nav-vi" role="tabpanel" aria-labelledby="nav-vi-tab">
                                <div class="col-xl-12">
                                    <section class="hk-sec-wrapper">
                                        <div class="row">
                                            <div class="col-sm">
                                                <div class="table-wrap">
                                                    <div class="table-responsive">
                                                        <table class="table mb-0">
                                                            <tbody>
                                                                <tr>
                                                                    <th scope="row">Tên nhà sản xuất <span class="red">*</span></th>
                                                                    <td><input type="text" name="name" class="form-control form-control-sm" placeholder="Tên"></td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Mã</th>
                                                                    <td><input type="text" name="code" class="form-control form-control-sm" placeholder="Mã"></td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Thuộc loại sp</th>
                                                                    <td>
                                                                        <select class="form-control custom-select d-block w-100" name="type">
                                                                            <option value="shoes">Giày</option>
                                                                            <option value="cosmetic">Mỹ phẩm</option>
                                                                        </select>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Bật</th>
                                                                    <td>
                                                                        <div class="form-check form-check-inline">
                                                                            <input class="form-check-input" type="radio" name="status" value="activated" checked="">
                                                                            <label class="form-check-label" >Có</label>
                                                                        </div>
                                                                        <div class="form-check form-check-inline">
                                                                            <input class="form-check-input" type="radio" name="status" value="inactive">
                                                                            <label class="form-check-label">Không</label>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Mô tả</th>
                                                                    <td><textarea id="editor_short" name="description" class="form-control" placeholder="Nhập mô tả"></textarea></td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Nội Dung</th>
                                                                    <td><textarea id="editor" name="content" class="form-control" placeholder="Nhập thông tin bảo hành"></textarea></td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Meta Title</th>
                                                                    <td><input type="text" name="metaTitle" class="form-control form-control-sm" placeholder=""></td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Meta Keywords</th>
                                                                    <td><input type="text" name="metaKeywords" class="form-control form-control-sm" placeholder=""></td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Meta Description</th>
                                                                    <td><input type="text" name="metaDescription" class="form-control form-control-sm" placeholder=""></td>
                                                                </tr>
                                                            <tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                </div>
                            </div>
                            <div class="tab-pane fade show" id="nav-en" role="tabpanel" aria-labelledby="nav-en-tab">
                                <div class="col-xl-12">
                                    <section class="hk-sec-wrapper">
                                        <div class="row">
                                            <div class="col-sm">
                                                <div class="table-wrap">
                                                    <div class="table-responsive">
                                                        <table class="table mb-0">
                                                            <tbody>
                                                            <tr>
                                                                <th scope="row">Tên nhà sản xuất</th>
                                                                <td><input type="text" name="nameEn" class="form-control form-control-sm" placeholder="Tên"></td>
                                                            </tr>
                                                            <tr>
                                                                <th scope="row">Mã</th>
                                                                <td><input type="text" name="codeEn" class="form-control form-control-sm" placeholder="Mã"></td>
                                                            </tr>
                                                            <tr>
                                                                <th scope="row">Mô tả</th>
                                                                <td><textarea id="editor_short_en" name="descriptionEn" class="form-control" placeholder="Nhập mô tả"></textarea></td>
                                                            </tr>
                                                            <tr>
                                                                <th scope="row">Nội Dung</th>
                                                                <td><textarea id="editor_en" name="contentEn" class="form-control" placeholder="Nhập thông tin bảo hành"></textarea></td>
                                                            </tr>
                                                            <tr>
                                                                <th scope="row">Meta Title</th>
                                                                <td><input type="text" name="metaTitleEn" class="form-control form-control-sm" placeholder=""></td>
                                                            </tr>
                                                            <tr>
                                                                <th scope="row">Meta Keywords</th>
                                                                <td><input type="text" name="metaKeywordsEn" class="form-control form-control-sm" placeholder=""></td>
                                                            </tr>
                                                            <tr>
                                                                <th scope="row">Meta Description</th>
                                                                <td><input type="text" name="metaDescriptionEn" class="form-control form-control-sm" placeholder=""></td>
                                                            </tr>
                                                            <tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="nav-upload" role="tabpanel" aria-labelledby="nav-upload-tab">
                                <div class="col-xl-12">
                                    <section class="hk-sec-wrapper">
                                        <h5 class="hk-sec-title">Ảnh đại diện</h5>
                                        <input type="file" name="imageAvatar" class="dropify" />
                                    </section>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </form>
@endsection
