<?php
$title = isset($data['title']) ? $data['title'] : '';

$bannerGroups = isset($data['bannerGroups']) ? $data['bannerGroups'] : [];
$blockGroups = isset($data['blockGroups']) ? $data['blockGroups'] : [];
?>

@extends('backend.main')

@section('content')
    @include('backend.elements.content_action', ['title' => $title, 'action' => 'backend.elements.actions.block.block_add'])
    <form method="POST" id="admin-form" action="{{app('UrlHelper')::admin('block', 'store')}}" enctype="multipart/form-data">
        <input type="hidden" name="action_type">
        @csrf
        <input type="hidden" name="typeSubmit" value="0">
        <div class="row">
            <div class="col-xl-12">
                <section class="hk-sec-wrapper">
                    <div class="row">
                        <nav id="nav-tabs">
                            <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                <a class="nav-item nav-link active" id="nav-vi-tab" data-toggle="tab" href="#nav-vi" role="tab" aria-controls="nav-vi" aria-selected="true">Thông tin (Tiếng Việt)</a>
                            </div>
                        </nav>
                        <div class="tab-content" id="nav-tabContent">
                            <div class="tab-pane fade show active" id="nav-vi" role="tabpanel" aria-labelledby="nav-vi-tab">
                                <div class="col-xl-12">
                                    <section class="hk-sec-wrapper">
                                        <div class="row">
                                            <div class="col-sm">
                                                <div class="table-wrap">
                                                    <div class="table-responsive">
                                                        <table class="table mb-0">
                                                            <tbody>
                                                                <tr>
                                                                    <th scope="row">Tên khối <span class="red">*</span></th>
                                                                    <td><input type="text" name="name" class="form-control form-control-sm" placeholder="Tên khối"></td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Vị trí</th>
                                                                    <td><input type="number" name="sorted" class="form-control form-control-sm" placeholder="Vị trí"></td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Bật</th>
                                                                    <td>
                                                                        <div class="form-check form-check-inline">
                                                                            <input class="form-check-input" type="radio" name="status" value="activated" checked="">
                                                                            <label class="form-check-label" >Có</label>
                                                                        </div>
                                                                        <div class="form-check form-check-inline">
                                                                            <input class="form-check-input" type="radio" name="status" value="inactive">
                                                                            <label class="form-check-label">Không</label>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Nhóm banner</th>
                                                                    <td>
                                                                        <select name="bannerGroupId" class="form-control custom-select form-control custom-select-sm mt-15">
                                                                            @if($bannerGroups->count() > 0)
                                                                                <option value="">Chọn nhóm</option>
                                                                                @foreach($bannerGroups as $key => $item)
                                                                                    <option value="{{ $item->bangroup_id }}">{{ $item->bangroup_name }}</option>
                                                                                @endforeach
                                                                            @endif
                                                                        </select>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Nhóm khối</th>
                                                                    <td>
                                                                        <select name="blockGroupId" class="form-control custom-select form-control custom-select-sm mt-15">
                                                                            @if($blockGroups->count() > 0)
                                                                                <option value="">Chọn nhóm</option>
                                                                                @foreach($blockGroups as $key => $item)
                                                                                    <option value="{{ $item->blockgroup_id }}">{{ $item->blockgroup_name }}</option>
                                                                                @endforeach
                                                                            @endif
                                                                        </select>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Mã</th>
                                                                    <td><input type="text" name="code" class="form-control form-control-sm" placeholder=""></td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Mô tả</th>
                                                                    <td><textarea id="editor_short" name="description" class="form-control" placeholder="Nhập mô tả"></textarea></td>
                                                                </tr>
                                                            <tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </form>
@endsection
