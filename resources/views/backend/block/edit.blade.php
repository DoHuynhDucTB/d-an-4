<?php
$block = !empty($data['block']) ? $data['block'] : null;
$title = !empty($data['title']) ? $data['title'] : '';

$bannerGroups = isset($data['bannerGroups']) ? $data['bannerGroups'] : [];
$blockGroups = isset($data['blockGroups']) ? $data['blockGroups'] : [];
?>

@extends('backend.main')

@section('content')
    @include('backend.elements.content_action', ['title' => $title, 'action' => 'backend.elements.actions.block.block_edit'])
    <form method="POST" id="admin-form" action="{{app('UrlHelper')::admin('block', 'update')}}" enctype="multipart/form-data">
        <input type="hidden" name="action_type">
        <input type="hidden" name="id" value="{{$block->block_id }}">
        @csrf
        <div class="row">
            <div class="col-xl-12">
                <section class="hk-sec-wrapper">
                    <div class="row">
                        <nav id="nav-tabs">
                            <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                <a class="nav-item nav-link active" id="nav-vi-tab" data-toggle="tab" href="#nav-vi" role="tab" aria-controls="nav-vi" aria-selected="true">Thông tin (Tiếng Việt)</a>
                            </div>
                        </nav>
                        <div class="tab-content" id="nav-tabContent">
                            <div class="tab-pane fade show active" id="nav-vi" role="tabpanel" aria-labelledby="nav-vi-tab">
                                <div class="col-xl-12">
                                    <section class="hk-sec-wrapper">
                                        <div class="row">
                                            <div class="col-sm">
                                                <div class="table-wrap">
                                                    <div class="table-responsive">
                                                        <table class="table mb-0">
                                                            <tbody>
                                                            <tr>
                                                                <th scope="row">Tên khối <span class="red">*</span></th>
                                                                <td><input type="text" name="name" class="form-control form-control-sm" value="{{ $block->block_name }}" placeholder="Tên khối"></td>
                                                            </tr>
                                                            <tr>
                                                                <th scope="row">Vị trí</th>
                                                                <td><input type="number" name="sorted" class="form-control form-control-sm" placeholder="Vị trí" value="{{ $block->block_sorted }}"></td>
                                                            </tr>
                                                            <tr>
                                                                <th scope="row">Bật</th>
                                                                <td>
                                                                    <div class="form-check form-check-inline">
                                                                        <input class="form-check-input" type="radio" name="status" value="activated" @if($block->block_status == 'activated') checked @endif>
                                                                        <label class="form-check-label" >Có</label>
                                                                    </div>
                                                                    <div class="form-check form-check-inline">
                                                                        <input class="form-check-input" type="radio" name="status" value="inactive" @if($block->block_status == 'inactive') checked @endif>
                                                                        <label class="form-check-label">Không</label>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th scope="row">Nhóm banner</th>
                                                                <td>
                                                                    <select name="bannerGroupId" class="form-control custom-select form-control custom-select-sm mt-15">
                                                                        @if($bannerGroups->count() > 0)
                                                                            <option value="">Chọn nhóm</option>
                                                                            @foreach($bannerGroups as $key => $item)
                                                                                <option value="{{ $item->bangroup_id }}"  @if($block->banner_group_id == $item->bangroup_id)  selected @endif>{{ $item->bangroup_name }}</option>
                                                                            @endforeach
                                                                        @endif
                                                                    </select>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th scope="row">Nhóm khối</th>
                                                                <td>
                                                                    <select name="blockGroupId" class="form-control custom-select form-control custom-select-sm mt-15">
                                                                        @if($blockGroups->count() > 0)
                                                                            <option value="">Chọn nhóm</option>
                                                                            @foreach($blockGroups as $key => $item)
                                                                                <option value="{{ $item->blockgroup_id }}" @if($block->block_group_id == $item->blockgroup_id)  selected @endif>{{ $item->blockgroup_name }}</option>
                                                                            @endforeach
                                                                        @endif
                                                                    </select>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th scope="row">Mã</th>
                                                                <td><input type="text" name="code" class="form-control form-control-sm" placeholder="" value="{{ $block->block_code }}"></td>
                                                            </tr>
                                                            <tr>
                                                                <th scope="row">Mô tả</th>
                                                                <td><textarea id="editor_short" name="description" class="form-control" placeholder="Nhập mô tả">{!! $block->block_description	 !!}</textarea></td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </form>
@endsection
