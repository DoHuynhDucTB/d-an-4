<?php
$title = isset($data['title']) ? $data['title'] : '';
$items = !empty($data['items']) ? $data['items'] : [];
$item = !empty($data['item']) ? $data['item'] : [];
$group = $data['group'];
?>

@extends('backend.main')

@section('content')
    @include('backend.elements.content_action', ['title' => $title, 'action' => 'backend.elements.actions.menu.menu_copy'])
    <form method="POST" id="admin-form" action="{{app('UrlHelper')::admin('menu', 'duplicate')}}" enctype="multipart/form-data">
        <input type="hidden" name="action_type">
        <input type="hidden" name="id" value="{{$item->menu_id}}">
        <input type="hidden" name="group" value="{{$group}}">
        @csrf
        <div class="row">
            <div class="col-xl-12">
                <section class="hk-sec-wrapper">
                    <div class="row product-module">
                        <nav id="nav-tabs">
                            <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                <a class="nav-item nav-link active" id="nav-vi-tab" data-toggle="tab" href="#nav-vi" role="tab" aria-controls="nav-vi" aria-selected="true">Thông tin (Tiếng Việt)</a>
                                <a class="nav-item nav-link" id="nav-en-tab" data-toggle="tab" href="#nav-en" role="tab" aria-controls="nav-en" aria-selected="true">Thông tin (Tiếng Anh)</a>
                            </div>
                        </nav>
                        <div class="tab-content" id="nav-tabContent">
                            <div class="tab-pane fade show active" id="nav-vi" role="tabpanel" aria-labelledby="nav-vi-tab">
                                <div class="col-xl-12">
                                    <section class="hk-sec-wrapper">
                                        <div class="row">
                                            <div class="col-sm">
                                                <div class="table-wrap">
                                                    <div class="table-responsive">
                                                        <table class="table mb-0">
                                                            <tbody>
                                                            <tr>
                                                                <th scope="row">Tên menu<span class="red">*</span></th>
                                                                <td><input type="text" name="name" class="form-control form-control-sm" placeholder="Tên" value="{{ $item->menu_name }}"></td>
                                                            </tr>
                                                            <tr>
                                                                <th scope="row">Url<span class="red">*</span></th>
                                                                <td><input type="text" name="url" class="form-control form-control-sm" placeholder="Url" value="{{ $item->menu_url }}"></td>
                                                            </tr>
                                                            <tr>
                                                                <th scope="row">Thứ tự</th>
                                                                <td><input type="number" name="number" class="form-control form-control-sm" placeholder="" value="{{ $item->menu_number }}"></td>
                                                            </tr>
                                                            <tr>
                                                                <th scope="row">Parent</th>
                                                                <td>
                                                                    <select class="form-control form-control-sm" name="parentId">
                                                                        <option value="">Chọn menu cha</option>
                                                                        @if($items->count() > 0)
                                                                            @foreach($items as $key => $itemMenu)
                                                                                <option value="{{ $itemMenu->menu_id }}" @if($itemMenu->menu_id == $item->parent_id ) selected @endif>{{ $itemMenu->menu_name }}</option>
                                                                            @endforeach
                                                                        @endif
                                                                    </select>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th scope="row">Bật</th>
                                                                <td>
                                                                    <div class="form-check form-check-inline">
                                                                        <input class="form-check-input" type="radio" name="status" value="activated" @if($item->menu_status == 'activated') checked @endif>
                                                                        <label class="form-check-label" >Có</label>
                                                                    </div>
                                                                    <div class="form-check form-check-inline">
                                                                        <input class="form-check-input" type="radio" name="status" value="inactive" @if($item->menu_status == 'inactive') checked @endif>
                                                                        <label class="form-check-label">Không</label>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="nav-en" role="tabpanel" aria-labelledby="nav-en-tab">
                                <div class="col-xl-12">
                                    <section class="hk-sec-wrapper">
                                        <div class="row">
                                            <div class="col-sm">
                                                <div class="table-wrap">
                                                    <div class="table-responsive">
                                                        <table class="table mb-0">
                                                            <tbody>
                                                            <tr>
                                                                <th scope="row">Tên menu</th>
                                                                <td><input type="text" name="nameEn" value="{{ $item->menu_name_en }}" class="form-control form-control-sm" placeholder="Tên"></td>
                                                            </tr>
                                                            <tr>
                                                                <th scope="row">Url</th>
                                                                <td><input type="text" name="urlEn" value="{{ $item->menu_url_en }}" class="form-control form-control-sm" placeholder="Url"></td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </form>
@endsection
