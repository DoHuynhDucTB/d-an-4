<?php
$title = isset($data['title']) ? $data['title'] : '';
$items = !empty($data['items']) ? $data['items'] : [];
$group = $data['group'];
?>

@extends('backend.main')

@section('content')
    @include('backend.elements.content_action', ['title' => $title, 'action' => 'backend.elements.actions.menu.menu_add'])
    <form method="POST" id="admin-form" action="{{app('UrlHelper')::admin('menu', 'store')}}" enctype="multipart/form-data">
        <input type="hidden" name="action_type">
        <input type="hidden" name="group" value="{{$group}}">
        @csrf
        <input type="hidden" name="typeSubmit" value="0">
        <div class="row">
            <div class="col-xl-12">
                <section class="hk-sec-wrapper">
                    <div class="row product-module">
                        <nav id="nav-tabs">
                            <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                <a class="nav-item nav-link active" id="nav-vi-tab" data-toggle="tab" href="#nav-vi" role="tab" aria-controls="nav-vi" aria-selected="true">Thông tin (Tiếng Việt)</a>
                                <a class="nav-item nav-link" id="nav-en-tab" data-toggle="tab" href="#nav-en" role="tab" aria-controls="nav-en" aria-selected="true">Thông tin (Tiếng Anh)</a>
                            </div>
                        </nav>
                        <div class="tab-content" id="nav-tabContent">
                            <div class="tab-pane fade show active" id="nav-vi" role="tabpanel" aria-labelledby="nav-vi-tab">
                                <div class="col-xl-12">
                                    <section class="hk-sec-wrapper">
                                        <div class="row">
                                            <div class="col-sm">
                                                <div class="table-wrap">
                                                    <div class="table-responsive">
                                                        <table class="table mb-0">
                                                            <tbody>
                                                                <tr>
                                                                    <th scope="row">Tên menu<span class="red">*</span></th>
                                                                    <td><input type="text" name="name" class="form-control form-control-sm" placeholder="Tên"></td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Url<span class="red">*</span></th>
                                                                    <td><input type="text" name="url" class="form-control form-control-sm" placeholder="Url"></td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Thứ tự</th>
                                                                    <td><input type="number" name="number" class="form-control form-control-sm" placeholder=""></td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Menu cha</th>
                                                                    <td>
                                                                        <select class="form-control form-control-sm" name="parentId">
                                                                            <option value="">--Chọn menu cha--</option>
                                                                            @if($items->count() > 0)
                                                                                @foreach($items as $key => $item)
                                                                                    <option value="{{ $item->menu_id }}">{{ $item->menu_name }}</option>
                                                                                @endforeach
                                                                            @endif
                                                                        </select>
                                                                        <span style="color: darkred; font-size: 10pt">(Không chọn mặc định sẽ là menu cha)</span>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Bật</th>
                                                                    <td>
                                                                        <div class="form-check form-check-inline">
                                                                            <input class="form-check-input" type="radio" name="status" value="activated" checked="">
                                                                            <label class="form-check-label" >Có</label>
                                                                        </div>
                                                                        <div class="form-check form-check-inline">
                                                                            <input class="form-check-input" type="radio" name="status" value="inactive">
                                                                            <label class="form-check-label">Không</label>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="nav-en" role="tabpanel" aria-labelledby="nav-en-tab">
                                <div class="col-xl-12">
                                    <section class="hk-sec-wrapper">
                                        <div class="row">
                                            <div class="col-sm">
                                                <div class="table-wrap">
                                                    <div class="table-responsive">
                                                        <table class="table mb-0">
                                                            <tbody>
                                                                <tr>
                                                                    <th scope="row">Tên menu</th>
                                                                    <td><input type="text" name="nameEn" class="form-control form-control-sm" placeholder="Tên"></td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Url</th>
                                                                    <td><input type="text" name="urlEn" class="form-control form-control-sm" placeholder="Url"></td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </form>
@endsection
