<?php
$title = isset($data['title']) ? $data['title'] : '';
$adminName = $data['adminName'];

$colors = !empty($data['colors']) ? $data['colors'] : [];
$sizes = !empty($data['sizes']) ? $data['sizes'] : [];
$producers = !empty($data['producers']) ? $data['producers'] : [];
$product = !empty($data['product']) ? $data['product'] : [];
$variants = !empty($data['variants']) ? $data['variants'] : [];
$type = $data['type'];
$productId = $data['product_id'];
?>

@extends('backend.main')

@section('content')
    @include('backend.elements.content_action', ['title' => $title, 'action' => 'backend.elements.actions.productvariant.productvariant_add'])
    <form method="POST" id="admin-form" action="{{app('UrlHelper')::admin('product', 'store')}}" enctype="multipart/form-data">
        <input type="hidden" name="action_type">
        <input type="hidden" name="type" value="{{$type}}">
        <input type="hidden" name="product_id" value="{{$productId}}">
        <input type="hidden" name="product_code" value="{{$product->product_code}}">

        @csrf
        <input type="hidden" name="typeSubmit" value="0">
        <div class="row">
            <div class="col-xl-12">
                <section class="hk-sec-wrapper">
                    <div class="row product-module">
                        <nav id="nav-tabs">
                            <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                <a class="nav-item nav-link active" id="nav-vi-tab" data-toggle="tab" href="#nav-vi" role="tab" aria-controls="nav-vi" aria-selected="true">Thông tin biến thể(Tiếng Việt)</a>
                                <a class="nav-item nav-link" id="nav-en-tab" data-toggle="tab" href="#nav-en" role="tab" aria-controls="nav-en" aria-selected="false">Thông tin biến thể (Tiếng Anh)</a>
                                <a class="nav-item nav-link" id="nav-setting-tab" data-toggle="tab" href="#nav-setting" role="tab" aria-controls="nav-setting" aria-selected="false">Cấu hình </a>
                                <a class="nav-item nav-link" id="nav-upload-tab" data-toggle="tab" href="#nav-upload" role="tab" aria-controls="nav-upload" aria-selected="false">Ảnh đại diện</a>
                                <a class="nav-item nav-link" id="nav-related-tab" data-toggle="tab" href="#nav-related" role="tab" aria-controls="nav-related" aria-selected="false">Biến thể liên quan</a>
                            </div>
                        </nav>
                        <div class="tab-content" id="nav-tabContent">
                            <div class="tab-pane fade show active" id="nav-vi" role="tabpanel" aria-labelledby="nav-vi-tab">
                                <div class="col-xl-12">
                                    <section class="hk-sec-wrapper">
                                        <div class="row">
                                            <div class="col-sm">
                                                <div class="table-wrap">
                                                    <div class="table-responsive">
                                                        <table class="table mb-0">
                                                            <tbody>
                                                                <tr>
                                                                    <th scope="row">Được tạo bởi</th>
                                                                    <td>{{ $adminName }}</td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Tên sản phẩm gốc</th>
                                                                    <td>{{$product->product_name}}</td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Mã sản phẩm gốc</th>
                                                                    <td>{{$product->product_code}}</td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Tên biến thể <span class="red">*</span></th>
                                                                    <td><input type="text" name="name" class="form-control form-control-sm" placeholder="Tên biến thể"></td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Giá</th>
                                                                    <td><input type="number" name="price" class="form-control form-control-sm" placeholder="Nhập giá"></td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Giá mới</th>
                                                                    <td><input type="number" name="newPrice" class="form-control form-control-sm" placeholder="Nhập giá mới"></td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Giảm giá</th>
                                                                    <td><input type="number" name="discount" class="form-control form-control-sm" placeholder="Nhập giảm giá"></td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Lượt xem</th>
                                                                    <td><input type="number" name="view" class="form-control form-control-sm" placeholder="Nhập lượt xem"></td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Trạng thái</th>
                                                                    <td>
                                                                        <select name="status" class="form-control custom-select form-control custom-select-sm mt-15">
                                                                            <option value="stocking">Còn Hàng</option>
                                                                            <option value="out_of_stock">Hết Hàng</option>
                                                                        </select>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Trọng lượng</th>
                                                                    <td><input type="number" name="weight" class="form-control form-control-sm" placeholder="Nhập trọng lượng"></td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Chiều dài</th>
                                                                    <td><input type="number" name="length" class="form-control form-control-sm" placeholder="Nhập chiều dài"></td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Chiều rộng</th>
                                                                    <td><input type="number" name="width" class="form-control form-control-sm" placeholder="Nhập chiều rộng"></td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Chiều cao</th>
                                                                    <td><input type="number" name="height" class="form-control form-control-sm" placeholder="Nhập chiều cao"></td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Mô tả ngắn</th>
                                                                    <td><textarea id="editor_short" name="shortDescription" class="form-control" placeholder="Nhập mô tả ngắn"></textarea></td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Mô tả</th>
                                                                    <td><textarea id="editor" name="description" class="form-control" placeholder="Nhập mô tả"></textarea></td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Meta Title</th>
                                                                    <td><input type="text" name="metaTitle" class="form-control form-control-sm" placeholder=""></td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Meta Keywords</th>
                                                                    <td><input type="text" name="metaKeywords" class="form-control form-control-sm" placeholder=""></td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Meta Description</th>
                                                                    <td><input type="text" name="metaDescription" class="form-control form-control-sm" placeholder=""></td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Ghi chú cho sản phẩm</th>
                                                                    <td><textarea name="note" class="form-control" rows="3" placeholder="Nhập ghi chú"></textarea></td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="nav-en" role="tabpanel" aria-labelledby="nav-en-tab">
                                <div class="col-xl-12">
                                    <section class="hk-sec-wrapper">
                                        <div class="row">
                                            <div class="col-sm">
                                                <div class="table-wrap">
                                                    <div class="table-responsive">
                                                        <table class="table mb-0">
                                                            <tbody>
{{--                                                                <tr>--}}
{{--                                                                    <th scope="row">Tên sản phẩm gốc</th>--}}
{{--                                                                    <td>{{$product->product_name_en}}</td>--}}
{{--                                                                </tr>--}}
                                                                <tr>
                                                                    <th scope="row">Tên biến thể</th>
                                                                    <td><input type="text" name="nameEn" class="form-control form-control-sm" placeholder="Tên biến thể"></td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Mô tả ngắn</th>
                                                                    <td><textarea id="editor_short_en" name="shortDescriptionEn" class="form-control" placeholder="Nhập mô tả ngắn"></textarea></td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Mô tả</th>
                                                                    <td><textarea id="editor_en" name="descriptionEn" class="form-control" placeholder="Nhập mô tả"></textarea></td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Meta Title</th>
                                                                    <td><input type="text" name="metaTitleEn" class="form-control form-control-sm" placeholder=""></td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Meta Keywords</th>
                                                                    <td><input type="text" name="metaKeywordsEn" class="form-control form-control-sm" placeholder=""></td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Meta Description</th>
                                                                    <td><input type="text" name="metaDescriptionEn" class="form-control form-control-sm" placeholder=""></td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Ghi chú cho sản phẩm</th>
                                                                    <td><textarea name="noteEn" class="form-control" rows="3" placeholder="Nhập ghi chú"></textarea></td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="nav-setting" role="tabpanel" aria-labelledby="nav-setting-tab">
                                <div class="col-xl-12">
                                    <section class="hk-sec-wrapper">
                                        <div class="row">
                                            <div class="col-sm">
                                                <div class="table-wrap">
                                                    <div class="table-responsive">
                                                        <table class="table mb-0">
                                                            <tbody>
                                                                <tr>
                                                                    <th scope="row">Màu</th>
                                                                    <td>
                                                                        @if(!empty($colors) && $colors->count() > 0)
                                                                            @foreach($colors as $itemColor)
                                                                                <label class="radio-inline label-right">
                                                                                    <input type="radio" name="color" value="{{ $itemColor->pcolor_id }}" > {{ $itemColor->pcolor_name }} <span class="span-color" style="background: {{ $itemColor->pcolor_hex }}"></span>
                                                                                </label>
                                                                            @endforeach
                                                                        @endif
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Size</th>
                                                                    <td>
                                                                        @if(!empty($sizes) && $sizes->count() > 0)
                                                                            @foreach($sizes as $itemSize)
                                                                                <label class="radio-inline label-right">
                                                                                    <input type="radio" name="size" value="{{ $itemSize->psize_id }}" > {{ $itemSize->psize_value }}
                                                                                </label>
                                                                            @endforeach
                                                                        @endif
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Giới tính</th>
                                                                    <td>
                                                                        <div class="form-check form-check-inline">
                                                                            <input class="form-check-input" type="radio" name="sex" value="male" checked="">
                                                                            <label class="form-check-label" >Dành cho nam</label>
                                                                        </div>
                                                                        <div class="form-check form-check-inline">
                                                                            <input class="form-check-input" type="radio" name="sex" value="female">
                                                                            <label class="form-check-label">Dành cho nữ</label>
                                                                        </div>
                                                                        <div class="form-check form-check-inline">
                                                                            <input class="form-check-input" type="radio" name="sex" value="unisex">
                                                                            <label class="form-check-label">Dành cho Unisex</label>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Bật/Tắt</th>
                                                                    <td>
                                                                        <div class="form-check form-check-inline">
                                                                            <input class="form-check-input" type="radio" name="statusShow" value="yes">
                                                                            <label class="form-check-label">Có</label>
                                                                        </div>
                                                                        <div class="form-check form-check-inline">
                                                                            <input class="form-check-input" type="radio" name="statusShow" value="no" checked>
                                                                            <label class="form-check-label">Không</label>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Sản phẩm HOT (hiện trang chủ)</th>
                                                                    <td>
                                                                        <div class="form-check form-check-inline">
                                                                            <input class="form-check-input" type="radio" name="isHot" value="yes">
                                                                            <label class="form-check-label">Có</label>
                                                                        </div>
                                                                        <div class="form-check form-check-inline">
                                                                            <input class="form-check-input" type="radio" name="isHot" value="no" checked="">
                                                                            <label class="form-check-label">Không</label>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Sản phẩm làm quà tặng</th>
                                                                    <td>
                                                                        <div class="form-check form-check-inline">
                                                                            <input class="form-check-input" type="radio" name="isGift" value="yes">
                                                                            <label class="form-check-label">Có</label>
                                                                        </div>
                                                                        <div class="form-check form-check-inline">
                                                                            <input class="form-check-input" type="radio" name="isGift" value="no" checked="">
                                                                            <label class="form-check-label">Không</label>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Sản phẩm mới</th>
                                                                    <td>
                                                                        <div class="form-check form-check-inline">
                                                                            <input class="form-check-input" type="radio" name="isNew" value="yes">
                                                                            <label class="form-check-label">Có</label>
                                                                        </div>
                                                                        <div class="form-check form-check-inline">
                                                                            <input class="form-check-input" type="radio" name="isNew" value="no" checked="">
                                                                            <label class="form-check-label">Không</label>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Sản phẩm khuyến mãi</th>
                                                                    <td>
                                                                        <div class="form-check form-check-inline">
                                                                            <input class="form-check-input" type="radio" name="isSale" value="yes">
                                                                            <label class="form-check-label">Có</label>
                                                                        </div>
                                                                        <div class="form-check form-check-inline">
                                                                            <input class="form-check-input" type="radio" name="isSale" value="no" checked="">
                                                                            <label class="form-check-label">Không</label>
                                                                            <img width="45px" src="{{asset(ADMIN_CSS_AND_JAVASCRIPT_PATH) . '/dist/icons/coupon.png'}}" alt="">
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Sản phẩm bán chạy</th>
                                                                    <td>
                                                                        <div class="form-check form-check-inline">
                                                                            <input class="form-check-input" type="radio" name="isSelling" value="yes">
                                                                            <label class="form-check-label">Có</label>
                                                                        </div>
                                                                        <div class="form-check form-check-inline">
                                                                            <input class="form-check-input" type="radio" name="isSelling" value="no" checked="">
                                                                            <label class="form-check-label">Không</label>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Miễn phí giao hàng</th>
                                                                    <td>
                                                                        <div class="form-check form-check-inline">
                                                                            <input class="form-check-input" type="radio" name="isFreeShip" value="yes">
                                                                            <label class="form-check-label">Có</label>
                                                                        </div>
                                                                        <div class="form-check form-check-inline">
                                                                            <input class="form-check-input" type="radio" name="isFreeShip" value="no" checked="">
                                                                            <label class="form-check-label">Không</label>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="nav-upload" role="tabpanel" aria-labelledby="nav-upload-tab">
                                <div class="col-xl-12">
                                    <section class="hk-sec-wrapper">
                                        <h5 class="hk-sec-title">Ảnh đại diện</h5>
                                        <input type="file" name="imageAvatar" class="dropify" />
                                    </section>
                                    <section class="hk-sec-wrapper">
                                        <div class="row">
                                            <div class="col-md-5">
                                                <h5 class="hk-sec-title">Ảnh thu nhỏ</h5>
                                            </div>
                                            <div class="col-md-5">
                                                <h5 class="hk-sec-title">Ảnh đầy đủ</h5>
                                            </div>
                                            <div class="col-md-2">
                                                <button class="btn btn-outline-light btn-sm add-image"><i class="glyphicon glyphicon-plus"></i> Thêm ảnh</button>
                                            </div>
                                            <div class="box-image col-md-12">
                                            </div>
                                        </div>
                                    </section>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="nav-related" role="tabpanel" aria-labelledby="nav-related-tab">
                                <div class="col-xl-6">
                                    <select multiple class="form-control custom-select-sm" name="related[]">
                                        @if($variants->count() > 0)
                                            @foreach($variants as $key => $itemVariant)
                                                <option value="{{ $itemVariant->pvariant_id }}">{{ $itemVariant->pvariant_name }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </form>
@endsection
