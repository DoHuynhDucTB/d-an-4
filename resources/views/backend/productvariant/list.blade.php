<?php
/**
 * @var \App\Helpers\UrlHelper $urlHelper
 */
$urlHelper = app('UrlHelper');
$variants = !empty($data['variants']) ? $data['variants'] : [];
$title = isset($data['title']) ? $data['title'] : '';
$type = $data['type'];
$productId = $data['product_id'];
?>

@extends('backend.main')

@section('content')
    @include('backend.elements.content_action', ['title' => $title, 'action' => 'backend.elements.actions.productvariant.productvariant_list'])
    <div class="row">
        <div class="col-xl-12">
            <section class="hk-sec-wrapper">
                <div class="row">
                    <div class="col-sm">
                        <div class="table-wrap">
                            <form action="#" method="post" id="admin-form">
                                <input type="hidden" name="type" value="{{$type}}">
                                <input type="hidden" name="product_id" value="{{$productId}}">
                                @csrf
                                <table id="datable_1" class="table table-hover w-100 display pb-30 js-main-table">
                                    <thead>
                                    <tr>
                                        <th><input type="checkbox" class="checkAll" name="checkAll" value=""></th>
                                        <th>#</th>
                                        <th>Ảnh</th>
                                        <th>Tên biến thể</th>
                                        <th>Màu sắc</th>
                                        <th>Size</th>
                                        <th>Giới tính</th>
                                        <th>Ngày tạo</th>
                                        <th>Trạng thái tồn kho</th>
                                        <th>Bật</th>
                                        <th>Id</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(!empty($variants) && count($variants) > 0)
                                        @foreach($variants as $key => $item)
                                            <tr>
                                                <th><input type="checkbox" class="checkItem" name="cid[]" value="{{ $item->pvariant_id  }}"></th>
                                                <td>{{ $key + 1 }}</td>
                                                <td>
                                                    @if(!empty($item->urlAvatar))
                                                        <img width="70px" class="js-lazy-loading" data-src="{{ asset($item->urlAvatar) }}">
                                                    @endif
                                                </td>
                                                <td><a href="@php echo $urlHelper::admin('productvariant', 'edit', ['id' => $item->pvariant_id, 'type' => $type, 'product_id' => $productId]) @endphp">{{ $item->pvariant_name }}</a></td>
                                                <td>{{ $item->color->pcolor_name}}</td>
                                                <td>{{ $item->size->psize_value}}</td>
                                                <td>{{ \App\Models\ProductVariant::SEX_NAME[$item->pvariant_sex] }}</td>
                                                <td>{{ $item->pvariant_created_at}}</td>
                                                <td>{{ \App\Models\ProductVariant::STATUS_NAME[$item->pvariant_status] }}</td>
                                                <td>{{ \App\Models\ProductVariant::STATUS_SHOW_NAME[$item->pvariant_status_show] }}</td>
                                                <td>{{ $item->pvariant_id }}</td>
                                            </tr>
                                        @endforeach
                                    @endif
                                    </tbody>
                                </table>
                            </form>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
@endsection
