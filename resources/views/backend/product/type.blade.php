@extends('backend.main')
@section('content')
    @include('backend.elements.content_action', ['title' => 'Quản lý sản phẩm'])
    <div class="row">
        <div class="col-xl-12">
            <section class="hk-sec-wrapper">
                <div class="row">
                    <div class="col-sm">
                        <div class="table-wrap">
                            <a href="{{app('UrlHelper')->admin('product', 'index?type=shoes')}}" class="btn btn-primary">Sản phẩm GIÀY</a>
                            <a href="{{app('UrlHelper')->admin('product', 'index?type=sandals')}}" class="btn btn-primary">Sản phẩm DÉP</a>
                            <a href="{{app('UrlHelper')->admin('product', 'index?type=raincoat')}}" class="btn btn-primary">Sản phẩm ÁO MƯA</a>
                            <a href="{{app('UrlHelper')->admin('product', 'index?type=accessories')}}" class="btn btn-primary">Sản phẩm PHỤ KIỆN</a>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
@endsection
