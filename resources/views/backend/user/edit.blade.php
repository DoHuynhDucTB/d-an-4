<?php
$user = !empty($data['user']) ? $data['user'] : null;
$title = isset($data['title']) ? $data['title'] : '';

$province = $data['province'];
?>

@extends('backend.main')

@section('content')
    @include('backend.elements.content_action', ['title' => $title, 'action' => 'backend.elements.actions.user.user_edit'])

    <form method="POST" id="admin-form" action="" enctype="multipart/form-data">
        <input type="hidden" name="action_type">
        <input type="hidden" name="id" value="{{ $user->id }}">
        <input type="hidden" name="task" value="update">
        @csrf
        <div class="row">
            <div class="col-xl-12">
                <section class="hk-sec-wrapper">
                    <div class="row">
                        <div class="col-xl-12">
                            <table class="table table-bordered">
                                <tbody>
                                    <tr>
                                        <td style="width: 20%">Tên</td>
                                        <td><input class="form-control" name="name" value="{{ $user->name }}"></td>
                                    </tr>
                                    <tr>
                                        <td style="width: 20%">Ngày sinh</td>
                                        <td><input class="form-control datepicker" name="date_of_birth" value="{{ $user->date_of_birth }}"></td>
                                    </tr>
                                    <tr>
                                        <td>Mã</td>
                                        <td><input class="form-control" name="code" value="{{ $user->code }}"></td>
                                    </tr>
                                    <tr>
                                        <td>Email</td>
                                        <td><input class="form-control" name="email" value="{{ $user->email }}"></td>
                                    </tr>
                                    <tr>
                                        <td>Phone</td>
                                        <td><input class="form-control" name="phone" value="{{ $user->phone }}"></td>
                                    </tr>
                                    <tr>
                                        <td>Loại</td>
                                        <td>
                                            <select class="form-control" name="type">
                                                <option value="facebook" @if($user->type == 'facebook') selected @endif>facebook</option>
                                                <option value="google" @if($user->type == 'google') selected @endif>google</option>
                                                <option value="system" @if($user->type == 'system') selected @endif>system</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Trạng thái</td>
                                        <td>
                                            <select class="form-control" name="status">
                                                <option value="activated" @if($user->type == 'activated') selected @endif>Kích hoạt</option>
                                                <option value="inactive" @if($user->type == 'inactive') selected @endif>Tạm dừng</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Ảnh</td>
                                        <td><img src="{{ $user->avatar }}"></td>
                                    </tr>
                                    <tr>
                                        <td>Giới tính</td>
                                        <td>
                                            <select class="form-control" name="sex">
                                                <option value="male" @if($user->sex == 'male') selected @endif>male</option>
                                                <option value="female" @if($user->sex == 'female') selected @endif>female</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Loại da</td>
                                        <td>
                                            @foreach($user->productSkins as $productSkins)
                                                {{ $productSkins->pskin_name }} <br>
                                            @endforeach
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Nhu cầu chăm sóc da</td>
                                        <td>
                                            @foreach($user->productCares as $productCare)
                                                {{ $productCare->pcare_name }} <br>
                                            @endforeach
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Phong cách trang điểm</td>
                                        <td>
                                            @foreach($user->productMakeUps as $productMakeUp)
                                                {{ $productMakeUp->pmakeup_name }} <br>
                                            @endforeach
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Địa chỉ</td>
                                        <td><input class="form-control" name="address" value="{{ $user->address }}"></td>
                                    </tr>
                                    <tr>
                                        <td>Tỉnh/Thành phố</td>
                                        <td>
                                            <select class="form-control" id="provinceId" name="provinceId" required>
                                                <option value="">Chọn</option>
                                                @if(!empty($province) && count($province) > 0)
                                                    @foreach($province as $key => $item)
                                                        <option value="{{ $item->id }}" @if($item->id == $user->province_id) selected @endif>{{ $item->name }}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Huyện/ Quận</td>
                                        <td>
                                            <select class="form-control" id="districtId" name="districtId" required>
                                                @if($user->district_id > 0)
                                                    <?php
                                                        $district = \App\Models\District::query()->where('id', $user->district_id)->first();
                                                        $districtName = !empty($district) ? $district->name : '';
                                                    ?>
                                                    <option value="{{ $user->district_id }}">{{ $districtName }}</option>
                                                @else
                                                    <option value="">Chọn</option>
                                                @endif
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Phường/Xã</td>
                                        <td>
                                            <select class="form-control" id="wardId" name="wardId" required>
                                                @if($user->ward_id > 0)
                                                    <?php
                                                    $ward = \App\Models\Ward::query()->where('id', $user->ward_id)->first();
                                                    $wardName = !empty($ward) ? $ward->name : '';
                                                    ?>
                                                    <option value="{{ $user->ward_id }}">{{ $wardName }}</option>
                                                @else
                                                    <option value="">Chọn</option>
                                                @endif
                                            </select>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </form>
@endsection
