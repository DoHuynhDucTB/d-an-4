<?php
$voucher = !empty($data['voucher']) ? $data['voucher'] : [];
$arrayRelated = !empty($data['arrayRelated']) ? $data['arrayRelated'] : [];
$title = !empty($data['title']) ? $data['title'] : '';
$urlAvatar = !empty($data['urlAvatar']) ? $data['urlAvatar'] : '';
$productCategories = isset($data['productCategories']) ? $data['productCategories'] : null;
$userVouchers = isset($data['userVouchers']) ? $data['userVouchers'] : [];
$products = !empty($data['products']) ? $data['products'] : [];
$linkSelectUser = !empty($data['linkSelectUser']) ? $data['linkSelectUser'] : '';

$users = isset($data['users']) ? $data['users'] : [];
$orders = isset($data['orders']) ? $data['orders'] : [];
?>

@extends('backend.main')

@section('content')
    @include('backend.elements.content_action', ['title' => $title, 'action' => 'backend.elements.actions.voucher.voucher_edit'])
    <form method="POST" id="admin-form" action="{{app('UrlHelper')::admin('post', 'update')}}" enctype="multipart/form-data">
        <input type="hidden" name="action_type">
        <input type="hidden" name="id" value="{{$voucher->voucher_id}}">
        @csrf
        <div class="row">
            <div class="col-xl-12">
                <section class="hk-sec-wrapper">
                    <div class="row">
                        <nav id="nav-tabs">
                            <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                <a class="nav-item nav-link active" id="nav-vi-tab" data-toggle="tab" href="#nav-vi" role="tab" aria-controls="nav-vi" aria-selected="true">Thông tin (Tiếng Việt)</a>
                                <a class="nav-item nav-link" id="nav-order-tab" data-toggle="tab" href="#nav-order" role="tab" aria-controls="nav-order" aria-selected="true">Thông tin sử dụng Voucher</a>
                                <a class="nav-item nav-link" id="nav-user-tab" data-toggle="tab" href="#nav-user" role="tab" aria-controls="nav-user" aria-selected="true">Thông tin tặng Voucher</a>
                                <a class="nav-item nav-link" id="nav-upload-tab" data-toggle="tab" href="#nav-upload" role="tab" aria-controls="nav-upload" aria-selected="false">Upload ảnh</a>
                            </div>
                        </nav>
                        <div class="tab-content" id="nav-tabContent">
                            <div class="tab-pane fade show active" id="nav-vi" role="tabpanel" aria-labelledby="nav-vi-tab">
                                <div class="col-xl-12">
                                    <section class="hk-sec-wrapper">
                                        <div class="row">
                                            <div class="col-sm">
                                                <div class="table-wrap">
                                                    <div class="table-responsive">
                                                        <table class="table mb-0">
                                                            <tbody>
                                                            <tr>
                                                                <th scope="row">Tiêu đề <span class="red">*</span></th>
                                                                <td><input type="text" name="name" class="form-control form-control-sm" value="{{ $voucher->voucher_name }}" placeholder="Tiêu đề"></td>
                                                            </tr>
                                                            <tr>
                                                                <th scope="row">Mã giảm giá</th>
                                                                <td><input type="text" name="code" class="form-control form-control-sm" value="{{ $voucher->voucher_code }}" placeholder="Mã giảm giá"></td>
                                                            </tr>
                                                            <tr>
                                                                <th scope="row">Số tiền giảm giá</th>
                                                                <td><input type="number" name="money" class="form-control form-control-sm" value="{{ $voucher->voucher_money }}" placeholder="Mã giảm giá"></td>
                                                            </tr>
                                                            <tr>
                                                                <th scope="row">Phần trăm giảm giá</th>
                                                                <td><input type="number" name="percent" max="100" class="form-control form-control-sm" value="{{ $voucher->voucher_percent }}" placeholder="% giảm giá"></td>
                                                            </tr>
                                                            <tr>
                                                                <th scope="row">Áp dụng Từ ngày</th>
                                                                <td><input class="form-control time-statistic" type="text" name="fromTime" value="{{ $voucher->voucher_from }}" /></td>
                                                            </tr>
                                                            <tr>
                                                                <th scope="row">Ngày hết hạn</th>
                                                                <td><input class="form-control time-statistic" type="text" name="toTime" value="{{ $voucher->voucher_to }}" /></td>
                                                            </tr>
                                                            <tr>
                                                                <th scope="row">Giá trị đơn hàng >=	</th>
                                                                <td><input type="number" name="minValueOrder" class="form-control form-control-sm" value="{{ $voucher->voucher_code }}" placeholder="Giá trị đơn hàng >="></td>
                                                            </tr>
                                                            <tr>
                                                                <th scope="row">Loại sản phẩm</th>
                                                                <td>
                                                                    <select class="select2 select2-multiple" name="type[]" multiple="multiple" id="SelectProductType">
                                                                        <option value="" disabled>Chọn loại sản phẩm</option>
                                                                        <option value="shoes" @if(!empty($voucher->product_type) && count(json_decode($voucher->product_type, 1)) > 0 && in_array('shoes', json_decode($voucher->product_type, 1)))  selected @endif>Giày</option>
                                                                        <option value="sandals" @if(!empty($voucher->product_type) &&  count(json_decode($voucher->product_type, 1)) > 0 &&  in_array('sandals', json_decode($voucher->product_type, 1)))  selected @endif>Dép</option>
                                                                        <option value="raincoat"  @if(!empty($voucher->product_type) &&  count(json_decode($voucher->product_type, 1)) > 0 &&  in_array('raincoat', json_decode($voucher->product_type, 1)))  selected @endif>Áo Mưa</option>
                                                                        <option value="accessories"  @if(!empty($voucher->product_type) &&  count(json_decode($voucher->product_type, 1)) > 0 &&  in_array('accessories', json_decode($voucher->product_type, 1)))  selected @endif>Phụ Kiện</option>
                                                                    </select>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th scope="row">Nhóm sản phẩm</th>
                                                                <td>
                                                                    <select class="select2 select2-multiple" name="pcatId[]" multiple="multiple" id="SelectProductCategory">
                                                                        <option value="" disabled>Chọn nhóm sản phẩm</option>
                                                                        @if(!empty($productCategories) && count($productCategories) > 0)
                                                                            @foreach($productCategories as $key => $item)
                                                                                <option value="{{ $item['pcat_id'] }}" @if(!empty($voucher->product_category) &&  count(json_decode($voucher->product_category, 1)) > 0 && in_array( $item['pcat_id'], json_decode($voucher->product_category, 1)))  selected @endif>{{ $item['pcat_name'] }}</option>
                                                                            @endforeach
                                                                        @endif
                                                                    </select>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th scope="row">Sản phẩm</th>
                                                                <td>
                                                                    <select class="select2 select2-multiple" name="productId[]" multiple="multiple" id="SelectProduct">
                                                                        <option value="" disabled>Chọn sản phẩm</option>
                                                                         @if(!empty($products) && $products->count() > 0)
                                                                            @foreach($products as $key => $item)
                                                                                <option value="{{ $item->product_id }}" @if(!empty($voucher->product_id) && count(json_decode($voucher->product_id, 1)) > 0  && in_array( $item->product_id, json_decode($voucher->product_id, 1)))  selected @endif>{{ $item->product_name }}</option>
                                                                            @endforeach
                                                                        @endif
                                                                    </select>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th scope="row">Số đơn hàng áp dụng	</th>
                                                                <td><input type="number" name="orderNumberApplication" value="{{ $voucher->order_number_application }}" class="form-control form-control-sm" placeholder="Số đơn hàng áp dụng"></td>
                                                            </tr>
                                                            <tr>
                                                                <th scope="row">Đơn hàng đã sử dụng	</th>
                                                                <td><input type="number" name="orderNumberUsed" value="{{ $voucher->order_number_used }}" class="form-control form-control-sm" placeholder="Đơn hàng đã sử dụng"></td>
                                                            </tr>
                                                            <tr>
                                                                <th scope="row">Số SP tối đa được mua (trên mỗi SP)	</th>
                                                                <td><input type="number" name="maxProduct" value="{{ $voucher->max_product }}" class="form-control form-control-sm" placeholder="Số SP tối đa được mua"></td>
                                                            </tr>
                                                            <tr>
                                                                <th scope="row">Trạng thái</th>
                                                                <td>
                                                                    <div class="form-check form-check-inline">
                                                                        <input class="form-check-input" type="radio" name="status" value="activated" @if($voucher->voucher_status == 'activated') checked @endif>
                                                                        <label class="form-check-label" >Có</label>
                                                                    </div>
                                                                    <div class="form-check form-check-inline">
                                                                        <input class="form-check-input" type="radio" name="status" value="inactive"  @if($voucher->voucher_status == 'inactive') checked @endif>
                                                                        <label class="form-check-label">Không</label>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th scope="row">Mô tả ngắn</th>
                                                                <td><textarea id="editor_short" name="description" class="form-control" placeholder="Nhập mô tả">{!! $voucher->voucher_description !!}</textarea></td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="nav-order" role="tabpanel" aria-labelledby="nav-order-tab">
                                <div class="col-xl-12">
                                    <section class="hk-sec-wrapper">
                                        <div class="row">
                                            <div class="col-sm">
                                                <div class="table-wrap">
                                                    <table id="datable_1" class="table table-hover w-100 display pb-30 js-main-table">                                                            <thead>
                                                        <tr>
                                                            <th>#</th>
                                                            <th>Tên</th>
                                                            <th>Email</th>
                                                            <th>Phone</th>
                                                            <th>Giá trị đơn hàng</th>
                                                            <th>Mã đơn hàng</th>
                                                            <th>Ngày</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        @if(!empty($orders) && $orders->count() > 0)
                                                            @foreach($orders as $key => $itemOrder)
                                                                <tr>
                                                                    <td>{{ $key+1 }}</td>
                                                                    <td>{{ $itemOrder->order_full_name }}</td>
                                                                    <td>{{ $itemOrder->order_email }}</td>
                                                                    <td>{{ $itemOrder->order_phone }}</td>
                                                                    <td>{{ number_format($itemOrder->ord_total_cost, 0, '.', ',')  }} VNĐ</td>
                                                                    <td>{{ $itemOrder->ord_code }}</td>
                                                                    <td>{{ $itemOrder->ord_created_at }}</td>
                                                                </tr>
                                                            @endforeach
                                                        @endif
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="nav-user" role="tabpanel" aria-labelledby="nav-user-tab">
                                <div class="col-xl-12">
                                    <a class="link-select-voucher" href="{{ $linkSelectUser }}" target="_blank">Tặng voucher</a>
                                </div><br>
                                <div class="col-xl-12">
                                    <section class="hk-sec-wrapper">
                                        <div class="row">
                                            <div class="col-sm">
                                                <div class="table-wrap">
                                                    <table id="datable_2" class="table table-hover w-100 display pb-30 js-main-table">                                                            <thead>
                                                        <tr>
                                                            <th>#</th>
                                                            <th>Tên</th>
                                                            <th>Email</th>
                                                            <th>Phone</th>
                                                            <th>Ngày tặng</th>
                                                            <th>Đã sử dụng</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        @if(!empty($userVouchers) && count($userVouchers) > 0)
                                                            @foreach($userVouchers as $key => $itemUser)
                                                                <tr>
                                                                    <td>{{ $key + 1 }}</td>
                                                                    <td>{{ $itemUser['name'] }}</td>
                                                                    <td>{{ $itemUser['email'] }}</td>
                                                                    <td>{{ $itemUser['phone'] }}</td>
                                                                    <td>{{ $itemUser['timeVoucher'] }}</td>
                                                                    <td>{{ $itemUser['orderNumber'] }}</td>
                                                                </tr>
                                                            @endforeach
                                                        @endif
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="nav-upload" role="tabpanel" aria-labelledby="nav-upload-tab">
                                <div class="col-xl-12">
                                    <section class="hk-sec-wrapper">
                                        <h5 class="hk-sec-title">Ảnh đại diện</h5>
                                        <input type="file" name="imageAvatar" class="dropify" @if(!empty($urlAvatar)) data-default-file="{{ asset($urlAvatar) }}" @endif/>
                                    </section>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
{{--        @include('backend.voucher.modal-user')--}}
    </form>
@endsection
