<?php
/**
 * @var \App\Helpers\UrlHelper $urlHelper
 */
$urlHelper = app('UrlHelper');
$vouchers = !empty($data['vouchers']) ? $data['vouchers'] : [];
$title = isset($data['title']) ? $data['title'] : '';
?>

@extends('backend.main')

@section('content')
    @include('backend.elements.content_action', ['title' => $title, 'action' => 'backend.elements.actions.voucher.voucher_list'])
    <div class="row">
        <div class="col-xl-12">
            <section class="hk-sec-wrapper">
                <div class="row">
                    <div class="col-sm">
                        <div class="table-wrap">
                            <form action="#" method="post" id="admin-form">
                                @csrf
                                <table id="datable_1" class="table table-hover w-100 display pb-30 js-main-table">
                                    <thead>
                                    <tr>
                                        <th><input type="checkbox" class="checkAll" name="checkAll" value=""></th>
                                        <th>#</th>
                                        <th>Mã giảm giá</th>
                                        <th>Tiêu đề</th>
                                        <th>Giảm giá</th>
                                        <th>% giảm</th>
                                        <th>Ngày tạo</th>
                                        <th>Ngày hết hạn</th>
                                        <th>Đã sử dụng</th>
                                        <th>Bật</th>
                                        <th>Id</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(!empty($vouchers) && count($vouchers) > 0)
                                        @foreach($vouchers as $key => $item)
                                            <tr>
                                                <th><input type="checkbox" class="checkItem" name="cid[]" value="{{ $item->voucher_id }}"></th>
                                                <td>{{ $key + 1 }}</td>
                                                <td><a href="@php echo $urlHelper::admin('voucher', 'edit')."?id=$item->voucher_id" @endphp">{{ $item->voucher_code }}</a></td>
                                                <td><a href="@php echo $urlHelper::admin('voucher', 'edit')."?id=$item->voucher_id" @endphp">{{ $item->voucher_name }}</a></td>
                                                <td>{{ $item->voucher_money }}</td>
                                                <td>{{ $item->voucher_percent }}</td>
                                                <td>{{ $item->voucher_created_at }}</td>
                                                <td>{{ $item->voucher_to }}</td>
                                                <td>{{ $item->order_number_used }}</td>
                                                <td>{{ $item->voucher_status }}</td>
                                                <td>{{ $item->voucher_id }}</td>
                                            </tr>
                                        @endforeach
                                    @endif
                                    </tbody>
                                </table>
                            </form>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
@endsection
