<?php
$title = isset($data['title']) ? $data['title'] : '';
$users = isset($data['users']) ? $data['users'] : '';
?>

@extends('backend.main')

@section('content')
    @include('backend.elements.content_action', ['title' => $title, 'action' => 'backend.elements.actions.voucher.voucher_add'])
    <form method="POST" id="admin-form" action="{{app('UrlHelper')::admin('voucher', 'store')}}" enctype="multipart/form-data">
        <input type="hidden" name="action_type">
        @csrf
        <input type="hidden" name="typeSubmit" value="0">
        <div class="row">
            <div class="col-xl-12">
                <section class="hk-sec-wrapper">
                    <div class="row">
                        <nav id="nav-tabs">
                            <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                <a class="nav-item nav-link active" id="nav-vi-tab" data-toggle="tab" href="#nav-vi" role="tab" aria-controls="nav-vi" aria-selected="true">Thông tin (Tiếng Việt)</a>
{{--                                <a class="nav-item nav-link" id="nav-user-tab" data-toggle="tab" href="#nav-user" role="tab" aria-controls="nav-user" aria-selected="true">Thông tin tặng Voucher</a>--}}
                                <a class="nav-item nav-link" id="nav-upload-tab" data-toggle="tab" href="#nav-upload" role="tab" aria-controls="nav-upload" aria-selected="false">Upload ảnh</a>
                            </div>
                        </nav>
                        <div class="tab-content" id="nav-tabContent">
                            <div class="tab-pane fade show active" id="nav-vi" role="tabpanel" aria-labelledby="nav-vi-tab">
                                <div class="col-xl-12">
                                    <section class="hk-sec-wrapper">
                                        <div class="row">
                                            <div class="col-sm">
                                                <div class="table-wrap">
                                                    <div class="table-responsive">
                                                        <table class="table mb-0">
                                                            <tbody>
                                                                <tr>
                                                                    <th scope="row">Tiêu đề <span class="red">*</span></th>
                                                                    <td><input type="text" name="name" class="form-control form-control-sm" placeholder="Tiêu đề"></td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Mã giảm giá</th>
                                                                    <td><input type="text" name="code" class="form-control form-control-sm" placeholder="Mã giảm giá"></td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Số tiền giảm giá</th>
                                                                    <td><input type="number" name="money" class="form-control form-control-sm" placeholder="Mã giảm giá"></td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Phần trăm giảm giá</th>
                                                                    <td><input type="number" name="percent" max="100" class="form-control form-control-sm" placeholder="% giảm giá"></td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Áp dụng Từ ngày</th>
                                                                    <td><input class="form-control time-statistic" type="text" name="fromTime" value="" /></td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Ngày hết hạn</th>
                                                                    <td><input class="form-control time-statistic" type="text" name="toTime" value="" /></td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Giá trị đơn hàng >=	</th>
                                                                    <td><input type="number" name="minValueOrder" class="form-control form-control-sm" placeholder="Giá trị đơn hàng >="></td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Loại sản phẩm</th>
                                                                    <td>
                                                                        <select class="select2 select2-multiple" name="type[]" multiple="multiple" id="SelectProductType">
                                                                            <option value="" disabled>Chọn loại sản phẩm</option>
                                                                            <option value="shoes">Giày</option>
                                                                            <option value="sandals">Dép</option>
                                                                            <option value="raincoat">Áo Mưa</option>
                                                                            <option value="accessories">Phụ Kiện</option>
                                                                        </select>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Nhóm sản phẩm</th>
                                                                    <td>
                                                                        <select class="select2 select2-multiple" name="pcatId[]" multiple="multiple" id="SelectProductCategory">
                                                                            <option value="" disabled>Chọn nhóm sản phẩm</option>
                                                                        </select>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Sản phẩm</th>
                                                                    <td>
                                                                        <select class="select2 select2-multiple" name="productId[]" multiple="multiple" id="SelectProduct">
                                                                            <option value="" disabled>Chọn sản phẩm</option>
                                                                        </select>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Số đơn hàng áp dụng	</th>
                                                                    <td><input type="number" name="orderNumberApplication" class="form-control form-control-sm" placeholder="Số đơn hàng áp dụng"></td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Đơn hàng đã sử dụng	</th>
                                                                    <td><input type="number" name="orderNumberUsed" class="form-control form-control-sm" placeholder="Đơn hàng đã sử dụng"></td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Số SP tối đa được mua (trên mỗi SP)	</th>
                                                                    <td><input type="number" name="maxProduct" class="form-control form-control-sm" placeholder="Số SP tối đa được mua"></td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Trạng thái</th>
                                                                    <td>
                                                                        <div class="form-check form-check-inline">
                                                                            <input class="form-check-input" type="radio" name="status" value="activated">
                                                                            <label class="form-check-label">Có</label>
                                                                        </div>
                                                                        <div class="form-check form-check-inline">
                                                                            <input class="form-check-input" type="radio" name="status" value="inactive" checked="">
                                                                            <label class="form-check-label">Không</label>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Mô tả ngắn</th>
                                                                    <td><textarea id="editor_short" name="description" class="form-control" placeholder="Nhập mô tả"></textarea></td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                </div>
                            </div>
{{--                            <div class="tab-pane fade" id="nav-user" role="tabpanel" aria-labelledby="nav-user-tab">--}}
{{--                                <div class="col-xl-12">--}}
{{--                                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-list-users">Tặng voucher</button>--}}
{{--                                </div>--}}
{{--                            </div>--}}
                            <div class="tab-pane fade" id="nav-upload" role="tabpanel" aria-labelledby="nav-upload-tab">
                                <div class="col-xl-12">
                                    <section class="hk-sec-wrapper">
                                        <h5 class="hk-sec-title">Ảnh đại diện</h5>
                                        <input type="file" name="imageAvatar" class="dropify" />
                                    </section>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
        @include('backend.voucher.modal-user')
    </form>
@endsection
