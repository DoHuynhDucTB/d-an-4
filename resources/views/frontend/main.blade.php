<?php
$favicon = app('Banner')::query()->where('bangroup_id', 7)->orderBy('banner_id', 'desc')->first();
$iconUrl = config('my.path.image_banner_of_module') . $favicon->avatar->image_name;
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    {!! SEOMeta::generate() !!}
    {!! OpenGraph::generate() !!}
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="{{$iconUrl}}">
    <link rel="icon" href="{{$iconUrl}}" type="image/x-icon">
    @if($agent->isDesktop())
        <meta name="viewport" content="width=device-width, initial-scale=0.0" />
    @else
        <meta name="viewport" content="width=device-width, initial-scale=1" />
    @endif
    @include('frontend.elements.css')
</head>
<body>

@include('frontend.elements.header')
@yield('frontend_content')

@include('frontend.elements.footer')
@include('frontend.elements.javascript')
</body>
</html>
