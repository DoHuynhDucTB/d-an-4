
@extends('frontend.main')
@section('frontend_content')
    <div class="login-register">
        <div class="container">
            <div class="row">
                <p class="title-login-register">{{__('Đăng nhập/Đăng ký thành viên')}}</p>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="box-login-register" style="margin-bottom: 30px">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="left">
                            <h2>{{__('Đăng nhập thành viên')}}</h2>
                            <form action="/login" method="post" id="login">
                                @csrf
                                <input name="email" type="text" placeholder="Địa chỉ email">
                                <input name="password" type="password" placeholder="Vui lòng nhập mập khẩu">

                                <button type="submit">{{__("Đăng nhập")}}</button>
                            </form>

                            <div class="row justify-content-center">
                                <div class="col-6"> <a href="{{ route('social.login.facebook') }}" class="hover-default btn btn-block btn-social btn-facebook"> <span class="fa fa-facebook"></span> Facebook </a> </div>
                                <div class="col-6"> <a href="{{ route('social.login.google') }}" class="hover-default btn btn-block btn-social btn-google"> <span class="fa fa-google"></span> Google </a> </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="right">
                            <h2>{{__('Đăng ký thành viên mới')}}</h2>
                            <p class="title-register">{{__('Đăng ký ngay để mua sắm sễ dàng hơn và tận hưởng thêm nhiều ưu đãi độc quyền cho thành viên Viba')}}</p>
                            <a class="hover-default" href="{{route('register')}}">{{__('Đăng ký thành viên')}}</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
