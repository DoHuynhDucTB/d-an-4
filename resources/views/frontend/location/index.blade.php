@php
/**
 * @var  \App\Models\Banner $banner
*/

$bannerUrl = $banner->avatar  ? config('my.path.image_banner_of_module') . $banner->avatar->image_name : '';

/**
* @var array $countrySelected
 */
$countrySelected = $countrySelected;
/**
* @var array $agencyInfo
 */
$agencyInfo = $agencyInfo;
@endphp

@extends('frontend.main', ['isHome' => true])
@section('frontend_content')
    <div class="main">
        <div class="app">
            <div class="heading" style='background: url("{{$bannerUrl}}") @if($agent->isDesktop()) right top / 52% no-repeat; @else right top / contain no-repeat @endif'>
                <div class="container">
                    <header class="header__content">
                        <h1 class="header__content-slogan">Một nơi-bán</h1>
                        <h5 class="header__content-description">
                            chào bạn, trong thời gian dịch covid-19, Một sẽ liên tục cập
                            nhật tình trạng mở cửa của các điểm bán. để chắc rằng điểm bán
                            bạn muốn đến có mở cửa và sẵn sàng tiếp đón, <b>hãy gọi hotline
                                trước khi ghé bạn nhé!</b></h5>
                        <h5 class="header__content-greeting">mời bạn ghé thăm Một!</h5>
                    </header>
                </div>
            </div>
            <div class="container">
                <div class="location-region">
                    <ul class="location__list" id="location-click">
                        @foreach($countrySelected as $key => $country)
                            @if($key == 1)
                                <li id="location__region-{{$key}}" class="location__item active-location-item"><h4>{{app('Agency')::COUNTRY[$key]}}</h4></li>
                            @else
                                <li id="location__region-{{$key}}" class="location__item"><h4>{{app('Agency')::COUNTRY[$key]}}</h4></li>
                            @endif
                        @endforeach
                    </ul>
                </div>

                @foreach($countrySelected as $key => $country)
                    @php
                        $active = ($key == 1) ? 'active': '';
                        $agencies = $agencyInfo[$key];
                    @endphp
                    <div id="location-detail__content{{$key}}" class="location-detail__items {{$active}}">
                        @foreach($agencies as $agencie)
                            @php
                                $agencieItems = $agencie['agencies'];
                            @endphp
                            <div class="row location-detail">
                                <div class="col-xl-2 col-sm-12 p-0">
                                    <h4 href="#" class="row-location">{{preg_replace('/(Tỉnh|Thành phố)/', '', $agencie['province']->province->name)}}<span class="number-nav">{{count($agencie['agencies'])}}</span></h4>
                                </div>
                                <div class="col-xl-9 col-sm-12 p-0">
                                    <div class="row-location-details" id="row-details__1">
                                        @foreach($agencieItems as $agencieItem)
                                            @php
                                                $shops = $agencieItem->shops
                                            @endphp
                                            @if(count($shops))
                                                <div class="row-location-details__item">
                                                    <h2>{{$agencieItem->getDataByLanguage('agency_name')}}</h2>
                                                    <div class="location-details__item-hide">
                                                        @foreach($shops as $key => $shop)
                                                            @if($key==0)
                                                            <div class="item-hide_content">
                                                                <h5 style="font-weight: 400;">{{$shop->getDataByLanguage('shop_address')}}</h5>
                                                                <h5 class="grey" style="font-weight: 400;"><span class="open-all">Mở cửa hàng ngày</span> {{$shop->getDataByLanguage('shop_time')}} <br /> Hotline {{$shop->getDataByLanguage('shop_hotline')}} </h5>
                                                            </div>
                                                            @else
                                                            <div class="mt-5 item-hide_content">
                                                                <h5 style="font-weight: 400;">{{$shop->getDataByLanguage('shop_address')}}</h5>
                                                                <h5 class="grey" style="font-weight: 400;"><span class="open-all">Mở cửa hàng ngày</span> {{$shop->getDataByLanguage('shop_time')}}<br />Hotline {{$shop->getDataByLanguage('shop_hotline')}}</h5>
                                                            </div>
                                                            @endif
                                                        @endforeach
                                                    </div>
                                                </div>
                                            @endif
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        @endforeach
                        </div>
                @endforeach

            </div>
        </div>
    </div>

@endsection
@section('css_tag')
    @parent
    <link rel="stylesheet" href="{{asset('/public/release2/css/location/base.css' . '?v=' . FRONTEND_CSS_AND_JAVASCRIPT_VERSION)}}" />
    @if($agent->isDesktop())
        <link rel="stylesheet" href="{{asset('/public/release2/css/location/desktop.css' . '?v=' . FRONTEND_CSS_AND_JAVASCRIPT_VERSION)}}" />
    @else
        <link rel="stylesheet" href="{{asset('/public/release2/css/location/mobile.css'. '?v=' . FRONTEND_CSS_AND_JAVASCRIPT_VERSION)}}" />
    @endif
@endsection

@section('javascript_tag')
    @parent
    <script>
        $(document).ready(function () {
            $(".location__item").click(function () {
                $(".location__list")
                    .children()
                    .toArray()
                    .forEach((element) => {
                        if (element.className.includes("active-location-item")) {
                            element.classList.remove("active-location-item");
                            $(this).addClass("active-location-item");
                        }
                    });
            });

            $("#location__region-1").click(function () {
                $(".location-detail__items")
                    .toArray()
                    .forEach((e) => {
                        if (e.className.includes("active")) {
                            e.classList.remove("active");
                        } else {
                            if (e.id.includes("location-detail__content1")) {
                                e.classList.add("active");
                            }
                        }
                    });
            });

            $("#location__region-2").click(function () {
                $(".location-detail__items")
                    .toArray()
                    .forEach((e) => {
                        if (e.className.includes("active")) {
                            e.classList.remove("active");
                        } else {
                            if (e.id.includes("location-detail__content2")) {
                                e.classList.add("active");
                            }
                        }
                    });
            });

            $("#location__region-3").click(function () {
                $(".location-detail__items")
                    .toArray()
                    .forEach((e) => {
                        if (e.className.includes("active")) {
                            e.classList.remove("active");
                        } else {
                            // console.log(e.classList)
                            if (e.id.includes("location-detail__content3")) {
                                e.classList.add("active");
                            }
                        }
                    });
            });

            $(".row-location-details__item").click(function (){
                $(".row-location-details .row-location-details__item > h2").addClass("grey")

                if($(this).hasClass("active")){
                    $(this).removeClass("active")
                    $(".row-location-details .row-location-details__item > h2").removeClass("grey")
                }
                else{
                    $(this).addClass("active")
                    $("h2",this).removeClass("grey")
                }
            });
        });

        var overlayElement = document.getElementById("overlay-element");
        var navSliderElement = document.getElementById("nav-slider-element");
        function ToggleNav() {
            overlayElement.classList.add("nav-active");
            navSliderElement.classList.add("nav-active");
        }
        function CloseNav() {
            overlayElement.classList.remove("nav-active");
            navSliderElement.classList.remove("nav-active");
        }
    </script>
@endsection
