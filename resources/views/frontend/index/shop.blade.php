@extends('frontend.main')
@section('frontend_content')
   @php
      /**
        * @var object $data
        */

       /**
       * @var \App\Models\Product $productCount
        */
      $productCount    = $data['productCount'];
      $productCountArr = \App\Helpers\ArrayHelper::valueAsKey($productCount->toArray(), 'product_type');
      $totalOfProduct = array_sum(array_column($productCountArr, 'count'));

       /**
       * @var array $productCategories
        */
      $productCategories = $data['productCategories'];
      $productColorArr = $data['productColorArr'];
   @endphp
    <div class="header container-left my-5">
        <h1>Một cho mọi người</h1>
        <hr/>
        <div class="menu_top" id="myTopnav">
            <div class="">
                <a class=""><h3>Tất cả <span class="number-nav">{{$totalOfProduct}}</span></h3></a>
                @foreach(app('Product')::PRODUCT_TYPE as $item)
                    <a><h3>{{__($item)}} <span class="number-nav">{{$productCountArr[$item]['count']}}</span></h3></a>
                @endforeach
                <a href="javascript:void(0);" class="icon" onclick="myFunction()">
                    <img src="{{asset('/public/release2/icons/Chevron_down.png')}}" alt=""/>
                </a>
            </div>
            <div class="mr-3">
                <button type="button" class="btn btn-menu" data-toggle="modal" data-target="#modalColor">Lọc<img src="{{asset('/public/release2/icons/plus.png')}}" style="width: 32px; height: 32px"/></button>
            </div>
            @include('frontend.index.shop-child.filter')
        </div>
    </div>
   @php
    $index = 0;
   @endphp
    @foreach($productCategories as $key => $item)
       @php
        $type = explode('-',$key)[0];
        if ($type == 'shoes'){
            $index = $index + 1;
        }
        @endphp
        @if($type == 'shoes')
            @php
                $layoutName = ($index%2 == 1 )  ? 'shoes' : 'shoes-2';
            @endphp
            @include('frontend.index.shop-child.'. $layoutName, ['item' => $item])
        @elseif($type == 'sandals')
            @include('frontend.index.shop-child.sandals', ['item' => $item])
        @elseif($type == 'raincoat')
            @include('frontend.index.shop-child.raincoat', ['item' => $item])
        @elseif($type == 'accessories')
            @include('frontend.index.shop-child.accessories', ['item' => $item])
        @endif
    @endforeach
@endsection

@section('css_tag')
    @parent
    <link rel="stylesheet" href="{{asset('/public/release2/css/shop/range.css'. '?v=' . FRONTEND_CSS_AND_JAVASCRIPT_VERSION)}}" />
    @if($agent->isDesktop())
        <link rel="stylesheet" href="{{asset('/public/release2/css/shop/desktop.css'. '?v=' . FRONTEND_CSS_AND_JAVASCRIPT_VERSION)}}" />
    @else
        <link rel="stylesheet" href="{{asset('/public/release2/css/shop/mobile.css'. '?v=' . FRONTEND_CSS_AND_JAVASCRIPT_VERSION)}}" />
    @endif
@endsection
@section('javascript_tag')
    @parent
    <script>
        $(document).ready(function () {
            $(".btn-size").click(function () {
                $("button").removeClass("active js-filter-size2");
                $(this).addClass("active js-filter-size2");
            });

            $(".actived-color").click(function () {
                var product = $(this).data('product');
                $("button[data-product='" +product+"']").removeClass("active-circle js-filter-color");
                $(this).addClass("active-circle");

                var blockParent = $(this).parent().parent().parent();
                blockParent.children()[0].src = $(this).data('avatar');
                $(".js-price-variant[data-product='" +product+"']").html($(this).data('price'));
            });


            if($(".noUi-origin.noUi-connect")[0].attributes.style !== undefined){
                console.log($(".noUi-origin.noUi-connect")[0].attributes)
            }
            else{
                console.log($(".noUi-origin.noUi-connect"))
            }


            // buy now
            $('button.js-buy-now').click(function () {
                window.location.replace($(this).data('href'));
            });
        });

        function handleColor() {
            var x = document.getElementById("colors");
            if (x.style.display === "none") {
                x.style.display = "block";
                document.getElementById("btn-plus-1").style.display = "none";
                document.getElementById("btn-minus-1").style.display = "block";
            } else {
                x.style.display = "none";
                document.getElementById("btn-plus-1").style.display = "block";
                document.getElementById("btn-minus-1").style.display = "none";
            }
        }
        function handlePrice() {
            var x = document.getElementById("price");
            if (x.style.display === "none") {
                x.style.display = "block";
                document.getElementById("btn-plus-2").style.display = "none";
                document.getElementById("btn-minus-2").style.display = "block";
            } else {
                x.style.display = "none";
                document.getElementById("btn-plus-2").style.display = "block";
                document.getElementById("btn-minus-2").style.display = "none";
            }
        }

        function handleSize() {
            var x = document.getElementById("size");
            if (x.style.display === "none") {
                x.style.display = "block";
                document.getElementById("btn-plus-3").style.display = "none";
                document.getElementById("btn-minus-3").style.display = "block";
            } else {
                x.style.display = "none";
                document.getElementById("btn-plus-3").style.display = "block";
                document.getElementById("btn-minus-3").style.display = "none";
            }
        }

        function myFunction() {
            var x = document.getElementById("myTopnav");
            if (x.className === "menu_top") {
                x.className += " responsive";
            } else {
                x.className = "menu_top";
            }
        }
    </script>
@endsection
