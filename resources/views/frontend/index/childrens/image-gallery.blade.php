<?php
/**
 * @var \App\Models\Banner $bannerModel
 */
$bannerModel = app('Banner');
$banners = $bannerModel::query()
    ->join(BANNER_GROUP_TBL, BANNER_GROUP_TBL . '.bangroup_id', BANNER_TBL . '.bangroup_id')
    ->leftJoin(IMAGE_TBL, '3rd_key', 'banner_id')
    ->where(['3rd_type' => 'banner', 'image_value' => config('my.image.value.banner.avatar'), 'image_status' => 'activated'])
    ->where([BANNER_GROUP_TBL .'.bangroup_id' => 3])
    ->isActivated()
    ->limit(20)
    ->orderBy('banner_sorted', 'asc')
    ->get();
?>
@if($banners->count())
<div class="container">
    <div class="main__insta mb-132">
        <div class="main__insta-content">
            <div class="main__insta-heading">
                <span class="main__insta-icon"><img src="/public/release2/icons/ins-black.png"/></span>
                <span class="main__insta-hastag">@Motdoigiay</span>
            </div>
            <div class="slider slider-01 owl-carousel owl-theme">
                @foreach($banners as $banner)
                    @php
                        $bannerUrl = @$banner->image_name ? config('my.path.image_banner_of_module') . @$banner->image_name : '';
                    @endphp
                    <div class="item">
                        <a href="{{asset($banner->url)}}"><img data-src="{{asset($bannerUrl) . '?v='}} @php echo time() @endphp" class="js-lazy-loading" alt="" style="width: auto"/></a>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</div>
@endif


