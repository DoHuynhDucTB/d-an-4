<?php
/**
 * @var \App\Models\Banner $bannerModel
 */
$bannerModel = app('Banner');
$productBanners = $bannerModel::query()
    ->join(BANNER_GROUP_TBL, BANNER_GROUP_TBL . '.bangroup_id', BANNER_TBL . '.bangroup_id')
    ->leftJoin(IMAGE_TBL, '3rd_key', 'banner_id')
    ->where(['3rd_type' => 'banner', 'image_value' => config('my.image.value.banner.avatar'), 'image_status' => 'activated'])
    ->where([BANNER_GROUP_TBL .'.bangroup_id' => 2])
    ->isActivated()
    ->limit(10)
    ->orderBy('banner_sorted', 'asc')
    ->get();
$productBannerCount = $productBanners->count();
$chunkNumber = 0;
if ($productBannerCount > 0) {
    $chunkNumber = intval($productBannerCount / 2);

}
?>
@if($productBanners->count())
<div class="container">
    <div class="main__product mb-3">
        <div class="row m-0">
            <h4 class="main__product-title bold">{{app('Config')->getConfig('xem-het-moi-thu')}}</h4>
        </div>
        @if($agent->isDesktop())
        <div class="row m-0 w-100 hide-on-tablet hide-on-mobile show-on-pc">
            @foreach($productBanners->chunk($chunkNumber) as $key => $bannerChunk)
                @if($key == 0)
                    <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-left p-0">
                        @foreach($bannerChunk as $banner)
                            @php
                                $avatar  = $banner->avatar ? config('my.path.image_banner_of_module') . $banner->avatar->image_name : '';
                            @endphp
                            <div class="main__product-item">
                                <div class="main__product-img">
                                    <a href="{{$banner->banner_url}}"><img data-src="{{asset($avatar)}}" class="js-lazy-loading" alt="" /></a>
                                </div>
                                <div class="main__product-description">
                                    <h5 class="main__product-description-sm">{{$banner->getDataByLanguage('banner_name')}}</h5>
                                    <h3 class="main__product-description-large">{!! $banner->getDataByLanguage('banner_description')  !!}</h3>
                                </div>
                            </div>
                        @endforeach
                    </div>
                @else
                    <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-right p-0 main__product-mt">
                        @foreach($bannerChunk as $banner)
                            @php
                                $avatar  = $banner->avatar ? config('my.path.image_banner_of_module') . $banner->avatar->image_name : '';
                            @endphp
                            <div class="main__product-item mr-0">
                                <div class="main__product-img">
                                    <a href="{{$banner->banner_url}}"><img data-src="{{asset($avatar)}}" class="js-lazy-loading" alt="" /></a>
                                </div>
                                <div class="main__product-description">
                                    <h5 class="main__product-description-sm">{{$banner->getDataByLanguage('banner_name')}}</h5>
                                    <h3 class="main__product-description-large">{!! $banner->getDataByLanguage('banner_description')  !!}</h3>
                                </div>
                            </div>
                        @endforeach
                    </div>
                @endif
            @endforeach
        </div>
        @else
            <div class="row m-0 w-100 hide-on-pc">
                @foreach($productBanners->chunk($chunkNumber) as $key => $bannerChunk)
                    @if($key == 0)
                        <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-left p-0">
                            @foreach($bannerChunk as $banner)
                                @php
                                    $avatar  = $banner->avatar ? config('my.path.image_banner_of_module') . $banner->avatar->image_name : '';
                                @endphp
                                <div class="main__product-item">
                                    <div class="main__product-img">
                                        <a href="{{$banner->banner_url}}"><img data-src="{{asset($avatar)}}" class="js-lazy-loading" alt="" /></a>
                                    </div>
                                    <div class="main__product-description">
                                        <h5 class="main__product-description-sm">{{$banner->getDataByLanguage('banner_name')}}</h5>
                                        <h3 class="main__product-description-large">{!! $banner->getDataByLanguage('banner_description')  !!}</h3>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    @else
                        <div class="col-xl-5 col-lg-6 col-md-12 col-sm-12 col-right p-0 main__product-mt">
                            @foreach($bannerChunk as $banner)
                                @php
                                    $avatar  = $banner->avatar ? config('my.path.image_banner_of_module') . $banner->avatar->image_name : '';
                                @endphp
                                <div class="main__product-item">
                                    <div class="main__product-img">
                                        <a href="{{$banner->banner_url}}"><img data-src="{{asset($avatar)}}" class="js-lazy-loading" alt="" /></a>
                                    </div>
                                    <div class="main__product-description">
                                        <h5 class="main__product-description-sm">{{$banner->getDataByLanguage('banner_name')}}</h5>
                                        <h3 class="main__product-description-large">{!! $banner->getDataByLanguage('banner_description')  !!}</h3>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    @endif
                @endforeach
            </div>
        @endif
    </div>
</div>
@endif
