<?php
/**
 * @var object $agent
 */
$bannerId = ($agent->isDesktop() == true) ? 42 : 48;
/**
 * @var \App\Models\Banner $bannerModel
 */
$bannerModel = app('Banner');
$banner = $bannerModel::query()
    //->join(BANNER_GROUP_TBL, BANNER_GROUP_TBL . '.bangroup_id', BANNER_TBL . '.bangroup_id')
    ->leftJoin(IMAGE_TBL, '3rd_key', 'banner_id')
    //->where([BANNER_GROUP_TBL .'.bangroup_id' => 4])
    ->where(['3rd_type' => 'banner', 'image_value' => config('my.image.value.banner.avatar'), 'image_status' => 'activated'])
    ->isActivated()
    ->orderBy('banner_sorted', 'asc')
    ->where('banner_id', $bannerId)
    ->first();
$bannerUrl = @$banner->image_name ? config('my.path.image_banner_of_module') . @$banner->image_name : '';
?>
@if($banner)
<div class="container">
    <div class="main__content">
        <div class="row p-0 m-0">
            <div class="main__content-story">
                <h3 class="main__content-preface m-0">
                    {!! $banner->getDataByLanguage('banner_description')  !!}
                </h3>
                <button class="main__content-btn btn button-conf mb-132">
                    <h5><a style="color: black" href="{{asset($banner->banner_url)}}">{{$banner->getDataByLanguage('banner_name')}}</a></h5>
                </button>
                <div class="main__content-slogan">
                    <h1 class="main__content-slogan-text"><img id="img-slogan" data-src="{{asset($bannerUrl)}}" class="js-lazy-loading"></h1>
                    {{--@if($agent->isDesktop())
                    <h1 class="main__content-slogan-text"><img data-src="{{asset($bannerUrl)}}" class="js-lazy-loading"></h1>
                    @else
                    <h1 class="main__content-slogan-text">{{__('Một cho tất cả')}}</h1>
                    @endif--}}
                </div>
            </div>
        </div>
    </div>
</div>
@endif
