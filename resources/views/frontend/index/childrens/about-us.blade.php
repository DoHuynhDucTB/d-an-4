<?php
/**
 * @var \App\Models\PerceivedValue $perceivedvalueModel
 */
$perceivedvalueModel = app('PerceivedValue');
$perceivedvalues = $perceivedvalueModel::query()->with('avatar')->orderBy('pervalue_sorted', 'asc')->get();
?>
<div class="container">
    <div class="main__about mb-132">
        <div class="main__about-navigation">
            <div class="main__about-sidebar">
                <div class="row m-0 mb-5">
                    <div class="col-xl-4 col-md-5 col-sm-6 col-5 p-0">
                        <div class="main__about-title">
                            <h4>Nói gì về Một ?</h4>
                        </div>
                    </div>

                    <div class="col-xl-8 col-md-7 col-sm-6 col-7 p-0">
                        <ul class="main__about-list mb-0">
                            @foreach($perceivedvalues as $key => $item)
                                @php
                                    $avatarUrl = @$item->avatar->image_name ? config('my.path.image_perceivedvalue_of_module') . $item->avatar->image_name : '';
                                @endphp
                                <li id="main_about_icon0{{$key+1}}" data-content="{{$item->getDataByLanguage('pervalue_description')}}" data-author="{{$item->pervalue_fullname}}" class="main__about-item @if($key == 0) main__about-item--active @endif" style="cursor: pointer">
                                    <img src="{{$avatarUrl}}" alt="" class="main__about-icon"/>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xl-4 col-md-5 col-sm-0"></div>
                    <div class="col-xl-8 col-md-7 col-sm-12">
                        <div id="main_about_text" class="item-about active">
                            <h3 class="main__about-content px-auto">
                                {{$perceivedvalues->first()->getDataByLanguage('pervalue_description')}}
                            </h3>
                            <span class="main__about-readmore">
                                - Read more on
                                <a href="" class="main__about-link">{{$perceivedvalues->first()->pervalue_fullname}}</a>
                              </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
