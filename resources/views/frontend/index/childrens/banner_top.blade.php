<?php
/**
 * @var \App\Models\Banner $bannerModel
 */
$bannerModel = app('Banner');
$topBanners = $bannerModel::query()
    ->join(BANNER_GROUP_TBL, BANNER_GROUP_TBL . '.bangroup_id', BANNER_TBL . '.bangroup_id')
    ->leftJoin(IMAGE_TBL, '3rd_key', 'banner_id')
    ->where(['3rd_type' => 'banner', 'image_value' => config('my.image.value.banner.avatar'), 'image_status' => 'activated'])
    ->where(['bangroup_code' => 'homepage_top'])
    ->isActivated()
    ->limit(10)
    ->orderBy('banner_sorted', 'asc')
    ->get();
$bannerCount = $topBanners->count();
?>
@if($bannerCount)
    <div id="myCarouse2" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->
        <div class="container">
            <ul class="carousel-indicators">
                @for($i = 0; $i <= $bannerCount - 1; $i++)
                    <li data-target="#myCarouse2"data-slide-to="{{$i}}"class="@if($i == 0) active @endif"></li>
                @endfor
            </ul>
        </div>

        <!-- The slideshow -->
        <div class="carousel-inner">
            @foreach($topBanners as $key => $banner)
                @php
                    $bannerUrl = @$banner->image_name ? config('my.path.image_banner_of_module') . $banner->image_name : '';
                @endphp
                <div class="carousel-item @if($key == 0) active @endif">
                    <div class="heading heading-bg-main" style="
                  background: linear-gradient(
                      180deg,
                      rgba(0, 0, 0, 0.4),
                      rgba(0, 0, 0, 0)
                    ),
                    url({{asset($bannerUrl)}}) @if(!$agent->isDesktop()) !important; @endif;
                ">
                        <div class="container">
                            <header class="header__content">
                                <h1 class="header__content-heading">{{$banner->getDataByLanguage('banner_name')}}</h1>
                                <h4 class="header__content-description">
                                    {!! $banner->getDataByLanguage('banner_description') !!}
                                </h4>
                                <button class="header__content-btn btn button-conf">
                                    <h5><a style="color: black" href="{{asset($banner->banner_url)}}">{{__('Mua Liền')}}</a></h5>
                                </button>
                            </header>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>

@endif
