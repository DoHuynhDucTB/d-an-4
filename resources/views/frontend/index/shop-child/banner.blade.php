@php
    $bannerAvatarUrl = $banner->avatar ? config('my.path.image_banner_of_module') . $banner->avatar->image_name : '';
@endphp

<div class="section-without-container section-two" style="background-color: {{$banner->banner_color_hex}}">
    <div class="row container">
        <div class="col-one col col-xl-8 col-mb-6 col-sm-12">
            <h2 class="title h2-special">
                {!! $banner->getDataByLanguage('banner_name') !!}
            </h2>
            <div class="image">
                <img src="{{asset($bannerAvatarUrl)}}" style="max-width: 480px" class="img" />
            </div>
        </div>

        <div class="col-two col col-xl-4 col-mb-6 col-sm-12">
            <div class="content">
                <h5>
                    {!! $banner->getDataByLanguage('banner_description') !!}
                </h5>
                <button data-href="{{asset($banner->banner_url)}}" class="btn-shop js-buy-now"><h5>mua liền !</h5></button>
            </div>
        </div>
    </div>
</div>
