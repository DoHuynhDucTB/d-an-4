@php
    /**
    * @var array $data
     */
    $productColors = $data['productColors'];
    $productSizes = $data['productSizes'];
    $selectedColors = $data['selectedColors'];
@endphp

<div class="modal fade" id="modalColor" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="close-modal">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <img src="{{asset('/public/release2/icons/Close.png')}}"/>
                </button>
            </div>
            <div class="modal-body">
                <h2>lọc</h2>
                <div class="my-content-modal d-flex justify-content-between">
                    <div class="col-one">
                        <hr />
                        <div class="item-toggle" style="margin-bottom: 1em;margin-top: .5em;">
                            <h4 style="margin-bottom: 0px">màu</h4>
                            <button class="btn-toggle" onclick="handleColor()">
                                <img src="{{asset('/public/release2/icons/plus.png')}}" id="btn-plus-1"/>
                                <img src="{{asset('/public/release2/icons/minus.png')}}" id="btn-minus-1"/>
                            </button>
                        </div>
                        <div id="colors">
                            <div class="btn-colors">
                                @foreach($productColors as $color)
                                <button data-color="{{$color->pcolor_id}}" data-href="{{route('shop', ['color' => $color->pcolor_id ])}}" class="js-filter-color color-green actived-color @if(in_array($color->pcolor_id, $selectedColors)) active-circle active-circle-2 @endif" style="background-color: {{$color->pcolor_hex}}"></button>
                                @endforeach
                            </div>
                        </div>
                        <hr />
                        <div class="item-toggle" style="margin-bottom: 1em;margin-top: .5em;">
                            <h4>giá <span>(ngàn VNĐ)</span></h4>
                            <button class="btn-toggle" onclick="handlePrice()">
                                <img src="{{asset('/public/release2/icons/plus.png')}}" id="btn-plus-2"/>
                                <img src="{{asset('/public/release2/icons/minus.png')}}" alt="" id="btn-minus-2"/>
                            </button>
                        </div>
                        <div id="price">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div id="slider-range"></div>
                                </div>
                            </div>
                            <div class="row slider-labels justify-content-between">
                                <div class="col-xs-6 caption">
                                    <h5 id="slider-range-value1"></h5>
                                </div>
                                <div class="col-xs-6 text-right caption">
                                    <h5 id="slider-range-value2"></h5>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <form>
                                        <input type="hidden" name="min-value" value="0" />
                                        <input type="hidden" name="max-value" value="" />
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-two">
                        <hr />
                        <div style="position: relative">
                            <div class="item-toggle mt-3">
                                <h4 style="margin-bottom: 0px">size</h4>
                                <button class="btn-toggle" onclick="handleSize()">
                                    <img src="{{asset('/public/release2/icons/plus.png')}}" id="btn-plus-3"/>
                                    <img src="{{asset('/public/release2/icons/minus.png')}}" id="btn-minus-3"/>
                                </button>
                            </div>
                            <span class="d-flex align-items-center">
                                            <img src="{{asset('/public/release2/icons/Ruler.png')}}" alt="" />
                                            <h5 class="bold m-0 ml-3">đo size làm sao?</h5>
                                        </span>
                        </div>
                        <div id="size">
                            <div class="item">
                                <h5>giày nữ</h5>
                                <div class="size d-flex">
                                    @foreach($productSizes as $size)
                                        @if(is_numeric($size->psize_code) && in_array($size->psize_code, app('ProductSize')::SIZE_TYPE['female']))
                                        <button data-size="{{$size->psize_id}}" data-sex="female" class="btn-size js-filter-size @if($size->psize_id == request()->get('size') && request()->get('sex') == 'female') active @endif" data-href="{{route('shop', ['size' => $size->psize_id, 'sex' => 'female' ])}}">
                                            <h5 class="bold mb-0" data-size="{{$size->psize_id}}">{{$size->psize_code}}</h5>
                                        </button>
                                        @endif
                                    @endforeach
                                </div>
                            </div>
                            <div class="item">
                                <h5>giày nam</h5>
                                <div class="size d-flex">
                                    @foreach($productSizes as $size)
                                        @if(is_numeric($size->psize_code) && in_array($size->psize_code, app('ProductSize')::SIZE_TYPE['male']))
                                            <button data-size="{{$size->psize_id}}" data-sex="male" class="btn-size js-filter-size @if($size->psize_id == request()->get('size') && request()->get('sex') == 'male') active @endif" data-href="{{route('shop', ['size' => $size->psize_id, 'sex' => 'male' ])}}">
                                                <h5 class="bold mb-0" data-size="{{$size->psize_id}}">{{$size->psize_code}}</h5>
                                            </button>
                                        @endif
                                    @endforeach
                                </div>
                            </div>
                            <div class="item">
                                <h5>size unisex</h5>
                                <div class="size d-flex">
                                    @php
                                    $hasKids  = false;
                                    $kidsInfo = [];
                                    @endphp
                                    @foreach($productSizes as $size)
                                        @if(!is_numeric($size->psize_code) && strtolower($size->psize_code) != 'kids')
                                            <button data-size="{{$size->psize_id}}" data-sex="unisex" class="btn-size js-filter-size @if($size->psize_id == request()->get('size') && request()->get('sex') == 'unisex') active @endif" data-href="{{route('shop', ['size' => $size->psize_id, 'sex' => 'unisex' ])}}">
                                                <h5 class="bold mb-0" data-size="{{$size->psize_id}}">{{$size->psize_code}}</h5>
                                            </button>
                                        @elseif(!is_numeric($size->psize_code))
                                            @php
                                                $kidsInfo = ['id' => $size->psize_id, 'code' => $size->psize_code];
                                                $hasKids = true;
                                            @endphp
                                        @endif
                                    @endforeach

                                    @if($hasKids == true)
                                        <button data-size="{{$kidsInfo['id']}}" data-sex="unisex" class="btn-size js-filter-size @if($kidsInfo['id'] == request()->get('size') && request()->get('sex') == 'unisex') active @endif" data-href="{{route('shop', ['size' => $kidsInfo['id'], 'sex' => 'unisex' ])}}">
                                            <h5 class="bold mb-0" data-size="{{$kidsInfo['id']}}">{{$kidsInfo['code']}}</h5>
                                        </button>
                                    @endif

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@section('javascript_tag')
    @parent
    <script>
        $(document).ready(function () {
            $(".actived-color").click(function () {
                if ($(this).hasClass('active-circle-2')) {
                    $(this).removeClass("active-circle active-circle-2");
                    return true;
                } else {
                    $(this).addClass("active-circle-2");
                }

                var colors = $('.js-filter-color');
                var countColorActive = 0;
                for (i = 0; i < colors.length; i++) {
                    var color = colors[i];
                    if ($(color).hasClass('active-circle')){
                        countColorActive = countColorActive + 1;
                    }
                }

                if (countColorActive > 4) {
                    $(this).removeClass('active-circle');
                }
            });

            $('.close').click(function (){
                var search = '';

                var colors = $("button.active-circle-2");
                var colorArr = [];
                for (i = 0; i < colors.length; i++) {
                    var color = colors[i];
                    colorArr.push($(color).data('color'));
                }
                if (colorArr) {
                    search += '&color=' + colorArr.join('-');
                }

                var size = $(".js-filter-size2")[0];
                if (size) {
                    var sizeID = size.dataset.size;
                    var sex = size.dataset.sex;
                    search += '&size=' + sizeID + '&sex=' + sex;
                }

                var maxPrice = $('#slider-range-value2').html();
                var minPrice = $('#slider-range-value1').html();
                search += '&min-price=' + minPrice.replace(',','') + '&max-price=' + maxPrice.replace(',','');

                window.location.replace('/shop.html?' + search);
            });
/*            $(".js-filter-color").click(function () {
                window.location.replace($(this).data('href'));
            });
            $(".js-filter-size").click(function () {
                window.location.replace($(this).data('href'));
            });

            $('.noUi-handle-upper').mouseup(function () {
                var maxPrice = $('#slider-range-value2').html();
                var minPrice = $('#slider-range-value1').html();
                window.location.replace( '/shop.html?min-price=' + minPrice.replace(',','') + '&max-price=' + maxPrice.replace(',','') );
            });
            $('.noUi-handle-lower').mouseup(function () {
                var maxPrice = $('#slider-range-value2').html();
                var minPrice = $('#slider-range-value1').html();
                window.location.replace( '/shop.html?min-price=' + minPrice.replace(',','') + '&max-price=' + maxPrice.replace(',','') );
            });*/
        });
    </script>

@endsection
