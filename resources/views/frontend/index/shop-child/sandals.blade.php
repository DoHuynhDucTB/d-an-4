@php
    $category = $item['product-category'];
    $bannerFeature =$category->bannerFeature;
    $products = $item['product'];
    $categoryAvatarUrl = $category->avatar ? config('my.path.image_product_cate_avatar_of_module') . $category->avatar->image_name : '';
@endphp
<div class="section section-five container">
      <div class="row justify-content-between">
        <div class="col-one col col-xl-6 col-mb-6 col-sm-12" style="background-color: {{$category->pcat_color_hex}}">
          <div class="custom-card-master">
            <div class="text">
                <h2>{{$category->getDataByLanguage('pcat_name')}}</h2>
                <h5>{{$category->getDataByLanguage('pcat_description')}}</h5>
            </div>

              <img data-src="{{$categoryAvatarUrl}}" class="js-lazy-loading"/>
            {{--<div class="box stack-top" style="background-color: {{$category->pcat_color_hex}}"></div>--}}
          </div>
        </div>
        <div class="col-two col col-xl-6 col-mb-6 col-sm-12">
            @foreach($products as $product)
                @php
                    /**
                    * @var object $product
                     */
                    $variants = $product->variants;
                    $colorHtml = '';
                    $priceDefault = 0;
                    $variantDefaultAvatarUrl = '';
                @endphp
                @if($variants->count() < 1)
                    @continue
                @else
                    @foreach($variants as $variant)
                        @php
                            $variantAvatarUrl = $variant->avatar ? config('my.path.image_product_variant_avatar_of_module') . $variant->avatar->image_name : '';
                            $price = $variant->pvariant_new_price > 0  ? $variant->pvariant_new_price : $variant->pvariant_price - $variant->pvariant_discount;
                            $price = app('ProductHelper')::formatMoney($price);
                        @endphp
                        @if($product->color_default_id == $variant->pvariant_color)
                            @php
                                $variantDefaultAvatarUrl = $variantAvatarUrl;
                                /**
                               * @var object $variant
                               */
                                $colorHtml .= '<button data-product="'.$product->product_id.'" data-price="'.$price.'" data-avatar="'.$variantAvatarUrl.'" class="actived-color active-circle" style="background-color:'.$productColorArr[$variant->pvariant_color]['pcolor_hex'].'"></button>';
                                $priceDefault = $price;
                            @endphp
                        @else
                            @php
                                $colorHtml .= '<button data-product="'.$product->product_id.'" data-price="'.$price.'" data-avatar="'.$variantAvatarUrl.'" class="actived-color" style="background-color:'.$productColorArr[$variant->pvariant_color]['pcolor_hex'].'"></button>';
                            @endphp
                        @endif
                    @endforeach
                @endif
            @endforeach
          <div class="item">
            <div class="content">
                <img data-src="{{asset($variantDefaultAvatarUrl)}}" class="js-lazy-loading js-img-variant" />
              <div class="container-text-color">
                <div class="text">
                    <h5 class="description">{{$product->getDataByLanguage('product_name')}}</h5>
                    <h5 class="money js-price-variant" data-product="{{$product->product_id}}">{{$priceDefault}}</h5>
                </div>
                <div class="btn-colors">
                    {!! $colorHtml !!}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
@if($bannerFeature)
    @include('frontend.index.shop-child.banner', ['banner' => $bannerFeature])
@endif
