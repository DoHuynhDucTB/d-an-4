@php
    $category = $item['product-category'];
    $bannerFeature =$category->bannerFeature;
    $products = $item['product'];
    $categoryAvatarUrl = $category->avatar ? config('my.path.image_product_cate_avatar_of_module') . $category->avatar->image_name : '';
@endphp
@if($agent->isDesktop())
<div class="section section-six container">
    <div class="row justify-content-between">
        <div class="col-two col col-xl-6 col-mb-6 col-sm-12">
            @foreach($products as $product)
                @php
                    /**
                    * @var object $product
                     */
                    $variants = $product->variants;
                @endphp
                @if($variants->count() < 1)
                    @continue
                @else
                    @foreach($variants as $variant)
                        @php
                            $variantAvatarUrl = $variant->avatar ? config('my.path.image_product_variant_avatar_of_module') . $variant->avatar->image_name : '';
                            $price = $variant->pvariant_new_price > 0  ? $variant->pvariant_new_price : $variant->pvariant_price - $variant->pvariant_discount;
                            $price = app('ProductHelper')::formatMoney($price);
                        @endphp
                        <div class="item">
                            <div class="content">
                                <img data-src="{{asset($variantAvatarUrl)}}" class="js-lazy-loading">
                                <div class="container-text-color">
                                    <div class="text">
                                        <h5 class="description">{{$variant->getDataByLanguage('pvariant_name')}}</h5>
                                        <h5 class="money">{{$price}}</h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @endif
            @endforeach
        </div>
        <div class="col-one col col-xl-6 col-mb-6 col-sm-12"  style="background-color: {{$category->pcat_color_hex}}">
            <div class="custom-card-master">
                <div class="text">
                    <h2>{{$category->getDataByLanguage('pcat_name')}}</h2>
                    <h5>{{$category->getDataByLanguage('pcat_description')}}</h5>
                </div>
                <img data-src="{{asset($categoryAvatarUrl)}}" class="js-lazy-loading">
            </div>
        </div>
    </div>
</div>
@else
    <div class="section section-six container">
        <div class="row justify-content-between">
            <div class="col-one col col-xl-6 col-mb-6 col-sm-12"  style="background-color: {{$category->pcat_color_hex}}">
                <div class="custom-card-master">
                    <div class="text">
                        <h2>{{$category->getDataByLanguage('pcat_name')}}</h2>
                        <h5>{{$category->getDataByLanguage('pcat_description')}}</h5>
                    </div>
                    <img data-src="{{asset($categoryAvatarUrl)}}" class="js-lazy-loading">
                </div>
            </div>
            <div class="col-two col col-xl-6 col-mb-6 col-sm-12">
                @foreach($products as $product)
                    @php
                        /**
                        * @var object $product
                         */
                        $variants = $product->variants;
                    @endphp
                    @if($variants->count() < 1)
                        @continue
                    @else
                        @foreach($variants as $variant)
                            @php
                                $variantAvatarUrl = $variant->avatar ? config('my.path.image_product_variant_avatar_of_module') . $variant->avatar->image_name : '';
                                $price = $variant->pvariant_new_price > 0  ? $variant->pvariant_new_price : $variant->pvariant_price - $variant->pvariant_discount;
                                $price = app('ProductHelper')::formatMoney($price);
                            @endphp
                            <div class="item">
                                <div class="content">
                                    <img data-src="{{asset($variantAvatarUrl)}}" class="js-lazy-loading">
                                    <div class="container-text-color">
                                        <div class="text">
                                            <h5 class="description">{{$variant->getDataByLanguage('pvariant_name')}}</h5>
                                            <h5 class="money">{{$price}}</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    @endif
                @endforeach
            </div>
        </div>
    </div>
@endif

@if($bannerFeature)
    @include('frontend.index.shop-child.banner', ['banner' => $bannerFeature])
@endif
