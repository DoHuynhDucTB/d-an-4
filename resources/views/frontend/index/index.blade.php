@extends('frontend.main', ['isHome' => true])
@section('frontend_content')
    <div class="main">
        <div class="app">
            @include('frontend.index.childrens.banner_top')
            @include('frontend.index.childrens.product')
            @include('frontend.index.childrens.one-for-all')
            @include('frontend.index.childrens.image-gallery')
            @include('frontend.index.childrens.about-us')
        </div>
    </div>
    <div id="overlay-cookie" class="cookie-overlay"></div>
    <div class="cookie" id="cookie-element">
        <div class="cookie-img">
            <img src="/public/release2/icons/cookies.png" alt="" />
        </div>
        <div class="cookie-text">
            <h5>Vô website này là bạn đã đồng ý để chúng tôi xài bánh quy.</h5>
            <h5>
                chúng tôi xài bánh quy để đảm bảo mang đến cho bạn một trải nghiệm
                tuyệt vời cũng như để website chạy mượt mà nhất có thể.
            </h5>
        </div>
        <div class="cookie-close-btn" onclick="CloseCookie()">
            <img src="/public/release2/icons/closecookie.svg" alt="" />
        </div>
    </div>
    <div id="email-element" class="your-email">
        <div class="your-email-main">Nói gì về Một ?
            <h4>giữ liên lạc nhé?</h4>
            <h5>điền địa chỉ e-mail để nhận mọi tin tức và quà tặng từ Một</h5>
            <input
                type="email"
                id="your-email"
                name="email"
                placeholder="email của bạn"
            />
            <span class="email-enter-input">
          <img src="/public/release2/icons/arowrightbig.svg" alt="" />
        </span>
        </div>

        <div class="email-close-btn" onclick="CloseEmail()">
            <img src="/public/release2/icons/closeemail.svg" alt="" />
        </div>
    </div>
@endsection
@section('css_tag')
    @parent
    @if($agent->isDesktop())
        <link rel="stylesheet" href="{{asset('/public/release2/css/home/desktop.css' . '?v=' . FRONTEND_CSS_AND_JAVASCRIPT_VERSION)}}" />
    @else
        <link rel="stylesheet" href="{{asset('/public/release2/css/home/mobile.css'. '?v=' . FRONTEND_CSS_AND_JAVASCRIPT_VERSION)}}" />
    @endif
@endsection

@section('javascript_tag')
    @parent
    <script>
        $(document).ready(function () {
            $(".main__about-item").click(function () {
                $(".main__about-list")
                    .children()
                    .toArray()
                    .forEach((element) => {
                        if (element.className.includes("main__about-item--active")) {
                            element.classList.remove("main__about-item--active");
                            $(this).addClass("main__about-item--active");
                        }
                    });
                $('h3.main__about-content').html($(this).data('content'));
                $('a.main__about-link').html($(this).data('author'));
            });
        });

        $(".slider-01").owlCarousel({
            loop: true,
            margin: 20,
            responsiveClass: true,
            autoWidth:true,
            dots: false,
            nav: false,

            responsive: {
                0: {
                    items: 1,
                    loop: true,
                },
                600: {
                    items: 1,
                    loop: true,
                },
                1000: {
                    items: 3,
                    loop: true,
                },
            },
        });

        function CloseCookie() {
            var cookieElement = document.getElementById("cookie-element");
            var cookieOverlay = document.getElementById("overlay-cookie");
            cookieElement.classList.add("pop-up-close");
            cookieOverlay.classList.add("pop-up-close");
        }

        function CloseEmail() {
            var emailElement = document.getElementById("email-element");
            emailElement.classList.add("pop-up-close");
        }
    </script>
@endsection
