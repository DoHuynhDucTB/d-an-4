<?php
$provinces      = $data['provinces'];
$districts      = $data['districts'];
$wards          = $data['wards'];
$districts2     = $data['districts2'];
$wards2         = $data['wards2'];
$productSkins   = $data['productSkins'];
$productCares   = $data['productCares'];
$productMakeups = $data['productMakeups'];
$user       = auth()->guard('web')->user();
$skins      = array_filter(explode(',', $user->pskin_ids));
$cares      = array_filter(explode(',', $user->pcare_ids));
$makeups    = array_filter(explode(',', $user->pmakeup_ids));
?>

@extends('frontend.main')
@section('frontend_content')
    <div class="form-registry">
        <form action="{{route('updateUser')}}" method="post">
            @csrf
            <div class="container">
                <div class="row">
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade active show" id="info" role="tabpanel" aria-labelledby="info-tab">
                            <h3 style="text-transform: uppercase;">{{__('Thông tin tài khoản')}}</h3>
                            <div class="row">
                                <div class="col-lg-3">
                                    <p>{{__('Họ và Tên')}}<span class="red">*</span></p>
                                </div>
                                <div class="col-lg-9">
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <input type="text" name="name" placeholder="" value="{{old('name') ?? $user->name}}">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <p>{{__('Địa chỉ email của bạn')}}<span class="red">*</span></p>
                                </div>
                                <div class="col-lg-9">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <input type="email" @if(in_array($user->type, ['system', 'google'])) disabled @endif name="email" placeholder="" value="{{old('email') ?? $user->email}}">
                                        </div>
                                    </div>
                                </div>
                                @if($user->type == 'system')
                                <div class="col-lg-3">
                                    <p>{{__('Thay đổi mật khẩu')}}</p>
                                </div>
                                <div class="col-lg-9">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <input type="password" name="password" placeholder="{{__('Vui lòng nhập 8-20 ký tự gồm chữ cái/chữ số')}}">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-3">
                                    <p>{{__('Xác nhận mật khẩu')}}</p>
                                </div>
                                <div class="col-lg-9">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <input type="password" name="password_confirmation" placeholder="{{__('Vui lòng nhập lại mật khẩu')}}">
                                        </div>
                                    </div>
                                </div>
                                @endif

                                <div class="col-lg-3">
                                    <p>{{__('Ngày sinh')}}</p>
                                </div>
                                <div class="col-lg-9">
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <input type="text" name="date_of_birth" class="datepicker" value="{{old('date_of_birth') ?? date('Y-m-d', strtotime($user->date_of_birth))}}">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-3">
                                    <p>{{__('Số điện thoại')}}</p>
                                </div>
                                <div class="col-lg-9">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <input type="number" name="phone" value="{{old('phone') ?? $user->phone}}">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-3">
                                    <p>{{__('Giới tính')}}</p>
                                </div>
                                <div class="col-lg-9">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="radio">
                                                <label><input type="radio" name="sex" @if($user->sex == 'female' || old('sex') == 'female') checked="checked" @endif value="female"> {{__('Nữ')}}</label>
                                                <label><input type="radio" name="sex" @if($user->sex == 'male' || old('sex') == 'male') checked="checked" @endif value="male"> {{__('Nam')}}</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-3">
                                    <p>{{__('Địa chỉ')}}</p>
                                </div>
                                <div class="col-lg-9">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <select class="form-select" name="province" id="provinceId" style="color: #757575;">
                                                <option value="0">{{__('Tỉnh thành')}}</option>
                                                @if($provinces->count())
                                                    @foreach($provinces as $item)
                                                        @if($item->id == $user->province_id)
                                                            <option value="{{$item->id}}" selected>{{$item->name}}</option>
                                                        @else
                                                            <option value="{{$item->id}}">{{$item->name}}</option>
                                                        @endif
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                        <div class="col-lg-12">
                                            <select class="form-select" name="district" id="districtId" style="color: #757575;">
                                                <option>{{__('Quận/Huyện')}}</option>
                                                @foreach($districts as $item )
                                                    @if($item->id == $user->district_id)
                                                        <option value="{{$item->id}}" selected>{{$item->name}}</option>
                                                    @else
                                                        <option value="{{$item->id}}">{{$item->name}}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-lg-12">
                                            <select class="form-select" name="ward" id="wardId" style="color: #757575;">
                                                <option>{{__('Phường/Xã')}}</option>
                                                @foreach($wards as $item )
                                                    @if($item->id == $user->ward_id)
                                                        <option value="{{$item->id}}" selected>{{$item->name}}</option>
                                                    @else
                                                        <option value="{{$item->id}}">{{$item->name}}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-lg-12">
                                            <input type="text" value="{{old('address') ?? $user->address}}" name="address" placeholder="{{__('Tên đường/ Số nhà')}}">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-3">
                                    <p>{{__('Loại da')}}</p>
                                </div>
                                <div class="col-lg-9">
                                    <div class="row">
                                        @if($productSkins->count())
                                            @foreach($productSkins as $item)
                                                @php
                                                    $checked = in_array($item->pskin_id, $skins) ? 'checked' : '';
                                                @endphp
                                                <div class="col-lg-3">
                                                    <div class="radio">
                                                        <label><input {{$checked}} type="checkbox" name="skin[]" value="{{$item->pskin_id}}"> {{$item->getDataByLanguage('pskin_name')}}</label>
                                                    </div>
                                                </div>
                                            @endforeach
                                        @endif

                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-3">
                                    <p>{{__('Nhu cầu chăm sóc da')}}</p>
                                </div>
                                <div class="col-lg-9">
                                    <div class="row">
                                        @if($productCares->count())
                                            @foreach($productCares as $item)
                                                @php
                                                    $checked = in_array($item->pcare_id, $cares) ? 'checked' : '';
                                                @endphp
                                                <div class="col-lg-3">
                                                    <div class="radio">
                                                        <label><input type="checkbox" {{$checked}} name="care[]" value="{{$item->pcare_id}}"> {{$item->getDataByLanguage('pcare_name')}}</label>
                                                    </div>
                                                </div>
                                            @endforeach
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-3">
                                    <p>{{__('Phong cách trang điểm')}}</p>
                                </div>
                                <div class="col-lg-9">
                                    <div class="row">
                                        @if($productMakeups->count())
                                            @foreach($productMakeups as $item)
                                                @php
                                                    $checked = in_array($item->pmakeup_id, $makeups) ? 'checked' : '';
                                                @endphp
                                                <div class="col-lg-3">
                                                    <div class="radio">
                                                        <label><input type="checkbox" {{$checked}} name="make-up[]" value="{{$item->pmakeup_id}}">
                                                            {{$item->getDataByLanguage('pmakeup_name')}}</label>
                                                    </div>
                                                </div>
                                            @endforeach
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <h3 style="text-transform: uppercase;">{{__('Địa chỉ giao hàng')}}</h3>
                            <div class="row">
                                <div class="col-lg-3">
                                    <p>{{__('Họ và Tên')}}</p>
                                </div>
                                <div class="col-lg-9">
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <input type="text" name="delivery_name" placeholder="" value="{{old('delivery_name') ?? $user->delivery_name}}">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <p>{{__('Địa chỉ email của bạn')}}</p>
                                </div>
                                <div class="col-lg-9">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <input type="email" name="delivery_email" placeholder="" value="{{old('delivery_email') ?? $user->delivery_email}}">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <p>{{__('Số điện thoại')}}</p>
                                </div>
                                <div class="col-lg-9">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <input type="number" name="delivery_phone" value="{{old('delivery_phone') ?? $user->delivery_phone}}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-3">
                                    <p>{{__('Địa chỉ')}}</p>
                                </div>
                                <div class="col-lg-9">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <select class="form-select" name="delivery_province" id="provinceIdTwo" style="color: #757575;">
                                                <option value="0">{{__('Tỉnh thành')}}</option>
                                                @if($provinces->count())
                                                    @foreach($provinces as $item)
                                                        @if($item->id == $user->delivery_province_id)
                                                            <option value="{{$item->id}}" selected>{{$item->name}}</option>
                                                        @else
                                                            <option value="{{$item->id}}">{{$item->name}}</option>
                                                        @endif
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                        <div class="col-lg-12">
                                            <select class="form-select" name="delivery_district" id="districtIdTwo" style="color: #757575;">
                                                <option>{{__('Quận/Huyện')}}</option>
                                                @foreach($districts2 as $item )
                                                    @if($item->id == $user->delivery_district_id)
                                                        <option value="{{$item->id}}" selected>{{$item->name}}</option>
                                                    @else
                                                        <option value="{{$item->id}}">{{$item->name}}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-lg-12">
                                            <select class="form-select" name="delivery_ward" id="wardIdTwo" style="color: #757575;">
                                                <option>{{__('Phường/Xã')}}</option>
                                                @foreach($wards2 as $item )
                                                    @if($item->id == $user->delivery_ward_id)
                                                        <option value="{{$item->id}}" selected>{{$item->name}}</option>
                                                    @else
                                                        <option value="{{$item->id}}">{{$item->name}}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-lg-12">
                                            <input type="text" value="{{old('delivery_address') ?? $user->delivery_address}}" name="delivery_address" placeholder="{{__('Tên đường/ Số nhà')}}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4" id="button-form-registry">
                                <button type="submit">{{__('Cập nhật')}}</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection
