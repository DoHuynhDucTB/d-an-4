<?php
$provinces      = $data['provinces'];
$productSkins   = $data['productSkins'];
$productCares   = $data['productCares'];
$productMakeups = $data['productMakeups'];
?>

@extends('frontend.main')
@section('frontend_content')
    <div class="form-registry">
        <form action="/register" method="post">
            @csrf
            <div class="container">
                <div class="row">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        {{--<li class="nav-item waves-effect waves-light">
                            <a class="nav-link active hover-default" id="info-tab" data-toggle="tab" href="#info" role="tab" aria-controls="info" aria-selected="true">1</a>
                            <p>{{__('Nhập thông tin')}}</p>
                        </li>--}}
                        {{--<li class="nav-item waves-effect waves-light">
                            <a class="nav-link hover-default" id="registry-tab" data-toggle="tab" href="#registry" role="tab" aria-controls="registry" aria-selected="false">2</a>
                            <p>{{__('Hoàn thành đăng ký')}}</p>
                        </li>--}}
                    </ul>
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade active show" id="info" role="tabpanel" aria-labelledby="info-tab">
                            <h3>{{__('Nhập thông tin thành viên')}}</h3>
                            <div class="row">
                                <div class="col-lg-3">
                                    <p>{{__('Họ và Tên')}}<span class="red">*</span></p>
                                </div>
                                <div class="col-lg-9">
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <input type="text" name="name" placeholder="" value="{{old('name')}}">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <p>{{__('Địa chỉ email của bạn')}}<span class="red">*</span></p>
                                </div>
                                <div class="col-lg-9">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <input type="email" name="email" placeholder="" value="{{old('email')}}">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <p>{{__('Mật khẩu')}}<span class="red">*</span></p>
                                </div>
                                <div class="col-lg-9">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <input type="password" name="password" placeholder="{{__('Vui lòng nhập 8-20 ký tự gồm chữ cái/chữ số')}}">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-3">
                                    <p>{{__('Xác nhận mật khẩu')}} <span class="red">*</span></p>
                                </div>
                                <div class="col-lg-9">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <input type="password" name="password_confirmation" placeholder="{{__('Vui lòng nhập lại mật khẩu')}}">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-3">
                                    <p>{{__('Ngày sinh')}}</p>
                                </div>
                                <div class="col-lg-9">
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <input type="text" name="date_of_birth" class="datepicker" value="{{old('date_of_birth')}}">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-3">
                                    <p>{{__('Số điện thoại')}}</p>
                                </div>
                                <div class="col-lg-9">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <input type="number" name="phone" value="{{old('phone')}}">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-3">
                                    <p>{{__('Giới tính')}}</p>
                                </div>
                                <div class="col-lg-9">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="radio">
                                                <label><input type="radio" name="sex" checked="checked" value="female"> {{__('Nữ')}}</label>
                                                <label><input type="radio" name="sex" value="male"> {{__('Nam')}}</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <h3>{{__('Đăng ký thông tin bổ sung')}}</h3>
                            <div class="row">
                                <div class="col-lg-3">
                                    <p>{{__('Địa chỉ')}}</p>
                                </div>
                                <div class="col-lg-9">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <select class="form-select" name="province" id="provinceId" style="color: #757575;">
                                                <option value="0">{{__('Tỉnh Thành')}}</option>
                                                @if($provinces->count())
                                                    @foreach($provinces as $item)
                                                    <option value="{{$item->id}}">{{$item->name}}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                        <div class="col-lg-12">
                                            <select class="form-select" name="district" id="districtId" style="color: #757575;">
                                                <option>{{__('Quận/Huyện')}}</option>
                                            </select>
                                        </div>
                                        <div class="col-lg-12">
                                            <select class="form-select" name="ward" id="wardId" style="color: #757575;">
                                                <option>{{__('Phường/Xã')}}</option>
                                            </select>
                                        </div>
                                        <div class="col-lg-12">
                                            <input type="text" name="address" placeholder="{{__('Tên đường/ Số nhà')}}">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-3">
                                    <p>{{__('Loại da')}}</p>
                                </div>
                                <div class="col-lg-9">
                                    <div class="row">
                                        @if($productSkins->count())
                                            @foreach($productSkins as $item)
                                                <div class="col-lg-3">
                                                    <div class="radio">
                                                        <label><input type="checkbox" name="skin[]" value="{{$item->pskin_id}}"> {{$item->getDataByLanguage('pskin_name')}}</label>
                                                    </div>
                                                </div>
                                            @endforeach
                                        @endif

                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-3">
                                    <p>{{__('Nhu cầu chăm sóc da')}}</p>
                                </div>
                                <div class="col-lg-9">
                                    <div class="row">
                                        @if($productCares->count())
                                            @foreach($productCares as $item)
                                                <div class="col-lg-3">
                                                    <div class="radio">
                                                        <label><input type="checkbox" name="care[]" value="{{$item->pcare_id}}"> {{$item->getDataByLanguage('pcare_name')}}</label>
                                                    </div>
                                                </div>
                                            @endforeach
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-3">
                                    <p>{{__('Phong cách trang điểm')}}</p>
                                </div>
                                <div class="col-lg-9">
                                    <div class="row">
                                        @if($productMakeups->count())
                                            @foreach($productMakeups as $item)
                                                <div class="col-lg-3">
                                                    <div class="radio">
                                                        <label><input type="checkbox" name="make-up[]" value="{{$item->pmakeup_id}}">
                                                            {{$item->getDataByLanguage('pmakeup_name')}}</label>
                                                    </div>
                                                </div>
                                            @endforeach
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="registry" role="tabpanel" aria-labelledby="registry-tab">
                            <h3>{{__('Hoàn thành đăng ký')}}</h3>
                        </div>
                        <div class="row">
                            <div class="col-lg-4" id="button-form-registry">
                                <button type="submit">{{__('Đăng ký thành viên')}}</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection
