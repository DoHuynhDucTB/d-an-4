<?php
$title = $data['title'];
$provinces = $data['provinces'];
$districts = $data['districts'];
$wards = $data['wards'];
$user = \Illuminate\Support\Facades\Auth::guard('web')->user();
?>

@extends('frontend.main')
@section('frontend_content')
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-lg-12">
                <div class="contact-form-wrap">
                    @include('frontend.elements.notify')
                    <h2 class="contact-title">{{$title}}</h2>
                    <form action="{{route('updateDeliveryUser')}}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="row">
                            <div class="col-md-6">
                                <div class="contact-form-style mb-20">
                                    <span><strong>Họ và Tên (<span style="color: #b11d22">*</span>)</strong></span>
                                    <input name="delivery_name" placeholder="Họ và Tên*" type="text" value="{{old('delivery_name') ?? $user->delivery_name }}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="contact-form-style mb-20">
                                    <span><strong>Số điện thoại (<span style="color: #b11d22">*</span>)</strong></span>
                                    <input name="delivery_phone" placeholder="Số điện thoại*" type="text" value="{{old('delivery_phone') ?? $user->delivery_phone}}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="contact-form-style mb-20">
                                    <span><strong>Email (<span style="color: #b11d22">*</span>)</strong></span>
                                    <input name="delivery_email" placeholder="Email*" type="email" value="{{ old('delivery_email') ?? $user->delivery_email}}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="contact-form-style mb-20">
                                    <span><strong>Tỉnh/Thành phố</strong></span>
                                    <select name="delivery_province" id="provinceId">
                                        <option value="0">Chọn</option>
                                        @foreach($provinces as $province )
                                            @if($province->id == $user->delivery_province_id)
                                                <option value="{{$province->id}}" selected>{{$province->name}}</option>
                                            @else
                                                <option value="{{$province->id}}">{{$province->name}}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="contact-form-style mb-20">
                                    <span><strong>Quận/Huyện</strong></span>
                                    <select name="delivery_district" id="districtId">
                                        <option value="0">Chọn</option>
                                        @foreach($districts as $district )
                                            @if($district->id == $user->delivery_district_id)
                                                <option value="{{$district->id}}" selected>{{$district->name}}</option>
                                            @else
                                                <option value="{{$district->id}}">{{$district->name}}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="contact-form-style mb-20">
                                    <span><strong>Phường/Xã</strong></span>
                                    <select name="delivery_ward" id="wardId">
                                        <option value="0">Chọn</option>
                                        @foreach($wards as $ward )
                                            @if($ward->id == $user->delivery_ward_id)
                                                <option value="{{$ward->id}}" selected>{{$ward->name}}</option>
                                            @else
                                                <option value="{{$ward->id}}">{{$ward->name}}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="contact-form-style mb-20">
                                    <span><strong>Địa chỉ</strong></span>
                                    <textarea name="delivery_address" cols="30" rows="10" placeholder="Số nhà, tên đường">{{old('delivery_address') ?? $user->delivery_address}}</textarea>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="contact-form-style mb-20 text-center">
                                    <button class="form-button" type="submit">Cập nhật thông tin</button>
                                </div>
                            </div>
                        </div>
                    </form>
                    <p class="form-messege"></p>
                </div>
            </div>
        </div>
    </div>
@endsection
