<?php
$bannerMain = $data['bannerMain'];
$bannerDetails = $data['bannerDetails'];
?>
@extends('frontend.main')
@section('frontend_content')
    @if($bannerMain)
        @php
            $avatarUrl = $bannerMain->avatar ? config('my.path.image_banner_of_module') . $bannerMain->avatar->image_name : '';
        @endphp
        <div class="container-fluid">
            <div class="philosophy">
                <div class="row">
                    <div class="col-lg-6 philosophy-left">
                        <h3 class="title-philosophy">{!! $bannerMain->getDataByLanguage('banner_name') !!}</h3>
                        <div class="content-philosophy">
                            {!! $bannerMain->getDataByLanguage('banner_description') !!}
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <img src="{{asset($avatarUrl)}}">
                    </div>
                </div>
            </div>
        </div>
    @endif
    <div class="philosophy-products container">
        <div class="row">
            @foreach($bannerDetails as $item)
                @php
                    $avatarUrl = $item->avatar ? config('my.path.image_banner_of_module') . $item->avatar->image_name : '';
                @endphp
                <div class="col-lg-4 item-product">
                    <div class="wrap" style="background-color: #8dc63f;">
                        <i style="color: #fff;" class="bi bi-chat-square-quote"></i>
                        <h6 style="color: #fff">{!! $item->getDataByLanguage('banner_name') !!}</h6>
                        <img src="{{asset($avatarUrl)}}" alt="">
                        <div class="perceived-value">
                            {!! $item->getDataByLanguage('banner_description') !!}
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection

