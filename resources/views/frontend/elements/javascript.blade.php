@php
    $javascriptArr = [
            'js/jquery-3.3.1.slim.min.js',
            'js/popper.min.js',
            'js/bootstrap.min.js',
            'owlcarousel/owl.carousel.min.js',
            'js/rangeSlider.js',
            'js/topnavar.js',
          ];
@endphp
@section('javascript_tag')
    @foreach($javascriptArr as $javascript)
        <script src="{{@asset(FRONTEND_CSS_AND_JAVASCRIPT_PATH . $javascript/*. '?v=' . FRONTEND_CSS_AND_JAVASCRIPT_VERSION*/)}}"></script>
    @endforeach
    <script>
         $('img.js-lazy-loading').each(function( index ) {
             var dataSrc = $(this).data('src');
             if (dataSrc) {
                 $(this).attr('src', dataSrc);
             }
        });

        $( document ).ready(function() {
            $('.js-language').on('change', function() {
                var languageName = this.value;
                document.location.href = '/set-language-' + languageName;
            });
        });
    </script>
@show
