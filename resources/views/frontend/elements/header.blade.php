<?php
/**
 * @var boolean $isHome
 */
$isHome = (@$isHome === true) ? true : false;
/**
 * @var \App\Models\Menu $menuModel
 */
$menuModel   = app('Menu');
$menus       = $menuModel::parentQuery()->where(['menu_status' => 'activated', 'group_id' => 6])->orderBy('menu_number', 'asc')->get();
$logo = app('Banner')::query()->where('bangroup_id', 6)->orderBy('banner_id', 'desc')->first();
$logoUrl = $logo->avatar->image_name ? config('my.path.image_banner_of_module') . $logo->avatar->image_name : '';
?>

<div class="top_nav d-flex justify-content-between px-5 py-4">
    <div class="menu">
        <nav class="nav-content">
            <div class="nav-btn" id="" onclick="ToggleNav()">
                <img src="@if($isHome === true) {{asset('/public/release2/icons/menu.svg')}} @else {{asset('/public/release2/icons/menu-black.png')}} @endif" alt="" class="icon" />
            </div>

            <div id="overlay-element" class="nav-overlay"></div>

            <div class="nav-slider" id="nav-slider-element">
                <div class="nav-btn-close" onclick="CloseNav()">
                    <img src="/public/release2/icons/sidebarclose.svg" alt="" />
                </div>
                <div class="slidebar-content d-flex flex-column">

                    <ul class="nav-list">
                        @foreach($menus as $menu)
                        <li class="nav-list-item">
                            <h2>
                                <a href="{{$menu->getDataByLanguage('menu_url_en')}}" class="nav-link">{{$menu->getDataByLanguage('menu_name')}}</a>
                            </h2>
                        </li>
                        @endforeach
                    </ul>

                    <div class="sidebar-bottom ml-3">
                        <div class="sidebar-bottom-ship d-flex justify-content-bettwen">

                            <div class="sidebar-bottom-region">
                                <label for="region">Ship to:</label>
                                <select name="region" id="region">
                                    <option selected value="0">VietNam</option>
                                    <option value="1">Other</option>
                                </select>
                            </div>
                            <div class="slidebar-bottom-separate"></div>
                            <div class="sidebar-bottom-language">
                                <label for="language"></label>
                                <select name="language" id="language" class="js-language">
                                    @if(app()->getLocale() == 'vi' || app()->getLocale() == '')
                                    <option selected value="vi">Tiếng Việt</option>
                                    <option value="en">English</option>
                                    @else
                                    <option  value="vi">Tiếng Việt</option>
                                    <option selected value="en">English</option>
                                    @endif
                                </select>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </nav>
    </div>
    <div class="logo">
        <a href="" class="navtop-icon">
            <img src="@if($isHome === true) {{asset('/public/release2/icons/Logo.png')}} @else {{asset($logoUrl)}} @endif" alt="" class="icon" />
        </a>
    </div>
    <div class="user">
        <a href="" class="navtop-icon">
            <img src="@if($isHome === true) {{asset('/public/release2/icons/user.svg')}} @else {{asset('/public/release2/icons/Smiley.svg')}} @endif" alt="" class="icon" />
        </a>
        <a href="" class="navtop-icon">
            <img src="@if($isHome === true) {{asset('/public/release2/icons/sneaker.svg')}} @else {{asset('/public/release2/icons/sneaker-black.png')}} @endif" alt="" class="icon" />
        </a>
        <span class="number-quantity" style="font-weight: bold; @if($isHome === true) color: white  @else color:black @endif">0</span>
    </div>
</div>
