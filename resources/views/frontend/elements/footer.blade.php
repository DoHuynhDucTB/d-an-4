<?php
/**
 * @var \App\Models\Menu $menuModel
 */
$menuModel = app('Menu');
$menus = $menuModel::parentQuery()
                ->where('group_id', 9)
                ->join(MENU_GROUP_TBL, MENU_GROUP_TBL . '.menugroup_id', MENU_TBL . '.group_id')
                ->where('menu_status', 'activated')
                ->orderBy('menu_number', 'asc')
                ->get();

$menus2 = $menuModel::parentQuery()
    ->where('group_id', 3)
    ->join(MENU_GROUP_TBL, MENU_GROUP_TBL . '.menugroup_id', MENU_TBL . '.group_id')
    ->where('menu_status', 'activated')
    ->orderBy('menu_number', 'asc')
    ->get();


/**
 * @var \App\Models\Banner $bannerModel
 */
$bannerModel = app('Banner');
/**
 * @var object $agent
 */
$bannerGroupId = ($agent->isDesktop() == true) ? 10 : 11;
$banners = $bannerModel::query()
    ->join(BANNER_GROUP_TBL, BANNER_GROUP_TBL . '.bangroup_id', BANNER_TBL . '.bangroup_id')
    ->leftJoin(IMAGE_TBL, '3rd_key', 'banner_id')
    ->where(['3rd_type' => 'banner', 'image_value' => config('my.image.value.banner.avatar'), 'image_status' => 'activated'])
    ->where([BANNER_GROUP_TBL .'.bangroup_id' => $bannerGroupId])
    ->isActivated()
    ->limit(10)
    ->orderBy('banner_sorted', 'asc')
    ->get();


?>
<style>
    a.item-menu-footer:hover {
        text-decoration: none;
    }
</style>
<footer class="my-footer">
    <div class="hero-footer d-flex justify-content-between">
        <div class="col-one">
            <h4 class="bold mb-4">Thiết kế tại Việt Nam</h4>
            <div class="menu-footer">
                @if($menus->count())
                    @foreach($menus->chunk(100) as $key => $menuChunk)
                        <ul class=" menu-one">
                        @foreach($menuChunk as $menu)
                                <a class="item-menu-footer" href="{{asset($menu->getDataByLanguage('menu_url'))}}"><h4>{{$menu->getDataByLanguage('menu_name')}}</h4></a>
                        @endforeach
                        </ul>
                    @endforeach
                @endif
                @if($menus2->count())
                    @foreach($menus2->chunk(100) as $key => $menuChunk)
                        <ul class="menu-two">
                            @foreach($menuChunk as $menu)
                                <a class="item-menu-footer" href="{{asset($menu->getDataByLanguage('menu_url'))}}"><h4>{{$menu->getDataByLanguage('menu_name')}}</h4></a>
                            @endforeach
                        </ul>
                    @endforeach
                @endif
            </div>
        </div>
        <div class="col-two">
            <div>
                <h4>giữ liên lạc nhé?</h4>
                <h5>điền địa chỉ e-mail để nhận mọi tin tức và quà tặng từ Một</h5>
                <div style="position: relative">
                    <input
                        type="email"
                        id="email"
                        name="email"
                        placeholder="email của bạn"
                    />
                    <hr class="email-hr">
                    <span class="enter-input">
                <img src="/public/release2/icons/right-white.png" alt="" />
              </span>
                </div>
            </div>
            <div class="logo-social">
                <div>
                    @foreach($banners as $banner)
                        @php
                            $avatar  = $banner->avatar ? config('my.path.image_banner_of_module') . $banner->avatar->image_name : '';
                        @endphp
                        <img src="{{asset($avatar)}}" alt="" />
                    @endforeach
                </div>
                <div>
                    <h5> &#169 2021 Mot</h5>
                </div>
            </div>
        </div>
    </div>
</footer>
