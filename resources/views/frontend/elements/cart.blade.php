<?php
$cart = Session()->get('cart') ?? [];
$total = Session()->get('total') ?? 0;

$htmlCartHearder = \App\Helpers\HtmlHelper::htmlListHeader($cart);
?>
<div id="cart">
    <div class="header__cart">
        <ul>
            <li class="close-cart"><a href="#"><i class="bi bi-x"></i></a></li>
            <li><a href="#"><i class="bi bi-cart3"></i><span class="count">{{ count($cart) }}</span></a></li>
        </ul>
    </div>
    <div class="box-table">
        {!! $htmlCartHearder !!}
    </div>
    {{--<p><a href=""><i class="bi bi-trash"></i> Xóa giỏ hàng </a></p>--}}
{{--    <h5>MÃ GIẢM GIÁ</h5>
    <div class="row">
        <div class="col-lg-8">
            <input type="text" name="code" placeholder="Vui lòng nhập mã giảm giá">
        </div>
        <div class="col-lg-4">
            <button>Áp dụng</button>
        </div>
    </div>--}}
    <div class="total clearfix" >
        <h5 class="title" style="font-size: 16px;">{{__('Tổng tiền sản phẩm')}}</h5>
        <h5 class="value" style="font-size: 16px;"> {{ \App\Helpers\ProductHelper::formatMoney($total)}} </h5>
    </div>
    <br>
    <button style="font-size: 16px"><a class="hover-default" style="color: #fff" href="{{asset('/checkout')}}">{{__('Mua tất cả sản phẩm')}}</a></button>
    {{--<button>Tiếp tục mua hàng</button>--}}

</div>

