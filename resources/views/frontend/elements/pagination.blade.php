<?php
/**
 * @var \Illuminate\Pagination\LengthAwarePaginator $paginator
 */
if ($paginator->count() < 1) {
    return false;
}
?>
@if ($paginator->hasPages())
    <nav aria-label="..." style="display: inline-block" class="js-pagination">
        <ul class="pagination">
            <li class="page-item" data-page="1"><a class="page-link hover-default color-default" href="#"><<</a></li>
            {{--@if ($paginator->onFirstPage())
                <li class="page-item" data-page="1"><a class="page-link hover-default color-default" href="#"><</a></li>
            @else
                <li class="page-item" data-page="1"><a class="page-link hover-default color-default" href="{{ $paginator->previousPageUrl() }}"><</a></li>
            @endif--}}
                {{-- Pagination Elements --}}
                @foreach ($elements as $element)
                    {{-- Array Of Links --}}
                    @if (is_array($element))
                        @foreach ($element as $page => $url)
                            @php
                                $active = request()->get('page') == $page ? 'active' : '';
                            @endphp

                            @if ($page == $paginator->currentPage())
                                <li class="page-item {{$active}}" data-page="{{$page}}"><a class="page-link hover-default color-default" href="#">{{$page}}</a></li>
                            @elseif (($page == $paginator->currentPage() + 1 || $page == $paginator->currentPage() + 2) || $page == $paginator->lastPage())
                                <li class="page-item {{$active}}" data-page="{{$page}}"><a class="page-link hover-default color-default" href="{{$url}}">{{$page}}</a></li>
                            @elseif ($page == $paginator->lastPage() - 1)
                            <li class="page-item disabled" data-page="{{$paginator->currentPage()}}"><a class="page-link" href="#"><i class="bi bi-three-dots"></i></a></li>
                            @endif
                        @endforeach
                    @endif
                @endforeach
            {{--@if ($paginator->hasMorePages())
                <li class="page-item" data-page="{{$paginator->lastPage()}}"><a class="page-link hover-default color-default" href="{{ $paginator->nextPageUrl() }}">></a></li>
            @else
                <li class="page-item" data-page="{{$paginator->lastPage()}}"><a class="page-link hover-default color-default" href="#">></a></li>
            @endif--}}
            <li class="page-item" data-page="{{$paginator->total()}}"><a class="page-link hover-default color-default" href="#">>></a></li>
        </ul>
    </nav>
@endif
