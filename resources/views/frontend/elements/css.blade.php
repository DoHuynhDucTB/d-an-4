@php
  $cssArr = [
            'fonts/LabGrotesque VN/stylesheet.css',
            'owlcarousel/assets/owl.carousel.min.css',
            'owlcarousel/assets/owl.theme.default.min.css',
            'owlcarousel/assets/owl.theme.default.min.css',
            'css/topmenu.css',
            'css/footer.css',
        ];
@endphp
@section('css_top_tag')
@show
@section('css_tag')
    <link
        rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/normalize/8.0.1/normalize.min.css"
        integrity="sha512-NhSC1YmyruXifcj/KFRWoC561YpHpc5Jtzgvbuzx5VozKpWvQ+4nXhPdFgmx8xqexRcpAglTj9sIBWINXa8x5w=="
        crossorigin="anonymous"
    />
    <link
        rel="stylesheet"
        href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
        crossorigin="anonymous"
    />
    @foreach($cssArr as $css)
        <link href="{{@asset(FRONTEND_CSS_AND_JAVASCRIPT_PATH . $css. '?v=' . FRONTEND_CSS_AND_JAVASCRIPT_VERSION)}}" rel="stylesheet" type="text/css">
    @endforeach
    <link
        rel="stylesheet"
        href="{{asset('/public/release2/owlcarousel/assets/owl.carousel.min.css')}}"
    />
@show



